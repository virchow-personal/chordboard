

#include <Arduino.h>

#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#include <HardwareSerial.h>

//HardwareSerial Serial2(2);

BluetoothSerial SerialBT;

void setup() {
  Serial.begin(115200);
  Serial2.begin(115200, SERIAL_8N1, 12, 13);
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
  Serial2.println("The device started, now you can pair it with bluetooth!");
}

void loop() {
  while (Serial.available()) {
    //SerialBT.write(Serial.read());
    Serial2.write(Serial.read());
  }
  if (SerialBT.available()) {
    Serial.write(SerialBT.read());
  }
  delay(20);

  while(Serial1.available()) {
    Serial.write(Serial1.read());
  }
}


