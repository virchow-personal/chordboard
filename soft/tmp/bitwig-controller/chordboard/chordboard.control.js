loadAPI(4);

// Remove this if you want to be able to use deprecated methods without causing script to stop.
// This is useful during development.
host.setShouldFailOnDeprecatedUse(true);

host.defineController("afshar", "chordboard", "0.1", "3f70cb19-1d52-41e0-b26f-8bb44333059a", "afshar");
host.addDeviceNameBasedDiscoveryPair(["Chordboard MIDI 1"], ["Chordboard MIDI 1"]);


host.defineMidiPorts(1, 1);

if (host.platformIsWindows())
{
   // TODO: Set the correct names of the ports for auto detection on Windows platform here
   // and uncomment this when port names are correct.
   // host.addDeviceNameBasedDiscoveryPair(["Input Port 0"], ["Output Port 0", "Output Port 1"]);
}
else if (host.platformIsMac())
{
   // TODO: Set the correct names of the ports for auto detection on Mac OSX platform here
   // and uncomment this when port names are correct.
   // host.addDeviceNameBasedDiscoveryPair(["Input Port 0"], ["Output Port 0", "Output Port 1"]);
}
else if (host.platformIsLinux())
{
   // TODO: Set the correct names of the ports for auto detection on Linux platform here
   // and uncomment this wheusbMIDI.sendSysEx(6, createSysexArray(0x02));
   // usbMIDI.sendSysEx(6, createSysexArray(0x02));
   // n port names are correct.
   // host.addDeviceNameBasedDiscoveryPair(["Input Port 0"], ["Output Port 0", "Output Port 1"]);
}

function init() {
   transport = host.createTransport();
   host.getMidiInPort(0).setMidiCallback(onMidi0);
   host.getMidiInPort(0).setSysexCallback(onSysex0);
	 board = host.getMidiInPort(0).createNoteInput("Chordboard (L+R)",  "??????");
   board.setShouldConsumeEvents(false);

	 left = host.getMidiInPort(0).createNoteInput("Chordboard (L)", "?0????");
   left.setShouldConsumeEvents(false);

	 right = host.getMidiInPort(0).createNoteInput("Chordboard (R)", "?1????");
   right.setShouldConsumeEvents(false);

  //transport.addIsPlayingObserver(playing_observer);
   // TODO: Perform further initialization here.
	//
	 bank = host.createCursorTrack(2, 1);
	 bank.hasNext().markInterested();
	 bank.trackType().markInterested();
	 transport.tempo().value().addValueObserver(onTempo);
	 transport.tempo().displayedValue().markInterested();
	 transport.isArrangerRecordEnabled().addValueObserver(onRecord);
	 transport.isPlaying().addValueObserver(onPlaying);
   println("chordboard initialized!");
}


function onTempo(t) {
	println(t);
	println(transport.tempo().displayedValue().get());
}

function onPlaying(is_on) {
	if (is_on) {
		sendSysex("f002f7");
	} else {
		sendSysex("f001f7");
	}
}

function onRecord(is_on) {
	if (is_on) {
		sendSysex("f004f7");
	} else {
		sendSysex("f003f7");
	}
}

// Called when a short MIDI message is received on MIDI input port 0.
function onMidi0(status, data1, data2) {
   // TODO: Implement your MIDI input handling code here.
  //
}

function nextTrack() {
				 if (bank.hasNext().get()) {
					bank.selectNext();
				 } else {
					bank.selectFirst();
				 }
}


// Called when a MIDI sysex message is received on MIDI input port 0.
function onSysex0(data) {
  println(data + ":sysex");
   // MMC Transport Controls:
   switch (data) {
      case "f07f7f0604f7":
				 transport.isArrangerLoopEnabled().toggle();
         break;
      case "f07f7f0605f7":
				 nextTrack();
         break;
      case "f07f7f0601f7":
         transport.stop();
         break;
      case "f07f7f0602f7":
         transport.play();
         break;
      case "f07f7f0606f7":
         transport.record();
         break;
   }
}

function flush() {
   // TODO: Flush any output to your controller here.
  println("Flush!");
  sendSysex("f07f7f0606f7");

}

function exit() {

}
