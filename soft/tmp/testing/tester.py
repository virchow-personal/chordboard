
import mido


def get_cb():
    for n in mido.get_input_names():
        if n.startswith('Chordboard'):
            return n

def open_cb():
    n = get_cb()
    if not n:
        raise RuntimeError('No chordboard found!')
    p = mido.open_input(n)
    return p


cb = open_cb()
print cb

for m in cb:
    print m
