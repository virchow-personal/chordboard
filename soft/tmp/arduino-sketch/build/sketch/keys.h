
#include <stdint.h>

// IO Expander Library.
#include <Wire.h>
#include "Adafruit_MCP23017.h"



class Keys {

  public:
    void setup();
    void loop();

    void new_down(uint8_t buf[48]);
    void new_up(uint8_t buf[48]);
    void current_down(uint8_t buf[48]);
    

  private:
    Adafruit_MCP23017 banks[3];
    uint8_t values[48];
    uint8_t to_id(uint8_t bank, uint8_t n);
};

