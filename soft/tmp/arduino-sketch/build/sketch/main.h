
#include <stdint.h>

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

#include "buttons.h"


#define INTERFACE_BUTTON_COUNT 39
#define INTERFACE_ENCODER_COUNT 3

#define NEOPIXELS_1_COUNT 12
#define NEOPIXELS_1_PIN 32

#define MAX7219_1_CLK_PIN 3
#define MAX7219_1_DAT_PIN 2
#define MAX7219_1_CS_PIN 4
#define MAX7219_2_CLK_PIN 24
#define MAX7219_2_DAT_PIN 5
#define MAX7219_2_CS_PIN 25



enum TrackType {
    TRACK_DRUM,
    TRACK_SYNTH,
    TRACK_BASS,
    TRACK_STRING,
    TRACK_FONT,
};

enum PlayStatus {
    PLAY_PAUSE,
    PLAY_ON,
};

enum RecordStatus {
    RECORD_PAUSE,
    RECORD_ON,
};


struct Bar {
    uint8_t beats[64];
};

struct Track {
   Bar bars[];
   TrackType type;
   bool mute;
   bool solo;
};

struct Mix {
    Track tracks[16];
    uint8_t tempo;
};

struct Button {
    bool pressed = false;
};

struct Interface {
    Button buttons[INTERFACE_BUTTON_COUNT];
    Encoder encoders[INTERFACE_ENCODER_COUNT];
    Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NEOPIXELS_1_COUNT,
                                                 NEOPIXELS_1_PIN,
                                                 NEO_GRB + NEO_KHZ800);
};

struct ChordBoard {
    Interface interface;
};


// BUTTONS

void setup_buttons(ChordBoard cb) {

}

void loop_buttons(ChordBoard cb) {
    for (int i = 0; i < INTERFACE_BUTTON_COUNT; i++) {
        // read the two banks
        // read the shift buttons
    }

    for (int i = 0; i < INTERFACE_BUTTON_COUNT; i++) {
        // read the press queue
    }

    for (int i = 0; i < INTERFACE_BUTTON_COUNT; i++) {
        // read the release queue
    }
}


void setup_rotaries(ChordBoard cb) {

}

void loop_rotaries(ChordBoard cb) {

}

void loop_sequencer(ChordBoard cb) {

}

void setup_all(ChordBoard cb) {
    setup_buttons(cb);
    setup_rotaries(cb);
}

void loop_all(ChordBoard cb) {
    loop_buttons(cb);
    loop_rotaries(cb);
}