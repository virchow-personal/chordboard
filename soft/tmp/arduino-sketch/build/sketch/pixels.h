
#include <stdint.h>
#include <elapsedMillis.h>
#include <Adafruit_NeoPixel.h>

#define PIXELS_COUNT      12
#define PIXELS_PIN 37
#define PIXELS_VALUE_TIME 1000


class Pixels {
  public:
    Pixels();
    void show_config_value(uint8_t v);
    void note_on(uint8_t n);
    void note_off(uint8_t n);
    void clear();
    void setup();
    void loop();
  private:
    Adafruit_NeoPixel pixels;
    uint32_t colors[8];
    uint32_t color_off;
    bool last_change_was_config = false;
    elapsedMillis last_change_timer = 0;
};
