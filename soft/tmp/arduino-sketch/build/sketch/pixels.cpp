
#include "./pixels.h"

Pixels::Pixels() {
  pixels = Adafruit_NeoPixel(PIXELS_COUNT, PIXELS_PIN, NEO_GRB + NEO_KHZ800);
  colors[0] = pixels.Color(50, 0,  0);
  colors[1] = pixels.Color(40, 15, 0);
  colors[2] = pixels.Color(25, 25, 0);
  colors[3] = pixels.Color(0,  50, 0);
  colors[4] = pixels.Color(0,  0,  50);
  colors[5] = pixels.Color(15, 0,  40);
  colors[6] = pixels.Color(25, 0,  25);
  colors[7] = pixels.Color(15, 15, 15);
  color_off = pixels.Color(0,  0,  0);
}

void Pixels::show_config_value(uint8_t v) {
  clear();
  if (v > 11) {
    v = 11;
  } else if (v < 0) {
    v = 0;
  }
  for (int i = 0; i < v + 1; i++) {
    // Config values always display in the first color.
    pixels.setPixelColor(i, colors[0]);
  } 
  last_change_was_config = true;
  last_change_timer = 0;
  pixels.show();
}

void Pixels::note_on(uint8_t n) {
  if (last_change_was_config) {
    clear();
  }
  last_change_was_config = false;
  uint8_t led_pos = n % 12;
  uint8_t led_octave = (n / 12) - 2;
  pixels.setPixelColor(led_pos, colors[led_octave]);
  pixels.show();
}

void Pixels::note_off(uint8_t n) {
  if (last_change_was_config) {
    clear();
  }
  last_change_was_config = false;
  uint8_t led_pos = n % 12;
  pixels.setPixelColor(led_pos, color_off);
  pixels.show();
}

void Pixels::clear() {
  pixels.clear();
  pixels.show();
}

void Pixels::setup() {
  pixels.begin();
  clear();
}

void Pixels::loop() {
  if (last_change_was_config &&
      last_change_timer > PIXELS_VALUE_TIME) {
    clear();
  }
}

