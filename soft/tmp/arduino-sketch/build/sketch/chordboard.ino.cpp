#include <Arduino.h>
#line 1 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
#line 1 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
/**
  Copyright 2018 Ali Afshar (aafshar@gmail.com)

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
  of the Software, and to permit persons to whom the Software is furnished to do
  so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
**/

/**
  TODO

  - config from file
  + decide a file scheme
  + load the file as json
  + send and receive the file over serial

  - save to file
  + decide a file scheme
  + list the files over serial
  + save the files

  - remote ui

  - midi host
*/




// Uses most of the functions in the library
#include <MD_MAX72xx.h>
#include <SPI.h>

// IO Expander Library.
#include <Wire.h>
#include "Adafruit_MCP23017.h"

#include <Audio.h>
#include <Wire.h>
#include <SD.h>
#include <SPI.h>
#include <MIDI.h>

// SD Library.
#include <SD_t3.h>
#include <SD.h>


#include "USBHost_t36.h"

#include <Adafruit_NeoPixel.h>


#include <ArduinoJson.h>

#include <ResponsiveAnalogRead.h>


#define ENCODER_DO_NOT_USE_INTERRUPTS
#include <Encoder.h>

#include "scales.cpp"
#include "app.h"



#define PIXELS_COUNT      13
#define PIXELS_PIN        32
#define PIXELS_STATUS_PIN 38
#define PIXELS_VALUE_TIME 1000

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      12

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(PIXELS_COUNT, PIXELS_PIN, NEO_GRB + NEO_KHZ800);

#define  MAX_DEVICES 1

#define CLK_PIN   3  // or SCK
#define DATA_PIN  2  // or MOSI
#define CS_PIN    4  // or SS

// SPI hardware interface
#define  MAX_DEVICES 1

#define CLK_PIN   3  // or SCK
#define DATA_PIN  2  // or MOSI
#define CS_PIN    4  // or SS

// SPI hardware interface
//MD_MAX72XX mx = MD_MAX72XX(CS_PIN, MAX_DEVICES);
// Arbitrary pins
MD_MAX72XX mx = MD_MAX72XX(DATA_PIN, CLK_PIN, CS_PIN, MAX_DEVICES);
MD_MAX72XX mx2 = MD_MAX72XX(5, 24, 25, MAX_DEVICES);


struct State2 {
  uint8_t root;
  Mode mode;
  int8_t l_octave;
  int8_t r_octave;
  int l_octave_mod;
  uint8_t note_on;
  uint8_t cmd_button;
  uint8_t key;
  uint8_t lchan;
  uint8_t rchan;
  uint8_t lnotes;
  uint8_t rnotes;
};


const float tune_frequencies2_PGM[128] =
{
    8.1758,    8.6620,    9.1770,    9.7227,    10.3009,    10.9134,    11.5623,    12.2499,
    12.9783,   13.7500,   14.5676,   15.4339,   16.3516,    17.3239,    18.3540,    19.4454,
    20.6017,   21.8268,   23.1247,   24.4997,   25.9565,    27.5000,    29.1352,    30.8677,
    32.7032,   34.6478,   36.7081,   38.8909,   41.2034,    43.6535,    46.2493,    48.9994,
    51.9131,   55.0000,   58.2705,   61.7354,   65.4064,    69.2957,    73.4162,    77.7817,
    82.4069,   87.3071,   92.4986,   97.9989,   103.8262,   110.0000,   116.5409,   123.4708,
    130.8128,  138.5913,  146.8324,  155.5635,  164.8138,   174.6141,   184.9972,   195.9977,
    207.6523,  220.0000,  233.0819,  246.9417,  261.6256,   277.1826,   293.6648,   311.1270,
    329.6276,  349.2282,  369.9944,  391.9954,  415.3047,   440.0000,   466.1638,   493.8833,
    523.2511,  554.3653,  587.3295,  622.2540,  659.2551,   698.4565,   739.9888,   783.9909,
    830.6094,  880.0000,  932.3275,  987.7666,  1046.5023,  1108.7305,  1174.6591,  1244.5079,
    1318.5102, 1396.9129, 1479.9777, 1567.9817, 1661.2188,  1760.0000,  1864.6550,  1975.5332,
    2093.0045, 2217.4610, 2349.3181, 2489.0159, 2637.0205,  2793.8259,  2959.9554,  3135.9635,
    3322.4376, 3520.0000, 3729.3101, 3951.0664, 4186.0090,  4434.9221,  4698.6363,  4978.0317,
    5274.0409, 5587.6517, 5919.9108, 6271.9270, 6644.8752,  7040.0000,  7458.6202,  7902.1328,
    8372.0181, 8869.8442, 9397.2726, 9956.0635, 10548.0818, 11175.3034, 11839.8215, 12543.8540
};




//unsigned char *sp = score;

#define AMPLITUDE (0.2)

// Create 16 waveforms, one for each MIDI channel
AudioSynthWaveform sine0, sine1, sine2, sine3;
AudioSynthWaveform sine4, sine5, sine6, sine7;
AudioSynthWaveform sine8, sine9, sine10, sine11;
AudioSynthWaveform sine12, sine13, sine14, sine15;
AudioSynthWaveform *waves[16] = {
  &sine0, &sine1, &sine2, &sine3,
  &sine4, &sine5, &sine6, &sine7,
  &sine8, &sine9, &sine10, &sine11,
  &sine12, &sine13, &sine14, &sine15
};

// allocate a wave type to each channel.
// The types used and their order is purely arbitrary.
short wave_type[16] = {
  WAVEFORM_SINE,
  WAVEFORM_SQUARE,
  WAVEFORM_SAWTOOTH,
  WAVEFORM_TRIANGLE,
  WAVEFORM_SINE,
  WAVEFORM_SQUARE,
  WAVEFORM_SAWTOOTH,
  WAVEFORM_TRIANGLE,
  WAVEFORM_SINE,
  WAVEFORM_SQUARE,
  WAVEFORM_SAWTOOTH,
  WAVEFORM_TRIANGLE,
  WAVEFORM_SINE,
  WAVEFORM_SQUARE,
  WAVEFORM_SAWTOOTH,
  WAVEFORM_TRIANGLE
};

// Each waveform will be shaped by an envelope
AudioEffectEnvelope env0, env1, env2, env3;
AudioEffectEnvelope env4, env5, env6, env7;
AudioEffectEnvelope env8, env9, env10, env11;
AudioEffectEnvelope env12, env13, env14, env15;
AudioEffectEnvelope *envs[16] = {
  &env0, &env1, &env2, &env3,
  &env4, &env5, &env6, &env7,
  &env8, &env9, &env10, &env11,
  &env12, &env13, &env14, &env15
};

// Route each waveform through its own envelope effect
AudioConnection patchCord01(sine0, env0);
AudioConnection patchCord02(sine1, env1);
AudioConnection patchCord03(sine2, env2);
AudioConnection patchCord04(sine3, env3);
AudioConnection patchCord05(sine4, env4);
AudioConnection patchCord06(sine5, env5);
AudioConnection patchCord07(sine6, env6);
AudioConnection patchCord08(sine7, env7);
AudioConnection patchCord09(sine8, env8);
AudioConnection patchCord10(sine9, env9);
AudioConnection patchCord11(sine10, env10);
AudioConnection patchCord12(sine11, env11);
AudioConnection patchCord13(sine12, env12);
AudioConnection patchCord14(sine13, env13);
AudioConnection patchCord15(sine14, env14);
AudioConnection patchCord16(sine15, env15);

// Four mixers are needed to handle 16 channels of music
AudioMixer4     mixer1;
AudioMixer4     mixer2;
AudioMixer4     mixer3;
AudioMixer4     mixer4;

// Mix the 16 channels down to 4 audio streams
AudioConnection patchCord17(env0, 0, mixer1, 0);
AudioConnection patchCord18(env1, 0, mixer1, 1);
AudioConnection patchCord19(env2, 0, mixer1, 2);
AudioConnection patchCord20(env3, 0, mixer1, 3);
AudioConnection patchCord21(env4, 0, mixer2, 0);
AudioConnection patchCord22(env5, 0, mixer2, 1);
AudioConnection patchCord23(env6, 0, mixer2, 2);
AudioConnection patchCord24(env7, 0, mixer2, 3);
AudioConnection patchCord25(env8, 0, mixer3, 0);
AudioConnection patchCord26(env9, 0, mixer3, 1);
AudioConnection patchCord27(env10, 0, mixer3, 2);
AudioConnection patchCord28(env11, 0, mixer3, 3);
AudioConnection patchCord29(env12, 0, mixer4, 0);
AudioConnection patchCord30(env13, 0, mixer4, 1);
AudioConnection patchCord31(env14, 0, mixer4, 2);
AudioConnection patchCord32(env15, 0, mixer4, 3);

// Now create 2 mixers for the main output
AudioMixer4     mixerLeft;
AudioMixer4     mixerRight;
AudioOutputI2S  audioOut;

// Mix all channels to both the outputs
AudioConnection patchCord33(mixer1, 0, mixerLeft, 0);
AudioConnection patchCord34(mixer2, 0, mixerLeft, 1);
AudioConnection patchCord35(mixer3, 0, mixerLeft, 2);
AudioConnection patchCord36(mixer4, 0, mixerLeft, 3);
AudioConnection patchCord37(mixer1, 0, mixerRight, 0);
AudioConnection patchCord38(mixer2, 0, mixerRight, 1);
AudioConnection patchCord39(mixer3, 0, mixerRight, 2);
AudioConnection patchCord40(mixer4, 0, mixerRight, 3);
AudioConnection patchCord41(mixerLeft, 0, audioOut, 0);
AudioConnection patchCord42(mixerRight, 0, audioOut, 1);

AudioControlSGTL5000 codec;

#line 260 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void setup_audio();
#line 606 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void setup();
#line 626 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void loop();
#line 700 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void on_encoder();
#line 721 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void init_app();
#line 726 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void init_ux();
#line 730 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void init_player();
#line 1026 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void setup_midi();
#line 1037 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void poll_pots();
#line 1081 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
int enc_to_v(int e);
#line 1085 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
int v_to_enc(int v);
#line 1092 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void poll_keys();
#line 1172 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void poll_looper();
#line 1178 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void poll_serial();
#line 1194 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void setup_keys();
#line 1216 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void setup_pots();
#line 1220 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void setup_leds();
#line 1227 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void do_play();
#line 1231 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void do_stop();
#line 1235 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void do_rec();
#line 1241 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void do_looper_rec();
#line 1247 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void do_looper_play();
#line 1251 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void on_led(int note);
#line 1257 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void off_led(int note);
#line 1262 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void play_chord(int o, int kid, int n, int chan);
#line 1282 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void stop_chord(int o, int kid, int n, int chan);
#line 1306 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
const uint8_t* createSysexArray(uint8_t value);
#line 1312 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void onRealtime(uint8_t realtimebyte);
#line 1316 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void onSysEx(const uint8_t *data, uint16_t length, bool complete);
#line 260 "/home/afshar/src/chordboard/soft/arduino-sketch/chordboard/chordboard.ino"
void setup_audio()
{
   
  // Proc = 12 (13),  Mem = 2 (8)
  // Audio connections require memory to work.
  // The memory usage code indicates that 10 is the maximum
  // so give it 12 just to be sure.
  AudioMemory(18);
  
  codec.enable();
  codec.volume(0.20);

  // reduce the gain on some channels, so half of the channels
  // are "positioned" to the left, half to the right, but all
  // are heard at least partially on both ears
  mixerLeft.gain(1, 0.36);
  mixerLeft.gain(3, 0.36);
  mixerRight.gain(0, 0.36);
  mixerRight.gain(2, 0.36);

  // set envelope parameters, for pleasing sound :-)
  for (int i=0; i<16; i++) {
    envs[i]->attack(9.2);
    envs[i]->hold(2.1);
    envs[i]->decay(31.4);
    envs[i]->sustain(0.6);
    envs[i]->release(84.5);
    // uncomment these to hear without envelope effects
    //envs[i]->attack(0.0);
    //envs[i]->hold(0.0);
    //envs[i]->decay(0.0);
    //envs[i]->release(0.0);
  }
  
  // Initialize processor and memory measurements
  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();
  
}

State2 state = {60, Ionian, -1, 0, 0, 0, HIGH,
               0, 1, 2, 2, 0,
              };


uint32_t pixel_colors[] = {
  pixels.Color(50, 0, 0),
  pixels.Color(40, 15, 0),
  pixels.Color(25, 25, 0),
  pixels.Color(0, 50, 0),
  pixels.Color(0, 0, 50),
  pixels.Color(15, 0, 40),
  pixels.Color(25, 0, 25),
  pixels.Color(15, 15, 15),
};

uint32_t color_off = pixels.Color(0,  0,  0);

/** Control for the 12 pixels on the front. **/
class Pixels {
  public:
    Pixels() {
    }

    void show_config_value(uint8_t n, uint8_t v) {
      clear();
      if (v > 11) {
        v = 11;
      } else if (v < 0) {
        v = 0;
      }
      for (int i = 0; i < v + 1; i++) {
        // Config values always display in the first color.
        pixels.setPixelColor(i, pixel_colors[0]);
      } 
      last_change_was_config = true;
      last_change_timer = 0;
      pixels.show();
    }

    void note_on(uint8_t n) {
      if (last_change_was_config) {
        clear();
      }
      last_change_was_config = false;
      uint8_t led_pos = n % 12;
      uint8_t led_octave = (n / 12) - 2;
      pixels.setPixelColor(led_pos, pixel_colors[led_octave]);
      pixels.show();
    }

    void note_off(uint8_t n) {
      if (last_change_was_config) {
        clear();
      }
      last_change_was_config = false;
      uint8_t led_pos = n % 12;
      pixels.setPixelColor(led_pos, color_off);
      pixels.show();
    }

    void clear() {
      pixels.clear();
      pixels.show();
    }

    void status_on(uint8_t c) {
      pixels.setPixelColor(13, pixel_colors[c]);
      pixels.show();
    }

    void status_off() {
      pixels.setPixelColor(13, color_off);
      pixels.show();
    }

    void setup() {
      pixels.begin();
      clear();
      pixels.show();
      //status_off();
    }

    void loop() {
      if (last_change_was_config &&
          last_change_timer > PIXELS_VALUE_TIME) {
        clear();
        last_change_was_config = false;
      }
    }

  private:
    bool last_change_was_config = false;
    elapsedMillis last_change_timer = 0;
    
};

Pixels pixel_bank;

enum Color  {
  GREEN, BLUE, RED
};


struct P {
  int r;
  int c;
};

struct L {
  P g;
  P b;
  P r;
};


class Lights {
  public:


  L lights[16];

  Lights() {
    for (int k = 0; k < 16; k++) {
     int rm = (k % 2) * 3;
     lights[k] = L{P{rm, k / 2}, P{rm + 1, k / 2}, P{rm + 2, k / 2}};
    }
  }

  void setup() {
    mx.begin();
     mx.control(0, 1, MD_MAX72XX::SHUTDOWN, MD_MAX72XX::OFF);
  mx.control(0, 1, MD_MAX72XX::DECODE, MD_MAX72XX::OFF);
  mx.control(0, 1, MD_MAX72XX::SCANLIMIT, 5);
  mx.control(MD_MAX72XX::INTENSITY, 7);
  mx.clear(0, 1);
  mx.control(0, 1, MD_MAX72XX::TEST, MD_MAX72XX::ON);
  mx2.begin();
  mx2.control(0, 1, MD_MAX72XX::SHUTDOWN, MD_MAX72XX::OFF);
  mx2.control(0, 1, MD_MAX72XX::DECODE, MD_MAX72XX::OFF);
  mx2.control(0, 1, MD_MAX72XX::SCANLIMIT, 5);
  mx2.control(MD_MAX72XX::INTENSITY, 7);
  mx2.clear(0, 1);
  mx2.control(0, 1, MD_MAX72XX::TEST, MD_MAX72XX::ON);
    Serial.println("lightstest");

  delay(500);
  Serial.println("lightstest");
  mx.control(0, 1, MD_MAX72XX::TEST, MD_MAX72XX::OFF);
  mx2.control(0, 1, MD_MAX72XX::TEST, MD_MAX72XX::OFF);
  
  //test();
  }

  void setRed(int i, bool val) {
    mx.setPoint(lights[i].r.r, lights[i].r.c, val);
    mx2.setPoint(lights[i].r.r, lights[i].r.c, val);
  }

  void setBlue(int i, bool val) {
    mx.setPoint(lights[i].b.r, lights[i].b.c, val);
    mx2.setPoint(lights[i].b.r, lights[i].b.c, val);
  }

  void setGreen(int i, bool val) {
    mx.setPoint(lights[i].g.r, lights[i].g.c, val);
    mx2.setPoint(lights[i].g.r, lights[i].g.c, val);
  }

  void setYellow(int i, bool val) {
    setGreen(i, val);
    setRed(i, val);
  }

  void setPurple(int i, bool val) {
    setRed(i, val);
    setBlue(i, val);
  }


   void note_on(uint8_t n) {
      uint8_t led_pos = n % 12;
      uint8_t led_octave = (n / 12) - 2;
      pixel_bank.note_on(n);
      Serial.println(led_pos + 48);
    }

    void note_off(uint8_t n) {
      uint8_t led_pos = n % 12;
      pixel_bank.note_off(n);
    }
};

/** Rotary encoder handling. **/
class Encoders {
  public:

  Encoders() {
  }

  void setup() {
    encs[0].write(v_to_enc(state.key));
    encs[1].write(v_to_enc(state.mode));
    encs[2].write(v_to_enc(state.l_octave + 3));
    //encs[3].write(v_to_enc(state.r_octave + 3));
    //encs[4].write(v_to_enc(state.lnotes));
    //encs[5].write(v_to_enc(state.rnotes));
  }

  uint32_t read(uint8_t i) {
    return 0;
  }

  void write(uint8_t i, uint32_t v) {
  }

  void loop() {
    for (int i = 0; i < 3; i++) {
      int v = -1 * encs[i].read() / 2;
      if (v < 0) {
        v = 0;
        vals[i] = v;
        encs[i].write(0);
      }
      if (v != vals[i]) {
        vals[i] = v;
        pixel_bank.show_config_value(i, v);
        Serial.print("changed: "); Serial.print(i); Serial.print(":"); Serial.println(v);
        if (i == 0) {
          state.key = v;
        } else if (i == 1) {
          state.mode = static_cast<Mode>(v);
        } else if (i == 2) {
          state.l_octave = v - 3;
        } //else if (i == 3) {
          //state.r_octave = v - 3;
        //} else if (i == 4) {
          //state.lnotes = v;
        //} else if (i == 5) {
         // state.rnotes = v;
        //}

      }
    }
  }



  private:
  
  Encoder encs[3] = {
    Encoder(30, 31),
    Encoder(26, 27),
    Encoder(28, 29),
  };

  int16_t vals[3] = {
    -999, -999, -999,
  };

  int enc_to_v(int e) {
    return -1 * e / 2;
  }

  int v_to_enc(int v) {
    return -2 * v;
  }

};


Encoders encoders;
App app;
Lights lights;

int16_t encs_val[] = {
  -999, -999, -999, -999, -999, -999, -999,
};



// Switch pins that are not on the IO Expander
#define SW_CMD 1
#define SW_LEFT 2
#define SW_RIGHT 3
#define SW_REC 0

// Two major modes of input, either playing or in command mode.
#define RUN_PLAY 0
#define RUN_CMD  1


#define TOTAL_NOTES 127

#define LED_0 35
#define LED_1 36

#define PIN            32


//Adafruit_NeoPixel pixels2 = Adafruit_NeoPixel(2, 38, NEO_GRB + NEO_KHZ800);




// Arduino setup function. Run one time.
void setup() {
  Serial.begin(115200);
  //while (!Serial);
  Serial.println("Chordboard 1.0.");
  setup_midi();
  setup_keys();
  encoders.setup();
  lights.setup();
  pixel_bank.setup();
  setup_leds();


  USBHost::begin();
  setup_audio();
  //setup_sd2();
}



// Arduino loop function. Run continuously.
void loop() {
  // Poll the keys.
  poll_keys();
  //poll_pots();
  // Play the looper.
  //poll_encs();

  pixel_bank.loop();
  encoders.loop();
  //poll_looper();
  // Required to read all available MIDI commands.
  //poll_noteon();
  
  //loop_sd();
  while (usbMIDI.read()) {}
}


int octaves[] = {
  -3, -2, -1, 0, 1, 2, 3,
};

int numnotes[] = {
  1, 2, 3, 4, 5, 6
};




int analogs[] = {
  A1, A2, A3, A6, A7,
};

ResponsiveAnalogRead ar(A17, true);

int anvals[13];


enum Bank {BANK_LEFT, BANK_RIGHT};
enum RunMode {RUNMODE_PLAY, RUNMODE_COMMAND};





// set up variables using the SD utility library functions:
Sd2Card SD_CARD;
SdVolume SD_VOLUME;
SdFile SD_ROOT;

struct SerialMessage {
  String c;
  String v;
};





struct BankConfig {
  int8_t octave;
  uint8_t numnotes;
  uint8_t channel;
};

struct KeySet {
};


struct Ux {
  RunMode runmode;
};


void on_encoder() {
}




struct Player {
  uint8_t root;
  Mode mode;
  BankConfig banks[2];
  int8_t notes[TOTAL_NOTES];
};

struct Appold {
  Ux ux;
  Player pl;
};

Appold self{};


void init_app() {
  init_player();
  init_ux();
}

void init_ux() {
  self.ux = Ux{RUNMODE_PLAY};
}

void init_player() {
  self.pl = Player{};
  for (int i = 0; i < TOTAL_NOTES; i++) {
    self.pl.notes[i] = 0;
  }
  self.pl.mode = Ionian;
  self.pl.banks[BANK_LEFT].octave = -1;
  self.pl.banks[BANK_LEFT].numnotes = 3;
  self.pl.banks[BANK_LEFT].channel = 1;
  self.pl.banks[BANK_RIGHT].octave = 0;
  self.pl.banks[BANK_RIGHT].numnotes = 1;
  self.pl.banks[BANK_RIGHT].channel = 2;
}




int ON_NOTES[127];





// set up variables using the SD utility library functions:
//Sd2Card card;
//SdVolume volume;
//SdFile root;


//void setup_sd() {
//  Serial.print("\nInitializing SD card...");
//
//	// Use the builtin SD card on the Teensy 3.6.
//  if (!card.init(SPI_HALF_SPEED, BUILTIN_SDCARD)) {
//    Serial.println("initialization failed. Things to check:");
//    Serial.println("* is a card inserted?");
//    Serial.println("* is your wiring correct?");
//    Serial.println("* did you change the chipSelect pin to match your shield or module?");
//    return;
//  } else {
//    Serial.println("Wiring is correct and a card is present.");
//  }
//
//  // print the type of card
//  Serial.print("\nCard type: ");
//  switch (card.type()) {
//    case SD_CARD_TYPE_SD1:
//      Serial.println("SD1");
//      break;
//    case SD_CARD_TYPE_SD2:
//      Serial.println("SD2");
//      break;
//    case SD_CARD_TYPE_SDHC:
//      Serial.println("SDHC");
//      break;
//    default:
//      Serial.println("Unknown");
//  }
//
//  // Now we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
//  if (!volume.init(card)) {
//    Serial.println("Could not find FAT16/FAT32 partition.\nMake sure you've formatted the card");
//    return;
//  }
//
//
//  // print the type and size of the first FAT-type volume
//  uint32_t volumesize;
//  Serial.print("\nVolume type is FAT");
//  Serial.println(volume.fatType(), DEC);
//  Serial.println();
//
//  volumesize = volume.blocksPerCluster();    // clusters are collections of blocks
//  volumesize *= volume.clusterCount();       // we'll have a lot of clusters
//  if (volumesize < 8388608ul) {
//    Serial.print("Volume size (bytes): ");
//    Serial.println(volumesize * 512);        // SD card blocks are always 512 bytes
//  }
//  Serial.print("Volume size (Kbytes): ");
//  volumesize /= 2;
//  Serial.println(volumesize);
//  Serial.print("Volume size (Mbytes): ");
//  volumesize /= 1024;
//  Serial.println(volumesize);
//
//
//  Serial.println("\nFiles found on the card (name, date and size in bytes): ");
//  root.openRoot(volume);
//
//  // list all files in the card with date and size
//  root.ls(LS_R | LS_DATE | LS_SIZE);
//}


int val_stop = HIGH;
int val_play = HIGH;
int val_rec = HIGH;


Adafruit_MCP23017 L_IO_BANK;
Adafruit_MCP23017 R_IO_BANK;
Adafruit_MCP23017 S_IO_BANK;




struct Note {
  uint8_t key;
  uint8_t channel;
  Bank bank;
};

class ChordPlayer {
  public:
    void play(uint8_t button, uint8_t channel, uint8_t numnotes, Bank bank) {
      int notes[numnotes];
      Scale::chord_for(state.mode, button, numnotes, notes);
      //_on_chords[button] = {1};
      uint8_t abase = 0;
      for (uint8_t i = 0; i < numnotes; i++) {
        uint8_t note = abase + state.key + notes[i];
        usbMIDI.sendNoteOn(note, 127, channel);
      }
    }

    //int notes[3];
    //int abase = base + (o * 12);
    //Scale::chord_for(state.mode, kid, 3, notes);
    //for (int i = 0; i < n; i++) {
    //  int note = abase + state.key + notes[i];
    //		usbMIDI.sendNoteOn(note, 127, chan);
    //	CURRENT_LOOPER.add(note, true);
    //}
    //state.note_on++;


    void stop(uint8_t key, uint8_t channel, Bank bank) {
    }

  private:
    uint8_t _on_chords[16][16];
};

class LooperNote {
  public:
    LooperNote() : LooperNote(0, 0, 0) {};
    LooperNote(uint64_t time, uint8_t note, bool ison) : _time(time), _note(note), _ison(ison) {};
    uint64_t get_time() {
      return _time;
    }
    bool is_on() {
      return _ison;
    }
    uint8_t get_note() {
      return _note;
    }
  private:
    uint64_t _time;
    uint8_t  _note;
    bool     _ison;
};

class Looper {

  public:
    void toggle_record() {
      if (_is_recording) {
        end_record();
      } else {
        start_record();
      }
    }

    void toggle_play() {
      if (_is_playing) {
        stop_play();
      } else {
        start_play();
      }
    }

    void start_record() {
      _note_count = 0;
      _is_playing = false;
      _is_recording = true;
      _flash_timer = 0;
      _note_timer = 0;
    }

    void end_record() {
      _end_time = _note_timer;
      _is_recording = false;
    }

    void start_play() {
      if (_is_recording) {
        end_record();
      }
      _is_playing = true;
      _play_counter = 0;
      _play_timer = 0;
    }

    void stop_play() {
      _is_playing = false;
    }

    void add(int note, bool ison) {
      if (_is_recording) {
        _notes[_note_count] = LooperNote(_note_timer, note, ison);
        _note_count++;
      }
    }

    void poll() {
      if (_is_playing) {
        _poll_playing();
      } else if (_is_recording) {
        _poll_recording();
      } else {
        _poll_stopped();
      }
    }

  private:
    uint8_t	_note_count = 0;
    uint64_t _end_time = 0;
    uint8_t _play_counter = 0;
    elapsedMillis _note_timer;
    elapsedMillis _play_timer;
    elapsedMillis _flash_timer;
    bool _is_playing = false;
    bool _is_recording = false;
    LooperNote _notes[500];

    void _poll_playing() {
      digitalWrite(LED_1, LOW);
      if (_play_counter >= _note_count) {
        if (_play_timer > _end_time) {
          _play_counter = 0;
          _play_timer = 0;
        }
        return;
      }
      LooperNote current_note = _notes[_play_counter];
      if (_play_timer > current_note.get_time()) {
        if (current_note.is_on()) {
          usbMIDI.sendNoteOn(current_note.get_note(), 127, 1);
          state.note_on++;
        } else {
          usbMIDI.sendNoteOff(current_note.get_note(), 127, 1);
          state.note_on--;
        }
        _play_counter++;
      }
    }

    void _poll_recording() {
      if (_flash_timer < 150) {
        digitalWrite(LED_1, LOW);
      } else if (_flash_timer < 300) {
        digitalWrite(LED_1, HIGH);
      } else if (_flash_timer > 450) {
        _flash_timer = 0;
      }
    }

    void _poll_stopped() {
      digitalWrite(LED_1, HIGH);
    }
};

Looper CURRENT_LOOPER;



//static uint8_t l_keymap[] = {8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7};
//static uint8_t r_keymap[] = {8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7};

static uint8_t l_keymap[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
static uint8_t r_keymap[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

int key_vals[] = {
  HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH,
  HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH,
};

int shift_vals[] = {
  HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH,
};






void setup_midi() {
  //usbMIDI.setHandleRealTimeSystem(onRealtime);
  //usbMIDI.setHandleSysEx(onSysEx);
}



bool lastConfigChange = false;

elapsedMillis pixelTimer;

void poll_pots() {
  analogReadAveraging(1);
  for (int i = 0; i < 5; i++) {
    int val = map(analogRead(analogs[i]), 0, 1023, 0, 11);
    if (anvals[i] != val) {
      Serial.print("changed: "); Serial.print(i); Serial.print(" @"); Serial.println(val);
      anvals[i] = val;
      if (i == 3 || i == 4) {
        break;
      }
      //pixels.clear();
      //for(int j=0;j<(12 - val);j++){
      //  // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
      //  pixels.setPixelColor(j, pot_cols[5]); // Moderately bright green color.
      //}
      //pixelTimer = 0;
      //pixels.show(); // This sends the updated pixel color to the hardware.
      //if (i == 0) {
      //  state.key = (12 - val) - 1;
      //  Serial.print("key: "); Serial.println(state.key);
      //} if (i == 1) {
      //  state.mode = static_cast<Mode>((12 - val) - 1);
      //  Serial.print("mode: "); Serial.println(state.mode);
      //}
      //ar.update();
      //if (ar.hasChanged()) {
      //  int val = ar.getValue();
      //  Serial.print("changed: "); Serial.println(val);
      // }





      //pixels.clear();
      //pixels.setPixelColor(11, pixels.Color(0, 50, 0));
      //pixels.show();
      //Serial.println(analogRead(A12));

    }
  }
}


int enc_to_v(int e) {
  return -1 * e / 2;
}

int v_to_enc(int v) {
  return -2 * v;
}




void poll_keys() {
  for (int i = 0; i < 16; i++) {
    int vl = L_IO_BANK.digitalRead(i);
    int vr = R_IO_BANK.digitalRead(i);
    int vs = S_IO_BANK.digitalRead(i);
    int li = i;
    int ri = i + 16;
    if (vs != shift_vals[i]) {
      shift_vals[i] = vs;
      if (vs == LOW) {
        Serial.print("shift: "); Serial.println(i);
        if (i > 7) {
          //pixel_bank.status_on(i % 8);
        }
      } else {
        Serial.println("shiftoff");
        //pixel_bank.status_off();
      }
    }
    if (vl != key_vals[li]) {
      key_vals[li] = vl;
      int cid = l_keymap[i];
      if (vl == LOW) {
        Serial.print("down "); Serial.print(i); Serial.print(" "); Serial.println(cid);
        play_chord(state.l_octave, cid, numnotes[state.lnotes], state.lchan);
  
      } else {
        Serial.print("down "); Serial.print(i); Serial.print(" "); Serial.println(cid);
        stop_chord(state.l_octave, cid, numnotes[state.lnotes], state.lchan);
      }
    }
    if (vr != key_vals[ri]) {
      key_vals[ri] = vr;
      int cid = r_keymap[i];
      if (vr == LOW) {
        Serial.print("down "); Serial.print(i); Serial.print(" "); Serial.println(cid);

        play_chord(state.r_octave, cid, numnotes[state.rnotes], state.rchan);



      } else {
        Serial.print("up "); Serial.print(i); Serial.print(" "); Serial.println(cid);

        stop_chord(state.r_octave, cid, numnotes[state.rnotes], state.rchan);


      }
    }
  }





  int play_r = digitalRead(SW_LEFT);
  if (play_r != val_play) {
    val_play = play_r;
    if (val_play == LOW) {
      do_looper_rec();
    }
  }

  int stop_r = digitalRead(SW_RIGHT);
  if (stop_r != val_stop) {
    val_stop = stop_r;
    if (val_stop == LOW) {
      do_looper_play();
    }
  }

  int rec_r = digitalRead(SW_REC);
  if (rec_r != val_rec) {
    val_rec = rec_r;
    if (val_rec == LOW) {
      do_rec();
    }
  }
}

void poll_looper() {
  CURRENT_LOOPER.poll();
  poll_serial();
}


void poll_serial() {
  if (Serial.available()) {
    String input = Serial.readStringUntil('\n');
    Serial.print("got: "); Serial.println(input);
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(input);
    String t = root[String("c")];

    if (t == "handshake") {
      Serial.println("pong");
    }
  }
}


// Setup the key handling.
void setup_keys() {
  // Left bank is at default address. LOW/LOW/LOW.
  // Right bank is at address 1. HIGH/LOW/LOW
  L_IO_BANK.begin();
  R_IO_BANK.begin(1);
  S_IO_BANK.begin(2);
  // MCP23017 has internal pullups, so let's use them.
  for (int i = 0; i < 16; i++) {
    L_IO_BANK.pinMode(i, INPUT);
    L_IO_BANK.pullUp(i, HIGH);
    R_IO_BANK.pinMode(i, INPUT);
    R_IO_BANK.pullUp(i, HIGH);
    S_IO_BANK.pinMode(i, INPUT);
    S_IO_BANK.pullUp(i, HIGH);
  }
  // Pull up the remaining keys.
  pinMode(SW_CMD, INPUT_PULLUP);
  pinMode(SW_LEFT, INPUT_PULLUP);
  pinMode(SW_RIGHT, INPUT_PULLUP);
  pinMode(SW_REC, INPUT_PULLUP);
}

void setup_pots() {

}

void setup_leds() {
  pinMode(LED_0, OUTPUT);
  digitalWrite(LED_0, HIGH);
  pinMode(LED_1, OUTPUT);
  digitalWrite(LED_1, HIGH);
}

void do_play() {
  usbMIDI.sendSysEx(6, createSysexArray(0x02));
}

void do_stop() {
  usbMIDI.sendSysEx(6, createSysexArray(0x01));
}

void do_rec() {
  usbMIDI.sendSysEx(6, createSysexArray(0x06));
}


// Toggle recording the looper.
void do_looper_rec() {
  //usbMIDI.sendSysEx(6, createSysexArray(0x05));
  CURRENT_LOOPER.toggle_record();
}

// Toggle playing the looper.
void do_looper_play() {
  CURRENT_LOOPER.toggle_play();
}

void on_led(int note) {
  //pixel_bank.note_on(note);
  Serial.println(note);
  lights.note_on(note);
}

void off_led(int note) {
  //pixel_bank.note_off(note);
  lights.note_off(note);
}

void play_chord(int o, int kid, int n, int chan) {
  int notes[6];
  lights.setYellow(kid, true);
  lights.setBlue((kid + 4) % 14, true);
  lights.setGreen((kid + 6) % 14, true);
  lights.setRed((kid + 3) % 14, true);
 lights.setPurple((kid + 7) % 14, true);
  int abase = state.root + (o * 12);
  Scale::chord_for(state.mode, kid, 6, notes);
  for (int i = 0; i < n; i++) {
    int note = abase + state.key + notes[i];
    ON_NOTES[note]++;
    usbMIDI.sendNoteOn(note, 127, chan);
    CURRENT_LOOPER.add(note, true);
    on_led(note);

  }
  state.note_on++;
}

void stop_chord(int o, int kid, int n, int chan) {
  Serial.println("STOPCHORD");
  lights.setYellow(kid, false);
  lights.setBlue((kid + 4) % 14, false);
  lights.setGreen((kid + 6) % 14, false);
  lights.setRed((kid + 3) % 14, false);
  lights.setPurple((kid + 7) % 14, false);

  int abase = state.root + (o * 12);
  int notes[6];
  Scale::chord_for(state.mode, kid, 6, notes);
  for (int i = 0; i < n; i++) {
    int note = abase + state.key + notes[i];
    ON_NOTES[note]--;
    if (ON_NOTES[note] > 0) {
      continue;
    }
    usbMIDI.sendNoteOff(note, 127, chan);
    CURRENT_LOOPER.add(note, false);
    off_led(note);
  }
  state.note_on--;
}

const uint8_t* createSysexArray(uint8_t value) {
  static uint8_t data[6] = {0xF0, 0x7F, 0x7F, 0x06, 0x00, 0xF7 };
  data[4] = value;
  return data;
}

void onRealtime(uint8_t realtimebyte) {
}


void onSysEx(const uint8_t *data, uint16_t length, bool complete) {
  const uint8_t v = data[length - 2];
  if (v == 1 || v == 2) {
  }

  if (v == 3 || v == 4) {
  }
}




