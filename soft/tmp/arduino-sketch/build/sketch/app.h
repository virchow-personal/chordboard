

#include <WString.h>

enum ActionType{KEY, CONTROL};
enum Modifier{MOD1, MOD2, MOD3};

class Action {
  public:
    ActionType type;
    Modifier mod;
};

class App {
  public:
   void setup();
   void on_action();
  private:

   //Pixels* pixels;
};

