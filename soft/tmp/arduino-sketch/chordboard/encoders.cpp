
#define ENCODER_DO_NOT_USE_INTERRUPTS
#include <Encoder.h>

static Encoder encs[] = {
  Encoder(2, 3),
  Encoder(4, 5),
  Encoder(24, 25),
  Encoder(26, 27),
  Encoder(28, 29),
  Encoder(30, 31),
  Encoder(33, 34),
};
