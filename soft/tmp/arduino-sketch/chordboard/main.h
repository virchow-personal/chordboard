
#include <stdint.h>

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>



#define INTERFACE_BUTTON_COUNT 39
#define INTERFACE_ENCODER_COUNT 3



enum TrackType {
    TRACK_DRUM,
    TRACK_SYNTH,
    TRACK_BASS,
    TRACK_STRING,
    TRACK_FONT,
};

enum PlayStatus {
    PLAY_ON,
    PLAY_OFF,
};

enum RecordStatus {
    RECORD_ON,
    RECORD_OFF,
};

enum RepeatStatus {
    REPEAT_ON,
    REPEAT_OFF,
};

enum InputStatus {
    INPUT_PLAYING,
    INPUT_SEQUENCE,
};

struct AppState {
    InputStatus input_state;
    PlayStatus play_state;
    RecordStatus record_state;
};


struct Bar {
    uint8_t beats[64];
};

struct Part {
   Bar bars[];
};

struct Track {
   Part parts[];
   TrackType type;
   bool mute;
   bool solo;
};

struct Mix {
    Track tracks[16];
    uint8_t tempo;
    uint8_t key;
};

struct Button {
    bool pressed = false;
};

struct Interface {
    Button buttons[INTERFACE_BUTTON_COUNT];
    Encoder encoders[INTERFACE_ENCODER_COUNT];
    InputStatus input_state;
    PlayStatus play_state;
    RecordStatus record_state;
};

struct Chordboard {
    Interface interface;
};


// BUTTONS

/**
 * Setup the buttons.
 */
void setup_buttons(Chordboard cb) {
    // First the expanders.
    //
    //
}


/**
 * Poll the buttons for changes.
 *
 * This depends on the various states of the app, and the various different types of button.
 *
 * The buttons are:
 *
 * 1. The playing keys
 *
 *    These either:
 *
 *    1. Play a note.
 *    2. Select a note/instrument for the sequencer.
 *
 * 2. The shift/control keys
 * 3. The sequencer pad keys
 * 4. The configuration keys
 */
void loop_buttons(Chordboard cb) {
    for (int i = 0; i < INTERFACE_BUTTON_COUNT; i++) {
        // read the two banks
        // read the shift buttons
    }

    for (int i = 0; i < INTERFACE_BUTTON_COUNT; i++) {
        // read the press queue
    }

    for (int i = 0; i < INTERFACE_BUTTON_COUNT; i++) {
        // read the release queue
    }
}


/**
 * Switch the input mode from player to sequencer or back.
 */
void toggle_input_mode(Chordboard cb) {
    if  (cb.state.input_state == INPUT_SEQUENCE) {
        cb.state.input_state = INPUT_PLAYER;
    } else {
        cb.state.input_state = INPUT_SEQUENCE;
    }
}



void setup_rotaries(Chordboard cb) {

}

void loop_rotaries(Chordboard cb) {

}

void loop_sequencer(Chordboard cb) {

}

void setup_all(Chordboard cb) {
    setup_buttons(cb);
    setup_rotaries(cb);
}

void loop_all(Chordboard cb) {
    loop_buttons(cb);
    loop_rotaries(cb);
}
