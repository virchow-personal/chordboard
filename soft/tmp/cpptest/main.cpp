

#include <iostream>
#include <stdint.h>


// ostensibly hard coded
#define SEQ_PART_N  16
#define SEQ_BARS_N  16
#define SEQ_BEAT_N  16
#define SEQ_TRAC_N  8
#define SEQ_VOIC_N  8


class SeqState {
  private:
    uint8_t _current_part;
    uint8_t _current_bars;
    uint8_t _current_beat;
};

class SeqSteps {
  public:
    int8_t ***_get_bar();
    
    uint8_t _beats[SEQ_PART_N]
                  [SEQ_BARS_N]
                  [SEQ_BEAT_N]
                  [SEQ_TRAC_N];
  private:
};



int main() {

 SeqSteps s;

 s._beats[0][0][0][0] = -40;

 std::cout << "banana\n";
 std::cout << (int)s._beats[0][0][0][0] << "\n";
 std::cout << sizeof(s._beats);
 std::cout << "\n";
 return 0;
}
