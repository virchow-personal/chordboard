

import json
import serial


CONFIG_PARAMS = {
    0: "key",
    1: "mode",
}

COMMANDS = {
    0: "handshake",
}


class Message(object):

    def __init__(self, cmd, val):
        self.cmd = cmd
        self.val = val

class SerialInterface(object):

    def __init__(self):
        self.s = serial.Serial('/dev/ttyACM0')

    def handshake(self):
        self.cmd('handshake')


    def cmd(self, c):
        d = {
            't': 'cmd',
            'c': c,
        }
        self.s.write(json.dumps(d))
        self.s.write('\n');


    def run_receiver(self):
        """Run the serial receiver for in a thread."""
        while self._running:
            line = self.s.readline()
            d = json.parse(line)
            cmd = d.get(cmd)
            if not cmd:
                # bad command
                continue
            act = getattr(self, 'oncmd_{}'.format(cmd), None)
            if not act:
                # bad command
                continue



def main():
    s = SerialInterface()
    s.handshake()
    while 1:
        line = s.s.readline()
        print 'got:', line



import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, QHBoxLayout, QTextEdit

import PyQt5.QtWidgets as Q


class UX(object):

    def __init__(self):
        self.init_ui()

    def init_ui(self):
        self.app = QApplication(sys.argv)
        self.w = QWidget()
        self.w.setGeometry(300, 300, 300, 150)
        self.w.setWindowTitle('Chordboard User Interface')    
        vb = QVBoxLayout()
        self.w.setLayout(vb)
        vb.addStretch(1)

        g = Q.QGridLayout()
        vb.addLayout(g)
        g.setSpacing(10)
        g.setColumnStretch(0, 0)
        g.setColumnStretch(1, 1)
        g.setColumnStretch(2, 1)
        g.addWidget(Q.QLabel('Key'), 1, 0)
        g.addWidget(Q.QLabel('Mode'), 2, 0)
        g.addWidget(Q.QLabel('Octave'), 3, 0)
        
        self.label_key = Q.QLabel('k')
        g.addWidget(self.label_key, 1, 1)
        self.dial_key = Q.QDial()
        g.addWidget(self.dial_key, 1, 1)
        self.dial_key.setRange(1, 12)
        self.dial_key.setNotchesVisible(1)

        self.label_mode = Q.QLabel('m')
        g.addWidget(self.label_mode, 2, 1)
        self.label_loctave = Q.QLabel('lo')
        g.addWidget(self.label_loctave, 3, 1)
        self.label_roctave = Q.QLabel('ro')
        g.addWidget(self.label_roctave, 3, 2)


        self.text_log = QTextEdit()
        vb.addWidget(self.text_log)

    def start(self):
        self.w.show()
        sys.exit(self.app.exec_())
        

if __name__ == '__main__':
    u = UX()
    u.start()

    


if __name__ == '__main__':
    pass#main()    
