

#include <Arduino.h>

#define COMMS_BAUD_RATE 115200
#define FLASH_BAUD_RATE 921600

#define ESP_RST_PIN 36
#define ESP_BOOT_PIN 35

void setup() {
  Serial.begin(COMMS_BAUD_RATE);
  //Serial5.begin(COMMS_BAUD_RATE);
  //Serial3.begin(COMMS_BAUD_RATE);
  pinMode(ESP_RST_PIN, OUTPUT);
  pinMode(ESP_BOOT_PIN, OUTPUT);
  digitalWrite(ESP_RST_PIN, HIGH);
  digitalWrite(ESP_BOOT_PIN, HIGH);

  pinMode(39, INPUT_PULLUP);
  Serial4.begin(COMMS_BAUD_RATE);
}

bool flashing = false;

void flash_mode() {
  //if (Serial.rts() && !Serial.dtr() && !flashing) {
    //Serial.begin(FLASH_BAUD_RATE);
    flashing  = true;
    Serial.println("flashing");
    digitalWrite(ESP_RST_PIN, LOW);
    digitalWrite(ESP_BOOT_PIN, LOW);
    delay(400);
    digitalWrite(ESP_RST_PIN, HIGH);
    delay(400);
    digitalWrite(ESP_BOOT_PIN, HIGH);
    
    Serial.println("done:flashing");
  //}
}



void loop() {
  //flash_mode();
  //while (Serial5.available()) {
  //  Serial.write(Serial5.read());
 // }
  while (Serial4.available()) {
    Serial.write(Serial4.read());
  }

  if (digitalRead(39) == LOW) {
    Serial.println("flash_mode");
    flash_mode();
  }

    //if (flashing && (Serial.dtr() || Serial.rts())) {
    //Serial3.print("dtr:");
    //Serial3.println(Serial.dtr());
    //Serial3.print("rts:");
    //Serial3.println(Serial.rts());
    //}


}
