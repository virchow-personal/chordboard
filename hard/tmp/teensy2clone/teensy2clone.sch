EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATMEGA32U4-AU U?
U 1 1 5A9C2E2E
P 4450 4300
F 0 "U?" H 3500 6000 50  0000 C CNN
F 1 "ATMEGA32U4-AU" H 5150 2800 50  0000 C CNN
F 2 "TQFP44" H 4450 4300 50  0001 C CIN
F 3 "" H 5550 5400 50  0001 C CNN
	1    4450 4300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
