EESchema Schematic File Version 2
LIBS:chordboard-components
LIBS:Connector
LIBS:device
LIBS:MCU_Microchip_PIC32
LIBS:power
LIBS:regul
LIBS:Switch
LIBS:MCU_Microchip_PIC16
LIBS:microchip
LIBS:pic32mx-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 3800 2950 2    60   ~ 0
~RST
Text Label 8800 3550 0    60   ~ 0
USBD+
Text Label 8800 3650 0    60   ~ 0
USBD-
$Comp
L +3.3V #PWR07
U 1 1 5AE7C740
P 9300 2050
AR Path="/5AE7C24A/5AE7C740" Ref="#PWR07"  Part="1" 
AR Path="/5AE7DFE0/5AE7C740" Ref="#PWR08"  Part="1" 
AR Path="/5AE857E8/5AE7C740" Ref="#PWR017"  Part="1" 
F 0 "#PWR08" H 9300 1900 50  0001 C CNN
F 1 "+3.3V" H 9300 2190 50  0000 C CNN
F 2 "" H 9300 2050 50  0001 C CNN
F 3 "" H 9300 2050 50  0001 C CNN
	1    9300 2050
	1    0    0    -1  
$EndComp
$Comp
L C_Small C8
U 1 1 5AE7C746
P 9850 3750
AR Path="/5AE7C24A/5AE7C746" Ref="C8"  Part="1" 
AR Path="/5AE7DFE0/5AE7C746" Ref="C14"  Part="1" 
AR Path="/5AE857E8/5AE7C746" Ref="C20"  Part="1" 
F 0 "C14" H 9860 3820 50  0000 L CNN
F 1 "0.1uF" H 9860 3670 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D5.1mm_W3.2mm_P5.00mm" H 9850 3750 50  0001 C CNN
F 3 "" H 9850 3750 50  0001 C CNN
	1    9850 3750
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR08
U 1 1 5AE7C74D
P 9600 2050
AR Path="/5AE7C24A/5AE7C74D" Ref="#PWR08"  Part="1" 
AR Path="/5AE7DFE0/5AE7C74D" Ref="#PWR09"  Part="1" 
AR Path="/5AE857E8/5AE7C74D" Ref="#PWR018"  Part="1" 
F 0 "#PWR09" H 9600 1900 50  0001 C CNN
F 1 "+5V" H 9600 2190 50  0000 C CNN
F 2 "" H 9600 2050 50  0001 C CNN
F 3 "" H 9600 2050 50  0001 C CNN
	1    9600 2050
	-1   0    0    -1  
$EndComp
$Comp
L C_Small C5
U 1 1 5AE7C753
P 9850 2950
AR Path="/5AE7C24A/5AE7C753" Ref="C5"  Part="1" 
AR Path="/5AE7DFE0/5AE7C753" Ref="C11"  Part="1" 
AR Path="/5AE857E8/5AE7C753" Ref="C17"  Part="1" 
F 0 "C11" H 9860 3020 50  0000 L CNN
F 1 "10uF" H 9860 2870 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.00mm" H 9850 2950 50  0001 C CNN
F 3 "" H 9850 2950 50  0001 C CNN
	1    9850 2950
	0    -1   1    0   
$EndComp
$Comp
L C_Small C6
U 1 1 5AE7C75A
P 9850 3450
AR Path="/5AE7C24A/5AE7C75A" Ref="C6"  Part="1" 
AR Path="/5AE7DFE0/5AE7C75A" Ref="C12"  Part="1" 
AR Path="/5AE857E8/5AE7C75A" Ref="C18"  Part="1" 
F 0 "C12" H 9860 3520 50  0000 L CNN
F 1 "10uF" H 9860 3370 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.00mm" H 9850 3450 50  0001 C CNN
F 3 "" H 9850 3450 50  0001 C CNN
	1    9850 3450
	0    1    1    0   
$EndComp
$Comp
L R_Small R2
U 1 1 5AE7C761
P 3300 2950
AR Path="/5AE7C24A/5AE7C761" Ref="R2"  Part="1" 
AR Path="/5AE7DFE0/5AE7C761" Ref="R4"  Part="1" 
AR Path="/5AE857E8/5AE7C761" Ref="R6"  Part="1" 
F 0 "R4" H 3330 2970 50  0000 L CNN
F 1 "1K" H 3330 2910 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 3300 2950 50  0001 C CNN
F 3 "" H 3300 2950 50  0001 C CNN
	1    3300 2950
	0    1    1    0   
$EndComp
$Comp
L C_Small C4
U 1 1 5AE7C768
P 2100 2750
AR Path="/5AE7C24A/5AE7C768" Ref="C4"  Part="1" 
AR Path="/5AE7DFE0/5AE7C768" Ref="C10"  Part="1" 
AR Path="/5AE857E8/5AE7C768" Ref="C16"  Part="1" 
F 0 "C10" H 2110 2820 50  0000 L CNN
F 1 "0.1uF" H 2110 2670 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D5.1mm_W3.2mm_P5.00mm" H 2100 2750 50  0001 C CNN
F 3 "" H 2100 2750 50  0001 C CNN
	1    2100 2750
	0    1    1    0   
$EndComp
$Comp
L R_Small R1
U 1 1 5AE7C76F
P 2900 2750
AR Path="/5AE7C24A/5AE7C76F" Ref="R1"  Part="1" 
AR Path="/5AE7DFE0/5AE7C76F" Ref="R3"  Part="1" 
AR Path="/5AE857E8/5AE7C76F" Ref="R5"  Part="1" 
F 0 "R3" H 2930 2770 50  0000 L CNN
F 1 "10k" H 2930 2710 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 2900 2750 50  0001 C CNN
F 3 "" H 2900 2750 50  0001 C CNN
	1    2900 2750
	-1   0    0    1   
$EndComp
$Comp
L +3.3V #PWR09
U 1 1 5AE7C776
P 2400 1650
AR Path="/5AE7C24A/5AE7C776" Ref="#PWR09"  Part="1" 
AR Path="/5AE7DFE0/5AE7C776" Ref="#PWR010"  Part="1" 
AR Path="/5AE857E8/5AE7C776" Ref="#PWR019"  Part="1" 
F 0 "#PWR010" H 2400 1500 50  0001 C CNN
F 1 "+3.3V" H 2400 1790 50  0000 C CNN
F 2 "" H 2400 1650 50  0001 C CNN
F 3 "" H 2400 1650 50  0001 C CNN
	1    2400 1650
	1    0    0    -1  
$EndComp
$Comp
L PIC32MX270F256B U2
U 1 1 5AE7C77C
P 6300 3600
AR Path="/5AE7C24A/5AE7C77C" Ref="U2"  Part="1" 
AR Path="/5AE7DFE0/5AE7C77C" Ref="U3"  Part="1" 
AR Path="/5AE857E8/5AE7C77C" Ref="U4"  Part="1" 
F 0 "U3" H 8550 2700 60  0000 C CNN
F 1 "PIC32MX270F256B" H 4450 2700 60  0000 C CNN
F 2 "Housings_DIP:DIP-28_W7.62mm_LongPads" H 4900 3550 60  0001 C CNN
F 3 "" H 4900 3550 60  0001 C CNN
	1    6300 3600
	1    0    0    -1  
$EndComp
Text Label 8800 3350 0    60   ~ 0
VSS19
Text Label 3800 3650 2    60   ~ 0
VSS8
Text Label 3800 4150 2    60   ~ 0
VDD13
$Comp
L Crystal_Small Y1
U 1 1 5AE7C786
P 1950 3800
AR Path="/5AE7C24A/5AE7C786" Ref="Y1"  Part="1" 
AR Path="/5AE7DFE0/5AE7C786" Ref="Y2"  Part="1" 
AR Path="/5AE857E8/5AE7C786" Ref="Y3"  Part="1" 
F 0 "Y2" H 1950 3900 50  0000 C CNN
F 1 "Crystal_Small" H 1950 3700 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_5032-2pin_5.0x3.2mm_HandSoldering" H 1950 3800 50  0001 C CNN
F 3 "" H 1950 3800 50  0001 C CNN
	1    1950 3800
	0    1    1    0   
$EndComp
$Comp
L C_Small C7
U 1 1 5AE7C78D
P 1700 3700
AR Path="/5AE7C24A/5AE7C78D" Ref="C7"  Part="1" 
AR Path="/5AE7DFE0/5AE7C78D" Ref="C13"  Part="1" 
AR Path="/5AE857E8/5AE7C78D" Ref="C19"  Part="1" 
F 0 "C13" H 1710 3770 50  0000 L CNN
F 1 "22pF" H 1710 3620 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D5.1mm_W3.2mm_P5.00mm" H 1700 3700 50  0001 C CNN
F 3 "" H 1700 3700 50  0001 C CNN
	1    1700 3700
	0    1    1    0   
$EndComp
$Comp
L C_Small C9
U 1 1 5AE7C794
P 1700 3900
AR Path="/5AE7C24A/5AE7C794" Ref="C9"  Part="1" 
AR Path="/5AE7DFE0/5AE7C794" Ref="C15"  Part="1" 
AR Path="/5AE857E8/5AE7C794" Ref="C21"  Part="1" 
F 0 "C15" H 1710 3970 50  0000 L CNN
F 1 "22pF" H 1710 3820 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D5.1mm_W3.2mm_P5.00mm" H 1700 3900 50  0001 C CNN
F 3 "" H 1700 3900 50  0001 C CNN
	1    1700 3900
	0    1    1    0   
$EndComp
$Comp
L GND #PWR010
U 1 1 5AE7C79B
P 10200 4450
AR Path="/5AE7C24A/5AE7C79B" Ref="#PWR010"  Part="1" 
AR Path="/5AE7DFE0/5AE7C79B" Ref="#PWR011"  Part="1" 
AR Path="/5AE857E8/5AE7C79B" Ref="#PWR020"  Part="1" 
F 0 "#PWR011" H 10200 4200 50  0001 C CNN
F 1 "GND" H 10200 4300 50  0000 C CNN
F 2 "" H 10200 4450 50  0001 C CNN
F 3 "" H 10200 4450 50  0001 C CNN
	1    10200 4450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5AE7C7A1
P 1300 4300
AR Path="/5AE7C24A/5AE7C7A1" Ref="#PWR011"  Part="1" 
AR Path="/5AE7DFE0/5AE7C7A1" Ref="#PWR012"  Part="1" 
AR Path="/5AE857E8/5AE7C7A1" Ref="#PWR021"  Part="1" 
F 0 "#PWR012" H 1300 4050 50  0001 C CNN
F 1 "GND" H 1300 4150 50  0000 C CNN
F 2 "" H 1300 4300 50  0001 C CNN
F 3 "" H 1300 4300 50  0001 C CNN
	1    1300 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 2950 9750 2950
Wire Wire Line
	9600 2050 9600 2950
Connection ~ 9600 2950
Wire Wire Line
	3400 2950 3800 2950
Wire Wire Line
	2250 2950 3200 2950
Wire Wire Line
	2900 2950 2900 2850
Connection ~ 2900 2950
Wire Wire Line
	2900 2650 2900 2550
Wire Wire Line
	8800 3450 9750 3450
Wire Wire Line
	8800 3750 9750 3750
Wire Wire Line
	9300 2050 9300 4250
Wire Wire Line
	10200 3350 8800 3350
Wire Wire Line
	10200 2950 10200 4450
Wire Wire Line
	3800 3650 1300 3650
Wire Wire Line
	1300 2950 1300 4300
Wire Wire Line
	2400 4150 3800 4150
Wire Wire Line
	2400 1650 2400 4150
Wire Wire Line
	3800 3750 2050 3750
Wire Wire Line
	2050 3750 2050 3700
Wire Wire Line
	2050 3700 1800 3700
Wire Wire Line
	3800 3850 2050 3850
Wire Wire Line
	2050 3850 2050 3900
Wire Wire Line
	2050 3900 1800 3900
Wire Wire Line
	2900 2550 2400 2550
Connection ~ 2400 2550
Connection ~ 1950 3700
Connection ~ 1950 3900
Wire Wire Line
	1600 3700 1550 3700
Wire Wire Line
	1550 3700 1550 3900
Wire Wire Line
	1550 3900 1600 3900
Wire Wire Line
	1300 2950 1950 2950
Connection ~ 1300 3650
Wire Wire Line
	1550 3800 1300 3800
Connection ~ 1300 3800
Connection ~ 1550 3800
Wire Wire Line
	9950 3450 10200 3450
Wire Wire Line
	9950 3750 10200 3750
Wire Wire Line
	9950 2950 10200 2950
Wire Wire Line
	8800 3550 10600 3550
Wire Wire Line
	10600 3650 8800 3650
Connection ~ 10200 3750
Connection ~ 10200 3350
Connection ~ 10200 3450
Connection ~ 9300 3750
Wire Wire Line
	9300 4250 8800 4250
Wire Wire Line
	8800 4150 10200 4150
Connection ~ 10200 4150
$Comp
L SPST S1
U 1 1 5AE7C7F5
P 2100 2950
AR Path="/5AE7C24A/5AE7C7F5" Ref="S1"  Part="1" 
AR Path="/5AE7DFE0/5AE7C7F5" Ref="S2"  Part="1" 
AR Path="/5AE857E8/5AE7C7F5" Ref="S3"  Part="1" 
F 0 "S2" H 2090 3035 60  0000 C CNN
F 1 "SPST" H 2100 2855 60  0000 C CNN
F 2 "chordboard-footprints:SW_PUSH_6mm" H 2100 2950 60  0001 C CNN
F 3 "" H 2100 2950 60  0001 C CNN
	1    2100 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 2750 2350 2750
Wire Wire Line
	2350 2750 2350 2950
Connection ~ 2350 2950
Wire Wire Line
	2000 2750 1850 2750
Wire Wire Line
	1850 2750 1850 2950
Connection ~ 1850 2950
Wire Wire Line
	3250 3250 3800 3250
Text Label 3500 3250 2    60   ~ 0
PGD
Wire Wire Line
	3250 3350 3800 3350
Text Label 3500 3350 2    60   ~ 0
PGC
Text HLabel 10600 3550 2    60   Input ~ 0
USBD+
Text HLabel 10600 3650 2    60   Input ~ 0
USBD-
Text HLabel 3250 3250 0    60   Input ~ 0
PGD
Text HLabel 3250 3350 0    60   Input ~ 0
PGC
Wire Wire Line
	3550 2950 3550 2750
Connection ~ 3550 2950
Text HLabel 3550 2750 1    60   Input ~ 0
~RST
Text HLabel 3800 3050 0    60   Input ~ 0
P02
Text HLabel 3800 3150 0    60   Input ~ 0
P03
Text HLabel 3800 3450 0    60   Input ~ 0
P04
Text HLabel 3800 3550 0    60   Input ~ 0
P05
Text HLabel 3800 3950 0    60   Input ~ 0
P11
Text HLabel 3800 4050 0    60   Input ~ 0
P12
Text HLabel 3800 4250 0    60   Input ~ 0
P14
Text HLabel 8800 3050 2    60   Input ~ 0
P16
Text HLabel 9000 3150 2    60   Input ~ 0
P17
Text HLabel 9000 3250 2    60   Input ~ 0
P18
Text HLabel 8800 3850 2    60   Input ~ 0
P24
Text HLabel 8800 3950 2    60   Input ~ 0
P25
Text HLabel 8800 4050 2    60   Input ~ 0
P26
Wire Wire Line
	9000 3150 8800 3150
Wire Wire Line
	8800 3250 9000 3250
Wire Wire Line
	8900 3150 8900 2550
Connection ~ 8900 3150
Wire Wire Line
	8950 3250 8950 2550
Connection ~ 8950 3250
$Comp
L R_Small R1
U 1 1 5AE8845F
P 8900 2450
F 0 "R1" H 8930 2470 50  0000 L CNN
F 1 "4.7K" H 8930 2410 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 8900 2450 50  0001 C CNN
F 3 "" H 8900 2450 50  0001 C CNN
	1    8900 2450
	-1   0    0    1   
$EndComp
Wire Wire Line
	8950 2550 9000 2550
$Comp
L R_Small R2
U 1 1 5AE885A3
P 9000 2450
F 0 "R2" H 9030 2470 50  0000 L CNN
F 1 "4.7K" H 9030 2410 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9000 2450 50  0001 C CNN
F 3 "" H 9000 2450 50  0001 C CNN
	1    9000 2450
	-1   0    0    1   
$EndComp
Wire Wire Line
	8900 2350 8900 2300
Wire Wire Line
	8900 2300 9000 2300
Wire Wire Line
	9000 2300 9000 2350
Wire Wire Line
	8950 2300 8950 2200
Wire Wire Line
	8950 2200 9300 2200
Connection ~ 9300 2200
Connection ~ 8950 2300
$EndSCHEMATC
