EESchema Schematic File Version 2
LIBS:chordboard-components
LIBS:Connector
LIBS:device
LIBS:MCU_Microchip_PIC32
LIBS:power
LIBS:regul
LIBS:Switch
LIBS:MCU_Microchip_PIC16
LIBS:microchip
LIBS:pic32mx-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Crystal_Small Y?
U 1 1 5AE7B7E7
P 3850 3200
F 0 "Y?" H 3850 3300 50  0000 C CNN
F 1 "Crystal_Small" H 3850 3100 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_5032-2pin_5.0x3.2mm" H 3850 3200 50  0001 C CNN
F 3 "" H 3850 3200 50  0001 C CNN
	1    3850 3200
	1    0    0    -1  
$EndComp
$Comp
L C_Small C?
U 1 1 5AE7B7EE
P 3750 3450
F 0 "C?" H 3760 3520 50  0000 L CNN
F 1 "22pF" H 3760 3370 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3750 3450 50  0001 C CNN
F 3 "" H 3750 3450 50  0001 C CNN
	1    3750 3450
	1    0    0    -1  
$EndComp
$Comp
L C_Small C?
U 1 1 5AE7B7F5
P 3950 3450
F 0 "C?" H 3960 3520 50  0000 L CNN
F 1 "22pF" H 3960 3370 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3950 3450 50  0001 C CNN
F 3 "" H 3950 3450 50  0001 C CNN
	1    3950 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2900 3800 3100
Wire Wire Line
	3800 3100 3750 3100
Wire Wire Line
	3750 3100 3750 3200
Wire Wire Line
	3750 3200 3750 3350
Wire Wire Line
	3900 2900 3900 3100
Wire Wire Line
	3900 3100 3950 3100
Wire Wire Line
	3950 3100 3950 3200
Wire Wire Line
	3950 3200 3950 3350
Connection ~ 3750 3200
Connection ~ 3950 3200
Wire Wire Line
	3750 3550 3750 3600
Wire Wire Line
	3750 3600 3850 3600
Wire Wire Line
	3850 3600 3950 3600
Wire Wire Line
	3950 3600 3950 3550
Wire Wire Line
	3850 3600 3850 3850
Connection ~ 3850 3600
$Comp
L GND #PWR?
U 1 1 5AE7B85B
P 3850 3850
F 0 "#PWR?" H 3850 3600 50  0001 C CNN
F 1 "GND" H 3850 3700 50  0000 C CNN
F 2 "" H 3850 3850 50  0001 C CNN
F 3 "" H 3850 3850 50  0001 C CNN
	1    3850 3850
	1    0    0    -1  
$EndComp
Text HLabel 3800 2900 0    60   Input ~ 0
OSC1
Text HLabel 3900 2900 2    60   Input ~ 0
OSC2
$EndSCHEMATC
