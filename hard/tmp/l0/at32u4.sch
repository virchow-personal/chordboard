EESchema Schematic File Version 2
LIBS:chordboard-rescue
LIBS:chordboard-components
LIBS:Kicad-Teensy
LIBS:ESP32-footprints-Shem-Lib
LIBS:MCU_NXP_Kinetis
LIBS:nxp
LIBS:texas
LIBS:Logic_74xx
LIBS:atmel
LIBS:eurocad
LIBS:Transistor
LIBS:power
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Crystal Y?
U 1 1 5AC6EADD
P 2900 4150
F 0 "Y?" H 2900 4300 50  0000 C CNN
F 1 "Crystal" H 2900 4000 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_5032-2pin_5.0x3.2mm_HandSoldering" H 2900 4150 50  0001 C CNN
F 3 "" H 2900 4150 50  0001 C CNN
	1    2900 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 4000 3050 4000
$Comp
L ATmega32U4 U?
U 1 1 5AC6EAE5
P 4300 3750
F 0 "U?" H 3400 5450 50  0000 C CNN
F 1 "ATmega32U4" H 3650 2200 50  0000 C CNN
F 2 "Housings_QFP:TQFP-44_10x10mm_Pitch0.8mm" H 5500 4850 50  0001 C CNN
F 3 "" H 5500 4850 50  0001 C CNN
	1    4300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 4300 3050 4300
Connection ~ 2900 4000
Connection ~ 2900 4300
$Comp
L C C?
U 1 1 5AC6EAEF
P 2550 4000
F 0 "C?" H 2560 4070 50  0000 L CNN
F 1 "18pF" H 2560 3920 50  0000 L CNN
F 2 "chordboard-footprints:C_0805_HandSoldering" H 2550 4000 50  0001 C CNN
F 3 "" H 2550 4000 50  0001 C CNN
	1    2550 4000
	0    1    1    0   
$EndComp
$Comp
L C C?
U 1 1 5AC6EAF6
P 2550 4300
F 0 "C?" H 2560 4370 50  0000 L CNN
F 1 "18pF" H 2560 4220 50  0000 L CNN
F 2 "chordboard-footprints:C_0805_HandSoldering" H 2550 4300 50  0001 C CNN
F 3 "" H 2550 4300 50  0001 C CNN
	1    2550 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	2250 4000 2450 4000
Wire Wire Line
	2250 3650 2250 5500
Connection ~ 2250 4300
$Comp
L C C?
U 1 1 5AC6EB00
P 2550 4450
F 0 "C?" H 2560 4520 50  0000 L CNN
F 1 "1uF" H 2560 4370 50  0000 L CNN
F 2 "chordboard-footprints:C_0805_HandSoldering" H 2550 4450 50  0001 C CNN
F 3 "" H 2550 4450 50  0001 C CNN
	1    2550 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	2450 4450 2250 4450
Connection ~ 2250 4000
$Comp
L R R?
U 1 1 5AC6EB09
P 2800 4550
F 0 "R?" V 2850 4650 50  0000 L CNN
F 1 "R" V 2800 4500 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2800 4550 50  0001 C CNN
F 3 "" H 2800 4550 50  0001 C CNN
	1    2800 4550
	0    1    1    0   
$EndComp
$Comp
L GND #PWR?
U 1 1 5AC6EB10
P 2250 5500
F 0 "#PWR?" H 2250 5250 50  0001 C CNN
F 1 "GND" H 2250 5350 50  0000 C CNN
F 2 "" H 2250 5500 50  0001 C CNN
F 3 "" H 2250 5500 50  0001 C CNN
	1    2250 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4800 2250 4800
Connection ~ 2250 4800
Wire Wire Line
	3050 4900 2250 4900
Connection ~ 2250 4900
Wire Wire Line
	3050 5000 2250 5000
Connection ~ 2250 5000
Wire Wire Line
	3050 5100 2250 5100
Connection ~ 2250 5100
Wire Wire Line
	2900 4550 3050 4550
Wire Wire Line
	2700 4550 2250 4550
Connection ~ 2250 4550
Connection ~ 2250 4450
$Comp
L USB_B J?
U 1 1 5AC6EB22
P 2350 3250
F 0 "J?" H 2150 3700 50  0000 L CNN
F 1 "USB_B" H 2150 3600 50  0000 L CNN
F 2 "Connectors:USB_B" H 2500 3200 50  0001 C CNN
F 3 "" H 2500 3200 50  0001 C CNN
	1    2350 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3250 3050 3250
Wire Wire Line
	3050 3350 2650 3350
Wire Wire Line
	2650 3050 3050 3050
Wire Wire Line
	2650 4450 3050 4450
Wire Wire Line
	2250 4300 2450 4300
Wire Wire Line
	2250 3850 3050 3850
Wire Wire Line
	2350 3650 2350 3700
Wire Wire Line
	2350 3700 2250 3700
Connection ~ 2250 3850
Connection ~ 2250 3700
Wire Wire Line
	3050 2950 2900 2950
Wire Wire Line
	2900 2950 2900 3050
Connection ~ 2900 3050
$Comp
L R R?
U 1 1 5AC6EB36
P 2900 2650
F 0 "R?" V 2950 2750 50  0000 L CNN
F 1 "R" V 2900 2600 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2900 2650 50  0001 C CNN
F 3 "" H 2900 2650 50  0001 C CNN
	1    2900 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 2650 3050 2650
Wire Wire Line
	2250 2650 2800 2650
Wire Wire Line
	2250 1750 2250 2650
Wire Wire Line
	3050 2400 2250 2400
Connection ~ 2250 2400
Wire Wire Line
	3050 2300 2250 2300
Connection ~ 2250 2300
Wire Wire Line
	3050 2500 2250 2500
Connection ~ 2250 2500
Wire Wire Line
	3050 2200 2250 2200
Connection ~ 2250 2200
Wire Wire Line
	3050 4700 2250 4700
Connection ~ 2250 4700
$Comp
L +5V #PWR?
U 1 1 5AC6EB4A
P 2250 1750
F 0 "#PWR?" H 2250 1600 50  0001 C CNN
F 1 "+5V" H 2250 1890 50  0000 C CNN
F 2 "" H 2250 1750 50  0001 C CNN
F 3 "" H 2250 1750 50  0001 C CNN
	1    2250 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 3050 2750 2650
Connection ~ 2750 2650
Connection ~ 2750 3050
$EndSCHEMATC
