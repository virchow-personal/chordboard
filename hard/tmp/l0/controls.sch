EESchema Schematic File Version 2
LIBS:chordboard-rescue
LIBS:maxim
LIBS:chordboard-components
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:switches
LIBS:mechanical
LIBS:Kicad-Teensy
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ROTARY_ENCODER_afshar ENC?
U 1 1 5AA977B5
P -50 1900
F 0 "ENC?" H -50 2300 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 0   1550 60  0000 C CNN
F 2 "chordboard-footprints:Rotary_Encoder_BI_EN12-HS22AF30" H -50 1450 60  0000 C CNN
F 3 "" H -50 1900 60  0000 C CNN
	1    -50  1900
	1    0    0    1   
$EndComp
$Comp
L C C?
U 1 1 5AA977BC
P -300 2400
F 0 "C?" H -290 2470 50  0000 L CNN
F 1 "0.1uF" H -290 2320 50  0000 L CNN
F 2 "chordboard-footprints:C_0805_HandSoldering" H -300 2400 50  0001 C CNN
F 3 "" H -300 2400 50  0001 C CNN
	1    -300 2400
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5AA977C3
P -800 2400
F 0 "C?" H -790 2470 50  0000 L CNN
F 1 "0.1uF" H -790 2320 50  0000 L CNN
F 2 "chordboard-footprints:C_0805_HandSoldering" H -800 2400 50  0001 C CNN
F 3 "" H -800 2400 50  0001 C CNN
	1    -800 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5AA977CA
P 450 2050
F 0 "#PWR?" H 450 1800 50  0001 C CNN
F 1 "GND" H 450 1900 50  0000 C CNN
F 2 "" H 450 2050 50  0001 C CNN
F 3 "" H 450 2050 50  0001 C CNN
	1    450  2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5AA977D0
P -550 2600
F 0 "#PWR?" H -550 2350 50  0001 C CNN
F 1 "GND" H -550 2450 50  0000 C CNN
F 2 "" H -550 2600 50  0001 C CNN
F 3 "" H -550 2600 50  0001 C CNN
	1    -550 2600
	1    0    0    -1  
$EndComp
Text Label -300 1800 2    60   ~ 0
RE1_SW
$Comp
L ROTARY_ENCODER_afshar ENC?
U 1 1 5AA977D7
P 2150 1900
F 0 "ENC?" H 2150 2300 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 2200 1550 60  0000 C CNN
F 2 "chordboard-footprints:Rotary_Encoder_BI_EN12-HS22AF30" H 2150 1450 60  0000 C CNN
F 3 "" H 2150 1900 60  0000 C CNN
	1    2150 1900
	1    0    0    1   
$EndComp
$Comp
L C C?
U 1 1 5AA977DE
P 1900 2400
F 0 "C?" H 1910 2470 50  0000 L CNN
F 1 "0.1uF" H 1910 2320 50  0000 L CNN
F 2 "chordboard-footprints:C_0805_HandSoldering" H 1900 2400 50  0001 C CNN
F 3 "" H 1900 2400 50  0001 C CNN
	1    1900 2400
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5AA977E5
P 1400 2400
F 0 "C?" H 1410 2470 50  0000 L CNN
F 1 "0.1uF" H 1410 2320 50  0000 L CNN
F 2 "chordboard-footprints:C_0805_HandSoldering" H 1400 2400 50  0001 C CNN
F 3 "" H 1400 2400 50  0001 C CNN
	1    1400 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5AA977EC
P 2650 2050
F 0 "#PWR?" H 2650 1800 50  0001 C CNN
F 1 "GND" H 2650 1900 50  0000 C CNN
F 2 "" H 2650 2050 50  0001 C CNN
F 3 "" H 2650 2050 50  0001 C CNN
	1    2650 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5AA977F2
P 1650 2600
F 0 "#PWR?" H 1650 2350 50  0001 C CNN
F 1 "GND" H 1650 2450 50  0000 C CNN
F 2 "" H 1650 2600 50  0001 C CNN
F 3 "" H 1650 2600 50  0001 C CNN
	1    1650 2600
	1    0    0    -1  
$EndComp
Text Label 1900 1800 2    60   ~ 0
RE2_SW
Text Notes -350 1150 0    60   ~ 0
Config: Key; Mode\nShift: Change Main mode
Text Notes 1300 1150 0    60   ~ 0
Config: Octave; spread; channel; etc.\nShift Config: change bank
Text Label -800 1950 2    60   ~ 0
RE1_B
Text Label -300 2150 2    60   ~ 0
RE1_A
Text Label 1400 1950 2    60   ~ 0
RE2_B
Text Label 1900 2150 2    60   ~ 0
RE2_A
Connection ~ 250  1900
Wire Wire Line
	450  1900 250  1900
Wire Wire Line
	250  1700 250  2050
Wire Wire Line
	-300 2150 -300 2300
Wire Wire Line
	-300 1950 -800 1950
Wire Wire Line
	-800 1950 -800 2300
Wire Wire Line
	-800 2500 -300 2500
Wire Wire Line
	-550 2500 -550 2600
Connection ~ -550 2500
Wire Wire Line
	450  1900 450  2050
Connection ~ 2450 1900
Wire Wire Line
	2650 1900 2450 1900
Wire Wire Line
	2450 1700 2450 2050
Wire Wire Line
	1900 2150 1900 2300
Wire Wire Line
	1900 1950 1400 1950
Wire Wire Line
	1400 1950 1400 2300
Wire Wire Line
	1400 2500 1900 2500
Wire Wire Line
	1650 2500 1650 2600
Connection ~ 1650 2500
Wire Wire Line
	2650 1900 2650 2050
$Comp
L KSW SW?
U 1 1 5AA981A6
P 4200 1850
F 0 "SW?" H 4100 1900 60  0000 C CNN
F 1 "KSW" H 4350 1900 60  0000 C CNN
F 2 "chordboard-footprints:SW_PUSH_6mm" H 4200 1850 60  0001 C CNN
F 3 "" H 4200 1850 60  0001 C CNN
	1    4200 1850
	-1   0    0    -1  
$EndComp
$Comp
L KSW SW?
U 1 1 5AA981AD
P 4200 1950
F 0 "SW?" H 4100 2000 60  0000 C CNN
F 1 "KSW" H 4350 2000 60  0000 C CNN
F 2 "chordboard-footprints:SW_PUSH_6mm" H 5150 2000 60  0001 C CNN
F 3 "" H 4200 1950 60  0001 C CNN
	1    4200 1950
	-1   0    0    -1  
$EndComp
$Comp
L KSW SW?
U 1 1 5AA981B4
P 4200 2050
F 0 "SW?" H 4100 2100 60  0000 C CNN
F 1 "KSW" H 4350 2100 60  0000 C CNN
F 2 "chordboard-footprints:SW_PUSH_6mm" H 5150 2100 60  0001 C CNN
F 3 "" H 4200 2050 60  0001 C CNN
	1    4200 2050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4350 1850 4850 1850
Wire Wire Line
	4350 1950 4850 1950
Wire Wire Line
	4050 1850 3650 1850
Wire Wire Line
	3650 1950 4050 1950
Wire Wire Line
	4050 2050 3650 2050
Wire Wire Line
	4350 2050 4850 2050
$Comp
L KSW SW?
U 1 1 5AA981C1
P 4200 2150
F 0 "SW?" H 4100 2200 60  0000 C CNN
F 1 "KSW" H 4350 2200 60  0000 C CNN
F 2 "chordboard-footprints:SW_PUSH_6mm" H 5150 2200 60  0001 C CNN
F 3 "" H 4200 2150 60  0001 C CNN
	1    4200 2150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4050 2150 3650 2150
Wire Wire Line
	4350 2150 4850 2150
$EndSCHEMATC
