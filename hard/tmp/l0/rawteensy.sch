EESchema Schematic File Version 2
LIBS:chordboard-rescue
LIBS:chordboard-components
LIBS:Kicad-Teensy
LIBS:ESP32-footprints-Shem-Lib
LIBS:MCU_NXP_Kinetis
LIBS:nxp
LIBS:texas
LIBS:Logic_74xx
LIBS:atmel
LIBS:eurocad
LIBS:Transistor
LIBS:power
LIBS:chordboard-cache
LIBS:leftpad-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MK66FX1M0VLQ18-Teensy U?
U 1 1 5AC633A1
P 4250 3600
F 0 "U?" H 3600 6750 60  0000 C CNN
F 1 "MK66FX1M0VLQ18-Teensy" H 4400 6750 60  0000 C CNN
F 2 "Housings_QFP:LQFP-144_20x20mm_Pitch0.5mm" H 4100 5850 60  0001 C CNN
F 3 "" H 4100 5850 60  0001 C CNN
	1    4250 3600
	1    0    0    -1  
$EndComp
$Comp
L Crystal_Small Y?
U 1 1 5AC633A8
P 3150 2850
F 0 "Y?" H 3150 2950 50  0000 C CNN
F 1 "Crystal_Small" H 3150 2750 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_5032-2pin_5.0x3.2mm_HandSoldering" H 3150 2850 50  0001 C CNN
F 3 "" H 3150 2850 50  0001 C CNN
	1    3150 2850
	0    1    1    0   
$EndComp
$Comp
L Crystal_Small Y?
U 1 1 5AC633AF
P 3150 3250
F 0 "Y?" H 3150 3350 50  0000 C CNN
F 1 "Crystal_Small" H 3150 3150 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_5032-2pin_5.0x3.2mm_HandSoldering" H 3150 3250 50  0001 C CNN
F 3 "" H 3150 3250 50  0001 C CNN
	1    3150 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	3150 3150 3150 3100
Wire Wire Line
	3150 3100 3350 3100
Wire Wire Line
	3150 3350 3150 3400
Wire Wire Line
	3150 3400 3350 3400
Wire Wire Line
	3350 3000 3150 3000
Wire Wire Line
	3150 3000 3150 2950
Wire Wire Line
	3350 2650 3150 2650
Wire Wire Line
	3150 2650 3150 2750
$Comp
L USB_A J?
U 1 1 5AC633BE
P 2350 2450
F 0 "J?" H 2150 2900 50  0000 L CNN
F 1 "USB_A" H 2150 2800 50  0000 L CNN
F 2 "Connectors:USB_B" H 2500 2400 50  0001 C CNN
F 3 "" H 2500 2400 50  0001 C CNN
	1    2350 2450
	1    0    0    1   
$EndComp
$Comp
L R R?
U 1 1 5AC633C5
P 3050 2350
F 0 "R?" V 3100 2450 50  0000 L CNN
F 1 "R" V 3050 2300 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3050 2350 50  0001 C CNN
F 3 "" H 3050 2350 50  0001 C CNN
	1    3050 2350
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 5AC633CC
P 3050 2450
F 0 "R?" V 3100 2550 50  0000 L CNN
F 1 "R" V 3050 2400 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3050 2450 50  0001 C CNN
F 3 "" H 3050 2450 50  0001 C CNN
	1    3050 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	3150 2350 3350 2350
Wire Wire Line
	3350 2450 3150 2450
Wire Wire Line
	2650 2350 2950 2350
Wire Wire Line
	2950 2450 2650 2450
$Comp
L USB_A J?
U 1 1 5AC633D7
P 2350 3900
F 0 "J?" H 2150 4350 50  0000 L CNN
F 1 "USB_A" H 2150 4250 50  0000 L CNN
F 2 "Connectors:USB_B" H 2500 3850 50  0001 C CNN
F 3 "" H 2500 3850 50  0001 C CNN
	1    2350 3900
	1    0    0    1   
$EndComp
$Comp
L R R?
U 1 1 5AC633DE
P 3050 3800
F 0 "R?" V 3100 3900 50  0000 L CNN
F 1 "R" V 3050 3750 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3050 3800 50  0001 C CNN
F 3 "" H 3050 3800 50  0001 C CNN
	1    3050 3800
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 5AC633E5
P 3050 3900
F 0 "R?" V 3100 4000 50  0000 L CNN
F 1 "R" V 3050 3850 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3050 3900 50  0001 C CNN
F 3 "" H 3050 3900 50  0001 C CNN
	1    3050 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	3150 3800 3350 3800
Wire Wire Line
	3350 3900 3150 3900
Wire Wire Line
	2650 3800 2950 3800
Wire Wire Line
	2950 3900 2650 3900
$EndSCHEMATC
