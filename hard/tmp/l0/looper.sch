EESchema Schematic File Version 2
LIBS:chordboard-rescue
LIBS:maxim
LIBS:chordboard-components
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:switches
LIBS:mechanical
LIBS:Kicad-Teensy
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ROTARY_ENCODER_afshar ENC?
U 1 1 5AA6E7A9
P 2650 1650
F 0 "ENC?" H 2650 2050 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 2700 1300 60  0000 C CNN
F 2 "chordboard-footprints:Rotary_Encoder_BI_EN12-HS22AF30" H 2650 1200 60  0000 C CNN
F 3 "" H 2650 1650 60  0000 C CNN
	1    2650 1650
	1    0    0    1   
$EndComp
$Comp
L C C?
U 1 1 5AA6E7B0
P 2400 2150
F 0 "C?" H 2410 2220 50  0000 L CNN
F 1 "0.1uF" H 2410 2070 50  0000 L CNN
F 2 "chordboard-footprints:C_0805_HandSoldering" H 2400 2150 50  0001 C CNN
F 3 "" H 2400 2150 50  0001 C CNN
	1    2400 2150
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5AA6E7B7
P 1900 2150
F 0 "C?" H 1910 2220 50  0000 L CNN
F 1 "0.1uF" H 1910 2070 50  0000 L CNN
F 2 "chordboard-footprints:C_0805_HandSoldering" H 1900 2150 50  0001 C CNN
F 3 "" H 1900 2150 50  0001 C CNN
	1    1900 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5AA6E7BE
P 3150 1800
F 0 "#PWR?" H 3150 1550 50  0001 C CNN
F 1 "GND" H 3150 1650 50  0000 C CNN
F 2 "" H 3150 1800 50  0001 C CNN
F 3 "" H 3150 1800 50  0001 C CNN
	1    3150 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5AA6E7C4
P 2150 2350
F 0 "#PWR?" H 2150 2100 50  0001 C CNN
F 1 "GND" H 2150 2200 50  0000 C CNN
F 2 "" H 2150 2350 50  0001 C CNN
F 3 "" H 2150 2350 50  0001 C CNN
	1    2150 2350
	1    0    0    -1  
$EndComp
Text Label 2400 1550 2    60   ~ 0
RE1_SW
Text Notes 2350 900  0    60   ~ 0
Config: Key; Mode\nShift: Change Main mode
Text Label 1900 1700 2    60   ~ 0
RE1_B
Text Label 2400 1900 2    60   ~ 0
RE1_A
Connection ~ 2950 1650
Wire Wire Line
	3150 1650 2950 1650
Wire Wire Line
	2950 1450 2950 1800
Wire Wire Line
	2400 1900 2400 2050
Wire Wire Line
	2400 1700 1900 1700
Wire Wire Line
	1900 1700 1900 2050
Wire Wire Line
	1900 2250 2400 2250
Wire Wire Line
	2150 2250 2150 2350
Connection ~ 2150 2250
Wire Wire Line
	3150 1650 3150 1800
$Comp
L KSW SW?
U 1 1 5AA6EECF
P 4350 1900
F 0 "SW?" H 4250 1950 60  0000 C CNN
F 1 "KSW" H 4500 1950 60  0000 C CNN
F 2 "chordboard-footprints:SW_PUSH_6mm" H 5300 1950 60  0001 C CNN
F 3 "" H 4350 1900 60  0001 C CNN
	1    4350 1900
	-1   0    0    -1  
$EndComp
$Comp
L KSW SW?
U 1 1 5AA6EED6
P 4350 2000
F 0 "SW?" H 4250 2050 60  0000 C CNN
F 1 "KSW" H 4500 2050 60  0000 C CNN
F 2 "chordboard-footprints:SW_PUSH_6mm" H 5300 2050 60  0001 C CNN
F 3 "" H 4350 2000 60  0001 C CNN
	1    4350 2000
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
