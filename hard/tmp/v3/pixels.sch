EESchema Schematic File Version 2
LIBS:chordboard-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:teensy
LIBS:switches
LIBS:Symbols_DCDC-ACDC-Converter_RevC_20Jul2012
LIBS:Symbols_EN60617_13Mar2013
LIBS:Symbols_EN60617-10_HF-Radio_DRAFT_12Sep2013
LIBS:Symbols_ICs-Diskrete_RevD10
LIBS:Symbols_ICs-Opto_RevB_16Sep2013
LIBS:Symbols_Microcontroller_Philips-NXP_RevA_06Oct2013
LIBS:SymbolsSimilarEN60617+oldDIN617-RevE8
LIBS:Symbols_Socket-DIN41612_RevA
LIBS:Symbols_Transformer-Diskrete_RevA
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Bosch
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:mcp73831t-2aci-ot
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:standard
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA22
P 2800 2550
F 0 "LED?" H 2600 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 2700 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 2800 2250 50  0000 C CNN
F 3 "" H 2800 2500 50  0000 C CNN
	1    2800 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA29
P 3650 2550
F 0 "LED?" H 3450 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 3550 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 3650 2250 50  0000 C CNN
F 3 "" H 3650 2500 50  0000 C CNN
	1    3650 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA30
P 4500 2550
F 0 "LED?" H 4300 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 4400 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 4500 2250 50  0000 C CNN
F 3 "" H 4500 2500 50  0000 C CNN
	1    4500 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA37
P 5350 2550
F 0 "LED?" H 5150 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 5250 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 5350 2250 50  0000 C CNN
F 3 "" H 5350 2500 50  0000 C CNN
	1    5350 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA3E
P 6200 2550
F 0 "LED?" H 6000 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 6100 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 6200 2250 50  0000 C CNN
F 3 "" H 6200 2500 50  0000 C CNN
	1    6200 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA45
P 7050 2550
F 0 "LED?" H 6850 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 6950 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 7050 2250 50  0000 C CNN
F 3 "" H 7050 2500 50  0000 C CNN
	1    7050 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA4C
P 7900 2550
F 0 "LED?" H 7700 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 7800 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 7900 2250 50  0000 C CNN
F 3 "" H 7900 2500 50  0000 C CNN
	1    7900 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA53
P 8750 2550
F 0 "LED?" H 8550 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 8650 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 8750 2250 50  0000 C CNN
F 3 "" H 8750 2500 50  0000 C CNN
	1    8750 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA5A
P 9600 2550
F 0 "LED?" H 9400 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 9500 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 9600 2250 50  0000 C CNN
F 3 "" H 9600 2500 50  0000 C CNN
	1    9600 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA61
P 10450 2550
F 0 "LED?" H 10250 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 10350 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 10450 2250 50  0000 C CNN
F 3 "" H 10450 2500 50  0000 C CNN
	1    10450 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA68
P 11300 2550
F 0 "LED?" H 11100 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 11200 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 11300 2250 50  0000 C CNN
F 3 "" H 11300 2500 50  0000 C CNN
	1    11300 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA6F
P 12150 2550
F 0 "LED?" H 11950 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 12050 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 12150 2250 50  0000 C CNN
F 3 "" H 12150 2500 50  0000 C CNN
	1    12150 2550
	1    0    0    -1  
$EndComp
$Comp
L WS2812B_afshar LED?
U 1 1 5A60CA76
P 13000 2550
F 0 "LED?" H 12800 2350 50  0000 C CNN
F 1 "WS2812B_afshar" H 12900 2650 50  0000 C CNN
F 2 "afshar-kicad-lib:LED_D8.0mm-Neopixel" H 13000 2250 50  0000 C CNN
F 3 "" H 13000 2500 50  0000 C CNN
	1    13000 2550
	1    0    0    -1  
$EndComp
Text Label 700  2600 2    60   ~ 0
PIXEL_IN
Wire Wire Line
	2950 2300 13150 2300
Connection ~ 3800 2300
Connection ~ 4650 2300
Connection ~ 5500 2300
Connection ~ 6350 2300
Connection ~ 7200 2300
Connection ~ 8050 2300
Connection ~ 8900 2300
Connection ~ 9750 2300
Connection ~ 10600 2300
Connection ~ 11450 2300
Connection ~ 12300 2300
NoConn ~ 13350 2600
Wire Wire Line
	2950 2900 13150 2900
Connection ~ 12300 2900
Connection ~ 10600 2900
Connection ~ 9750 2900
Connection ~ 8900 2900
Connection ~ 8050 2900
Connection ~ 7200 2900
Connection ~ 6350 2900
Connection ~ 5500 2900
Connection ~ 4650 2900
Connection ~ 3800 2900
$Comp
L GND #PWR?
U 1 1 5A60CA96
P 2950 3050
F 0 "#PWR?" H 2950 2800 50  0001 C CNN
F 1 "GND" H 2950 2900 50  0000 C CNN
F 2 "" H 2950 3050 50  0000 C CNN
F 3 "" H 2950 3050 50  0000 C CNN
	1    2950 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2900 2950 3050
Wire Wire Line
	2950 2300 2950 2150
$Comp
L +5V #PWR?
U 1 1 5A60CA9E
P 2950 2150
F 0 "#PWR?" H 2950 2000 50  0001 C CNN
F 1 "+5V" H 2950 2290 50  0000 C CNN
F 2 "" H 2950 2150 50  0001 C CNN
F 3 "" H 2950 2150 50  0001 C CNN
	1    2950 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 2600 2400 2600
Wire Wire Line
	3200 2600 3250 2600
Wire Wire Line
	4050 2600 4100 2600
Wire Wire Line
	4900 2600 4950 2600
Wire Wire Line
	5750 2600 5800 2600
Wire Wire Line
	6600 2600 6650 2600
Wire Wire Line
	7450 2600 7500 2600
Wire Wire Line
	8300 2600 8350 2600
Wire Wire Line
	9150 2600 9200 2600
Wire Wire Line
	10000 2600 10050 2600
Wire Wire Line
	10850 2600 10900 2600
Wire Wire Line
	11700 2600 11750 2600
Wire Wire Line
	12550 2600 12600 2600
Wire Wire Line
	700  2600 800  2600
Wire Wire Line
	650  3500 800  3500
Wire Wire Line
	650  3350 650  3500
Wire Wire Line
	800  3600 650  3600
Wire Wire Line
	650  3600 650  4000
$Comp
L +5V #PWR?
U 1 1 5A60CAB6
P 650 3350
F 0 "#PWR?" H 650 3200 50  0001 C CNN
F 1 "+5V" H 650 3490 50  0000 C CNN
F 2 "" H 650 3350 50  0001 C CNN
F 3 "" H 650 3350 50  0001 C CNN
	1    650  3350
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5A60CABC
P 550 3550
F 0 "C?" H 575 3650 50  0000 L CNN
F 1 "0.1uF" V 650 3300 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 588 3400 50  0001 C CNN
F 3 "" H 550 3550 50  0001 C CNN
	1    550  3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	650  3400 550  3400
Connection ~ 650  3400
Wire Wire Line
	650  3700 550  3700
Connection ~ 650  3700
$Comp
L 74LS245_aa U?
U 1 1 5A60CAC7
P 1500 3100
F 0 "U?" H 1600 3675 50  0000 L BNN
F 1 "74LS245_aa" H 1550 2525 50  0000 L TNN
F 2 "Housings_DIP:DIP-20_W7.62mm" H 1500 3100 50  0001 C CNN
F 3 "" H 1500 3100 50  0001 C CNN
	1    1500 3100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 5A60CACE
P 1500 2200
F 0 "#PWR?" H 1500 2050 50  0001 C CNN
F 1 "+5V" H 1500 2340 50  0000 C CNN
F 2 "" H 1500 2200 50  0001 C CNN
F 3 "" H 1500 2200 50  0001 C CNN
	1    1500 2200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A60CAD4
P 1500 4050
F 0 "#PWR?" H 1500 3800 50  0001 C CNN
F 1 "GND" H 1500 3900 50  0000 C CNN
F 2 "" H 1500 4050 50  0000 C CNN
F 3 "" H 1500 4050 50  0000 C CNN
	1    1500 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2250 1500 2200
Wire Wire Line
	1500 3950 1500 4050
Wire Wire Line
	650  4000 1500 4000
Connection ~ 1500 4000
$Comp
L Jumper_NO_Small JP?
U 1 1 5A60CADE
P 2300 2400
F 0 "JP?" H 2300 2480 50  0000 C CNN
F 1 "Jumper_NO_Small" H 2310 2340 50  0000 C CNN
F 2 "Resistors_Universal:Resistor_SMD+THTuniversal_0805to1206_RM10_HandSoldering" H 2300 2400 50  0001 C CNN
F 3 "" H 2300 2400 50  0001 C CNN
	1    2300 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 2500 2300 2600
Connection ~ 2300 2600
Text Label 2300 2250 1    60   ~ 0
PIXEL_IN
Wire Wire Line
	2300 2250 2300 2300
$EndSCHEMATC
