EESchema Schematic File Version 4
LIBS:chordboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Audio:PCM5102 DAC?
U 1 1 5D8C2361
P 4050 3650
F 0 "DAC?" H 4050 3700 50  0000 C CNN
F 1 "PCM5102" H 4050 3600 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 4000 4400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/pcm5102.pdf" H 4000 4400 50  0001 C CNN
	1    4050 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3550 2700 3550
Wire Wire Line
	3550 3250 2700 3250
Wire Wire Line
	3550 3350 2700 3350
Wire Wire Line
	3550 3450 2700 3450
Text Label 2700 3250 2    50   ~ 0
DAC_IN_LRCK
Text Label 2700 3350 2    50   ~ 0
DAC_IN_DIN
Text Label 2700 3450 2    50   ~ 0
DAC_IN_BCK
Wire Wire Line
	4550 3250 5550 3250
Wire Wire Line
	4550 3350 5550 3350
$Comp
L Device:R R?
U 1 1 5D8C2371
P 5700 3250
F 0 "R?" V 5800 3150 50  0000 C CNN
F 1 "470" V 5800 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5630 3250 50  0001 C CNN
F 3 "~" H 5700 3250 50  0001 C CNN
	1    5700 3250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D8C2378
P 5700 3350
F 0 "R?" V 5600 3250 50  0000 C CNN
F 1 "470" V 5600 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5630 3350 50  0001 C CNN
F 3 "~" H 5700 3350 50  0001 C CNN
	1    5700 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 3250 6000 3250
Wire Wire Line
	5850 3350 6300 3350
$Comp
L Device:C C?
U 1 1 5D8C2381
P 6000 3700
F 0 "C?" H 6050 3800 50  0000 L CNN
F 1 "2.2nF" V 6050 3400 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 6038 3550 50  0001 C CNN
F 3 "~" H 6000 3700 50  0001 C CNN
	1    6000 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D8C2388
P 6300 3700
F 0 "C?" H 6350 3800 50  0000 L CNN
F 1 "2.2nF" V 6350 3400 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 6338 3550 50  0001 C CNN
F 3 "~" H 6300 3700 50  0001 C CNN
	1    6300 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3550 6000 3250
Connection ~ 6000 3250
Wire Wire Line
	6000 3250 6600 3250
Wire Wire Line
	6300 3350 6300 3550
Connection ~ 6300 3350
Wire Wire Line
	6300 3350 6600 3350
Wire Wire Line
	6300 3850 6300 4050
Wire Wire Line
	6000 3850 6000 4050
Wire Wire Line
	6000 4050 6150 4050
Wire Wire Line
	6150 4050 6150 4600
Wire Wire Line
	6150 4050 6300 4050
Connection ~ 6150 4050
Text Label 6600 3250 0    50   ~ 0
DAC_OUT_L
Text Label 6600 3350 0    50   ~ 0
DAC_OUT_R
$Comp
L Device:C C?
U 1 1 5D8C239D
P 4800 3700
F 0 "C?" H 4850 3800 50  0000 L CNN
F 1 "2.2uF" V 4850 3400 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4838 3550 50  0001 C CNN
F 3 "~" H 4800 3700 50  0001 C CNN
	1    4800 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3550 4800 3550
Wire Wire Line
	4800 3850 4550 3850
$Comp
L Device:C C?
U 1 1 5D8C23A6
P 4800 4300
F 0 "C?" H 4850 4400 50  0000 L CNN
F 1 "2.2uF" V 4850 4000 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4838 4150 50  0001 C CNN
F 3 "~" H 4800 4300 50  0001 C CNN
	1    4800 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4150 4800 4150
Wire Wire Line
	4800 4600 4800 4450
Text Label 3950 4600 3    50   ~ 0
DAC_CPGND
Text Label 6150 4600 3    50   ~ 0
DAC_CPGND
Text Label 4050 4600 3    50   ~ 0
DAC_DGND
Text Label 4150 4600 3    50   ~ 0
DAC_AGND
Text Label 3950 2150 2    50   ~ 0
DAC_CPVDD
Wire Wire Line
	4150 3050 4150 2950
$Comp
L Device:C C?
U 1 1 5D8C23B5
P 3650 2750
F 0 "C?" H 3700 2850 50  0000 L CNN
F 1 "0.1uF" V 3700 2450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3688 2600 50  0001 C CNN
F 3 "~" H 3650 2750 50  0001 C CNN
	1    3650 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	3800 2750 3950 2750
$Comp
L Device:CP C?
U 1 1 5D8C23BD
P 3650 2550
F 0 "C?" V 3905 2550 50  0000 C CNN
F 1 "10uF" V 3814 2550 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3688 2400 50  0001 C CNN
F 3 "~" H 3650 2550 50  0001 C CNN
	1    3650 2550
	0    1    -1   0   
$EndComp
Wire Wire Line
	3800 2550 3950 2550
Wire Wire Line
	3500 2550 3450 2550
Wire Wire Line
	3450 2550 3450 2650
Wire Wire Line
	3450 2750 3500 2750
Wire Wire Line
	3450 2650 3200 2650
Connection ~ 3450 2650
Wire Wire Line
	3450 2650 3450 2750
Text Label 3200 2650 2    50   ~ 0
DAC_CPGND
Wire Wire Line
	3950 2550 3950 2150
Connection ~ 3950 2550
Connection ~ 3950 2750
Wire Wire Line
	3950 2750 3950 2550
Wire Wire Line
	3950 3050 3950 2950
$Comp
L Device:C C?
U 1 1 5D8C23D1
P 4450 2750
F 0 "C?" H 4500 2850 50  0000 L CNN
F 1 "0.1uF" V 4500 2450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4488 2600 50  0001 C CNN
F 3 "~" H 4450 2750 50  0001 C CNN
	1    4450 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 2750 4650 2750
$Comp
L Device:CP C?
U 1 1 5D8C23D9
P 4450 2550
F 0 "C?" V 4705 2550 50  0000 C CNN
F 1 "10uF" V 4614 2550 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4488 2400 50  0001 C CNN
F 3 "~" H 4450 2550 50  0001 C CNN
	1    4450 2550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4600 2550 4650 2550
Wire Wire Line
	4300 2550 4150 2550
Wire Wire Line
	4150 2750 4300 2750
Connection ~ 4150 2550
Wire Wire Line
	4150 2550 4150 2150
Connection ~ 4150 2750
Wire Wire Line
	4150 2750 4150 2550
Wire Wire Line
	4650 2550 4650 2650
Wire Wire Line
	4650 2650 4900 2650
Connection ~ 4650 2650
Wire Wire Line
	4650 2650 4650 2750
Text Label 4900 2650 0    50   ~ 0
DAC_AGND
Text Label 4150 2150 0    50   ~ 0
DAC_AVDD
Text Label 4800 4600 3    50   ~ 0
DAC_CPGND
$Comp
L Device:C C?
U 1 1 5D8C23EE
P 3650 1850
F 0 "C?" H 3700 1950 50  0000 L CNN
F 1 "0.1uF" V 3700 1550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3688 1700 50  0001 C CNN
F 3 "~" H 3650 1850 50  0001 C CNN
	1    3650 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	3800 1850 4050 1850
$Comp
L Device:CP C?
U 1 1 5D8C23F6
P 3650 1650
F 0 "C?" V 3905 1650 50  0000 C CNN
F 1 "10uF" V 3814 1650 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3688 1500 50  0001 C CNN
F 3 "~" H 3650 1650 50  0001 C CNN
	1    3650 1650
	0    1    -1   0   
$EndComp
Wire Wire Line
	3800 1650 4050 1650
Wire Wire Line
	3500 1650 3450 1650
Wire Wire Line
	3450 1850 3500 1850
Wire Wire Line
	4050 1450 4050 1650
Connection ~ 4050 1650
Wire Wire Line
	4050 1650 4050 1850
Connection ~ 4050 1850
Wire Wire Line
	4050 1850 4050 2950
Wire Wire Line
	3450 1650 3450 1750
Wire Wire Line
	3450 1750 3200 1750
Connection ~ 3450 1750
Wire Wire Line
	3450 1750 3450 1850
Text Label 4050 1450 0    50   ~ 0
DAC_DVDD
Text Label 3200 1750 2    50   ~ 0
DAC_DGND
Text Label 2700 3550 2    50   ~ 0
DAC_DGND
Text Notes 5050 1550 0    50   ~ 0
http://www.pavouk.org/hw/audiosystem20/en_pcm5102dac.html
Text Notes 5050 1450 0    50   ~ 0
http://www.ti.com/lit/ds/symlink/pcm5102a.pdf
Wire Notes Line
	5000 1300 7600 1300
Wire Notes Line
	7600 1300 7600 1600
Wire Notes Line
	7600 1600 5000 1600
Wire Notes Line
	5000 1600 5000 1300
Wire Wire Line
	4550 4050 5300 4050
$Comp
L Device:C C?
U 1 1 5D8C2413
P 5300 4300
F 0 "C?" H 5350 4400 50  0000 L CNN
F 1 "0.1uF" H 5350 4200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5338 4150 50  0001 C CNN
F 3 "~" H 5300 4300 50  0001 C CNN
	1    5300 4300
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C?
U 1 1 5D8C241A
P 5500 4300
F 0 "C?" H 5382 4346 50  0000 R CNN
F 1 "10uF" H 5382 4255 50  0000 R CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 5538 4150 50  0001 C CNN
F 3 "~" H 5500 4300 50  0001 C CNN
	1    5500 4300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5300 4050 5300 4150
Wire Wire Line
	5300 4050 5500 4050
Wire Wire Line
	5500 4050 5500 4150
Connection ~ 5300 4050
Wire Wire Line
	5300 4500 5400 4500
Wire Wire Line
	5400 4500 5400 4600
Connection ~ 5400 4500
Wire Wire Line
	5400 4500 5500 4500
Wire Wire Line
	5500 4450 5500 4500
Wire Wire Line
	5300 4450 5300 4500
Text Label 5400 4600 3    50   ~ 0
DAC_DGND
Wire Wire Line
	3950 2950 4050 2950
Connection ~ 3950 2950
Wire Wire Line
	3950 2950 3950 2750
Connection ~ 4050 2950
Wire Wire Line
	4050 2950 4050 3050
Wire Wire Line
	4050 2950 4150 2950
Connection ~ 4150 2950
Wire Wire Line
	4150 2950 4150 2750
Wire Wire Line
	4150 2950 4900 2950
Text Label 4900 2950 0    50   ~ 0
DAC_VDD
Text Label 4050 5600 3    50   ~ 0
DAC_GND
Wire Wire Line
	3550 3750 2000 3750
Wire Wire Line
	3550 3850 2000 3850
Wire Wire Line
	3550 3950 2000 3950
Wire Wire Line
	3550 4050 2000 4050
$Comp
L Device:R R?
U 1 1 5D8C243B
P 1850 3950
F 0 "R?" V 1900 3800 50  0000 C CNN
F 1 "10K" V 1900 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 3950 50  0001 C CNN
F 3 "~" H 1850 3950 50  0001 C CNN
	1    1850 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1700 3950 1500 3950
Text Label 1500 4050 2    50   ~ 0
DAC_GND
$Comp
L Device:R R?
U 1 1 5D8C2444
P 1850 3850
F 0 "R?" V 1900 3700 50  0000 C CNN
F 1 "10K" V 1900 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 3850 50  0001 C CNN
F 3 "~" H 1850 3850 50  0001 C CNN
	1    1850 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D8C244B
P 1850 3750
F 0 "R?" V 1900 3600 50  0000 C CNN
F 1 "10K" V 1900 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 3750 50  0001 C CNN
F 3 "~" H 1850 3750 50  0001 C CNN
	1    1850 3750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D8C2452
P 1850 4050
F 0 "R?" V 1900 3900 50  0000 C CNN
F 1 "10K" V 1900 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 4050 50  0001 C CNN
F 3 "~" H 1850 4050 50  0001 C CNN
	1    1850 4050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1700 3850 1500 3850
Wire Wire Line
	1700 3750 1500 3750
Wire Wire Line
	1700 4050 1500 4050
Text Label 1500 3850 2    50   ~ 0
DAC_GND
Text Label 1500 3750 2    50   ~ 0
DAC_GND
Text Label 1500 3950 2    50   ~ 0
DAC_DVDD
Text Label 2700 3750 2    50   ~ 0
DAC_IN_FLT
Text Label 2700 3850 2    50   ~ 0
DAC_IN_DEMP
Text Label 2700 3950 2    50   ~ 0
DAC_IN_XSMT
Text Label 2700 4050 2    50   ~ 0
DAC_IN_FMT
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 5D8C2463
P 3950 5250
F 0 "JP?" V 3904 5308 50  0000 L CNN
F 1 "Jumper_2_Bridged" V 3995 5308 50  0000 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_RoundedPad1.0x1.5mm" H 3950 5250 50  0001 C CNN
F 3 "~" H 3950 5250 50  0001 C CNN
	1    3950 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 4350 3950 5050
Wire Wire Line
	4050 4350 4050 5050
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 5D8C246C
P 4050 5250
F 0 "JP?" V 4004 5308 50  0000 L CNN
F 1 "Jumper_2_Bridged" V 4095 5308 50  0000 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_RoundedPad1.0x1.5mm" H 4050 5250 50  0001 C CNN
F 3 "~" H 4050 5250 50  0001 C CNN
	1    4050 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 4350 4150 5050
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 5D8C2474
P 4150 5250
F 0 "JP?" V 4104 5308 50  0000 L CNN
F 1 "Jumper_2_Bridged" V 4195 5308 50  0000 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_RoundedPad1.0x1.5mm" H 4150 5250 50  0001 C CNN
F 3 "~" H 4150 5250 50  0001 C CNN
	1    4150 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 5450 3950 5500
Wire Wire Line
	3950 5500 4050 5500
Wire Wire Line
	4050 5500 4050 5450
Wire Wire Line
	4050 5500 4150 5500
Wire Wire Line
	4150 5500 4150 5450
Connection ~ 4050 5500
Wire Wire Line
	4050 5600 4050 5500
$Comp
L Connector_Generic:Conn_01x10 J?
U 1 1 5D8C2482
P 2950 4800
F 0 "J?" H 2870 5417 50  0000 C CNN
F 1 "Conn_01x10" H 2870 5326 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 2950 4800 50  0001 C CNN
F 3 "~" H 2950 4800 50  0001 C CNN
	1    2950 4800
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J?
U 1 1 5D8C2489
P 5750 5100
F 0 "J?" H 5830 5092 50  0000 L CNN
F 1 "Conn_01x10" H 5830 5001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 5750 5100 50  0001 C CNN
F 3 "~" H 5750 5100 50  0001 C CNN
	1    5750 5100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
