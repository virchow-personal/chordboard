EESchema Schematic File Version 4
LIBS:chordboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5450 3850 0    50   ~ 0
ESP_TX0
Text Label 5450 3950 0    50   ~ 0
ESP_RX0
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 774925C8
P 3700 3000
F 0 "JP?" H 3700 3195 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 3700 3104 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_Pad1.0x1.5mm" H 3700 3000 50  0001 C CNN
F 3 "~" H 3700 3000 50  0001 C CNN
	1    3700 3000
	0    1    1    0   
$EndComp
$Comp
L esp32-wrover:ESP32-WROVER U?
U 1 1 5CB271F7
P 4600 4200
F 0 "U?" H 4575 5537 60  0000 C CNN
F 1 "ESP32-WROVER" H 4575 5431 60  0000 C CNN
F 2 "esp32-wrover:ESP32-WROVER" H 5050 3900 60  0001 C CNN
F 3 "" H 5050 3900 60  0001 C CNN
	1    4600 4200
	1    0    0    -1  
$EndComp
Text Label 5450 4850 0    50   ~ 0
ESP_D0
Text Label 3700 3650 2    50   ~ 0
ESP_RST
Text Label 3700 3550 2    50   ~ 0
ESP_3V3
Text Label 3700 3450 2    50   ~ 0
ESP_GND
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 774925DA
P 2950 3550
F 0 "JP?" H 2950 3745 50  0000 C CNN
F 1 "J_ESP_3V3" H 2950 3654 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_Pad1.0x1.5mm" H 2950 3550 50  0001 C CNN
F 3 "~" H 2950 3550 50  0001 C CNN
	1    2950 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3550 3700 3550
NoConn ~ 3700 5050
NoConn ~ 3700 5150
NoConn ~ 3700 5250
NoConn ~ 5450 5350
NoConn ~ 5450 5250
NoConn ~ 5450 5150
Wire Wire Line
	150  4350 3700 4350
Wire Wire Line
	150  4450 3700 4450
Wire Wire Line
	3700 4550 150  4550
Text Label 900  4950 0    50   ~ 0
ESP_GND
Text Label 150  5050 0    50   ~ 0
DAC_3V3
Text Label 5450 4050 0    50   ~ 0
ESP_SDA
Text Label 5450 3750 0    50   ~ 0
ESP_SCL
Text Label 5450 3550 0    50   ~ 0
ESP_GND
NoConn ~ 5450 3450
Wire Wire Line
	2650 3550 2750 3550
Wire Wire Line
	3700 3650 2200 3650
$Comp
L Device:R R?
U 1 1 5CB271F9
P 2200 3400
F 0 "R?" V 1993 3400 50  0000 C CNN
F 1 "R" V 2084 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 2130 3400 50  0001 C CNN
F 3 "~" H 2200 3400 50  0001 C CNN
	1    2200 3400
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 774925FA
P 5950 4600
F 0 "R?" V 5743 4600 50  0000 C CNN
F 1 "R" V 5834 4600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5880 4600 50  0001 C CNN
F 3 "~" H 5950 4600 50  0001 C CNN
	1    5950 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5450 4850 5950 4850
Text Label 2200 3250 2    50   ~ 0
ESP_3V3
Text Label 5950 4450 0    50   ~ 0
ESP_3V3
Wire Wire Line
	2200 3550 2200 3650
Wire Wire Line
	2200 3650 2100 3650
Connection ~ 2200 3650
Wire Wire Line
	5950 4750 5950 4850
Wire Wire Line
	5950 4850 6200 4850
Connection ~ 5950 4850
$Comp
L Device:R R?
U 1 1 7749260A
P 5900 4050
F 0 "R?" V 5693 4050 50  0000 C CNN
F 1 "R" V 5784 4050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5830 4050 50  0001 C CNN
F 3 "~" H 5900 4050 50  0001 C CNN
	1    5900 4050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5450 4050 5750 4050
Wire Wire Line
	6050 4050 6100 4050
Text Label 6100 4050 0    50   ~ 0
ESP_3V3
$Comp
L Device:R R?
U 1 1 77492614
P 5900 3750
F 0 "R?" V 5693 3750 50  0000 C CNN
F 1 "R" V 5784 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5830 3750 50  0001 C CNN
F 3 "~" H 5900 3750 50  0001 C CNN
	1    5900 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5450 3750 5750 3750
Text Label 6050 3750 0    50   ~ 0
ESP_3V3
Text Label 150  4950 0    50   ~ 0
DAC_GND
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 7749261E
P 700 4950
F 0 "JP?" H 700 5145 50  0000 C CNN
F 1 "J_ESP_3V3" H 700 5054 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_Pad1.0x1.5mm" H 700 4950 50  0001 C CNN
F 3 "~" H 700 4950 50  0001 C CNN
	1    700  4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	500  4950 150  4950
Text Label 3700 4850 2    50   ~ 0
ESP_GND
Text Label 150  4650 0    50   ~ 0
DAC_GND
Text Label 150  4750 0    50   ~ 0
DAC_GND
Text Label 150  4850 0    50   ~ 0
DAC_GND
Text Label 150  4250 0    50   ~ 0
DAC_GND
Text Label 150  4150 0    50   ~ 0
DAC_3V3
Text Label 3700 4750 2    50   ~ 0
ESP_RX2
Text Label 3700 4950 2    50   ~ 0
ESP_TX2
$Comp
L Device:C C?
U 1 1 7749262E
P 3300 3300
F 0 "C?" H 3415 3346 50  0000 L CNN
F 1 "C" H 3415 3255 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 3338 3150 50  0001 C CNN
F 3 "~" H 3300 3300 50  0001 C CNN
	1    3300 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3450 3300 3450
Text Label 3300 3150 0    50   ~ 0
ESP_3V3
$Comp
L chordboard-components:FTDI_MCU J?
U 1 1 77492637
P 5800 1900
F 0 "J?" H 6379 1900 50  0000 L CNN
F 1 "FTDI_MCU" H 6379 1809 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 5800 1900 50  0001 C CNN
F 3 "~" H 5800 1900 50  0001 C CNN
	1    5800 1900
	1    0    0    -1  
$EndComp
Text Label 5600 1700 2    50   ~ 0
ESP_GND
Text Label 5600 2100 2    50   ~ 0
ESP_TX0
Text Label 5600 2000 2    50   ~ 0
ESP_RX0
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 77492641
P -1300 5000
F 0 "J?" H -1380 5317 50  0000 C CNN
F 1 "Conn_01x03" H -1380 5226 50  0000 C CNN
F 2 "Connector_JST:JST_EH_B03B-EH-A_1x03_P2.50mm_Vertical" H -1300 5000 50  0001 C CNN
F 3 "~" H -1300 5000 50  0001 C CNN
	1    -1300 5000
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 77492648
P 3100 3300
F 0 "C?" H 3215 3346 50  0000 L CNN
F 1 "C" H 3215 3255 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 3138 3150 50  0001 C CNN
F 3 "~" H 3100 3300 50  0001 C CNN
	1    3100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 3450 3100 3450
Connection ~ 3300 3450
Wire Wire Line
	3100 3150 3300 3150
$Comp
L chordboard-components:GROVE_I2C J?
U 1 1 77492652
P 7950 1500
F 0 "J?" H 8277 1551 50  0000 L CNN
F 1 "GROVE_I2C" H 8277 1460 50  0000 L CNN
F 2 "Connector_JST:JST_PH_B4B-PH-K_1x04_P2.00mm_Vertical" H 7950 1500 50  0001 C CNN
F 3 "" H 7950 1500 50  0001 C CNN
	1    7950 1500
	1    0    0    -1  
$EndComp
Text Label 7750 1550 2    50   ~ 0
ESP_3V3
Text Label 7750 1650 2    50   ~ 0
ESP_GND
Text Label 7750 1350 2    50   ~ 0
ESP_SCL
Text Label 7750 1450 2    50   ~ 0
ESP_SDA
Text Label 150  5150 0    50   ~ 0
T_5V
$Comp
L chordboard-components:DMPCM5102 U?
U 1 1 5CB27203
P -400 4700
F 0 "U?" H -325 3863 60  0000 C CNN
F 1 "DMPCM5102" H -325 3969 60  0000 C CNN
F 2 "chordboard-footprints:DMPCM5102_BLACK" H -400 4700 60  0001 C CNN
F 3 "" H -400 4700 60  0001 C CNN
	1    -400 4700
	-1   0    0    1   
$EndComp
Text Label 5450 3650 0    50   ~ 0
ESP_MOSI
Text Label 5450 4450 0    50   ~ 0
ESP_CS
Text Label 5450 4350 0    50   ~ 0
ESP_SCK
Text Label 5450 4250 0    50   ~ 0
ESP_MISO
$Comp
L Connector:AVR-ISP-6 J?
U 1 1 77492669
P 10850 4950
F 0 "J?" H 10570 5046 50  0000 R CNN
F 1 "AVR-ISP-6" H 10570 4955 50  0000 R CNN
F 2 "Connector_IDC:IDC-Header_2x03_P2.54mm_Vertical" V 10600 5000 50  0001 C CNN
F 3 " ~" H 9575 4400 50  0001 C CNN
	1    10850 4950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9150 5050 10450 5050
Wire Wire Line
	9150 4650 10200 4650
Wire Wire Line
	10200 4650 10200 4750
Wire Wire Line
	10200 4750 10450 4750
Wire Wire Line
	9150 4550 9300 4550
Wire Wire Line
	10100 4550 10100 4850
Wire Wire Line
	10100 4850 10450 4850
Wire Wire Line
	10450 4950 10000 4950
Wire Wire Line
	10000 4950 10000 4750
Wire Wire Line
	10000 4750 9150 4750
Text Label 2650 3550 2    50   ~ 0
T_3V3
Text Label 8550 4250 0    50   ~ 0
Y_3V3
Text Label 8550 3800 0    50   ~ 0
T_3V3
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 7749267D
P 8550 4000
F 0 "JP?" H 8550 4195 50  0000 C CNN
F 1 "J_ESP_3V3" H 8550 4104 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_Pad1.0x1.5mm" H 8550 4000 50  0001 C CNN
F 3 "~" H 8550 4000 50  0001 C CNN
	1    8550 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	8550 4200 8550 4250
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20PU U?
U 1 1 77492685
P 8550 4850
F 0 "U?" H 8020 4896 50  0000 R CNN
F 1 "ATtiny85-20PU" H 8020 4805 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 8550 4850 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 8550 4850 50  0001 C CNN
	1    8550 4850
	1    0    0    -1  
$EndComp
Text Label 8550 5450 0    50   ~ 0
Y_GND
Wire Wire Line
	8550 5450 8550 5550
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 7749268E
P 8550 5750
F 0 "JP?" H 8550 5945 50  0000 C CNN
F 1 "J_ESP_3V3" H 8550 5854 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_Pad1.0x1.5mm" H 8550 5750 50  0001 C CNN
F 3 "~" H 8550 5750 50  0001 C CNN
	1    8550 5750
	0    1    1    0   
$EndComp
Text Label 3700 2800 2    50   ~ 0
T_GND
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 77492696
P 9350 4850
F 0 "JP?" H 9350 5045 50  0000 C CNN
F 1 "J_ESP_3V3" H 9350 4954 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_Pad1.0x1.5mm" H 9350 4850 50  0001 C CNN
F 3 "~" H 9350 4850 50  0001 C CNN
	1    9350 4850
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 5CB27209
P 9350 4950
F 0 "JP?" H 9350 5145 50  0000 C CNN
F 1 "J_ESP_3V3" H 9350 5054 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_Pad1.0x1.5mm" H 9350 4950 50  0001 C CNN
F 3 "~" H 9350 4950 50  0001 C CNN
	1    9350 4950
	1    0    0    -1  
$EndComp
Text Label 9550 4950 0    50   ~ 0
ESP_D0
Text Label 9550 4850 0    50   ~ 0
ESP_RST
Wire Wire Line
	9300 4550 9300 4300
Wire Wire Line
	9300 4300 9350 4300
Connection ~ 9300 4550
Wire Wire Line
	9300 4550 10100 4550
Text Label 9750 4300 0    50   ~ 0
Y_GND
Text Label 8550 5950 0    50   ~ 0
T_GND
Text Label 10950 5350 0    50   ~ 0
Y_GND
$Comp
L Switch:SW_DIP_x04 SW?
U 1 1 774926AD
P 9450 2800
F 0 "SW?" H 9450 3267 50  0000 C CNN
F 1 "SW_DIP_x04" H 9450 3176 50  0000 C CNN
F 2 "Button_Switch_THT:SW_DIP_SPSTx04_Slide_9.78x12.34mm_W7.62mm_P2.54mm" H 9450 2800 50  0001 C CNN
F 3 "" H 9450 2800 50  0001 C CNN
	1    9450 2800
	1    0    0    -1  
$EndComp
Text Label 9150 2600 2    50   ~ 0
ESP_RST
Text Label 9150 2700 2    50   ~ 0
ESP_D0
Text Label 9150 2900 2    50   ~ 0
ESP_RX2
Text Label 9150 2800 2    50   ~ 0
ESP_TX2
Text Label 9750 2900 0    50   ~ 0
T_TX4
Text Label 9750 2800 0    50   ~ 0
T_RX4
Text Label 9750 2600 0    50   ~ 0
ESP_GND
Text Label 9750 2700 0    50   ~ 0
ESP_GND
$Comp
L chordboard-components:FTDI_MCU J?
U 1 1 774926BC
P 5800 950
F 0 "J?" H 6379 950 50  0000 L CNN
F 1 "FTDI_MCU" H 6379 859 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 5800 950 50  0001 C CNN
F 3 "~" H 5800 950 50  0001 C CNN
	1    5800 950 
	1    0    0    -1  
$EndComp
Text Label 5600 750  2    50   ~ 0
ESP_GND
Text Label 5600 1150 2    50   ~ 0
ESP_TX2
Text Label 5600 1050 2    50   ~ 0
ESP_RX0
Wire Wire Line
	3700 3200 3700 3450
Connection ~ 3700 3450
$Comp
L chordboard-components:R R?
U 1 1 5CB2720C
P 3500 4650
F 0 "R?" V 3304 4650 50  0000 C CNN
F 1 "R" V 3395 4650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 3500 4650 50  0001 C CNN
F 3 "" H 3500 4650 50  0001 C CNN
	1    3500 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 4650 2900 4650
$Comp
L Device:LED HLED?
U 1 1 5CB2720D
P 2750 4650
F 0 "HLED?" H 2950 4600 50  0000 C CNN
F 1 "LED" H 2900 4700 50  0000 C CNN
F 2 "LED_THT:LED_D1.8mm_W3.3mm_H2.4mm" H 2750 4650 50  0001 C CNN
F 3 "~" H 2750 4650 50  0001 C CNN
	1    2750 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4650 2500 4650
Wire Wire Line
	3600 4650 3700 4650
Text Label 2500 4650 2    50   ~ 0
ESP_GND
$Comp
L Switch:SW_Push RSW?
U 1 1 774926DA
P 9550 4300
F 0 "RSW?" H 9350 4350 50  0000 C CNN
F 1 "SW_Push" H 9850 4350 50  0000 C CNN
F 2 "Button_Switch_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 9550 4500 50  0001 C CNN
F 3 "" H 9550 4500 50  0001 C CNN
	1    9550 4300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
