EESchema Schematic File Version 4
LIBS:chordboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C?
U 1 1 5C30A74C
P 1550 2900
F 0 "C?" H 1665 2946 50  0000 L CNN
F 1 "C" H 1665 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 1588 2750 50  0001 C CNN
F 3 "~" H 1550 2900 50  0001 C CNN
	1    1550 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5C30A753
P 1350 2900
F 0 "C?" H 1465 2946 50  0000 L CNN
F 1 "C" H 1465 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 1388 2750 50  0001 C CNN
F 3 "~" H 1350 2900 50  0001 C CNN
	1    1350 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 3050 1350 3100
Wire Wire Line
	1350 3100 1450 3100
Wire Wire Line
	1550 3100 1550 3050
Wire Wire Line
	1450 3100 1450 3250
Connection ~ 1450 3100
Wire Wire Line
	1450 3100 1550 3100
Text Label 1350 2700 2    50   ~ 0
RE1A
Text Label 1550 2700 0    50   ~ 0
RE1B
Text Label 2000 2700 2    50   ~ 0
RE2A
Text Label 2200 2700 0    50   ~ 0
RE2B
Wire Wire Line
	1450 2700 1450 3100
Wire Wire Line
	1350 2700 1350 2750
Wire Wire Line
	1550 2700 1550 2750
$Comp
L Device:C C?
U 1 1 5C30A767
P 2200 2900
F 0 "C?" H 2315 2946 50  0000 L CNN
F 1 "C" H 2315 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 2238 2750 50  0001 C CNN
F 3 "~" H 2200 2900 50  0001 C CNN
	1    2200 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5C30A76E
P 2000 2900
F 0 "C?" H 2115 2946 50  0000 L CNN
F 1 "C" H 2115 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 2038 2750 50  0001 C CNN
F 3 "~" H 2000 2900 50  0001 C CNN
	1    2000 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 3050 2000 3100
Wire Wire Line
	2000 3100 2100 3100
Wire Wire Line
	2200 3100 2200 3050
Wire Wire Line
	2100 3100 2100 3250
Connection ~ 2100 3100
Wire Wire Line
	2100 3100 2200 3100
Wire Wire Line
	2100 2700 2100 3100
Wire Wire Line
	2000 2700 2000 2750
Wire Wire Line
	2200 2700 2200 2750
$Comp
L Device:Rotary_Encoder RE?
U 1 1 5C30A77E
P 1450 2400
F 0 "RE?" V 1441 2170 50  0000 R CNN
F 1 "Rotary_Encoder" V 1350 2170 50  0000 R CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E_Vertical_H20mm" H 1300 2560 50  0001 C CNN
F 3 "~" H 1450 2660 50  0001 C CNN
	1    1450 2400
	0    -1   -1   0   
$EndComp
$Comp
L Device:Rotary_Encoder RE?
U 1 1 5C30A785
P 2100 2400
F 0 "RE?" V 2091 2170 50  0000 R CNN
F 1 "Rotary_Encoder" V 2000 2170 50  0000 R CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E_Vertical_H20mm" H 1950 2560 50  0001 C CNN
F 3 "~" H 2100 2660 50  0001 C CNN
	1    2100 2400
	0    -1   -1   0   
$EndComp
Text Label 1800 3300 3    50   ~ 0
LC_GND
Wire Wire Line
	1450 3250 1800 3250
Wire Wire Line
	1800 3300 1800 3250
Connection ~ 1800 3250
Wire Wire Line
	1800 3250 2100 3250
$Comp
L Device:C C?
U 1 1 5C30A791
P 2850 2900
F 0 "C?" H 2965 2946 50  0000 L CNN
F 1 "C" H 2965 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 2888 2750 50  0001 C CNN
F 3 "~" H 2850 2900 50  0001 C CNN
	1    2850 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5C30A798
P 2650 2900
F 0 "C?" H 2765 2946 50  0000 L CNN
F 1 "C" H 2765 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 2688 2750 50  0001 C CNN
F 3 "~" H 2650 2900 50  0001 C CNN
	1    2650 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3050 2650 3100
Wire Wire Line
	2650 3100 2750 3100
Wire Wire Line
	2850 3100 2850 3050
Wire Wire Line
	2750 3100 2750 3250
Connection ~ 2750 3100
Wire Wire Line
	2750 3100 2850 3100
Text Label 2650 2700 2    50   ~ 0
RE3A
Text Label 2850 2700 0    50   ~ 0
RE3B
Text Label 3300 2700 2    50   ~ 0
RE4A
Text Label 3500 2700 0    50   ~ 0
RE4B
Wire Wire Line
	2750 2700 2750 3100
Wire Wire Line
	2650 2700 2650 2750
Wire Wire Line
	2850 2700 2850 2750
$Comp
L Device:C C?
U 1 1 5C30A7AC
P 3500 2900
F 0 "C?" H 3615 2946 50  0000 L CNN
F 1 "C" H 3615 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 3538 2750 50  0001 C CNN
F 3 "~" H 3500 2900 50  0001 C CNN
	1    3500 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5C30A7B3
P 3300 2900
F 0 "C?" H 3415 2946 50  0000 L CNN
F 1 "C" H 3415 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 3338 2750 50  0001 C CNN
F 3 "~" H 3300 2900 50  0001 C CNN
	1    3300 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 3050 3300 3100
Wire Wire Line
	3300 3100 3400 3100
Wire Wire Line
	3500 3100 3500 3050
Wire Wire Line
	3400 3100 3400 3250
Connection ~ 3400 3100
Wire Wire Line
	3400 3100 3500 3100
Wire Wire Line
	3400 2700 3400 3100
Wire Wire Line
	3300 2700 3300 2750
Wire Wire Line
	3500 2700 3500 2750
$Comp
L Device:Rotary_Encoder RE?
U 1 1 5C30A7C3
P 2750 2400
F 0 "RE?" V 2741 2170 50  0000 R CNN
F 1 "Rotary_Encoder" V 2650 2170 50  0000 R CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E_Vertical_H20mm" H 2600 2560 50  0001 C CNN
F 3 "~" H 2750 2660 50  0001 C CNN
	1    2750 2400
	0    -1   -1   0   
$EndComp
$Comp
L Device:Rotary_Encoder RE?
U 1 1 5C30A7CA
P 3400 2400
F 0 "RE?" V 3391 2170 50  0000 R CNN
F 1 "Rotary_Encoder" V 3300 2170 50  0000 R CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E_Vertical_H20mm" H 3250 2560 50  0001 C CNN
F 3 "~" H 3400 2660 50  0001 C CNN
	1    3400 2400
	0    -1   -1   0   
$EndComp
Text Label 3100 3300 3    50   ~ 0
LC_GND
Wire Wire Line
	2750 3250 3100 3250
Wire Wire Line
	3100 3300 3100 3250
Connection ~ 3100 3250
Wire Wire Line
	3100 3250 3400 3250
$Comp
L Device:C C?
U 1 1 5C30A7D6
P 4150 2900
F 0 "C?" H 4265 2946 50  0000 L CNN
F 1 "C" H 4265 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4188 2750 50  0001 C CNN
F 3 "~" H 4150 2900 50  0001 C CNN
	1    4150 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5C30A7DD
P 3950 2900
F 0 "C?" H 4065 2946 50  0000 L CNN
F 1 "C" H 4065 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 3988 2750 50  0001 C CNN
F 3 "~" H 3950 2900 50  0001 C CNN
	1    3950 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3050 3950 3100
Wire Wire Line
	3950 3100 4050 3100
Wire Wire Line
	4150 3100 4150 3050
Wire Wire Line
	4050 3100 4050 3250
Connection ~ 4050 3100
Wire Wire Line
	4050 3100 4150 3100
Text Label 3950 2700 2    50   ~ 0
RE5A
Text Label 4150 2700 0    50   ~ 0
RE5B
Text Label 4600 2700 2    50   ~ 0
RE6A
Text Label 4800 2700 0    50   ~ 0
RE6B
Wire Wire Line
	4050 2700 4050 3100
Wire Wire Line
	3950 2700 3950 2750
Wire Wire Line
	4150 2700 4150 2750
$Comp
L Device:C C?
U 1 1 5C30A7F1
P 4800 2900
F 0 "C?" H 4915 2946 50  0000 L CNN
F 1 "C" H 4915 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4838 2750 50  0001 C CNN
F 3 "~" H 4800 2900 50  0001 C CNN
	1    4800 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5C30A7F8
P 4600 2900
F 0 "C?" H 4715 2946 50  0000 L CNN
F 1 "C" H 4715 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4638 2750 50  0001 C CNN
F 3 "~" H 4600 2900 50  0001 C CNN
	1    4600 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3050 4600 3100
Wire Wire Line
	4600 3100 4700 3100
Wire Wire Line
	4800 3100 4800 3050
Wire Wire Line
	4700 3100 4700 3250
Connection ~ 4700 3100
Wire Wire Line
	4700 3100 4800 3100
Wire Wire Line
	4700 2700 4700 3100
Wire Wire Line
	4600 2700 4600 2750
Wire Wire Line
	4800 2700 4800 2750
$Comp
L Device:Rotary_Encoder RE?
U 1 1 5C30A808
P 4050 2400
F 0 "RE?" V 4041 2170 50  0000 R CNN
F 1 "Rotary_Encoder" V 3950 2170 50  0000 R CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E_Vertical_H20mm" H 3900 2560 50  0001 C CNN
F 3 "~" H 4050 2660 50  0001 C CNN
	1    4050 2400
	0    -1   -1   0   
$EndComp
$Comp
L Device:Rotary_Encoder RE?
U 1 1 5C30A80F
P 4700 2400
F 0 "RE?" V 4691 2170 50  0000 R CNN
F 1 "Rotary_Encoder" V 4600 2170 50  0000 R CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E_Vertical_H20mm" H 4550 2560 50  0001 C CNN
F 3 "~" H 4700 2660 50  0001 C CNN
	1    4700 2400
	0    -1   -1   0   
$EndComp
Text Label 4400 3300 3    50   ~ 0
LC_GND
Wire Wire Line
	4050 3250 4400 3250
Wire Wire Line
	4400 3300 4400 3250
Connection ~ 4400 3250
Wire Wire Line
	4400 3250 4700 3250
$Comp
L Device:C C?
U 1 1 5C30A81B
P 5450 2900
F 0 "C?" H 5565 2946 50  0000 L CNN
F 1 "C" H 5565 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 5488 2750 50  0001 C CNN
F 3 "~" H 5450 2900 50  0001 C CNN
	1    5450 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5C30A822
P 5250 2900
F 0 "C?" H 5365 2946 50  0000 L CNN
F 1 "C" H 5365 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 5288 2750 50  0001 C CNN
F 3 "~" H 5250 2900 50  0001 C CNN
	1    5250 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3050 5250 3100
Wire Wire Line
	5250 3100 5350 3100
Wire Wire Line
	5450 3100 5450 3050
Wire Wire Line
	5350 3100 5350 3250
Connection ~ 5350 3100
Wire Wire Line
	5350 3100 5450 3100
Text Label 5250 2700 2    50   ~ 0
RE7A
Text Label 5450 2700 0    50   ~ 0
RE7B
Text Label 5900 2700 2    50   ~ 0
RE8A
Text Label 6100 2700 0    50   ~ 0
RE8B
Wire Wire Line
	5350 2700 5350 3100
Wire Wire Line
	5250 2700 5250 2750
Wire Wire Line
	5450 2700 5450 2750
$Comp
L Device:C C?
U 1 1 5C30A836
P 6100 2900
F 0 "C?" H 6215 2946 50  0000 L CNN
F 1 "C" H 6215 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 6138 2750 50  0001 C CNN
F 3 "~" H 6100 2900 50  0001 C CNN
	1    6100 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5C30A83D
P 5900 2900
F 0 "C?" H 6015 2946 50  0000 L CNN
F 1 "C" H 6015 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 5938 2750 50  0001 C CNN
F 3 "~" H 5900 2900 50  0001 C CNN
	1    5900 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3050 5900 3100
Wire Wire Line
	5900 3100 6000 3100
Wire Wire Line
	6100 3100 6100 3050
Wire Wire Line
	6000 3100 6000 3250
Connection ~ 6000 3100
Wire Wire Line
	6000 3100 6100 3100
Wire Wire Line
	6000 2700 6000 3100
Wire Wire Line
	5900 2700 5900 2750
Wire Wire Line
	6100 2700 6100 2750
$Comp
L Device:Rotary_Encoder RE?
U 1 1 5C30A84D
P 5350 2400
F 0 "RE?" V 5341 2170 50  0000 R CNN
F 1 "Rotary_Encoder" V 5250 2170 50  0000 R CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E_Vertical_H20mm" H 5200 2560 50  0001 C CNN
F 3 "~" H 5350 2660 50  0001 C CNN
	1    5350 2400
	0    -1   -1   0   
$EndComp
$Comp
L Device:Rotary_Encoder RE?
U 1 1 5C30A854
P 6000 2400
F 0 "RE?" V 5991 2170 50  0000 R CNN
F 1 "Rotary_Encoder" V 5900 2170 50  0000 R CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E_Vertical_H20mm" H 5850 2560 50  0001 C CNN
F 3 "~" H 6000 2660 50  0001 C CNN
	1    6000 2400
	0    -1   -1   0   
$EndComp
Text Label 5700 3300 3    50   ~ 0
LC_GND
Wire Wire Line
	5350 3250 5700 3250
Wire Wire Line
	5700 3300 5700 3250
Connection ~ 5700 3250
Wire Wire Line
	5700 3250 6000 3250
Text Label 1450 4300 2    50   ~ 0
RE1B
Text Label 1450 5200 2    50   ~ 0
RE1A
Text Label 1450 5300 2    50   ~ 0
RE2A
Text Label 1450 4400 2    50   ~ 0
RE2B
Text Label 1450 5400 2    50   ~ 0
RE3A
Text Label 1450 4500 2    50   ~ 0
RE3B
Text Label 1450 5500 2    50   ~ 0
RE4A
Text Label 1450 4600 2    50   ~ 0
RE4B
Text Label 1450 5600 2    50   ~ 0
RE5A
Text Label 1450 4700 2    50   ~ 0
RE5B
Text Label 1450 5700 2    50   ~ 0
RE6A
Text Label 1450 4800 2    50   ~ 0
RE6B
Text Label 1450 5800 2    50   ~ 0
RE7A
Text Label 1450 4900 2    50   ~ 0
RE7B
Text Label 1450 5900 2    50   ~ 0
RE8A
Text Label 1450 5000 2    50   ~ 0
RE8B
$EndSCHEMATC
