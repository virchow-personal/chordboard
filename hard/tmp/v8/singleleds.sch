EESchema Schematic File Version 4
LIBS:chordboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AB5B
P 3000 2650
F 0 "LED?" H 3250 2600 60  0000 R CNN
F 1 "LED" H 3050 2700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3000 2650 60  0001 C CNN
F 3 "" H 3000 2650 60  0001 C CNN
	1    3000 2650
	1    0    0    -1  
$EndComp
Text Label 2750 2750 2    60   ~ 0
X0
Text Label 2750 4150 2    60   ~ 0
X7
Text Label 2750 3950 2    60   ~ 0
X6
Text Label 2750 3750 2    60   ~ 0
X5
Text Label 2750 3550 2    60   ~ 0
X4
Text Label 2750 3350 2    60   ~ 0
X3
Text Label 2750 3150 2    60   ~ 0
X2
Text Label 2750 2950 2    60   ~ 0
X1
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AB6A
P 3500 2650
F 0 "LED?" H 3750 2600 60  0000 R CNN
F 1 "LED" H 3550 2700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3500 2650 60  0001 C CNN
F 3 "" H 3500 2650 60  0001 C CNN
	1    3500 2650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AB71
P 4000 2650
F 0 "LED?" H 4250 2600 60  0000 R CNN
F 1 "LED" H 4050 2700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4000 2650 60  0001 C CNN
F 3 "" H 4000 2650 60  0001 C CNN
	1    4000 2650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AB78
P 4500 2650
F 0 "LED?" H 4750 2600 60  0000 R CNN
F 1 "LED" H 4550 2700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4500 2650 60  0001 C CNN
F 3 "" H 4500 2650 60  0001 C CNN
	1    4500 2650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AB7F
P 5000 2650
F 0 "LED?" H 5250 2600 60  0000 R CNN
F 1 "LED" H 5050 2700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5000 2650 60  0001 C CNN
F 3 "" H 5000 2650 60  0001 C CNN
	1    5000 2650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AB86
P 5500 2650
F 0 "LED?" H 5750 2600 60  0000 R CNN
F 1 "LED" H 5550 2700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5500 2650 60  0001 C CNN
F 3 "" H 5500 2650 60  0001 C CNN
	1    5500 2650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AB8D
P 6000 2650
F 0 "LED?" H 6250 2600 60  0000 R CNN
F 1 "LED" H 6050 2700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6000 2650 60  0001 C CNN
F 3 "" H 6000 2650 60  0001 C CNN
	1    6000 2650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AB94
P 6500 2650
F 0 "LED?" H 6750 2600 60  0000 R CNN
F 1 "LED" H 6550 2700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6500 2650 60  0001 C CNN
F 3 "" H 6500 2650 60  0001 C CNN
	1    6500 2650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AB9B
P 3000 2850
F 0 "LED?" H 3250 2800 60  0000 R CNN
F 1 "LED" H 3050 2900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3000 2850 60  0001 C CNN
F 3 "" H 3000 2850 60  0001 C CNN
	1    3000 2850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABA2
P 3500 2850
F 0 "LED?" H 3750 2800 60  0000 R CNN
F 1 "LED" H 3550 2900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3500 2850 60  0001 C CNN
F 3 "" H 3500 2850 60  0001 C CNN
	1    3500 2850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABA9
P 4000 2850
F 0 "LED?" H 4250 2800 60  0000 R CNN
F 1 "LED" H 4050 2900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4000 2850 60  0001 C CNN
F 3 "" H 4000 2850 60  0001 C CNN
	1    4000 2850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABB0
P 4500 2850
F 0 "LED?" H 4750 2800 60  0000 R CNN
F 1 "LED" H 4550 2900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4500 2850 60  0001 C CNN
F 3 "" H 4500 2850 60  0001 C CNN
	1    4500 2850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABB7
P 5000 2850
F 0 "LED?" H 5250 2800 60  0000 R CNN
F 1 "LED" H 5050 2900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5000 2850 60  0001 C CNN
F 3 "" H 5000 2850 60  0001 C CNN
	1    5000 2850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABBE
P 5500 2850
F 0 "LED?" H 5750 2800 60  0000 R CNN
F 1 "LED" H 5550 2900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5500 2850 60  0001 C CNN
F 3 "" H 5500 2850 60  0001 C CNN
	1    5500 2850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABC5
P 6000 2850
F 0 "LED?" H 6250 2800 60  0000 R CNN
F 1 "LED" H 6050 2900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6000 2850 60  0001 C CNN
F 3 "" H 6000 2850 60  0001 C CNN
	1    6000 2850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABCC
P 6500 2850
F 0 "LED?" H 6750 2800 60  0000 R CNN
F 1 "LED" H 6550 2900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6500 2850 60  0001 C CNN
F 3 "" H 6500 2850 60  0001 C CNN
	1    6500 2850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABD3
P 3000 3050
F 0 "LED?" H 3250 3000 60  0000 R CNN
F 1 "LED" H 3050 3100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3000 3050 60  0001 C CNN
F 3 "" H 3000 3050 60  0001 C CNN
	1    3000 3050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABDA
P 3500 3050
F 0 "LED?" H 3750 3000 60  0000 R CNN
F 1 "LED" H 3550 3100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3500 3050 60  0001 C CNN
F 3 "" H 3500 3050 60  0001 C CNN
	1    3500 3050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABE1
P 4000 3050
F 0 "LED?" H 4250 3000 60  0000 R CNN
F 1 "LED" H 4050 3100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4000 3050 60  0001 C CNN
F 3 "" H 4000 3050 60  0001 C CNN
	1    4000 3050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABE8
P 4500 3050
F 0 "LED?" H 4750 3000 60  0000 R CNN
F 1 "LED" H 4550 3100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4500 3050 60  0001 C CNN
F 3 "" H 4500 3050 60  0001 C CNN
	1    4500 3050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABEF
P 5000 3050
F 0 "LED?" H 5250 3000 60  0000 R CNN
F 1 "LED" H 5050 3100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5000 3050 60  0001 C CNN
F 3 "" H 5000 3050 60  0001 C CNN
	1    5000 3050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABF6
P 5500 3050
F 0 "LED?" H 5750 3000 60  0000 R CNN
F 1 "LED" H 5550 3100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5500 3050 60  0001 C CNN
F 3 "" H 5500 3050 60  0001 C CNN
	1    5500 3050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ABFD
P 6000 3050
F 0 "LED?" H 6250 3000 60  0000 R CNN
F 1 "LED" H 6050 3100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6000 3050 60  0001 C CNN
F 3 "" H 6000 3050 60  0001 C CNN
	1    6000 3050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC04
P 6500 3050
F 0 "LED?" H 6750 3000 60  0000 R CNN
F 1 "LED" H 6550 3100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6500 3050 60  0001 C CNN
F 3 "" H 6500 3050 60  0001 C CNN
	1    6500 3050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC0B
P 3000 3250
F 0 "LED?" H 3250 3200 60  0000 R CNN
F 1 "LED" H 3050 3300 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3000 3250 60  0001 C CNN
F 3 "" H 3000 3250 60  0001 C CNN
	1    3000 3250
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC12
P 3500 3250
F 0 "LED?" H 3750 3200 60  0000 R CNN
F 1 "LED" H 3550 3300 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3500 3250 60  0001 C CNN
F 3 "" H 3500 3250 60  0001 C CNN
	1    3500 3250
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC19
P 4000 3250
F 0 "LED?" H 4250 3200 60  0000 R CNN
F 1 "LED" H 4050 3300 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4000 3250 60  0001 C CNN
F 3 "" H 4000 3250 60  0001 C CNN
	1    4000 3250
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC20
P 4500 3250
F 0 "LED?" H 4750 3200 60  0000 R CNN
F 1 "LED" H 4550 3300 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4500 3250 60  0001 C CNN
F 3 "" H 4500 3250 60  0001 C CNN
	1    4500 3250
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC27
P 5000 3250
F 0 "LED?" H 5250 3200 60  0000 R CNN
F 1 "LED" H 5050 3300 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5000 3250 60  0001 C CNN
F 3 "" H 5000 3250 60  0001 C CNN
	1    5000 3250
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC2E
P 5500 3250
F 0 "LED?" H 5750 3200 60  0000 R CNN
F 1 "LED" H 5550 3300 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5500 3250 60  0001 C CNN
F 3 "" H 5500 3250 60  0001 C CNN
	1    5500 3250
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC35
P 6000 3250
F 0 "LED?" H 6250 3200 60  0000 R CNN
F 1 "LED" H 6050 3300 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6000 3250 60  0001 C CNN
F 3 "" H 6000 3250 60  0001 C CNN
	1    6000 3250
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC3C
P 6500 3250
F 0 "LED?" H 6750 3200 60  0000 R CNN
F 1 "LED" H 6550 3300 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6500 3250 60  0001 C CNN
F 3 "" H 6500 3250 60  0001 C CNN
	1    6500 3250
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC43
P 3000 3450
F 0 "LED?" H 3250 3400 60  0000 R CNN
F 1 "LED" H 3050 3500 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3000 3450 60  0001 C CNN
F 3 "" H 3000 3450 60  0001 C CNN
	1    3000 3450
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC4A
P 3500 3450
F 0 "LED?" H 3750 3400 60  0000 R CNN
F 1 "LED" H 3550 3500 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3500 3450 60  0001 C CNN
F 3 "" H 3500 3450 60  0001 C CNN
	1    3500 3450
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC51
P 4000 3450
F 0 "LED?" H 4250 3400 60  0000 R CNN
F 1 "LED" H 4050 3500 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4000 3450 60  0001 C CNN
F 3 "" H 4000 3450 60  0001 C CNN
	1    4000 3450
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC58
P 4500 3450
F 0 "LED?" H 4750 3400 60  0000 R CNN
F 1 "LED" H 4550 3500 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4500 3450 60  0001 C CNN
F 3 "" H 4500 3450 60  0001 C CNN
	1    4500 3450
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC5F
P 5000 3450
F 0 "LED?" H 5250 3400 60  0000 R CNN
F 1 "LED" H 5050 3500 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5000 3450 60  0001 C CNN
F 3 "" H 5000 3450 60  0001 C CNN
	1    5000 3450
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC66
P 5500 3450
F 0 "LED?" H 5750 3400 60  0000 R CNN
F 1 "LED" H 5550 3500 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5500 3450 60  0001 C CNN
F 3 "" H 5500 3450 60  0001 C CNN
	1    5500 3450
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC6D
P 6000 3450
F 0 "LED?" H 6250 3400 60  0000 R CNN
F 1 "LED" H 6050 3500 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6000 3450 60  0001 C CNN
F 3 "" H 6000 3450 60  0001 C CNN
	1    6000 3450
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC74
P 6500 3450
F 0 "LED?" H 6750 3400 60  0000 R CNN
F 1 "LED" H 6550 3500 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6500 3450 60  0001 C CNN
F 3 "" H 6500 3450 60  0001 C CNN
	1    6500 3450
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC7B
P 3000 3650
F 0 "LED?" H 3250 3600 60  0000 R CNN
F 1 "LED" H 3050 3700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3000 3650 60  0001 C CNN
F 3 "" H 3000 3650 60  0001 C CNN
	1    3000 3650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC82
P 3500 3650
F 0 "LED?" H 3750 3600 60  0000 R CNN
F 1 "LED" H 3550 3700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3500 3650 60  0001 C CNN
F 3 "" H 3500 3650 60  0001 C CNN
	1    3500 3650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC89
P 4000 3650
F 0 "LED?" H 4250 3600 60  0000 R CNN
F 1 "LED" H 4050 3700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4000 3650 60  0001 C CNN
F 3 "" H 4000 3650 60  0001 C CNN
	1    4000 3650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC90
P 4500 3650
F 0 "LED?" H 4750 3600 60  0000 R CNN
F 1 "LED" H 4550 3700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4500 3650 60  0001 C CNN
F 3 "" H 4500 3650 60  0001 C CNN
	1    4500 3650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC97
P 5000 3650
F 0 "LED?" H 5250 3600 60  0000 R CNN
F 1 "LED" H 5050 3700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5000 3650 60  0001 C CNN
F 3 "" H 5000 3650 60  0001 C CNN
	1    5000 3650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AC9E
P 5500 3650
F 0 "LED?" H 5750 3600 60  0000 R CNN
F 1 "LED" H 5550 3700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5500 3650 60  0001 C CNN
F 3 "" H 5500 3650 60  0001 C CNN
	1    5500 3650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACA5
P 6000 3650
F 0 "LED?" H 6250 3600 60  0000 R CNN
F 1 "LED" H 6050 3700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6000 3650 60  0001 C CNN
F 3 "" H 6000 3650 60  0001 C CNN
	1    6000 3650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACAC
P 6500 3650
F 0 "LED?" H 6750 3600 60  0000 R CNN
F 1 "LED" H 6550 3700 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6500 3650 60  0001 C CNN
F 3 "" H 6500 3650 60  0001 C CNN
	1    6500 3650
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACB3
P 3000 3850
F 0 "LED?" H 3250 3800 60  0000 R CNN
F 1 "LED" H 3050 3900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3000 3850 60  0001 C CNN
F 3 "" H 3000 3850 60  0001 C CNN
	1    3000 3850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACBA
P 3500 3850
F 0 "LED?" H 3750 3800 60  0000 R CNN
F 1 "LED" H 3550 3900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3500 3850 60  0001 C CNN
F 3 "" H 3500 3850 60  0001 C CNN
	1    3500 3850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACC1
P 4000 3850
F 0 "LED?" H 4250 3800 60  0000 R CNN
F 1 "LED" H 4050 3900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4000 3850 60  0001 C CNN
F 3 "" H 4000 3850 60  0001 C CNN
	1    4000 3850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACC8
P 4500 3850
F 0 "LED?" H 4750 3800 60  0000 R CNN
F 1 "LED" H 4550 3900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4500 3850 60  0001 C CNN
F 3 "" H 4500 3850 60  0001 C CNN
	1    4500 3850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACCF
P 5000 3850
F 0 "LED?" H 5250 3800 60  0000 R CNN
F 1 "LED" H 5050 3900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5000 3850 60  0001 C CNN
F 3 "" H 5000 3850 60  0001 C CNN
	1    5000 3850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACD6
P 5500 3850
F 0 "LED?" H 5750 3800 60  0000 R CNN
F 1 "LED" H 5550 3900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5500 3850 60  0001 C CNN
F 3 "" H 5500 3850 60  0001 C CNN
	1    5500 3850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACDD
P 6000 3850
F 0 "LED?" H 6250 3800 60  0000 R CNN
F 1 "LED" H 6050 3900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6000 3850 60  0001 C CNN
F 3 "" H 6000 3850 60  0001 C CNN
	1    6000 3850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACE4
P 6500 3850
F 0 "LED?" H 6750 3800 60  0000 R CNN
F 1 "LED" H 6550 3900 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6500 3850 60  0001 C CNN
F 3 "" H 6500 3850 60  0001 C CNN
	1    6500 3850
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACEB
P 3000 4050
F 0 "LED?" H 3250 4000 60  0000 R CNN
F 1 "LED" H 3050 4100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3000 4050 60  0001 C CNN
F 3 "" H 3000 4050 60  0001 C CNN
	1    3000 4050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACF2
P 3500 4050
F 0 "LED?" H 3750 4000 60  0000 R CNN
F 1 "LED" H 3550 4100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 3500 4050 60  0001 C CNN
F 3 "" H 3500 4050 60  0001 C CNN
	1    3500 4050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34ACF9
P 4000 4050
F 0 "LED?" H 4250 4000 60  0000 R CNN
F 1 "LED" H 4050 4100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4000 4050 60  0001 C CNN
F 3 "" H 4000 4050 60  0001 C CNN
	1    4000 4050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AD00
P 4500 4050
F 0 "LED?" H 4750 4000 60  0000 R CNN
F 1 "LED" H 4550 4100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 4500 4050 60  0001 C CNN
F 3 "" H 4500 4050 60  0001 C CNN
	1    4500 4050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AD07
P 5000 4050
F 0 "LED?" H 5250 4000 60  0000 R CNN
F 1 "LED" H 5050 4100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5000 4050 60  0001 C CNN
F 3 "" H 5000 4050 60  0001 C CNN
	1    5000 4050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AD0E
P 5500 4050
F 0 "LED?" H 5750 4000 60  0000 R CNN
F 1 "LED" H 5550 4100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 5500 4050 60  0001 C CNN
F 3 "" H 5500 4050 60  0001 C CNN
	1    5500 4050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AD15
P 6000 4050
F 0 "LED?" H 6250 4000 60  0000 R CNN
F 1 "LED" H 6050 4100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6000 4050 60  0001 C CNN
F 3 "" H 6000 4050 60  0001 C CNN
	1    6000 4050
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:LED LED?
U 1 1 5C34AD1C
P 6500 4050
F 0 "LED?" H 6750 4000 60  0000 R CNN
F 1 "LED" H 6550 4100 60  0000 L CNN
F 2 "chordboard-footprints:LED_1.8mm" H 6500 4050 60  0001 C CNN
F 3 "" H 6500 4050 60  0001 C CNN
	1    6500 4050
	1    0    0    -1  
$EndComp
Text Label 3250 2450 1    60   ~ 0
Y0
Text Label 3750 2450 1    60   ~ 0
Y1
Text Label 4250 2450 1    60   ~ 0
Y2
Text Label 4750 2450 1    60   ~ 0
Y3
Text Label 5250 2450 1    60   ~ 0
Y4
Text Label 5750 2450 1    60   ~ 0
Y5
Text Label 6250 2450 1    60   ~ 0
Y6
Text Label 6750 2450 1    60   ~ 0
Y7
Connection ~ 6750 2850
Connection ~ 6250 2850
Connection ~ 5750 2850
Connection ~ 5250 2850
Connection ~ 4750 2850
Connection ~ 4250 2850
Connection ~ 3750 2850
Connection ~ 3250 2850
Connection ~ 6750 2650
Connection ~ 6250 2650
Connection ~ 5750 2650
Connection ~ 5250 2650
Connection ~ 4750 2650
Connection ~ 4250 2650
Connection ~ 3750 2650
Connection ~ 3250 2650
Connection ~ 5850 2950
Connection ~ 5350 2950
Wire Wire Line
	6750 2850 6650 2850
Wire Wire Line
	6350 2950 6350 2850
Wire Wire Line
	6250 2850 6150 2850
Wire Wire Line
	5850 2950 5850 2850
Wire Wire Line
	5750 2850 5650 2850
Wire Wire Line
	5350 2950 5350 2850
Connection ~ 4850 2950
Connection ~ 4350 2950
Wire Wire Line
	4850 2950 4850 2850
Wire Wire Line
	5250 2850 5150 2850
Wire Wire Line
	4750 2850 4650 2850
Connection ~ 3850 2950
Wire Wire Line
	4350 2950 4350 2850
Wire Wire Line
	4250 2850 4150 2850
Connection ~ 3350 2950
Wire Wire Line
	3850 2950 3850 2850
Wire Wire Line
	3750 2850 3650 2850
Connection ~ 2850 2950
Wire Wire Line
	3350 2950 3350 2850
Wire Wire Line
	3250 2850 3150 2850
Wire Wire Line
	2850 2950 2850 2850
Wire Wire Line
	2750 2950 2850 2950
Connection ~ 5850 2750
Connection ~ 5350 2750
Wire Wire Line
	6750 2450 6750 2650
Wire Wire Line
	6650 2650 6750 2650
Wire Wire Line
	6350 2750 6350 2650
Wire Wire Line
	6250 2450 6250 2650
Wire Wire Line
	6150 2650 6250 2650
Wire Wire Line
	5850 2750 5850 2650
Wire Wire Line
	5750 2450 5750 2650
Wire Wire Line
	5650 2650 5750 2650
Wire Wire Line
	5350 2750 5350 2650
Connection ~ 4850 2750
Connection ~ 4350 2750
Wire Wire Line
	4850 2750 4850 2650
Wire Wire Line
	5250 2450 5250 2650
Wire Wire Line
	5150 2650 5250 2650
Wire Wire Line
	4750 2450 4750 2650
Wire Wire Line
	4650 2650 4750 2650
Connection ~ 3850 2750
Wire Wire Line
	4350 2750 4350 2650
Wire Wire Line
	4250 2450 4250 2650
Wire Wire Line
	4150 2650 4250 2650
Connection ~ 3350 2750
Wire Wire Line
	3850 2750 3850 2650
Wire Wire Line
	3750 2450 3750 2650
Wire Wire Line
	3650 2650 3750 2650
Connection ~ 2850 2750
Wire Wire Line
	3350 2750 3350 2650
Wire Wire Line
	3250 2450 3250 2650
Wire Wire Line
	3150 2650 3250 2650
Wire Wire Line
	2850 2750 2850 2650
Wire Wire Line
	2750 2750 2850 2750
Connection ~ 6750 3250
Connection ~ 6250 3250
Connection ~ 5750 3250
Connection ~ 5250 3250
Connection ~ 4750 3250
Connection ~ 4250 3250
Connection ~ 3750 3250
Connection ~ 3250 3250
Connection ~ 6750 3050
Connection ~ 6250 3050
Connection ~ 5750 3050
Connection ~ 5250 3050
Connection ~ 4750 3050
Connection ~ 4250 3050
Connection ~ 3750 3050
Connection ~ 3250 3050
Connection ~ 5850 3350
Connection ~ 5350 3350
Wire Wire Line
	6750 3250 6650 3250
Wire Wire Line
	6350 3350 6350 3250
Wire Wire Line
	6250 3250 6150 3250
Wire Wire Line
	5850 3350 5850 3250
Wire Wire Line
	5750 3250 5650 3250
Wire Wire Line
	5350 3350 5350 3250
Connection ~ 4850 3350
Connection ~ 4350 3350
Wire Wire Line
	4850 3350 4850 3250
Wire Wire Line
	5250 3250 5150 3250
Wire Wire Line
	4750 3250 4650 3250
Connection ~ 3850 3350
Wire Wire Line
	4350 3350 4350 3250
Wire Wire Line
	4250 3250 4150 3250
Connection ~ 3350 3350
Wire Wire Line
	3850 3350 3850 3250
Wire Wire Line
	3750 3250 3650 3250
Connection ~ 2850 3350
Wire Wire Line
	3350 3350 3350 3250
Wire Wire Line
	3250 3250 3150 3250
Wire Wire Line
	2850 3350 2850 3250
Wire Wire Line
	2750 3350 2850 3350
Connection ~ 5850 3150
Connection ~ 5350 3150
Wire Wire Line
	6750 3050 6650 3050
Wire Wire Line
	6350 3150 6350 3050
Wire Wire Line
	6250 3050 6150 3050
Wire Wire Line
	5850 3150 5850 3050
Wire Wire Line
	5750 3050 5650 3050
Wire Wire Line
	5350 3150 5350 3050
Connection ~ 4850 3150
Connection ~ 4350 3150
Wire Wire Line
	4850 3150 4850 3050
Wire Wire Line
	5250 3050 5150 3050
Wire Wire Line
	4750 3050 4650 3050
Connection ~ 3850 3150
Wire Wire Line
	4350 3150 4350 3050
Wire Wire Line
	4250 3050 4150 3050
Connection ~ 3350 3150
Wire Wire Line
	3850 3150 3850 3050
Wire Wire Line
	3750 3050 3650 3050
Connection ~ 2850 3150
Wire Wire Line
	3350 3150 3350 3050
Wire Wire Line
	3250 3050 3150 3050
Wire Wire Line
	2850 3150 2850 3050
Wire Wire Line
	2750 3150 2850 3150
Connection ~ 3250 3450
Wire Wire Line
	3250 3450 3150 3450
Wire Wire Line
	2850 3550 2850 3450
Connection ~ 3750 3450
Wire Wire Line
	3750 3450 3650 3450
Wire Wire Line
	3350 3550 3350 3450
Connection ~ 4250 3450
Wire Wire Line
	4250 3450 4150 3450
Wire Wire Line
	3850 3550 3850 3450
Connection ~ 4750 3450
Wire Wire Line
	4750 3450 4650 3450
Wire Wire Line
	4350 3550 4350 3450
Connection ~ 5250 3450
Wire Wire Line
	5250 3450 5150 3450
Wire Wire Line
	4850 3550 4850 3450
Connection ~ 5750 3450
Wire Wire Line
	5750 3450 5650 3450
Wire Wire Line
	5350 3550 5350 3450
Connection ~ 6250 3450
Wire Wire Line
	6250 3450 6150 3450
Wire Wire Line
	5850 3550 5850 3450
Connection ~ 6750 3450
Wire Wire Line
	6750 3450 6650 3450
Wire Wire Line
	6350 3550 6350 3450
Wire Wire Line
	2750 3550 2850 3550
Connection ~ 2850 3550
Connection ~ 3350 3550
Connection ~ 3850 3550
Connection ~ 4350 3550
Connection ~ 4850 3550
Connection ~ 5350 3550
Connection ~ 5850 3550
Wire Wire Line
	6750 2650 6750 2850
Wire Wire Line
	6250 2650 6250 2850
Wire Wire Line
	5750 2650 5750 2850
Wire Wire Line
	5250 2650 5250 2850
Wire Wire Line
	4750 2650 4750 2850
Wire Wire Line
	4250 2650 4250 2850
Wire Wire Line
	3750 2650 3750 2850
Wire Wire Line
	3250 2650 3250 2850
Connection ~ 3250 3650
Wire Wire Line
	3250 3650 3150 3650
Wire Wire Line
	2850 3750 2850 3650
Connection ~ 3750 3650
Wire Wire Line
	3750 3650 3650 3650
Wire Wire Line
	3350 3750 3350 3650
Connection ~ 4250 3650
Wire Wire Line
	4250 3650 4150 3650
Wire Wire Line
	3850 3750 3850 3650
Connection ~ 4750 3650
Wire Wire Line
	4750 3650 4650 3650
Wire Wire Line
	4350 3750 4350 3650
Connection ~ 5250 3650
Wire Wire Line
	5250 3650 5150 3650
Wire Wire Line
	4850 3750 4850 3650
Connection ~ 5750 3650
Wire Wire Line
	5750 3650 5650 3650
Wire Wire Line
	5350 3750 5350 3650
Connection ~ 6250 3650
Wire Wire Line
	6250 3650 6150 3650
Wire Wire Line
	5850 3750 5850 3650
Connection ~ 6750 3650
Wire Wire Line
	6750 3650 6650 3650
Wire Wire Line
	6350 3750 6350 3650
Wire Wire Line
	2750 3750 2850 3750
Connection ~ 2850 3750
Connection ~ 3350 3750
Connection ~ 3850 3750
Connection ~ 4350 3750
Connection ~ 4850 3750
Connection ~ 5350 3750
Connection ~ 5850 3750
Wire Wire Line
	6750 2850 6750 3050
Wire Wire Line
	6250 2850 6250 3050
Wire Wire Line
	5750 2850 5750 3050
Wire Wire Line
	5250 2850 5250 3050
Wire Wire Line
	4750 2850 4750 3050
Wire Wire Line
	4250 2850 4250 3050
Wire Wire Line
	3750 2850 3750 3050
Wire Wire Line
	3250 2850 3250 3050
Connection ~ 3250 3850
Wire Wire Line
	3250 3850 3150 3850
Wire Wire Line
	2850 3950 2850 3850
Connection ~ 3750 3850
Wire Wire Line
	3750 3850 3650 3850
Wire Wire Line
	3350 3950 3350 3850
Connection ~ 4250 3850
Wire Wire Line
	4250 3850 4150 3850
Wire Wire Line
	3850 3950 3850 3850
Connection ~ 4750 3850
Wire Wire Line
	4750 3850 4650 3850
Wire Wire Line
	4350 3950 4350 3850
Connection ~ 5250 3850
Wire Wire Line
	5250 3850 5150 3850
Wire Wire Line
	4850 3950 4850 3850
Connection ~ 5750 3850
Wire Wire Line
	5750 3850 5650 3850
Wire Wire Line
	5350 3950 5350 3850
Connection ~ 6250 3850
Wire Wire Line
	6250 3850 6150 3850
Wire Wire Line
	5850 3950 5850 3850
Connection ~ 6750 3850
Wire Wire Line
	6750 3850 6650 3850
Wire Wire Line
	6350 3950 6350 3850
Wire Wire Line
	2750 3950 2850 3950
Connection ~ 2850 3950
Connection ~ 3350 3950
Connection ~ 3850 3950
Connection ~ 4350 3950
Connection ~ 4850 3950
Connection ~ 5350 3950
Connection ~ 5850 3950
Wire Wire Line
	3250 4050 3150 4050
Wire Wire Line
	2850 4150 2850 4050
Wire Wire Line
	3750 4050 3650 4050
Wire Wire Line
	3350 4150 3350 4050
Wire Wire Line
	4250 4050 4150 4050
Wire Wire Line
	3850 4150 3850 4050
Wire Wire Line
	4750 4050 4650 4050
Wire Wire Line
	4350 4150 4350 4050
Wire Wire Line
	5250 4050 5150 4050
Wire Wire Line
	4850 4150 4850 4050
Wire Wire Line
	5750 4050 5650 4050
Wire Wire Line
	5350 4150 5350 4050
Wire Wire Line
	6250 4050 6150 4050
Wire Wire Line
	5850 4150 5850 4050
Wire Wire Line
	6750 4050 6650 4050
Wire Wire Line
	6350 4150 6350 4050
Wire Wire Line
	2750 4150 2850 4150
Connection ~ 2850 4150
Connection ~ 3350 4150
Connection ~ 3850 4150
Connection ~ 4350 4150
Connection ~ 4850 4150
Connection ~ 5350 4150
Connection ~ 5850 4150
Wire Wire Line
	5850 2950 6350 2950
Wire Wire Line
	5350 2950 5850 2950
Wire Wire Line
	4850 2950 5350 2950
Wire Wire Line
	4350 2950 4850 2950
Wire Wire Line
	3850 2950 4350 2950
Wire Wire Line
	3350 2950 3850 2950
Wire Wire Line
	2850 2950 3350 2950
Wire Wire Line
	5850 2750 6350 2750
Wire Wire Line
	5350 2750 5850 2750
Wire Wire Line
	4850 2750 5350 2750
Wire Wire Line
	4350 2750 4850 2750
Wire Wire Line
	3850 2750 4350 2750
Wire Wire Line
	3350 2750 3850 2750
Wire Wire Line
	2850 2750 3350 2750
Wire Wire Line
	6750 3250 6750 3450
Wire Wire Line
	6250 3250 6250 3450
Wire Wire Line
	5750 3250 5750 3450
Wire Wire Line
	5250 3250 5250 3450
Wire Wire Line
	4750 3250 4750 3450
Wire Wire Line
	4250 3250 4250 3450
Wire Wire Line
	3750 3250 3750 3450
Wire Wire Line
	3250 3250 3250 3450
Wire Wire Line
	6750 3050 6750 3250
Wire Wire Line
	6250 3050 6250 3250
Wire Wire Line
	5750 3050 5750 3250
Wire Wire Line
	5250 3050 5250 3250
Wire Wire Line
	4750 3050 4750 3250
Wire Wire Line
	4250 3050 4250 3250
Wire Wire Line
	3750 3050 3750 3250
Wire Wire Line
	3250 3050 3250 3250
Wire Wire Line
	5850 3350 6350 3350
Wire Wire Line
	5350 3350 5850 3350
Wire Wire Line
	4850 3350 5350 3350
Wire Wire Line
	4350 3350 4850 3350
Wire Wire Line
	3850 3350 4350 3350
Wire Wire Line
	3350 3350 3850 3350
Wire Wire Line
	2850 3350 3350 3350
Wire Wire Line
	5850 3150 6350 3150
Wire Wire Line
	5350 3150 5850 3150
Wire Wire Line
	4850 3150 5350 3150
Wire Wire Line
	4350 3150 4850 3150
Wire Wire Line
	3850 3150 4350 3150
Wire Wire Line
	3350 3150 3850 3150
Wire Wire Line
	2850 3150 3350 3150
Wire Wire Line
	3250 3450 3250 3650
Wire Wire Line
	3750 3450 3750 3650
Wire Wire Line
	4250 3450 4250 3650
Wire Wire Line
	4750 3450 4750 3650
Wire Wire Line
	5250 3450 5250 3650
Wire Wire Line
	5750 3450 5750 3650
Wire Wire Line
	6250 3450 6250 3650
Wire Wire Line
	6750 3450 6750 3650
Wire Wire Line
	2850 3550 3350 3550
Wire Wire Line
	3350 3550 3850 3550
Wire Wire Line
	3850 3550 4350 3550
Wire Wire Line
	4350 3550 4850 3550
Wire Wire Line
	4850 3550 5350 3550
Wire Wire Line
	5350 3550 5850 3550
Wire Wire Line
	5850 3550 6350 3550
Wire Wire Line
	3250 3650 3250 3850
Wire Wire Line
	3750 3650 3750 3850
Wire Wire Line
	4250 3650 4250 3850
Wire Wire Line
	4750 3650 4750 3850
Wire Wire Line
	5250 3650 5250 3850
Wire Wire Line
	5750 3650 5750 3850
Wire Wire Line
	6250 3650 6250 3850
Wire Wire Line
	6750 3650 6750 3850
Wire Wire Line
	2850 3750 3350 3750
Wire Wire Line
	3350 3750 3850 3750
Wire Wire Line
	3850 3750 4350 3750
Wire Wire Line
	4350 3750 4850 3750
Wire Wire Line
	4850 3750 5350 3750
Wire Wire Line
	5350 3750 5850 3750
Wire Wire Line
	5850 3750 6350 3750
Wire Wire Line
	3250 3850 3250 4050
Wire Wire Line
	3750 3850 3750 4050
Wire Wire Line
	4250 3850 4250 4050
Wire Wire Line
	4750 3850 4750 4050
Wire Wire Line
	5250 3850 5250 4050
Wire Wire Line
	5750 3850 5750 4050
Wire Wire Line
	6250 3850 6250 4050
Wire Wire Line
	6750 3850 6750 4050
Wire Wire Line
	2850 3950 3350 3950
Wire Wire Line
	3350 3950 3850 3950
Wire Wire Line
	3850 3950 4350 3950
Wire Wire Line
	4350 3950 4850 3950
Wire Wire Line
	4850 3950 5350 3950
Wire Wire Line
	5350 3950 5850 3950
Wire Wire Line
	5850 3950 6350 3950
Wire Wire Line
	2850 4150 3350 4150
Wire Wire Line
	3350 4150 3850 4150
Wire Wire Line
	3850 4150 4350 4150
Wire Wire Line
	4350 4150 4850 4150
Wire Wire Line
	4850 4150 5350 4150
Wire Wire Line
	5350 4150 5850 4150
Wire Wire Line
	5850 4150 6350 4150
$EndSCHEMATC
