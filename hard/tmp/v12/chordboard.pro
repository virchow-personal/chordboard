update=Wed 11 Jul 2018 11:35:03 PM BST
version=1
last_client=kicad
[pcbnew]
version=1
LastNetListRead=
UseCmpFile=1
PadDrill=0.600000000000
PadDrillOvalY=0.600000000000
PadSizeH=1.500000000000
PadSizeV=1.500000000000
PcbTextSizeV=1.500000000000
PcbTextSizeH=1.500000000000
PcbTextThickness=0.300000000000
ModuleTextSizeV=1.000000000000
ModuleTextSizeH=1.000000000000
ModuleTextSizeThickness=0.150000000000
SolderMaskClearance=0.000000000000
SolderMaskMinWidth=0.000000000000
DrawSegmentWidth=0.200000000000
BoardOutlineThickness=0.100000000000
ModuleOutlineThickness=0.150000000000
[cvpcb]
version=1
NetIExt=net
[general]
version=1
[schematic_editor]
version=1
PageLayoutDescrFile=
PlotDirectoryName=./
SubpartIdSeparator=0
SubpartFirstId=65
NetFmtName=Pcbnew
SpiceForceRefPrefix=0
SpiceUseNetNumbers=0
LabSize=60
[eeschema]
version=1
LibDir=
[eeschema/libraries]
LibName1=chordboard-rescue
LibName2=/home/afshar/src/kicad-libraries/kicad-library/library/ac-dc
LibName3=/home/afshar/src/kicad-libraries/kicad-library/library/adc-dac
LibName4=/home/afshar/src/kicad-libraries/kicad-library/library/Altera
LibName5=/home/afshar/src/kicad-libraries/kicad-library/library/analog_devices
LibName6=/home/afshar/src/kicad-libraries/kicad-library/library/analog_switches
LibName7=/home/afshar/src/kicad-libraries/kicad-library/library/atmel
LibName8=/home/afshar/src/kicad-libraries/kicad-library/library/audio
LibName9=/home/afshar/src/kicad-libraries/kicad-library/library/Battery_Management
LibName10=/home/afshar/src/kicad-libraries/kicad-library/library/bbd
LibName11=/home/afshar/src/kicad-libraries/kicad-library/library/Bosch
LibName12=/home/afshar/src/kicad-libraries/kicad-library/library/brooktre
LibName13=/home/afshar/src/kicad-libraries/kicad-library/library/Connector
LibName14=/home/afshar/src/kicad-libraries/kicad-library/library/contrib
LibName15=/home/afshar/src/kicad-libraries/kicad-library/library/cypress
LibName16=/home/afshar/src/kicad-libraries/kicad-library/library/dc-dc
LibName17=/home/afshar/src/kicad-libraries/kicad-library/library/Decawave
LibName18=/home/afshar/src/kicad-libraries/kicad-library/library/device
LibName19=/home/afshar/src/kicad-libraries/kicad-library/library/digital-audio
LibName20=/home/afshar/src/kicad-libraries/kicad-library/library/Diode
LibName21=/home/afshar/src/kicad-libraries/kicad-library/library/Display
LibName22=/home/afshar/src/kicad-libraries/kicad-library/library/driver_gate
LibName23=/home/afshar/src/kicad-libraries/kicad-library/library/dsp
LibName24=/home/afshar/src/kicad-libraries/kicad-library/library/DSP_Microchip_DSPIC33
LibName25=/home/afshar/src/kicad-libraries/kicad-library/library/elec-unifil
LibName26=/home/afshar/src/kicad-libraries/kicad-library/library/ESD_Protection
LibName27=/home/afshar/src/kicad-libraries/kicad-library/library/Espressif
LibName28=/home/afshar/src/kicad-libraries/kicad-library/library/FPGA_Actel
LibName29=/home/afshar/src/kicad-libraries/kicad-library/library/ftdi
LibName30=/home/afshar/src/kicad-libraries/kicad-library/library/gennum
LibName31=/home/afshar/src/kicad-libraries/kicad-library/library/Graphic
LibName32=/home/afshar/src/kicad-libraries/kicad-library/library/hc11
LibName33=/home/afshar/src/kicad-libraries/kicad-library/library/infineon
LibName34=/home/afshar/src/kicad-libraries/kicad-library/library/intel
LibName35=/home/afshar/src/kicad-libraries/kicad-library/library/interface
LibName36=/home/afshar/src/kicad-libraries/kicad-library/library/intersil
LibName37=/home/afshar/src/kicad-libraries/kicad-library/library/ir
LibName38=/home/afshar/src/kicad-libraries/kicad-library/library/Lattice
LibName39=/home/afshar/src/kicad-libraries/kicad-library/library/LED
LibName40=/home/afshar/src/kicad-libraries/kicad-library/library/LEM
LibName41=/home/afshar/src/kicad-libraries/kicad-library/library/linear
LibName42=/home/afshar/src/kicad-libraries/kicad-library/library/Logic_74xgxx
LibName43=/home/afshar/src/kicad-libraries/kicad-library/library/Logic_74xx
LibName44=/home/afshar/src/kicad-libraries/kicad-library/library/Logic_CMOS_4000
LibName45=/home/afshar/src/kicad-libraries/kicad-library/library/Logic_CMOS_IEEE
LibName46=/home/afshar/src/kicad-libraries/kicad-library/library/logic_programmable
LibName47=/home/afshar/src/kicad-libraries/kicad-library/library/Logic_TTL_IEEE
LibName48=/home/afshar/src/kicad-libraries/kicad-library/library/maxim
LibName49=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_Microchip_PIC10
LibName50=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_Microchip_PIC12
LibName51=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_Microchip_PIC16
LibName52=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_Microchip_PIC18
LibName53=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_Microchip_PIC24
LibName54=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_Microchip_PIC32
LibName55=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_NXP_Kinetis
LibName56=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_NXP_LPC
LibName57=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_NXP_S08
LibName58=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_Parallax
LibName59=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_ST_STM8
LibName60=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_ST_STM32
LibName61=/home/afshar/src/kicad-libraries/kicad-library/library/MCU_Texas_MSP430
LibName62=/home/afshar/src/kicad-libraries/kicad-library/library/Mechanical
LibName63=/home/afshar/src/kicad-libraries/kicad-library/library/memory
LibName64=/home/afshar/src/kicad-libraries/kicad-library/library/microchip
LibName65=/home/afshar/src/kicad-libraries/kicad-library/library/microcontrollers
LibName66=/home/afshar/src/kicad-libraries/kicad-library/library/modules
LibName67=/home/afshar/src/kicad-libraries/kicad-library/library/Motor
LibName68=/home/afshar/src/kicad-libraries/kicad-library/library/motor_drivers
LibName69=/home/afshar/src/kicad-libraries/kicad-library/library/motorola
LibName70=/home/afshar/src/kicad-libraries/kicad-library/library/nordicsemi
LibName71=/home/afshar/src/kicad-libraries/kicad-library/library/nxp
LibName72=/home/afshar/src/kicad-libraries/kicad-library/library/onsemi
LibName73=/home/afshar/src/kicad-libraries/kicad-library/library/opto
LibName74=/home/afshar/src/kicad-libraries/kicad-library/library/Oscillators
LibName75=/home/afshar/src/kicad-libraries/kicad-library/library/philips
LibName76=/home/afshar/src/kicad-libraries/kicad-library/library/power
LibName77=/home/afshar/src/kicad-libraries/kicad-library/library/powerint
LibName78=/home/afshar/src/kicad-libraries/kicad-library/library/Power_Management
LibName79=/home/afshar/src/kicad-libraries/kicad-library/library/pspice
LibName80=/home/afshar/src/kicad-libraries/kicad-library/library/references
LibName81=/home/afshar/src/kicad-libraries/kicad-library/library/regul
LibName82=/home/afshar/src/kicad-libraries/kicad-library/library/Relay
LibName83=/home/afshar/src/kicad-libraries/kicad-library/library/RF_Bluetooth
LibName84=/home/afshar/src/kicad-libraries/kicad-library/library/rfcom
LibName85=/home/afshar/src/kicad-libraries/kicad-library/library/RFSolutions
LibName86=/home/afshar/src/kicad-libraries/kicad-library/library/Sensor_Current
LibName87=/home/afshar/src/kicad-libraries/kicad-library/library/Sensor_Humidity
LibName88=/home/afshar/src/kicad-libraries/kicad-library/library/sensors
LibName89=/home/afshar/src/kicad-libraries/kicad-library/library/silabs
LibName90=/home/afshar/src/kicad-libraries/kicad-library/library/siliconi
LibName91=/home/afshar/src/kicad-libraries/kicad-library/library/supertex
LibName92=/home/afshar/src/kicad-libraries/kicad-library/library/Switch
LibName93=/home/afshar/src/kicad-libraries/kicad-library/library/texas
LibName94=/home/afshar/src/kicad-libraries/kicad-library/library/Transformer
LibName95=/home/afshar/src/kicad-libraries/kicad-library/library/Transistor
LibName96=/home/afshar/src/kicad-libraries/kicad-library/library/triac_thyristor
LibName97=/home/afshar/src/kicad-libraries/kicad-library/library/Valve
LibName98=/home/afshar/src/kicad-libraries/kicad-library/library/video
LibName99=/home/afshar/src/kicad-libraries/kicad-library/library/wiznet
LibName100=/home/afshar/src/kicad-libraries/kicad-library/library/Worldsemi
LibName101=/home/afshar/src/kicad-libraries/kicad-library/library/Xicor
LibName102=/home/afshar/src/kicad-libraries/kicad-library/library/xilinx
LibName103=/home/afshar/src/kicad-libraries/kicad-library/library/xilinx-artix7
LibName104=/home/afshar/src/kicad-libraries/kicad-library/library/xilinx-kintex7
LibName105=/home/afshar/src/kicad-libraries/kicad-library/library/xilinx-spartan6
LibName106=/home/afshar/src/kicad-libraries/kicad-library/library/xilinx-virtex5
LibName107=/home/afshar/src/kicad-libraries/kicad-library/library/xilinx-virtex6
LibName108=/home/afshar/src/kicad-libraries/kicad-library/library/xilinx-virtex7
LibName109=/home/afshar/src/kicad-libraries/kicad-library/library/zetex
LibName110=/home/afshar/src/kicad-libraries/kicad-library/library/Zilog
LibName111=/home/afshar/src/kicad-libraries/teensy_library/teensy
LibName112=/home/afshar/src/kicad-libraries/Rotary-Encoders.pretty/Schematic-Symbols/Rotary-Encoders
LibName113=/home/afshar/src/kicad-libraries/nebs-eurocad/eurocad
LibName114=/home/afshar/src/kicad-libraries/afshar/afshar
LibName115=/home/afshar/src/kicad-libraries/libs/74hct125d
LibName116=/home/afshar/src/kicad-libraries/libs/74hct245
LibName117=/home/afshar/src/kicad-libraries/libs/4000-ic
LibName118=/home/afshar/src/kicad-libraries/libs/7400-ic
LibName119=/home/afshar/src/kicad-libraries/libs/75176
LibName120=/home/afshar/src/kicad-libraries/libs/Abracon
LibName121=/home/afshar/src/kicad-libraries/libs/ABS07-32.768KHZ-T
LibName122=/home/afshar/src/kicad-libraries/libs/acorn_electron_expansion_connector
LibName123=/home/afshar/src/kicad-libraries/libs/ActiveSemi
LibName124=/home/afshar/src/kicad-libraries/libs/afshar
LibName125=/home/afshar/src/kicad-libraries/libs/akn_holtek
LibName126=/home/afshar/src/kicad-libraries/libs/akn_maxim
LibName127=/home/afshar/src/kicad-libraries/libs/akn_misc
LibName128=/home/afshar/src/kicad-libraries/libs/akn_transformers
LibName129=/home/afshar/src/kicad-libraries/libs/altera
LibName130=/home/afshar/src/kicad-libraries/libs/Altera
LibName131=/home/afshar/src/kicad-libraries/libs/Amplifiers
LibName132=/home/afshar/src/kicad-libraries/libs/AMS
LibName133=/home/afshar/src/kicad-libraries/libs/analog-devices
LibName134=/home/afshar/src/kicad-libraries/libs/AnalogDevices
LibName135=/home/afshar/src/kicad-libraries/libs/analog-ic
LibName136=/home/afshar/src/kicad-libraries/libs/AOS
LibName137=/home/afshar/src/kicad-libraries/libs/arm-swd-header
LibName138=/home/afshar/src/kicad-libraries/libs/Atmel
LibName139=/home/afshar/src/kicad-libraries/libs/avr-mcu
LibName140=/home/afshar/src/kicad-libraries/libs/bluegiga
LibName141=/home/afshar/src/kicad-libraries/libs/Bosch
LibName142=/home/afshar/src/kicad-libraries/libs/Chipkit_Shield_Max32-cache
LibName143=/home/afshar/src/kicad-libraries/libs/Chipkit_Shield_uC-cache
LibName144=/home/afshar/src/kicad-libraries/libs/conn-2mm
LibName145=/home/afshar/src/kicad-libraries/libs/conn-100mil
LibName146=/home/afshar/src/kicad-libraries/libs/conn-amphenol
LibName147=/home/afshar/src/kicad-libraries/libs/conn-assmann
LibName148=/home/afshar/src/kicad-libraries/libs/conn-cui
LibName149=/home/afshar/src/kicad-libraries/libs/connector
LibName150=/home/afshar/src/kicad-libraries/libs/Connectors
LibName151=/home/afshar/src/kicad-libraries/libs/conn-fci
LibName152=/home/afshar/src/kicad-libraries/libs/conn-jae
LibName153=/home/afshar/src/kicad-libraries/libs/conn-linx
LibName154=/home/afshar/src/kicad-libraries/libs/conn_mics
LibName155=/home/afshar/src/kicad-libraries/libs/conn-molex
LibName156=/home/afshar/src/kicad-libraries/libs/conn-special-headers
LibName157=/home/afshar/src/kicad-libraries/libs/conn-tagconnect
LibName158=/home/afshar/src/kicad-libraries/libs/conn-te
LibName159=/home/afshar/src/kicad-libraries/libs/conn-test
LibName160=/home/afshar/src/kicad-libraries/libs/CubeSatKit_StackBoard-cache
LibName161=/home/afshar/src/kicad-libraries/libs/DataStorage
LibName162=/home/afshar/src/kicad-libraries/libs/diode-inc-ic
LibName163=/home/afshar/src/kicad-libraries/libs/Diodes
LibName164=/home/afshar/src/kicad-libraries/libs/DiodesInc
LibName165=/home/afshar/src/kicad-libraries/libs/display
LibName166=/home/afshar/src/kicad-libraries/libs/EKB
LibName167=/home/afshar/src/kicad-libraries/libs/electomech-misc
LibName168=/home/afshar/src/kicad-libraries/libs/_electromech
LibName169=/home/afshar/src/kicad-libraries/libs/esp8266-esp-01
LibName170=/home/afshar/src/kicad-libraries/libs/esp8266-esp-03
LibName171=/home/afshar/src/kicad-libraries/libs/esp8266-esp-12e
LibName172=/home/afshar/src/kicad-libraries/libs/ESP8266
LibName173=/home/afshar/src/kicad-libraries/libs/eurocad
LibName174=/home/afshar/src/kicad-libraries/libs/Fairchild
LibName175=/home/afshar/src/kicad-libraries/libs/freescale-ic
LibName176=/home/afshar/src/kicad-libraries/libs/FTDI
LibName177=/home/afshar/src/kicad-libraries/libs/ftdi-ic
LibName178=/home/afshar/src/kicad-libraries/libs/hm-11
LibName179=/home/afshar/src/kicad-libraries/libs/Infineon
LibName180=/home/afshar/src/kicad-libraries/libs/Intersil
LibName181=/home/afshar/src/kicad-libraries/libs/iso15
LibName182=/home/afshar/src/kicad-libraries/libs/kbox
LibName183=/home/afshar/src/kicad-libraries/libs/kbox-cache
LibName184=/home/afshar/src/kicad-libraries/libs/KCDA02-123
LibName185=/home/afshar/src/kicad-libraries/libs/Lattice
LibName186=/home/afshar/src/kicad-libraries/libs/led
LibName187=/home/afshar/src/kicad-libraries/libs/_linear
LibName188=/home/afshar/src/kicad-libraries/libs/LinearTech
LibName189=/home/afshar/src/kicad-libraries/libs/Littelfuse
LibName190=/home/afshar/src/kicad-libraries/libs/_logic
LibName191=/home/afshar/src/kicad-libraries/libs/logic-4000
LibName192=/home/afshar/src/kicad-libraries/libs/logic-7400
LibName193=/home/afshar/src/kicad-libraries/libs/logic-7400-new
LibName194=/home/afshar/src/kicad-libraries/libs/LogicDevices
LibName195=/home/afshar/src/kicad-libraries/libs/lpc11u14fbd48
LibName196=/home/afshar/src/kicad-libraries/libs/lt
LibName197=/home/afshar/src/kicad-libraries/libs/MACOM
LibName198=/home/afshar/src/kicad-libraries/libs/Macrofab
LibName199=/home/afshar/src/kicad-libraries/libs/max
LibName200=/home/afshar/src/kicad-libraries/libs/maxim
LibName201=/home/afshar/src/kicad-libraries/libs/maxim-ic
LibName202=/home/afshar/src/kicad-libraries/libs/mcp1700t-3302e-tt
LibName203=/home/afshar/src/kicad-libraries/libs/mcp73831t-2aci-ot
LibName204=/home/afshar/src/kicad-libraries/libs/mechanical
LibName205=/home/afshar/src/kicad-libraries/libs/micrel-ic
LibName206=/home/afshar/src/kicad-libraries/libs/Microchip
LibName207=/home/afshar/src/kicad-libraries/libs/microchip-ic
LibName208=/home/afshar/src/kicad-libraries/libs/Micron
LibName209=/home/afshar/src/kicad-libraries/libs/Microprocessors
LibName210=/home/afshar/src/kicad-libraries/libs/micro_usb_socket
LibName211=/home/afshar/src/kicad-libraries/libs/MiscellaneousDevices
LibName212=/home/afshar/src/kicad-libraries/libs/mke02z64vld2
LibName213=/home/afshar/src/kicad-libraries/libs/mke04z8vtg4
LibName214=/home/afshar/src/kicad-libraries/libs/Murata
LibName215=/home/afshar/src/kicad-libraries/libs/nrf24l01p_smd
LibName216=/home/afshar/src/kicad-libraries/libs/nrf51822-04
LibName217=/home/afshar/src/kicad-libraries/libs/nRF24L01+
LibName218=/home/afshar/src/kicad-libraries/libs/NXP
LibName219=/home/afshar/src/kicad-libraries/libs/nxp-ic
LibName220=/home/afshar/src/kicad-libraries/libs/OceanOptics
LibName221=/home/afshar/src/kicad-libraries/libs/onsemi
LibName222=/home/afshar/src/kicad-libraries/libs/on-semi-ic
LibName223=/home/afshar/src/kicad-libraries/libs/opamps
LibName224=/home/afshar/src/kicad-libraries/libs/_passive
LibName225=/home/afshar/src/kicad-libraries/libs/Passives
LibName226=/home/afshar/src/kicad-libraries/libs/pasv-BelFuse
LibName227=/home/afshar/src/kicad-libraries/libs/pasv-BiTech
LibName228=/home/afshar/src/kicad-libraries/libs/pasv-Bourns
LibName229=/home/afshar/src/kicad-libraries/libs/pasv-cap
LibName230=/home/afshar/src/kicad-libraries/libs/pasv-ind
LibName231=/home/afshar/src/kicad-libraries/libs/pasv-Murata
LibName232=/home/afshar/src/kicad-libraries/libs/pasv-res
LibName233=/home/afshar/src/kicad-libraries/libs/pasv-TDK
LibName234=/home/afshar/src/kicad-libraries/libs/pasv-xtal
LibName235=/home/afshar/src/kicad-libraries/libs/pcb
LibName236=/home/afshar/src/kicad-libraries/libs/PMOD
LibName237=/home/afshar/src/kicad-libraries/libs/power
LibName238=/home/afshar/src/kicad-libraries/libs/PowerComponents
LibName239=/home/afshar/src/kicad-libraries/libs/pp_ws2812b
LibName240=/home/afshar/src/kicad-libraries/libs/random-mics
LibName241=/home/afshar/src/kicad-libraries/libs/Recom
LibName242=/home/afshar/src/kicad-libraries/libs/recom-r1se
LibName243=/home/afshar/src/kicad-libraries/libs/regulator
LibName244=/home/afshar/src/kicad-libraries/libs/relays-a
LibName245=/home/afshar/src/kicad-libraries/libs/RepeaterParts
LibName246=/home/afshar/src/kicad-libraries/libs/RF_OEM_Parts
LibName247=/home/afshar/src/kicad-libraries/libs/Richtek
LibName248=/home/afshar/src/kicad-libraries/libs/rohm
LibName249=/home/afshar/src/kicad-libraries/libs/Rotary-Encoders
LibName250=/home/afshar/src/kicad-libraries/libs/_semi
LibName251=/home/afshar/src/kicad-libraries/libs/semi-diode-DiodesInc
LibName252=/home/afshar/src/kicad-libraries/libs/semi-diode-generic
LibName253=/home/afshar/src/kicad-libraries/libs/semi-diode-MCC
LibName254=/home/afshar/src/kicad-libraries/libs/semi-diode-NXP
LibName255=/home/afshar/src/kicad-libraries/libs/semi-diode-OnSemi
LibName256=/home/afshar/src/kicad-libraries/libs/semi-diode-Semtech
LibName257=/home/afshar/src/kicad-libraries/libs/semi-diode-ST
LibName258=/home/afshar/src/kicad-libraries/libs/semi-diode-Toshiba
LibName259=/home/afshar/src/kicad-libraries/libs/semi-opto-generic
LibName260=/home/afshar/src/kicad-libraries/libs/semi-opto-liteon
LibName261=/home/afshar/src/kicad-libraries/libs/semi-thyristor-generic
LibName262=/home/afshar/src/kicad-libraries/libs/semi-trans-AOS
LibName263=/home/afshar/src/kicad-libraries/libs/semi-trans-DiodesInc
LibName264=/home/afshar/src/kicad-libraries/libs/semi-trans-EPC
LibName265=/home/afshar/src/kicad-libraries/libs/semi-trans-Fairchild
LibName266=/home/afshar/src/kicad-libraries/libs/semi-trans-generic
LibName267=/home/afshar/src/kicad-libraries/libs/semi-trans-Infineon
LibName268=/home/afshar/src/kicad-libraries/libs/semi-trans-IRF
LibName269=/home/afshar/src/kicad-libraries/libs/semi-trans-IXYS
LibName270=/home/afshar/src/kicad-libraries/libs/semi-trans-NXP
LibName271=/home/afshar/src/kicad-libraries/libs/semi-trans-OnSemi
LibName272=/home/afshar/src/kicad-libraries/libs/semi-trans-Panasonic
LibName273=/home/afshar/src/kicad-libraries/libs/semi-trans-ST
LibName274=/home/afshar/src/kicad-libraries/libs/semi-trans-TI
LibName275=/home/afshar/src/kicad-libraries/libs/semi-trans-Toshiba
LibName276=/home/afshar/src/kicad-libraries/libs/semi-trans-Vishay
LibName277=/home/afshar/src/kicad-libraries/libs/Sensors
LibName278=/home/afshar/src/kicad-libraries/libs/sharp-relay
LibName279=/home/afshar/src/kicad-libraries/libs/silabs
LibName280=/home/afshar/src/kicad-libraries/libs/skyworks
LibName281=/home/afshar/src/kicad-libraries/libs/Snickerdoodle
LibName282=/home/afshar/src/kicad-libraries/libs/sparkfun
LibName283=/home/afshar/src/kicad-libraries/libs/ST
LibName284=/home/afshar/src/kicad-libraries/libs/standard
LibName285=/home/afshar/src/kicad-libraries/libs/st_ic
LibName286=/home/afshar/src/kicad-libraries/libs/stm32f37xxx_48pin
LibName287=/home/afshar/src/kicad-libraries/libs/stm32f102xx_48pin
LibName288=/home/afshar/src/kicad-libraries/libs/stm32f103c8t6-module-china
LibName289=/home/afshar/src/kicad-libraries/libs/stm32f302xx_48pin
LibName290=/home/afshar/src/kicad-libraries/libs/stmicro-mcu
LibName291=/home/afshar/src/kicad-libraries/libs/supertex
LibName292=/home/afshar/src/kicad-libraries/libs/switches
LibName293=/home/afshar/src/kicad-libraries/libs/symbol
LibName294=/home/afshar/src/kicad-libraries/libs/teensy
LibName295=/home/afshar/src/kicad-libraries/libs/TexasInstruments
LibName296=/home/afshar/src/kicad-libraries/libs/ti
LibName297=/home/afshar/src/kicad-libraries/libs/ti-ic
LibName298=/home/afshar/src/kicad-libraries/libs/tinkerforge
LibName299=/home/afshar/src/kicad-libraries/libs/transistor
LibName300=/home/afshar/src/kicad-libraries/libs/TransistorParts
LibName301=/home/afshar/src/kicad-libraries/libs/transistors-mics
LibName302=/home/afshar/src/kicad-libraries/libs/txs2sa-relay
LibName303=/home/afshar/src/kicad-libraries/libs/uart_pp
LibName304=/home/afshar/src/kicad-libraries/libs/u-blox
LibName305=/home/afshar/src/kicad-libraries/libs/uln-ic
LibName306=/home/afshar/src/kicad-libraries/libs/usb_plug
LibName307=/home/afshar/src/kicad-libraries/libs/Vishay
LibName308=/home/afshar/src/kicad-libraries/libs/Winbond
LibName309=/home/afshar/src/kicad-libraries/libs/ws2812b
LibName310=/home/afshar/src/kicad-libraries/libs/Xilinx
LibName311=/home/afshar/src/kicad-libraries/libs/SparkFun-DigitalIC
LibName312=/home/afshar/src/k66lqfp-kicad/k66lqfp
LibName313=/home/afshar/src/kicad-libraries/kicad-ESP8266/ESP8266
LibName314=/home/afshar/src/kicad-libraries/libs/Wetmelon
LibName315=/home/afshar/src/chordboard/hard/chordboard-kicad-libs/chordboard-components
