EESchema Schematic File Version 2
LIBS:chordboard-components
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:logo
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:modules
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:transf
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:standard
LIBS:keypad4x4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L +3.3V #PWR017
U 1 1 5A7074DC
P 3750 1700
F 0 "#PWR017" H 3750 1550 50  0001 C CNN
F 1 "+3.3V" H 3750 1840 50  0000 C CNN
F 2 "" H 3750 1700 50  0000 C CNN
F 3 "" H 3750 1700 50  0000 C CNN
	1    3750 1700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR018
U 1 1 5A703CB6
P 4700 2250
F 0 "#PWR018" H 4700 2000 50  0001 C CNN
F 1 "GND" H 4700 2100 50  0000 C CNN
F 2 "" H 4700 2250 50  0001 C CNN
F 3 "" H 4700 2250 50  0001 C CNN
	1    4700 2250
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 5A7074DD
P 3750 5100
F 0 "#PWR019" H 3750 4850 50  0001 C CNN
F 1 "GND" H 3750 4950 50  0000 C CNN
F 2 "" H 3750 5100 50  0001 C CNN
F 3 "" H 3750 5100 50  0001 C CNN
	1    3750 5100
	-1   0    0    -1  
$EndComp
Text HLabel 4450 3700 2    60   Input ~ 0
SCL
Text HLabel 4450 3800 2    60   Input ~ 0
SDA
Text HLabel 4450 3000 2    60   Input ~ 0
INTA
Text HLabel 4450 3100 2    60   Input ~ 0
INTB
Wire Wire Line
	4250 3800 4450 3800
Wire Wire Line
	4250 3700 4450 3700
Wire Wire Line
	3750 4400 3750 5100
Wire Wire Line
	4350 1800 4350 2600
Wire Wire Line
	4350 2600 4250 2600
Wire Wire Line
	3750 1700 3750 2400
Connection ~ 2850 4000
Connection ~ 2850 3900
Connection ~ 2850 3800
Connection ~ 2850 3700
Connection ~ 2850 3600
Connection ~ 2850 3500
Connection ~ 2850 3300
Connection ~ 2850 3200
Connection ~ 2850 3100
Connection ~ 2850 3000
Connection ~ 2850 2900
Connection ~ 2850 2800
Connection ~ 2850 2700
Wire Wire Line
	2850 2600 2850 4450
Connection ~ 3750 1800
Wire Wire Line
	2850 4450 3750 4450
Connection ~ 3750 4450
Wire Wire Line
	4250 3100 4450 3100
Wire Wire Line
	4250 3000 4450 3000
Wire Wire Line
	4700 1800 4700 1950
Wire Wire Line
	4700 2150 4700 2250
Wire Wire Line
	3250 2600 3200 2600
Wire Wire Line
	2900 2600 2850 2600
Wire Wire Line
	3250 2700 3200 2700
Wire Wire Line
	2900 2700 2850 2700
Wire Wire Line
	3250 2800 3200 2800
Wire Wire Line
	3250 2900 3200 2900
Wire Wire Line
	2900 2800 2850 2800
Wire Wire Line
	2900 2900 2850 2900
Wire Wire Line
	3250 3000 3200 3000
Wire Wire Line
	2900 3000 2850 3000
Wire Wire Line
	3250 3100 3200 3100
Wire Wire Line
	2900 3100 2850 3100
Wire Wire Line
	3250 3200 3200 3200
Wire Wire Line
	3250 3300 3200 3300
Wire Wire Line
	2900 3200 2850 3200
Wire Wire Line
	2900 3300 2850 3300
Wire Wire Line
	3250 3500 3200 3500
Wire Wire Line
	2900 3500 2850 3500
Wire Wire Line
	3250 3600 3200 3600
Wire Wire Line
	2900 3600 2850 3600
Wire Wire Line
	3250 3700 3200 3700
Wire Wire Line
	3250 3800 3200 3800
Wire Wire Line
	2900 3700 2850 3700
Wire Wire Line
	2900 3800 2850 3800
Wire Wire Line
	3250 3900 3200 3900
Wire Wire Line
	2900 3900 2850 3900
Wire Wire Line
	3250 4000 3200 4000
Wire Wire Line
	2900 4000 2850 4000
Wire Wire Line
	3250 4100 3200 4100
Wire Wire Line
	3250 4200 3200 4200
Connection ~ 4350 1800
$Comp
L MCP23017 U3
U 1 1 5A7074DE
P 3750 3400
F 0 "U3" H 3650 4425 50  0000 R CNN
F 1 "MCP23017" H 3650 4350 50  0000 R CNN
F 2 "Housings_SSOP:SSOP-28_5.3x10.2mm_Pitch0.65mm" H 3800 2450 50  0001 L CNN
F 3 "" H 4000 4400 50  0001 C CNN
	1    3750 3400
	1    0    0    -1  
$EndComp
$Comp
L KSW K1
U 1 1 5A7074DF
P 3050 2600
F 0 "K1" H 2950 2650 60  0000 C CNN
F 1 "KSW" H 3200 2650 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 3050 2600 60  0001 C CNN
F 3 "" H 3050 2600 60  0001 C CNN
	1    3050 2600
	-1   0    0    -1  
$EndComp
$Comp
L KSW K2
U 1 1 5A7074E0
P 3050 2700
F 0 "K2" H 2950 2750 60  0000 C CNN
F 1 "KSW" H 3200 2750 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 2750 60  0001 C CNN
F 3 "" H 3050 2700 60  0001 C CNN
	1    3050 2700
	-1   0    0    -1  
$EndComp
$Comp
L KSW K3
U 1 1 5A7074E7
P 3050 2800
F 0 "K3" H 2950 2850 60  0000 C CNN
F 1 "KSW" H 3200 2850 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 2850 60  0001 C CNN
F 3 "" H 3050 2800 60  0001 C CNN
	1    3050 2800
	-1   0    0    -1  
$EndComp
$Comp
L KSW K4
U 1 1 5A7074E1
P 3050 2900
F 0 "K4" H 2950 2950 60  0000 C CNN
F 1 "KSW" H 3200 2950 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 2950 60  0001 C CNN
F 3 "" H 3050 2900 60  0001 C CNN
	1    3050 2900
	-1   0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5A6E0377
P 4700 2050
F 0 "C1" H 4710 2120 50  0000 L CNN
F 1 "0.1uF" H 4710 1970 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4700 2050 50  0001 C CNN
F 3 "" H 4700 2050 50  0001 C CNN
	1    4700 2050
	-1   0    0    -1  
$EndComp
$Comp
L KSW K5
U 1 1 5A7074E8
P 3050 3000
F 0 "K5" H 2950 3050 60  0000 C CNN
F 1 "KSW" H 3200 3050 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3050 60  0001 C CNN
F 3 "" H 3050 3000 60  0001 C CNN
	1    3050 3000
	-1   0    0    -1  
$EndComp
$Comp
L KSW K6
U 1 1 5A7074E9
P 3050 3100
F 0 "K6" H 2950 3150 60  0000 C CNN
F 1 "KSW" H 3200 3150 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3150 60  0001 C CNN
F 3 "" H 3050 3100 60  0001 C CNN
	1    3050 3100
	-1   0    0    -1  
$EndComp
$Comp
L KSW K7
U 1 1 5A7074EA
P 3050 3200
F 0 "K7" H 2950 3250 60  0000 C CNN
F 1 "KSW" H 3200 3250 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3250 60  0001 C CNN
F 3 "" H 3050 3200 60  0001 C CNN
	1    3050 3200
	-1   0    0    -1  
$EndComp
$Comp
L KSW K9
U 1 1 5A7074EC
P 3050 3500
F 0 "K9" H 2950 3550 60  0000 C CNN
F 1 "KSW" H 3200 3550 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3550 60  0001 C CNN
F 3 "" H 3050 3500 60  0001 C CNN
	1    3050 3500
	-1   0    0    -1  
$EndComp
$Comp
L KSW K10
U 1 1 5A7074ED
P 3050 3600
F 0 "K10" H 2950 3650 60  0000 C CNN
F 1 "KSW" H 3200 3650 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3650 60  0001 C CNN
F 3 "" H 3050 3600 60  0001 C CNN
	1    3050 3600
	-1   0    0    -1  
$EndComp
$Comp
L KSW K11
U 1 1 5A6E0B66
P 3050 3700
F 0 "K11" H 2950 3750 60  0000 C CNN
F 1 "KSW" H 3200 3750 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3750 60  0001 C CNN
F 3 "" H 3050 3700 60  0001 C CNN
	1    3050 3700
	-1   0    0    -1  
$EndComp
$Comp
L KSW K12
U 1 1 5A6E0BB2
P 3050 3800
F 0 "K12" H 2950 3850 60  0000 C CNN
F 1 "KSW" H 3200 3850 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3850 60  0001 C CNN
F 3 "" H 3050 3800 60  0001 C CNN
	1    3050 3800
	-1   0    0    -1  
$EndComp
$Comp
L KSW K13
U 1 1 5A7074E5
P 3050 3900
F 0 "K13" H 2950 3950 60  0000 C CNN
F 1 "KSW" H 3200 3950 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3950 60  0001 C CNN
F 3 "" H 3050 3900 60  0001 C CNN
	1    3050 3900
	-1   0    0    -1  
$EndComp
$Comp
L KSW K14
U 1 1 5A703CCE
P 3050 4000
F 0 "K14" H 2950 4050 60  0000 C CNN
F 1 "KSW" H 3200 4050 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 4050 60  0001 C CNN
F 3 "" H 3050 4000 60  0001 C CNN
	1    3050 4000
	-1   0    0    -1  
$EndComp
Connection ~ 3750 4800
$Comp
L KSW K8
U 1 1 5A7074EB
P 3050 3300
F 0 "K8" H 2950 3350 60  0000 C CNN
F 1 "KSW" H 3200 3350 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3350 60  0001 C CNN
F 3 "" H 3050 3300 60  0001 C CNN
	1    3050 3300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4250 4000 4450 4000
Wire Wire Line
	4250 4100 4450 4100
Wire Wire Line
	4250 4200 4450 4200
Text HLabel 4450 4000 2    60   Input ~ 0
A0
Text HLabel 4450 4100 2    60   Input ~ 0
A1
Text HLabel 4450 4200 2    60   Input ~ 0
A2
Wire Wire Line
	3750 1800 4700 1800
$Comp
L KSW K15
U 1 1 5A7064D4
P 3050 4100
F 0 "K15" H 2950 4150 60  0000 C CNN
F 1 "KSW" H 3200 4150 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 4150 60  0001 C CNN
F 3 "" H 3050 4100 60  0001 C CNN
	1    3050 4100
	-1   0    0    -1  
$EndComp
$Comp
L KSW K16
U 1 1 5A706517
P 3050 4200
F 0 "K16" H 2950 4250 60  0000 C CNN
F 1 "KSW" H 3200 4250 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 4250 60  0001 C CNN
F 3 "" H 3050 4200 60  0001 C CNN
	1    3050 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2900 4100 2850 4100
Connection ~ 2850 4100
Wire Wire Line
	2900 4200 2850 4200
Connection ~ 2850 4200
$EndSCHEMATC
