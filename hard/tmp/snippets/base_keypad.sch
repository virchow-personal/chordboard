EESchema Schematic File Version 2
LIBS:chordboard-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:standard
LIBS:teensy
LIBS:switches
LIBS:Symbols_DCDC-ACDC-Converter_RevC_20Jul2012
LIBS:Symbols_EN60617_13Mar2013
LIBS:Symbols_EN60617-10_HF-Radio_DRAFT_12Sep2013
LIBS:Symbols_ICs-Diskrete_RevD10
LIBS:Symbols_ICs-Opto_RevB_16Sep2013
LIBS:Symbols_Microcontroller_Philips-NXP_RevA_06Oct2013
LIBS:SymbolsSimilarEN60617+oldDIN617-RevE8
LIBS:Symbols_Socket-DIN41612_RevA
LIBS:Symbols_Transformer-Diskrete_RevA
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Bosch
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:mcp73831t-2aci-ot
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:ESP32
LIBS:ESP32-footprints-Shem-Lib
LIBS:espressif-xess
LIBS:adafruit
LIBS:GeekAmmo
LIBS:LilyPad-Wearables
LIBS:SparkFun-Aesthetics
LIBS:SparkFun-AnalogIC
LIBS:SparkFun-Boards
LIBS:SparkFun-Capacitors
LIBS:SparkFun-Connectors
LIBS:SparkFun-DiscreteSemi
LIBS:SparkFun-Displays
LIBS:SparkFun-Electromechanical
LIBS:SparkFun-FreqCtrl
LIBS:SparkFun-LED
LIBS:SparkFun-Passives
LIBS:SparkFun-PowerIC
LIBS:SparkFun-Resistors
LIBS:SparkFun-Retired
LIBS:SparkFun-RF
LIBS:SparkFun-Sensors
LIBS:Teensy_3_and_LC_Series_Boards_v1.1
LIBS:User-Submitted
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP23017 U3
U 1 1 5A6A250F
P 4750 3650
AR Path="/5A6A462B/5A6A250F" Ref="U3"  Part="1" 
AR Path="/5A6A699C/5A6A250F" Ref="U4"  Part="1" 
F 0 "U3" H 5050 4150 50  0000 R CNN
F 1 "MCP23017" H 5050 4250 50  0000 R CNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 4800 2700 50  0001 L CNN
F 3 "" H 5000 4650 50  0001 C CNN
	1    4750 3650
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR16
U 1 1 5A6A2586
P 4750 1950
AR Path="/5A6A462B/5A6A2586" Ref="#PWR16"  Part="1" 
AR Path="/5A6A699C/5A6A2586" Ref="#PWR19"  Part="1" 
F 0 "#PWR16" H 4750 1800 50  0001 C CNN
F 1 "+3.3V" H 4750 2090 50  0000 C CNN
F 2 "" H 4750 1950 50  0000 C CNN
F 3 "" H 4750 1950 50  0000 C CNN
	1    4750 1950
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR15
U 1 1 5A6A2593
P 3800 2500
AR Path="/5A6A462B/5A6A2593" Ref="#PWR15"  Part="1" 
AR Path="/5A6A699C/5A6A2593" Ref="#PWR18"  Part="1" 
F 0 "#PWR15" H 3800 2250 50  0001 C CNN
F 1 "GND" H 3800 2350 50  0000 C CNN
F 2 "" H 3800 2500 50  0001 C CNN
F 3 "" H 3800 2500 50  0001 C CNN
	1    3800 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4250 4250 4250
Wire Wire Line
	4250 4050 3650 4050
Wire Wire Line
	4250 3950 3650 3950
Wire Wire Line
	4750 4650 4750 5050
Wire Wire Line
	4150 2850 4150 2050
Wire Wire Line
	4250 2850 4150 2850
Wire Wire Line
	4750 1950 4750 2650
Wire Wire Line
	3650 4350 4250 4350
Wire Wire Line
	3650 4450 4250 4450
Connection ~ 5650 4450
Connection ~ 5650 4350
Connection ~ 5650 4250
Connection ~ 5650 4150
Connection ~ 5650 4050
Connection ~ 5650 3950
Connection ~ 5650 3850
Connection ~ 5650 3750
Connection ~ 5650 3550
Connection ~ 5650 3450
Connection ~ 5650 3350
Connection ~ 5650 3250
Connection ~ 5650 3150
Connection ~ 5650 3050
Connection ~ 5650 2950
Wire Wire Line
	5650 2850 5650 4700
Connection ~ 4750 2050
$Comp
L GND #PWR17
U 1 1 5A6CC380
P 4750 5050
AR Path="/5A6A462B/5A6CC380" Ref="#PWR17"  Part="1" 
AR Path="/5A6A699C/5A6CC380" Ref="#PWR20"  Part="1" 
F 0 "#PWR17" H 4750 4800 50  0001 C CNN
F 1 "GND" H 4750 4900 50  0000 C CNN
F 2 "" H 4750 5050 50  0001 C CNN
F 3 "" H 4750 5050 50  0001 C CNN
	1    4750 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4700 4750 4700
Connection ~ 4750 4700
Text HLabel 3650 3950 0    60   Input ~ 0
SCL
Text HLabel 3650 4050 0    60   Input ~ 0
SDA
Text HLabel 3650 4250 0    60   Input ~ 0
A0
Text HLabel 3650 4350 0    60   Input ~ 0
A1
Text HLabel 3650 4450 0    60   Input ~ 0
A2
Wire Wire Line
	3750 4250 3750 4550
Connection ~ 3750 4250
Wire Wire Line
	3950 4350 3950 4550
Connection ~ 3950 4350
Wire Wire Line
	4150 4450 4150 4550
Connection ~ 4150 4450
$Comp
L R_Small R5
U 1 1 5A6CC379
P 4150 4650
AR Path="/5A6A462B/5A6CC379" Ref="R5"  Part="1" 
AR Path="/5A6A699C/5A6CC379" Ref="R8"  Part="1" 
F 0 "R5" H 4180 4670 50  0000 L CNN
F 1 "R_Small" H 4180 4610 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 4150 4650 50  0001 C CNN
F 3 "" H 4150 4650 50  0001 C CNN
	1    4150 4650
	1    0    0    -1  
$EndComp
$Comp
L R_Small R4
U 1 1 5A6A14F9
P 3950 4650
AR Path="/5A6A462B/5A6A14F9" Ref="R4"  Part="1" 
AR Path="/5A6A699C/5A6A14F9" Ref="R7"  Part="1" 
F 0 "R4" H 3980 4670 50  0000 L CNN
F 1 "R_Small" H 3980 4610 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 3950 4650 50  0001 C CNN
F 3 "" H 3950 4650 50  0001 C CNN
	1    3950 4650
	1    0    0    -1  
$EndComp
$Comp
L R_Small R3
U 1 1 5A6CC37B
P 3750 4650
AR Path="/5A6A462B/5A6CC37B" Ref="R3"  Part="1" 
AR Path="/5A6A699C/5A6CC37B" Ref="R6"  Part="1" 
F 0 "R3" H 3780 4670 50  0000 L CNN
F 1 "R_Small" H 3780 4610 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 3750 4650 50  0001 C CNN
F 3 "" H 3750 4650 50  0001 C CNN
	1    3750 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 4750 3750 4900
Wire Wire Line
	3750 4900 4750 4900
Wire Wire Line
	3950 4900 3950 4750
Wire Wire Line
	4150 4900 4150 4750
Connection ~ 3950 4900
Connection ~ 4750 4900
Connection ~ 4150 4900
$Comp
L C_Small C1
U 1 1 5A6CC37C
P 3800 2300
AR Path="/5A6A462B/5A6CC37C" Ref="C1"  Part="1" 
AR Path="/5A6A699C/5A6CC37C" Ref="C2"  Part="1" 
F 0 "C1" H 3810 2370 50  0000 L CNN
F 1 "C_Small" H 3810 2220 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 3800 2300 50  0001 C CNN
F 3 "" H 3800 2300 50  0001 C CNN
	1    3800 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3350 3650 3350
Wire Wire Line
	4200 3250 3650 3250
Text HLabel 3650 3250 0    60   Input ~ 0
INTA
Text HLabel 3650 3350 0    60   Input ~ 0
INTB
Wire Wire Line
	3800 2050 3800 2200
Wire Wire Line
	3800 2400 3800 2500
$Comp
L SPST K1
U 1 1 5A6C9B13
P 5450 2850
AR Path="/5A6A462B/5A6C9B13" Ref="K1"  Part="1" 
AR Path="/5A6A699C/5A6C9B13" Ref="K17"  Part="1" 
F 0 "K1" H 5550 2900 60  0000 C CNN
F 1 "SPST" H 5300 2900 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 2850 60  0001 C CNN
F 3 "" H 5450 2850 60  0001 C CNN
	1    5450 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 2850 5300 2850
Wire Wire Line
	5600 2850 5650 2850
$Comp
L SPST K2
U 1 1 5A6C9EDB
P 5450 2950
AR Path="/5A6A462B/5A6C9EDB" Ref="K2"  Part="1" 
AR Path="/5A6A699C/5A6C9EDB" Ref="K18"  Part="1" 
F 0 "K2" H 5550 3000 60  0000 C CNN
F 1 "SPST" H 5300 3000 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 2950 60  0001 C CNN
F 3 "" H 5450 2950 60  0001 C CNN
	1    5450 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 2950 5300 2950
Wire Wire Line
	5600 2950 5650 2950
$Comp
L SPST K3
U 1 1 5A6C9F73
P 5450 3050
AR Path="/5A6A462B/5A6C9F73" Ref="K3"  Part="1" 
AR Path="/5A6A699C/5A6C9F73" Ref="K19"  Part="1" 
F 0 "K3" H 5550 3100 60  0000 C CNN
F 1 "SPST" H 5300 3100 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 3050 60  0001 C CNN
F 3 "" H 5450 3050 60  0001 C CNN
	1    5450 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3050 5300 3050
$Comp
L SPST K4
U 1 1 5A6C9F7A
P 5450 3150
AR Path="/5A6A462B/5A6C9F7A" Ref="K4"  Part="1" 
AR Path="/5A6A699C/5A6C9F7A" Ref="K20"  Part="1" 
F 0 "K4" H 5550 3200 60  0000 C CNN
F 1 "SPST" H 5300 3200 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 3150 60  0001 C CNN
F 3 "" H 5450 3150 60  0001 C CNN
	1    5450 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3150 5300 3150
Wire Wire Line
	5600 3050 5650 3050
Wire Wire Line
	5600 3150 5650 3150
$Comp
L SPST K5
U 1 1 5A6CA1E8
P 5450 3250
AR Path="/5A6A462B/5A6CA1E8" Ref="K5"  Part="1" 
AR Path="/5A6A699C/5A6CA1E8" Ref="K21"  Part="1" 
F 0 "K5" H 5550 3300 60  0000 C CNN
F 1 "SPST" H 5300 3300 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 3250 60  0001 C CNN
F 3 "" H 5450 3250 60  0001 C CNN
	1    5450 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3250 5300 3250
Wire Wire Line
	5600 3250 5650 3250
$Comp
L SPST K6
U 1 1 5A6CA1F0
P 5450 3350
AR Path="/5A6A462B/5A6CA1F0" Ref="K6"  Part="1" 
AR Path="/5A6A699C/5A6CA1F0" Ref="K22"  Part="1" 
F 0 "K6" H 5550 3400 60  0000 C CNN
F 1 "SPST" H 5300 3400 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 3350 60  0001 C CNN
F 3 "" H 5450 3350 60  0001 C CNN
	1    5450 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3350 5300 3350
Wire Wire Line
	5600 3350 5650 3350
$Comp
L SPST K7
U 1 1 5A6CA1F8
P 5450 3450
AR Path="/5A6A462B/5A6CA1F8" Ref="K7"  Part="1" 
AR Path="/5A6A699C/5A6CA1F8" Ref="K23"  Part="1" 
F 0 "K7" H 5550 3500 60  0000 C CNN
F 1 "SPST" H 5300 3500 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 3450 60  0001 C CNN
F 3 "" H 5450 3450 60  0001 C CNN
	1    5450 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3450 5300 3450
$Comp
L SPST K8
U 1 1 5A6CA1FF
P 5450 3550
AR Path="/5A6A462B/5A6CA1FF" Ref="K8"  Part="1" 
AR Path="/5A6A699C/5A6CA1FF" Ref="K24"  Part="1" 
F 0 "K8" H 5550 3600 60  0000 C CNN
F 1 "SPST" H 5300 3600 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 3550 60  0001 C CNN
F 3 "" H 5450 3550 60  0001 C CNN
	1    5450 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3550 5300 3550
Wire Wire Line
	5600 3450 5650 3450
Wire Wire Line
	5600 3550 5650 3550
$Comp
L SPST K9
U 1 1 5A6CA4FB
P 5450 3750
AR Path="/5A6A462B/5A6CA4FB" Ref="K9"  Part="1" 
AR Path="/5A6A699C/5A6CA4FB" Ref="K25"  Part="1" 
F 0 "K9" H 5550 3800 60  0000 C CNN
F 1 "SPST" H 5300 3800 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 3750 60  0001 C CNN
F 3 "" H 5450 3750 60  0001 C CNN
	1    5450 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3750 5300 3750
Wire Wire Line
	5600 3750 5650 3750
$Comp
L SPST K10
U 1 1 5A6CA503
P 5450 3850
AR Path="/5A6A462B/5A6CA503" Ref="K10"  Part="1" 
AR Path="/5A6A699C/5A6CA503" Ref="K26"  Part="1" 
F 0 "K10" H 5550 3900 60  0000 C CNN
F 1 "SPST" H 5300 3900 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 3850 60  0001 C CNN
F 3 "" H 5450 3850 60  0001 C CNN
	1    5450 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3850 5300 3850
Wire Wire Line
	5600 3850 5650 3850
$Comp
L SPST K11
U 1 1 5A6CA50B
P 5450 3950
AR Path="/5A6A462B/5A6CA50B" Ref="K11"  Part="1" 
AR Path="/5A6A699C/5A6CA50B" Ref="K27"  Part="1" 
F 0 "K11" H 5550 4000 60  0000 C CNN
F 1 "SPST" H 5300 4000 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 3950 60  0001 C CNN
F 3 "" H 5450 3950 60  0001 C CNN
	1    5450 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3950 5300 3950
$Comp
L SPST K12
U 1 1 5A6CA512
P 5450 4050
AR Path="/5A6A462B/5A6CA512" Ref="K12"  Part="1" 
AR Path="/5A6A699C/5A6CA512" Ref="K28"  Part="1" 
F 0 "K12" H 5550 4100 60  0000 C CNN
F 1 "SPST" H 5300 4100 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 4050 60  0001 C CNN
F 3 "" H 5450 4050 60  0001 C CNN
	1    5450 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4050 5300 4050
Wire Wire Line
	5600 3950 5650 3950
Wire Wire Line
	5600 4050 5650 4050
$Comp
L SPST K13
U 1 1 5A6CA51B
P 5450 4150
AR Path="/5A6A462B/5A6CA51B" Ref="K13"  Part="1" 
AR Path="/5A6A699C/5A6CA51B" Ref="K29"  Part="1" 
F 0 "K13" H 5550 4200 60  0000 C CNN
F 1 "SPST" H 5300 4200 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 4150 60  0001 C CNN
F 3 "" H 5450 4150 60  0001 C CNN
	1    5450 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4150 5300 4150
Wire Wire Line
	5600 4150 5650 4150
$Comp
L SPST K14
U 1 1 5A6CA523
P 5450 4250
AR Path="/5A6A462B/5A6CA523" Ref="K14"  Part="1" 
AR Path="/5A6A699C/5A6CA523" Ref="K30"  Part="1" 
F 0 "K14" H 5550 4300 60  0000 C CNN
F 1 "SPST" H 5300 4300 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 4250 60  0001 C CNN
F 3 "" H 5450 4250 60  0001 C CNN
	1    5450 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4250 5300 4250
Wire Wire Line
	5600 4250 5650 4250
$Comp
L SPST K15
U 1 1 5A6CA52B
P 5450 4350
AR Path="/5A6A462B/5A6CA52B" Ref="K15"  Part="1" 
AR Path="/5A6A699C/5A6CA52B" Ref="K31"  Part="1" 
F 0 "K15" H 5550 4400 60  0000 C CNN
F 1 "SPST" H 5300 4400 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 4350 60  0001 C CNN
F 3 "" H 5450 4350 60  0001 C CNN
	1    5450 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4350 5300 4350
$Comp
L SPST K16
U 1 1 5A6CA532
P 5450 4450
AR Path="/5A6A462B/5A6CA532" Ref="K16"  Part="1" 
AR Path="/5A6A699C/5A6CA532" Ref="K32"  Part="1" 
F 0 "K16" H 5550 4500 60  0000 C CNN
F 1 "SPST" H 5300 4500 60  0000 C CNN
F 2 "Keyboard:CHERRY_PCB_100H" H 5450 4450 60  0001 C CNN
F 3 "" H 5450 4450 60  0001 C CNN
	1    5450 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4450 5300 4450
Wire Wire Line
	5600 4350 5650 4350
Wire Wire Line
	5600 4450 5650 4450
Wire Wire Line
	3800 2050 4750 2050
Connection ~ 4150 2050
$EndSCHEMATC
