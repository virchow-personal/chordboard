EESchema Schematic File Version 2
LIBS:chordboard-components
LIBS:chordboard-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:standard
LIBS:teensy
LIBS:switches
LIBS:Symbols_DCDC-ACDC-Converter_RevC_20Jul2012
LIBS:Symbols_EN60617_13Mar2013
LIBS:Symbols_EN60617-10_HF-Radio_DRAFT_12Sep2013
LIBS:Symbols_ICs-Diskrete_RevD10
LIBS:Symbols_ICs-Opto_RevB_16Sep2013
LIBS:Symbols_Microcontroller_Philips-NXP_RevA_06Oct2013
LIBS:SymbolsSimilarEN60617+oldDIN617-RevE8
LIBS:Symbols_Socket-DIN41612_RevA
LIBS:Symbols_Transformer-Diskrete_RevA
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Bosch
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:ESP32
LIBS:ESP32-footprints-Shem-Lib
LIBS:espressif-xess
LIBS:adafruit
LIBS:GeekAmmo
LIBS:LilyPad-Wearables
LIBS:SparkFun-Aesthetics
LIBS:SparkFun-AnalogIC
LIBS:SparkFun-Boards
LIBS:SparkFun-Capacitors
LIBS:SparkFun-Connectors
LIBS:SparkFun-DiscreteSemi
LIBS:SparkFun-Displays
LIBS:SparkFun-Electromechanical
LIBS:SparkFun-FreqCtrl
LIBS:SparkFun-LED
LIBS:SparkFun-Passives
LIBS:SparkFun-PowerIC
LIBS:SparkFun-Resistors
LIBS:SparkFun-Retired
LIBS:SparkFun-RF
LIBS:SparkFun-Sensors
LIBS:Teensy_3_and_LC_Series_Boards_v1.1
LIBS:User-Submitted
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 4 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP23017 U5
U 1 1 5A6CC37D
P 2550 2550
F 0 "U5" H 2850 3050 50  0000 R CNN
F 1 "MCP23017" H 2850 3150 50  0000 R CNN
F 2 "Housings_SSOP:SSOP-28_5.3x10.2mm_Pitch0.65mm" H 2600 1600 50  0001 L CNN
F 3 "" H 2800 3550 50  0001 C CNN
	1    2550 2550
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR023
U 1 1 5A6CC37E
P 2300 850
F 0 "#PWR023" H 2300 700 50  0001 C CNN
F 1 "+3.3V" H 2300 990 50  0000 C CNN
F 2 "" H 2300 850 50  0000 C CNN
F 3 "" H 2300 850 50  0000 C CNN
	1    2300 850 
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR024
U 1 1 5A6CC37F
P 2900 1400
F 0 "#PWR024" H 2900 1150 50  0001 C CNN
F 1 "GND" H 2900 1250 50  0000 C CNN
F 2 "" H 2900 1400 50  0001 C CNN
F 3 "" H 2900 1400 50  0001 C CNN
	1    2900 1400
	1    0    0    -1  
$EndComp
Connection ~ 2550 950 
Wire Wire Line
	1450 3150 2050 3150
Wire Wire Line
	2050 2950 1450 2950
Wire Wire Line
	2050 2850 1450 2850
Wire Wire Line
	2550 3550 2550 3950
Wire Wire Line
	1950 1750 1950 950 
Wire Wire Line
	2050 1750 1950 1750
Wire Wire Line
	2550 950  2550 1550
Wire Wire Line
	1450 3250 2050 3250
Wire Wire Line
	1450 3350 2050 3350
Wire Wire Line
	2300 950  2300 850 
Connection ~ 2300 950 
$Comp
L GND #PWR025
U 1 1 5A6A31F6
P 2550 3950
F 0 "#PWR025" H 2550 3700 50  0001 C CNN
F 1 "GND" H 2550 3800 50  0000 C CNN
F 2 "" H 2550 3950 50  0001 C CNN
F 3 "" H 2550 3950 50  0001 C CNN
	1    2550 3950
	1    0    0    -1  
$EndComp
Connection ~ 2550 3600
Text HLabel 1450 2850 0    60   Input ~ 0
SCL
Text HLabel 1450 2950 0    60   Input ~ 0
SDA
Text HLabel 1450 3150 0    60   Input ~ 0
A0
Text HLabel 1450 3250 0    60   Input ~ 0
A1
Text HLabel 1450 3350 0    60   Input ~ 0
A2
Wire Wire Line
	1950 950  2900 950 
Connection ~ 1550 3150
Connection ~ 1750 3250
Connection ~ 1950 3350
Connection ~ 2550 3800
Wire Wire Line
	2050 2250 1500 2250
Wire Wire Line
	2050 2150 1500 2150
Text HLabel 1500 2150 0    60   Input ~ 0
INTA
Text HLabel 1500 2250 0    60   Input ~ 0
INTB
Wire Wire Line
	2900 1300 2900 1400
$Comp
L ROTARY_ENCODER_afshar ENC1
U 1 1 5A6B0B21
P 5900 1300
F 0 "ENC1" H 5900 1700 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 5950 950 60  0000 C CNN
F 2 "Rotary-Encoders:BI_EN12-HS22AF30" H 5900 850 60  0000 C CNN
F 3 "" H 5900 1300 60  0000 C CNN
	1    5900 1300
	1    0    0    1   
$EndComp
Connection ~ 6200 1300
Wire Wire Line
	6400 1300 6200 1300
Wire Wire Line
	6200 1100 6200 1450
Text Label 3050 1750 0    60   ~ 0
RE1_SW
Text Label 3050 1850 0    60   ~ 0
RE2_SW
Text Label 3050 1950 0    60   ~ 0
RE3_SW
Text Label 3050 2050 0    60   ~ 0
RE4_SW
Text Label 3050 2150 0    60   ~ 0
RE5_SW
Text Label 3050 2250 0    60   ~ 0
RE6_SW
Text Label 3050 2350 0    60   ~ 0
RE7_SW
Text Label 3050 2450 0    60   ~ 0
RE8_SW
Text Label 3050 2650 0    60   ~ 0
RE9_SW
Text Label 3050 2750 0    60   ~ 0
RE10_SW
$Comp
L C_Small C7
U 1 1 5A6C4420
P 5650 1800
F 0 "C7" H 5660 1870 50  0000 L CNN
F 1 "C_Small" H 5660 1720 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 5650 1800 50  0001 C CNN
F 3 "" H 5650 1800 50  0001 C CNN
	1    5650 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1550 5650 1700
$Comp
L C_Small C4
U 1 1 5A6C480B
P 5150 1800
F 0 "C4" H 5160 1870 50  0000 L CNN
F 1 "C_Small" H 5160 1720 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 5150 1800 50  0001 C CNN
F 3 "" H 5150 1800 50  0001 C CNN
	1    5150 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1350 5150 1350
Wire Wire Line
	5150 1350 5150 1700
Wire Wire Line
	5150 1900 5650 1900
Wire Wire Line
	5400 1900 5400 2000
Connection ~ 5400 1900
Wire Wire Line
	6400 1300 6400 1450
$Comp
L GND #PWR026
U 1 1 5A6C5820
P 6400 1450
F 0 "#PWR026" H 6400 1200 50  0001 C CNN
F 1 "GND" H 6400 1300 50  0000 C CNN
F 2 "" H 6400 1450 50  0001 C CNN
F 3 "" H 6400 1450 50  0001 C CNN
	1    6400 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR027
U 1 1 5A6C5844
P 5400 2000
F 0 "#PWR027" H 5400 1750 50  0001 C CNN
F 1 "GND" H 5400 1850 50  0000 C CNN
F 2 "" H 5400 2000 50  0001 C CNN
F 3 "" H 5400 2000 50  0001 C CNN
	1    5400 2000
	1    0    0    -1  
$EndComp
Text HLabel 5150 1350 0    60   Input ~ 0
RE1_B
Text HLabel 5650 1550 0    60   Input ~ 0
RE1_A
Text Label 5650 1200 2    60   ~ 0
RE1_SW
$Comp
L ROTARY_ENCODER_afshar ENC2
U 1 1 5A6C5DDE
P 5900 2850
F 0 "ENC2" H 5900 3250 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 5950 2500 60  0000 C CNN
F 2 "Rotary-Encoders:BI_EN12-HS22AF30" H 5900 2400 60  0000 C CNN
F 3 "" H 5900 2850 60  0000 C CNN
	1    5900 2850
	1    0    0    1   
$EndComp
Connection ~ 6200 2850
Wire Wire Line
	6400 2850 6200 2850
Wire Wire Line
	6200 2650 6200 3000
$Comp
L C_Small C8
U 1 1 5A6C5DE7
P 5650 3350
F 0 "C8" H 5660 3420 50  0000 L CNN
F 1 "C_Small" H 5660 3270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 5650 3350 50  0001 C CNN
F 3 "" H 5650 3350 50  0001 C CNN
	1    5650 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3100 5650 3250
$Comp
L C_Small C5
U 1 1 5A6C5DEE
P 5150 3350
F 0 "C5" H 5160 3420 50  0000 L CNN
F 1 "C_Small" H 5160 3270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 5150 3350 50  0001 C CNN
F 3 "" H 5150 3350 50  0001 C CNN
	1    5150 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2900 5150 2900
Wire Wire Line
	5150 2900 5150 3250
Wire Wire Line
	5150 3450 5650 3450
Wire Wire Line
	5400 3450 5400 3550
Connection ~ 5400 3450
Wire Wire Line
	6400 2850 6400 3000
$Comp
L GND #PWR028
U 1 1 5A6C5DFA
P 6400 3000
F 0 "#PWR028" H 6400 2750 50  0001 C CNN
F 1 "GND" H 6400 2850 50  0000 C CNN
F 2 "" H 6400 3000 50  0001 C CNN
F 3 "" H 6400 3000 50  0001 C CNN
	1    6400 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR029
U 1 1 5A6C5E00
P 5400 3550
F 0 "#PWR029" H 5400 3300 50  0001 C CNN
F 1 "GND" H 5400 3400 50  0000 C CNN
F 2 "" H 5400 3550 50  0001 C CNN
F 3 "" H 5400 3550 50  0001 C CNN
	1    5400 3550
	1    0    0    -1  
$EndComp
Text HLabel 5150 2900 0    60   Input ~ 0
RE5_B
Text HLabel 5650 3100 0    60   Input ~ 0
RE5_A
Text Label 5650 2750 2    60   ~ 0
RE5_SW
$Comp
L ROTARY_ENCODER_afshar ENC4
U 1 1 5A6C60C9
P 8100 1300
F 0 "ENC4" H 8100 1700 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 8150 950 60  0000 C CNN
F 2 "Rotary-Encoders:BI_EN12-HS22AF30" H 8100 850 60  0000 C CNN
F 3 "" H 8100 1300 60  0000 C CNN
	1    8100 1300
	1    0    0    1   
$EndComp
Connection ~ 8400 1300
Wire Wire Line
	8600 1300 8400 1300
Wire Wire Line
	8400 1100 8400 1450
$Comp
L C_Small C13
U 1 1 5A6C60D2
P 7850 1800
F 0 "C13" H 7860 1870 50  0000 L CNN
F 1 "C_Small" H 7860 1720 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 7850 1800 50  0001 C CNN
F 3 "" H 7850 1800 50  0001 C CNN
	1    7850 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 1550 7850 1700
$Comp
L C_Small C10
U 1 1 5A6C60D9
P 7350 1800
F 0 "C10" H 7360 1870 50  0000 L CNN
F 1 "C_Small" H 7360 1720 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 7350 1800 50  0001 C CNN
F 3 "" H 7350 1800 50  0001 C CNN
	1    7350 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 1350 7350 1350
Wire Wire Line
	7350 1350 7350 1700
Wire Wire Line
	7350 1900 7850 1900
Wire Wire Line
	7600 1900 7600 2000
Connection ~ 7600 1900
Wire Wire Line
	8600 1300 8600 1450
$Comp
L GND #PWR030
U 1 1 5A6C60E5
P 8600 1450
F 0 "#PWR030" H 8600 1200 50  0001 C CNN
F 1 "GND" H 8600 1300 50  0000 C CNN
F 2 "" H 8600 1450 50  0001 C CNN
F 3 "" H 8600 1450 50  0001 C CNN
	1    8600 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 5A6C60EB
P 7600 2000
F 0 "#PWR031" H 7600 1750 50  0001 C CNN
F 1 "GND" H 7600 1850 50  0000 C CNN
F 2 "" H 7600 2000 50  0001 C CNN
F 3 "" H 7600 2000 50  0001 C CNN
	1    7600 2000
	1    0    0    -1  
$EndComp
Text HLabel 7350 1350 0    60   Input ~ 0
RE2_B
Text HLabel 7850 1550 0    60   Input ~ 0
RE2_A
Text Label 7850 1200 2    60   ~ 0
RE2_SW
$Comp
L ROTARY_ENCODER_afshar ENC5
U 1 1 5A6C60F4
P 8100 2850
F 0 "ENC5" H 8100 3250 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 8150 2500 60  0000 C CNN
F 2 "Rotary-Encoders:BI_EN12-HS22AF30" H 8100 2400 60  0000 C CNN
F 3 "" H 8100 2850 60  0000 C CNN
	1    8100 2850
	1    0    0    1   
$EndComp
Connection ~ 8400 2850
Wire Wire Line
	8600 2850 8400 2850
Wire Wire Line
	8400 2650 8400 3000
$Comp
L C_Small C14
U 1 1 5A6C60FD
P 7850 3350
F 0 "C14" H 7860 3420 50  0000 L CNN
F 1 "C_Small" H 7860 3270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 7850 3350 50  0001 C CNN
F 3 "" H 7850 3350 50  0001 C CNN
	1    7850 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 3100 7850 3250
$Comp
L C_Small C11
U 1 1 5A6C6104
P 7350 3350
F 0 "C11" H 7360 3420 50  0000 L CNN
F 1 "C_Small" H 7360 3270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 7350 3350 50  0001 C CNN
F 3 "" H 7350 3350 50  0001 C CNN
	1    7350 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 2900 7350 2900
Wire Wire Line
	7350 2900 7350 3250
Wire Wire Line
	7350 3450 7850 3450
Wire Wire Line
	7600 3450 7600 3550
Connection ~ 7600 3450
Wire Wire Line
	8600 2850 8600 3000
$Comp
L GND #PWR032
U 1 1 5A6C6110
P 8600 3000
F 0 "#PWR032" H 8600 2750 50  0001 C CNN
F 1 "GND" H 8600 2850 50  0000 C CNN
F 2 "" H 8600 3000 50  0001 C CNN
F 3 "" H 8600 3000 50  0001 C CNN
	1    8600 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR033
U 1 1 5A6C6116
P 7600 3550
F 0 "#PWR033" H 7600 3300 50  0001 C CNN
F 1 "GND" H 7600 3400 50  0000 C CNN
F 2 "" H 7600 3550 50  0001 C CNN
F 3 "" H 7600 3550 50  0001 C CNN
	1    7600 3550
	1    0    0    -1  
$EndComp
Text HLabel 7350 2900 0    60   Input ~ 0
RE6_B
Text HLabel 7850 3100 0    60   Input ~ 0
RE6_A
Text Label 7850 2750 2    60   ~ 0
RE6_SW
$Comp
L ROTARY_ENCODER_afshar ENC7
U 1 1 5A6C6624
P 10100 1300
F 0 "ENC7" H 10100 1700 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 10150 950 60  0000 C CNN
F 2 "Rotary-Encoders:BI_EN12-HS22AF30" H 10100 850 60  0000 C CNN
F 3 "" H 10100 1300 60  0000 C CNN
	1    10100 1300
	1    0    0    1   
$EndComp
Connection ~ 10400 1300
Wire Wire Line
	10600 1300 10400 1300
Wire Wire Line
	10400 1100 10400 1450
$Comp
L C_Small C18
U 1 1 5A6C662D
P 9850 1800
F 0 "C18" H 9860 1870 50  0000 L CNN
F 1 "C_Small" H 9860 1720 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9850 1800 50  0001 C CNN
F 3 "" H 9850 1800 50  0001 C CNN
	1    9850 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 1550 9850 1700
$Comp
L C_Small C16
U 1 1 5A6C6634
P 9350 1800
F 0 "C16" H 9360 1870 50  0000 L CNN
F 1 "C_Small" H 9360 1720 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9350 1800 50  0001 C CNN
F 3 "" H 9350 1800 50  0001 C CNN
	1    9350 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 1350 9350 1350
Wire Wire Line
	9350 1350 9350 1700
Wire Wire Line
	9350 1900 9850 1900
Wire Wire Line
	9600 1900 9600 2000
Connection ~ 9600 1900
Wire Wire Line
	10600 1300 10600 1450
$Comp
L GND #PWR034
U 1 1 5A6C6640
P 10600 1450
F 0 "#PWR034" H 10600 1200 50  0001 C CNN
F 1 "GND" H 10600 1300 50  0000 C CNN
F 2 "" H 10600 1450 50  0001 C CNN
F 3 "" H 10600 1450 50  0001 C CNN
	1    10600 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR035
U 1 1 5A6C6646
P 9600 2000
F 0 "#PWR035" H 9600 1750 50  0001 C CNN
F 1 "GND" H 9600 1850 50  0000 C CNN
F 2 "" H 9600 2000 50  0001 C CNN
F 3 "" H 9600 2000 50  0001 C CNN
	1    9600 2000
	1    0    0    -1  
$EndComp
Text HLabel 9350 1350 0    60   Input ~ 0
RE3_B
Text HLabel 9850 1550 0    60   Input ~ 0
RE3_A
Text Label 9850 1200 2    60   ~ 0
RE3_SW
$Comp
L ROTARY_ENCODER_afshar ENC8
U 1 1 5A6C664F
P 10100 2850
F 0 "ENC8" H 10100 3250 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 10150 2500 60  0000 C CNN
F 2 "Rotary-Encoders:BI_EN12-HS22AF30" H 10100 2400 60  0000 C CNN
F 3 "" H 10100 2850 60  0000 C CNN
	1    10100 2850
	1    0    0    1   
$EndComp
Connection ~ 10400 2850
Wire Wire Line
	10600 2850 10400 2850
Wire Wire Line
	10400 2650 10400 3000
$Comp
L C_Small C19
U 1 1 5A6C6658
P 9850 3350
F 0 "C19" H 9860 3420 50  0000 L CNN
F 1 "C_Small" H 9860 3270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9850 3350 50  0001 C CNN
F 3 "" H 9850 3350 50  0001 C CNN
	1    9850 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 3100 9850 3250
$Comp
L C_Small C17
U 1 1 5A6C665F
P 9350 3350
F 0 "C17" H 9360 3420 50  0000 L CNN
F 1 "C_Small" H 9360 3270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9350 3350 50  0001 C CNN
F 3 "" H 9350 3350 50  0001 C CNN
	1    9350 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 2900 9350 2900
Wire Wire Line
	9350 2900 9350 3250
Wire Wire Line
	9350 3450 9850 3450
Wire Wire Line
	9600 3450 9600 3550
Connection ~ 9600 3450
Wire Wire Line
	10600 2850 10600 3000
$Comp
L GND #PWR036
U 1 1 5A6C666B
P 10600 3000
F 0 "#PWR036" H 10600 2750 50  0001 C CNN
F 1 "GND" H 10600 2850 50  0000 C CNN
F 2 "" H 10600 3000 50  0001 C CNN
F 3 "" H 10600 3000 50  0001 C CNN
	1    10600 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR037
U 1 1 5A6C6671
P 9600 3550
F 0 "#PWR037" H 9600 3300 50  0001 C CNN
F 1 "GND" H 9600 3400 50  0000 C CNN
F 2 "" H 9600 3550 50  0001 C CNN
F 3 "" H 9600 3550 50  0001 C CNN
	1    9600 3550
	1    0    0    -1  
$EndComp
Text HLabel 9350 2900 0    60   Input ~ 0
RE7_B
Text HLabel 9850 3100 0    60   Input ~ 0
RE7_A
Text Label 9850 2750 2    60   ~ 0
RE7_SW
$Comp
L ROTARY_ENCODER_afshar ENC9
U 1 1 5A6C667A
P 12300 1300
F 0 "ENC9" H 12300 1700 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 12350 950 60  0000 C CNN
F 2 "Rotary-Encoders:BI_EN12-HS22AF30" H 12300 850 60  0000 C CNN
F 3 "" H 12300 1300 60  0000 C CNN
	1    12300 1300
	1    0    0    1   
$EndComp
Connection ~ 12600 1300
Wire Wire Line
	12800 1300 12600 1300
Wire Wire Line
	12600 1100 12600 1450
$Comp
L C_Small C22
U 1 1 5A6C6683
P 12050 1800
F 0 "C22" H 12060 1870 50  0000 L CNN
F 1 "C_Small" H 12060 1720 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 12050 1800 50  0001 C CNN
F 3 "" H 12050 1800 50  0001 C CNN
	1    12050 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	12050 1550 12050 1700
$Comp
L C_Small C20
U 1 1 5A6C668A
P 11550 1800
F 0 "C20" H 11560 1870 50  0000 L CNN
F 1 "C_Small" H 11560 1720 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 11550 1800 50  0001 C CNN
F 3 "" H 11550 1800 50  0001 C CNN
	1    11550 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	12050 1350 11550 1350
Wire Wire Line
	11550 1350 11550 1700
Wire Wire Line
	11550 1900 12050 1900
Wire Wire Line
	11800 1900 11800 2000
Connection ~ 11800 1900
Wire Wire Line
	12800 1300 12800 1450
$Comp
L GND #PWR038
U 1 1 5A6C6696
P 12800 1450
F 0 "#PWR038" H 12800 1200 50  0001 C CNN
F 1 "GND" H 12800 1300 50  0000 C CNN
F 2 "" H 12800 1450 50  0001 C CNN
F 3 "" H 12800 1450 50  0001 C CNN
	1    12800 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR039
U 1 1 5A6C669C
P 11800 2000
F 0 "#PWR039" H 11800 1750 50  0001 C CNN
F 1 "GND" H 11800 1850 50  0000 C CNN
F 2 "" H 11800 2000 50  0001 C CNN
F 3 "" H 11800 2000 50  0001 C CNN
	1    11800 2000
	1    0    0    -1  
$EndComp
Text HLabel 11550 1350 0    60   Input ~ 0
RE4_B
Text HLabel 12050 1550 0    60   Input ~ 0
RE4_A
Text Label 12050 1200 2    60   ~ 0
RE4_SW
$Comp
L ROTARY_ENCODER_afshar ENC10
U 1 1 5A6C66A5
P 12300 2850
F 0 "ENC10" H 12300 3250 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 12350 2500 60  0000 C CNN
F 2 "Rotary-Encoders:BI_EN12-HS22AF30" H 12300 2400 60  0000 C CNN
F 3 "" H 12300 2850 60  0000 C CNN
	1    12300 2850
	1    0    0    1   
$EndComp
Connection ~ 12600 2850
Wire Wire Line
	12800 2850 12600 2850
Wire Wire Line
	12600 2650 12600 3000
$Comp
L C_Small C23
U 1 1 5A6C66AE
P 12050 3350
F 0 "C23" H 12060 3420 50  0000 L CNN
F 1 "C_Small" H 12060 3270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 12050 3350 50  0001 C CNN
F 3 "" H 12050 3350 50  0001 C CNN
	1    12050 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	12050 3100 12050 3250
$Comp
L C_Small C21
U 1 1 5A6C66B5
P 11550 3350
F 0 "C21" H 11560 3420 50  0000 L CNN
F 1 "C_Small" H 11560 3270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 11550 3350 50  0001 C CNN
F 3 "" H 11550 3350 50  0001 C CNN
	1    11550 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	12050 2900 11550 2900
Wire Wire Line
	11550 2900 11550 3250
Wire Wire Line
	11550 3450 12050 3450
Wire Wire Line
	11800 3450 11800 3550
Connection ~ 11800 3450
Wire Wire Line
	12800 2850 12800 3000
$Comp
L GND #PWR040
U 1 1 5A6C66C1
P 12800 3000
F 0 "#PWR040" H 12800 2750 50  0001 C CNN
F 1 "GND" H 12800 2850 50  0000 C CNN
F 2 "" H 12800 3000 50  0001 C CNN
F 3 "" H 12800 3000 50  0001 C CNN
	1    12800 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR041
U 1 1 5A6C66C7
P 11800 3550
F 0 "#PWR041" H 11800 3300 50  0001 C CNN
F 1 "GND" H 11800 3400 50  0000 C CNN
F 2 "" H 11800 3550 50  0001 C CNN
F 3 "" H 11800 3550 50  0001 C CNN
	1    11800 3550
	1    0    0    -1  
$EndComp
Text HLabel 11550 2900 0    60   Input ~ 0
RE8_B
Text HLabel 12050 3100 0    60   Input ~ 0
RE8_A
Text Label 12050 2750 2    60   ~ 0
RE8_SW
$Comp
L ROTARY_ENCODER_afshar ENC3
U 1 1 5A6C6913
P 5950 4450
F 0 "ENC3" H 5950 4850 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 6000 4100 60  0000 C CNN
F 2 "Rotary-Encoders:BI_EN12-HS22AF30" H 5950 4000 60  0000 C CNN
F 3 "" H 5950 4450 60  0000 C CNN
	1    5950 4450
	1    0    0    1   
$EndComp
Connection ~ 6250 4450
Wire Wire Line
	6450 4450 6250 4450
Wire Wire Line
	6250 4250 6250 4600
$Comp
L C_Small C9
U 1 1 5A6C691C
P 5700 4950
F 0 "C9" H 5710 5020 50  0000 L CNN
F 1 "C_Small" H 5710 4870 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 5700 4950 50  0001 C CNN
F 3 "" H 5700 4950 50  0001 C CNN
	1    5700 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 4700 5700 4850
$Comp
L C_Small C6
U 1 1 5A6C6923
P 5200 4950
F 0 "C6" H 5210 5020 50  0000 L CNN
F 1 "C_Small" H 5210 4870 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 5200 4950 50  0001 C CNN
F 3 "" H 5200 4950 50  0001 C CNN
	1    5200 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 4500 5200 4500
Wire Wire Line
	5200 4500 5200 4850
Wire Wire Line
	5200 5050 5700 5050
Wire Wire Line
	5450 5050 5450 5150
Connection ~ 5450 5050
Wire Wire Line
	6450 4450 6450 4600
$Comp
L GND #PWR042
U 1 1 5A6C692F
P 6450 4600
F 0 "#PWR042" H 6450 4350 50  0001 C CNN
F 1 "GND" H 6450 4450 50  0000 C CNN
F 2 "" H 6450 4600 50  0001 C CNN
F 3 "" H 6450 4600 50  0001 C CNN
	1    6450 4600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR043
U 1 1 5A6C6935
P 5450 5150
F 0 "#PWR043" H 5450 4900 50  0001 C CNN
F 1 "GND" H 5450 5000 50  0000 C CNN
F 2 "" H 5450 5150 50  0001 C CNN
F 3 "" H 5450 5150 50  0001 C CNN
	1    5450 5150
	1    0    0    -1  
$EndComp
Text HLabel 5200 4500 0    60   Input ~ 0
RE9_B
Text HLabel 5700 4700 0    60   Input ~ 0
RE9_A
Text Label 5700 4350 2    60   ~ 0
RE9_SW
$Comp
L ROTARY_ENCODER_afshar ENC6
U 1 1 5A6C693E
P 8150 4450
F 0 "ENC6" H 8150 4850 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 8200 4100 60  0000 C CNN
F 2 "Rotary-Encoders:BI_EN12-HS22AF30" H 8150 4000 60  0000 C CNN
F 3 "" H 8150 4450 60  0000 C CNN
	1    8150 4450
	1    0    0    1   
$EndComp
Connection ~ 8450 4450
Wire Wire Line
	8650 4450 8450 4450
Wire Wire Line
	8450 4250 8450 4600
$Comp
L C_Small C15
U 1 1 5A6C6947
P 7900 4950
F 0 "C15" H 7910 5020 50  0000 L CNN
F 1 "C_Small" H 7910 4870 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 7900 4950 50  0001 C CNN
F 3 "" H 7900 4950 50  0001 C CNN
	1    7900 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 4700 7900 4850
$Comp
L C_Small C12
U 1 1 5A6C694E
P 7400 4950
F 0 "C12" H 7410 5020 50  0000 L CNN
F 1 "C_Small" H 7410 4870 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 7400 4950 50  0001 C CNN
F 3 "" H 7400 4950 50  0001 C CNN
	1    7400 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 4500 7400 4500
Wire Wire Line
	7400 4500 7400 4850
Wire Wire Line
	7400 5050 7900 5050
Wire Wire Line
	7650 5050 7650 5150
Connection ~ 7650 5050
Wire Wire Line
	8650 4450 8650 4600
$Comp
L GND #PWR044
U 1 1 5A6C695A
P 8650 4600
F 0 "#PWR044" H 8650 4350 50  0001 C CNN
F 1 "GND" H 8650 4450 50  0000 C CNN
F 2 "" H 8650 4600 50  0001 C CNN
F 3 "" H 8650 4600 50  0001 C CNN
	1    8650 4600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR045
U 1 1 5A6C6960
P 7650 5150
F 0 "#PWR045" H 7650 4900 50  0001 C CNN
F 1 "GND" H 7650 5000 50  0000 C CNN
F 2 "" H 7650 5150 50  0001 C CNN
F 3 "" H 7650 5150 50  0001 C CNN
	1    7650 5150
	1    0    0    -1  
$EndComp
Text HLabel 7400 4500 0    60   Input ~ 0
RE10_B
Text HLabel 7900 4700 0    60   Input ~ 0
RE10_A
Text Label 7900 4350 2    60   ~ 0
RE10_SW
Wire Wire Line
	3050 2850 3100 2850
Wire Wire Line
	3050 2950 3100 2950
Wire Wire Line
	3050 3050 3100 3050
Wire Wire Line
	3050 3150 3100 3150
Wire Wire Line
	3050 3250 3100 3250
Wire Wire Line
	3050 3350 3100 3350
$Comp
L SPST SW1
U 1 1 5A6C7E9C
P 3250 2850
F 0 "SW1" H 3350 2900 60  0000 C CNN
F 1 "SPST" H 3150 2900 60  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_PTS645" H 3250 2850 60  0001 C CNN
F 3 "" H 3250 2850 60  0001 C CNN
	1    3250 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2850 3450 2850
$Comp
L SPST SW2
U 1 1 5A6D14AF
P 3250 2950
F 0 "SW2" H 3350 3000 60  0000 C CNN
F 1 "SPST" H 3150 3000 60  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_PTS645" H 3250 2950 60  0001 C CNN
F 3 "" H 3250 2950 60  0001 C CNN
	1    3250 2950
	1    0    0    -1  
$EndComp
$Comp
L SPST SW3
U 1 1 5A6D152E
P 3250 3050
F 0 "SW3" H 3350 3100 60  0000 C CNN
F 1 "SPST" H 3150 3100 60  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_PTS645" H 3250 3050 60  0001 C CNN
F 3 "" H 3250 3050 60  0001 C CNN
	1    3250 3050
	1    0    0    -1  
$EndComp
$Comp
L SPST SW4
U 1 1 5A6D15B0
P 3250 3150
F 0 "SW4" H 3350 3200 60  0000 C CNN
F 1 "SPST" H 3150 3200 60  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_PTS645" H 3250 3150 60  0001 C CNN
F 3 "" H 3250 3150 60  0001 C CNN
	1    3250 3150
	1    0    0    -1  
$EndComp
$Comp
L SPST SW5
U 1 1 5A6D16AA
P 3250 3250
F 0 "SW5" H 3350 3300 60  0000 C CNN
F 1 "SPST" H 3150 3300 60  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_PTS645" H 3250 3250 60  0001 C CNN
F 3 "" H 3250 3250 60  0001 C CNN
	1    3250 3250
	1    0    0    -1  
$EndComp
$Comp
L SPST SW6
U 1 1 5A6D1732
P 3250 3350
F 0 "SW6" H 3350 3400 60  0000 C CNN
F 1 "SPST" H 3150 3400 60  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_PTS645" H 3250 3350 60  0001 C CNN
F 3 "" H 3250 3350 60  0001 C CNN
	1    3250 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2950 3450 2950
Wire Wire Line
	3450 2850 3450 3600
Wire Wire Line
	3450 3050 3400 3050
Wire Wire Line
	3450 3150 3400 3150
Connection ~ 3450 3050
Wire Wire Line
	3450 3250 3400 3250
Connection ~ 3450 3150
Wire Wire Line
	3450 3350 3400 3350
Connection ~ 3450 3250
Connection ~ 3450 2950
Wire Wire Line
	3450 3600 2550 3600
Connection ~ 3450 3350
$Comp
L C_Small C3
U 1 1 5A6D214A
P 2900 1200
F 0 "C3" H 2910 1270 50  0000 L CNN
F 1 "C_Small" H 2910 1120 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2900 1200 50  0001 C CNN
F 3 "" H 2900 1200 50  0001 C CNN
	1    2900 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 950  2900 1100
$EndSCHEMATC
