EESchema Schematic File Version 2
LIBS:chordboard-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:standard
LIBS:teensy
LIBS:switches
LIBS:Symbols_DCDC-ACDC-Converter_RevC_20Jul2012
LIBS:Symbols_EN60617_13Mar2013
LIBS:Symbols_EN60617-10_HF-Radio_DRAFT_12Sep2013
LIBS:Symbols_ICs-Diskrete_RevD10
LIBS:Symbols_ICs-Opto_RevB_16Sep2013
LIBS:Symbols_Microcontroller_Philips-NXP_RevA_06Oct2013
LIBS:SymbolsSimilarEN60617+oldDIN617-RevE8
LIBS:Symbols_Socket-DIN41612_RevA
LIBS:Symbols_Transformer-Diskrete_RevA
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Bosch
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:mcp73831t-2aci-ot
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:ESP32
LIBS:ESP32-footprints-Shem-Lib
LIBS:espressif-xess
LIBS:adafruit
LIBS:GeekAmmo
LIBS:LilyPad-Wearables
LIBS:SparkFun-Aesthetics
LIBS:SparkFun-AnalogIC
LIBS:SparkFun-Boards
LIBS:SparkFun-Capacitors
LIBS:SparkFun-Connectors
LIBS:SparkFun-DiscreteSemi
LIBS:SparkFun-Displays
LIBS:SparkFun-Electromechanical
LIBS:SparkFun-FreqCtrl
LIBS:SparkFun-LED
LIBS:SparkFun-Passives
LIBS:SparkFun-PowerIC
LIBS:SparkFun-Resistors
LIBS:SparkFun-Retired
LIBS:SparkFun-RF
LIBS:SparkFun-Sensors
LIBS:Teensy_3_and_LC_Series_Boards_v1.1
LIBS:User-Submitted
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R?
U 1 1 5A6F6396
P 6350 4850
F 0 "R?" V 6250 4800 50  0000 C CNN
F 1 "4.7K" V 6350 4850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6280 4850 50  0001 C CNN
F 3 "" H 6350 4850 50  0000 C CNN
	1    6350 4850
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5A6F639D
P 6450 4850
F 0 "R?" V 6550 4800 50  0000 C CNN
F 1 "4.7K" V 6450 4850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6380 4850 50  0001 C CNN
F 3 "" H 6450 4850 50  0000 C CNN
	1    6450 4850
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR?
U 1 1 5A6F63A4
P 6400 3700
F 0 "#PWR?" H 6400 3550 50  0001 C CNN
F 1 "+3.3V" H 6400 3840 50  0000 C CNN
F 2 "" H 6400 3700 50  0000 C CNN
F 3 "" H 6400 3700 50  0000 C CNN
	1    6400 3700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A6F63AA
P 3250 1000
F 0 "#PWR?" H 3250 750 50  0001 C CNN
F 1 "GND" H 3250 850 50  0000 C CNN
F 2 "" H 3250 1000 50  0000 C CNN
F 3 "" H 3250 1000 50  0000 C CNN
	1    3250 1000
	-1   0    0    1   
$EndComp
Text Label 3250 1600 2    60   ~ 0
UART_TX
Text Label 3250 1500 2    60   ~ 0
UART_RX
NoConn ~ 3250 3800
NoConn ~ 5250 4900
NoConn ~ 5250 4600
NoConn ~ 5250 4500
NoConn ~ 5250 3700
NoConn ~ 5250 3600
NoConn ~ 5250 3500
NoConn ~ 5250 3400
NoConn ~ 5250 3300
NoConn ~ 5250 3200
NoConn ~ 5250 3100
NoConn ~ 5250 3000
NoConn ~ 5250 2900
NoConn ~ 5250 2800
NoConn ~ 5250 2700
NoConn ~ 5250 2600
NoConn ~ 5250 2500
NoConn ~ 5250 2400
NoConn ~ 5250 2300
NoConn ~ 5250 2000
NoConn ~ 5250 1900
NoConn ~ 5250 1800
NoConn ~ 5250 1700
NoConn ~ 5250 1600
NoConn ~ 5250 1500
NoConn ~ 5250 1400
Text Label 3250 2600 2    60   ~ 0
AU_MCLK
Text Label 3250 2500 2    60   ~ 0
AUSD_CS
Text Label 3250 2400 2    60   ~ 0
AU_BCLK
$Comp
L SW_SPST CORE_SW?
U 1 1 5A6F63D1
P 2550 4200
F 0 "CORE_SW?" H 2400 4250 50  0000 C CNN
F 1 "SW_RST" H 2750 4250 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 2550 4200 50  0001 C CNN
F 3 "" H 2550 4200 50  0001 C CNN
	1    2550 4200
	1    0    0    -1  
$EndComp
$Comp
L SW_SPST CORE_SW?
U 1 1 5A6F63D8
P 2550 4100
F 0 "CORE_SW?" H 2400 4150 50  0000 C CNN
F 1 "SW_PGM" H 2750 4150 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 2550 4100 50  0001 C CNN
F 3 "" H 2550 4100 50  0001 C CNN
	1    2550 4100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A6F63DF
P 2050 6200
F 0 "#PWR?" H 2050 5950 50  0001 C CNN
F 1 "GND" H 2050 6050 50  0000 C CNN
F 2 "" H 2050 6200 50  0000 C CNN
F 3 "" H 2050 6200 50  0000 C CNN
	1    2050 6200
	1    0    0    -1  
$EndComp
$Comp
L USB_A_afshar J?
U 1 1 5A6F63E5
P 5950 4000
F 0 "J?" H 5750 4450 50  0000 L CNN
F 1 "USB_A_afshar" H 5750 4350 50  0000 L CNN
F 2 "Connectors:USB_A" H 6100 3950 50  0001 C CNN
F 3 "" H 6100 3950 50  0001 C CNN
	1    5950 4000
	-1   0    0    -1  
$EndComp
NoConn ~ 3250 3900
$Comp
L AGND #PWR?
U 1 1 5A6F63ED
P 5250 4800
F 0 "#PWR?" H 5250 4550 50  0001 C CNN
F 1 "AGND" H 5250 4600 50  0000 C CNN
F 2 "" H 5250 4800 50  0000 C CNN
F 3 "" H 5250 4800 50  0000 C CNN
	1    5250 4800
	0    -1   -1   0   
$EndComp
Text Label 3250 5300 2    60   ~ 0
AU_RX
Text Label 5250 5000 0    60   ~ 0
AU_LRCLK
Text Label 5250 5100 0    60   ~ 0
AU_TX
Text Label 3250 2700 2    60   ~ 0
AUSD_MISO
$Comp
L Teensy_Audio_Board U?
U 1 1 5A6F63F7
P 8550 2600
F 0 "U?" H 8350 3750 60  0000 C CNN
F 1 "Teensy_Audio_Board" H 8550 2050 60  0000 C CNN
F 2 "afshar-kicad-lib:Teensy_Audio_Board" H 8350 2050 60  0001 C CNN
F 3 "" H 8350 2050 60  0000 C CNN
	1    8550 2600
	1    0    0    -1  
$EndComp
Text Label 7300 2850 2    60   ~ 0
AU_MCLK
Text Label 7300 2650 2    60   ~ 0
AU_BCLK
Text Label 9850 2950 0    60   ~ 0
AU_RX
Text Label 9850 2350 0    60   ~ 0
SCL
Text Label 9850 2450 0    60   ~ 0
SDA
$Comp
L GND #PWR?
U 1 1 5A6F6403
P 6950 1800
F 0 "#PWR?" H 6950 1550 50  0001 C CNN
F 1 "GND" H 6950 1650 50  0000 C CNN
F 2 "" H 6950 1800 50  0000 C CNN
F 3 "" H 6950 1800 50  0000 C CNN
	1    6950 1800
	1    0    0    -1  
$EndComp
Text Label 9850 2050 0    60   ~ 0
AU_TX
Text Label 9850 1950 0    60   ~ 0
AU_LRCLK
$Comp
L +3.3V #PWR?
U 1 1 5A6F640B
P 10450 1300
F 0 "#PWR?" H 10450 1150 50  0001 C CNN
F 1 "+3.3V" H 10450 1440 50  0000 C CNN
F 2 "" H 10450 1300 50  0001 C CNN
F 3 "" H 10450 1300 50  0001 C CNN
	1    10450 1300
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR?
U 1 1 5A6F6411
P 9850 1750
F 0 "#PWR?" H 9850 1500 50  0001 C CNN
F 1 "AGND" H 9850 1550 50  0000 C CNN
F 2 "" H 9850 1750 50  0000 C CNN
F 3 "" H 9850 1750 50  0000 C CNN
	1    9850 1750
	0    -1   -1   0   
$EndComp
Text Label 7300 2950 2    60   ~ 0
AU_SDMISO
Text Label 3250 5400 2    60   ~ 0
AUSD_SCLK
Text Label 9850 2850 0    60   ~ 0
AUSD_SCLK
Text Label 7300 2750 2    60   ~ 0
AUSD_CS
Text Label 3250 2200 2    60   ~ 0
AUSD_MOSI
Text Label 7300 2450 2    60   ~ 0
AUSD_MOSI
Text Label 3250 2100 2    60   ~ 0
AUMEM_CS
Text Label 7300 2350 2    60   ~ 0
AUMEM_CS
Text Label 3250 5000 2    60   ~ 0
DAC0
Text Label 3250 5100 2    60   ~ 0
DAC1
NoConn ~ 5250 2100
NoConn ~ 5250 2200
NoConn ~ 5250 4300
NoConn ~ 5250 4400
$Comp
L +5V #PWR?
U 1 1 5A6F6433
P 5500 3450
F 0 "#PWR?" H 5500 3300 50  0001 C CNN
F 1 "+5V" H 5500 3590 50  0000 C CNN
F 2 "" H 5500 3450 50  0001 C CNN
F 3 "" H 5500 3450 50  0001 C CNN
	1    5500 3450
	1    0    0    -1  
$EndComp
$Comp
L Teensy3.6 U?
U 1 1 5A6F643F
P 4250 3500
F 0 "U?" H 4250 5800 60  0000 C CNN
F 1 "Teensy3.6" H 4250 1200 60  0000 C CNN
F 2 "teensy:Teensy35_36" H 4250 3500 60  0001 C CNN
F 3 "" H 4250 3500 60  0000 C CNN
	1    4250 3500
	1    0    0    -1  
$EndComp
Text Label 3250 5600 2    60   ~ 0
UART_RST
Text Label 5250 5600 0    60   ~ 0
UART_PGM
$Comp
L +3.3V #PWR?
U 1 1 5A6F6449
P 2050 2800
F 0 "#PWR?" H 2050 2650 50  0001 C CNN
F 1 "+3.3V" H 2050 2940 50  0000 C CNN
F 2 "" H 2050 2800 50  0001 C CNN
F 3 "" H 2050 2800 50  0001 C CNN
	1    2050 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4200 5650 4200
Wire Wire Line
	5250 4100 5650 4100
Wire Wire Line
	5250 4000 5650 4000
Wire Wire Line
	5650 3900 5250 3900
Wire Wire Line
	5250 3800 5650 3800
Connection ~ 2050 4100
Wire Wire Line
	2050 4000 3250 4000
Connection ~ 2050 4200
Wire Wire Line
	2050 4000 2050 6200
Wire Wire Line
	2050 4200 2350 4200
Wire Wire Line
	2050 4100 2350 4100
Wire Wire Line
	2750 4200 3250 4200
Wire Wire Line
	2750 4100 3250 4100
Wire Wire Line
	2050 2800 3250 2800
Wire Wire Line
	3250 1400 3250 1000
Wire Wire Line
	5250 5500 6650 5500
Wire Wire Line
	5250 5400 6650 5400
Connection ~ 6400 4700
Wire Wire Line
	6400 4700 6400 3700
Wire Wire Line
	6350 4700 6450 4700
Connection ~ 6450 5400
Wire Wire Line
	6450 5400 6450 5000
Connection ~ 6350 5500
Wire Wire Line
	6350 5500 6350 5000
Wire Wire Line
	3250 5200 2050 5200
Connection ~ 2050 5200
Wire Wire Line
	6950 1650 7300 1650
Wire Wire Line
	6950 1650 6950 1800
Wire Wire Line
	9850 1850 10450 1850
Wire Wire Line
	10450 1850 10450 1300
Wire Wire Line
	5500 4200 5500 3450
Connection ~ 5500 4200
Wire Wire Line
	5250 5300 5300 5300
Wire Wire Line
	5250 5200 5300 5200
Text HLabel 6650 5400 2    60   Input ~ 0
SCL
Text HLabel 6650 5500 2    60   Input ~ 0
SDA
Text HLabel 3250 1700 0    60   Input ~ 0
RE1_A
Text HLabel 3250 1800 0    60   Input ~ 0
RE1_B
Text HLabel 3250 1900 0    60   Input ~ 0
RE2_A
Text HLabel 3250 2000 0    60   Input ~ 0
RE2_B
Text HLabel 3250 2300 0    60   Input ~ 0
RE8_A
Text HLabel 3250 2900 0    60   Input ~ 0
RE3_A
Text HLabel 3250 3000 0    60   Input ~ 0
RE3_B
Text HLabel 3250 3100 0    60   Input ~ 0
RE4_A
Text HLabel 3250 3200 0    60   Input ~ 0
RE4_B
Text HLabel 3250 3300 0    60   Input ~ 0
RE5_A
Text HLabel 3250 3400 0    60   Input ~ 0
RE5_B
Text HLabel 3250 3500 0    60   Input ~ 0
RE6_A
Text HLabel 3250 3600 0    60   Input ~ 0
RE6_B
Text HLabel 3250 3700 0    60   Input ~ 0
RE8_B
Text HLabel 3250 4300 0    60   Input ~ 0
RE7_A
Text HLabel 3250 4400 0    60   Input ~ 0
RE7_B
Text HLabel 3250 4500 0    60   Input ~ 0
RE9_A
Text HLabel 3250 4600 0    60   Input ~ 0
RE9_B
Text HLabel 3250 4700 0    60   Input ~ 0
PIXEL1
Text HLabel 3250 4800 0    60   Input ~ 0
PIXEL2
Text HLabel 3250 4900 0    60   Input ~ 0
RE10_A
Text HLabel 3250 5500 0    60   Input ~ 0
RE10_B
$EndSCHEMATC
