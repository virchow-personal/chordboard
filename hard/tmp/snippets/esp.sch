EESchema Schematic File Version 2
LIBS:chordboard-components
LIBS:chordboard-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:standard
LIBS:teensy
LIBS:switches
LIBS:Symbols_DCDC-ACDC-Converter_RevC_20Jul2012
LIBS:Symbols_EN60617_13Mar2013
LIBS:Symbols_EN60617-10_HF-Radio_DRAFT_12Sep2013
LIBS:Symbols_ICs-Diskrete_RevD10
LIBS:Symbols_ICs-Opto_RevB_16Sep2013
LIBS:Symbols_Microcontroller_Philips-NXP_RevA_06Oct2013
LIBS:SymbolsSimilarEN60617+oldDIN617-RevE8
LIBS:Symbols_Socket-DIN41612_RevA
LIBS:Symbols_Transformer-Diskrete_RevA
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Bosch
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:mcp73831t-2aci-ot
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:ESP32
LIBS:ESP32-footprints-Shem-Lib
LIBS:espressif-xess
LIBS:adafruit
LIBS:GeekAmmo
LIBS:LilyPad-Wearables
LIBS:SparkFun-Aesthetics
LIBS:SparkFun-AnalogIC
LIBS:SparkFun-Boards
LIBS:SparkFun-Capacitors
LIBS:SparkFun-Connectors
LIBS:SparkFun-DiscreteSemi
LIBS:SparkFun-Displays
LIBS:SparkFun-Electromechanical
LIBS:SparkFun-FreqCtrl
LIBS:SparkFun-LED
LIBS:SparkFun-Passives
LIBS:SparkFun-PowerIC
LIBS:SparkFun-Resistors
LIBS:SparkFun-Retired
LIBS:SparkFun-RF
LIBS:SparkFun-Sensors
LIBS:Teensy_3_and_LC_Series_Boards_v1.1
LIBS:User-Submitted
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R_Small R3
U 1 1 5A6BCF6B
P 2050 2900
F 0 "R3" H 2080 2920 50  0000 L CNN
F 1 "10k" H 2080 2860 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 2050 2900 50  0001 C CNN
F 3 "" H 2050 2900 50  0001 C CNN
	1    2050 2900
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR088
U 1 1 5A6BCF72
P 4000 1600
F 0 "#PWR088" H 4000 1450 50  0001 C CNN
F 1 "+3.3V" H 4000 1740 50  0000 C CNN
F 2 "" H 4000 1600 50  0001 C CNN
F 3 "" H 4000 1600 50  0001 C CNN
	1    4000 1600
	1    0    0    -1  
$EndComp
$Comp
L ESP-12 U7
U 1 1 5A6BCF78
P 4000 3200
F 0 "U7" H 4000 3100 50  0000 C CNN
F 1 "ESP-12" H 4000 3300 50  0000 C CNN
F 2 "afshar-kicad-lib:ESP-12_smd" H 4000 3200 50  0001 C CNN
F 3 "" H 4000 3200 50  0001 C CNN
	1    4000 3200
	1    0    0    -1  
$EndComp
$Comp
L R_Small R4
U 1 1 5A6BCF7F
P 2050 3100
F 0 "R4" H 2080 3120 50  0000 L CNN
F 1 "10k" H 2080 3060 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 2050 3100 50  0001 C CNN
F 3 "" H 2050 3100 50  0001 C CNN
	1    2050 3100
	0    1    1    0   
$EndComp
$Comp
L GND #PWR089
U 1 1 5A6BCF86
P 4000 4350
F 0 "#PWR089" H 4000 4100 50  0001 C CNN
F 1 "GND" H 4000 4200 50  0000 C CNN
F 2 "" H 4000 4350 50  0001 C CNN
F 3 "" H 4000 4350 50  0001 C CNN
	1    4000 4350
	1    0    0    -1  
$EndComp
$Comp
L R_Small R5
U 1 1 5A6BCF8C
P 6050 3300
F 0 "R5" H 6080 3320 50  0000 L CNN
F 1 "10k" H 6080 3260 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 6050 3300 50  0001 C CNN
F 3 "" H 6050 3300 50  0001 C CNN
	1    6050 3300
	0    -1   1    0   
$EndComp
$Comp
L R_Small R6
U 1 1 5A6BCF93
P 6050 3400
F 0 "R6" H 6080 3420 50  0000 L CNN
F 1 "10k" H 6080 3360 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 6050 3400 50  0001 C CNN
F 3 "" H 6050 3400 50  0001 C CNN
	1    6050 3400
	0    -1   1    0   
$EndComp
$Comp
L R_Small R7
U 1 1 5A6BCF9A
P 6050 3500
F 0 "R7" H 6080 3520 50  0000 L CNN
F 1 "10k" H 6080 3460 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 6050 3500 50  0001 C CNN
F 3 "" H 6050 3500 50  0001 C CNN
	1    6050 3500
	0    -1   1    0   
$EndComp
$Comp
L CONN_6 P1
U 1 1 5A6BCFA1
P 7000 3050
F 0 "P1" V 6950 3050 60  0000 C CNN
F 1 "CONN_6" V 7050 3050 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 7000 3050 60  0001 C CNN
F 3 "" H 7000 3050 60  0000 C CNN
	1    7000 3050
	1    0    0    1   
$EndComp
$Comp
L SW_SPST CORE_SW3
U 1 1 5A6BCFA8
P 2250 3500
F 0 "CORE_SW3" H 2100 3550 50  0000 C CNN
F 1 "SW_RST" H 2450 3550 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_B3U-3000P-B" H 2250 3500 50  0001 C CNN
F 3 "" H 2250 3500 50  0001 C CNN
	1    2250 3500
	0    1    1    0   
$EndComp
$Comp
L SW_SPST CORE_SW4
U 1 1 5A6BCFAF
P 5650 3850
F 0 "CORE_SW4" H 5500 3900 50  0000 C CNN
F 1 "SW_PGM" H 5850 3900 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_B3U-3000P-B" H 5650 3850 50  0001 C CNN
F 3 "" H 5650 3850 50  0001 C CNN
	1    5650 3850
	0    1    1    0   
$EndComp
Text HLabel 3100 2750 1    60   Input ~ 0
WIFI_RST
Text HLabel 4900 3300 2    60   Input ~ 0
WIFI_PGM
Text HLabel 4900 3100 2    60   Input ~ 0
WIFI_CMDIN
Text HLabel 4900 3200 2    60   Input ~ 0
WIFI_CMDOUT
Wire Wire Line
	1850 1600 6250 1600
Wire Wire Line
	4000 1600 4000 2300
Connection ~ 4000 1600
Wire Wire Line
	4000 4100 4000 4350
Wire Wire Line
	1850 1600 1850 3100
Connection ~ 1850 2900
Wire Wire Line
	5950 3400 4900 3400
Wire Wire Line
	4900 3300 5950 3300
Wire Wire Line
	4900 3500 5950 3500
Wire Wire Line
	2250 4150 6650 4150
Connection ~ 4000 4150
Wire Wire Line
	4900 2900 6650 2900
Wire Wire Line
	4900 3000 6650 3000
Wire Wire Line
	6150 3300 6250 3300
Wire Wire Line
	6250 1600 6250 3400
Wire Wire Line
	6250 3400 6150 3400
Connection ~ 6250 3300
Wire Wire Line
	6150 3500 6250 3500
Wire Wire Line
	6250 3500 6250 4150
Wire Wire Line
	5650 3650 5650 3300
Connection ~ 5650 3300
Wire Wire Line
	5650 4050 5650 4150
Connection ~ 5650 4150
Wire Wire Line
	2250 2900 2250 3300
Connection ~ 2250 2900
Wire Wire Line
	2250 3700 2250 4150
Wire Wire Line
	6650 4150 6650 3300
Connection ~ 6250 4150
Text HLabel 5100 2400 1    60   Input ~ 0
WIFI_TXD
Text HLabel 5300 2400 1    60   Input ~ 0
WIFI_RXD
Wire Wire Line
	5100 2900 5100 2800
Wire Wire Line
	5300 3000 5300 2800
Connection ~ 5100 2900
Connection ~ 5300 3000
Wire Wire Line
	3100 2900 3100 2750
Wire Wire Line
	1850 2900 1950 2900
Wire Wire Line
	2150 2900 3100 2900
Wire Wire Line
	2150 3100 3100 3100
Wire Wire Line
	1850 3100 1950 3100
$Comp
L SW_SPST SW8
U 1 1 5A6F420F
P 5300 2600
F 0 "SW8" H 5300 2725 50  0000 C CNN
F 1 "SW_SPST" H 5300 2500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5300 2600 50  0001 C CNN
F 3 "" H 5300 2600 50  0001 C CNN
	1    5300 2600
	0    -1   -1   0   
$EndComp
$Comp
L SW_SPST SW7
U 1 1 5A6F4194
P 5100 2600
F 0 "SW7" H 5100 2725 50  0000 C CNN
F 1 "SW_SPST" H 5100 2500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5100 2600 50  0001 C CNN
F 3 "" H 5100 2600 50  0001 C CNN
	1    5100 2600
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
