EESchema Schematic File Version 2
LIBS:chordboard-components
LIBS:chordboard-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:standard
LIBS:teensy
LIBS:switches
LIBS:Symbols_DCDC-ACDC-Converter_RevC_20Jul2012
LIBS:Symbols_EN60617_13Mar2013
LIBS:Symbols_EN60617-10_HF-Radio_DRAFT_12Sep2013
LIBS:Symbols_ICs-Diskrete_RevD10
LIBS:Symbols_ICs-Opto_RevB_16Sep2013
LIBS:Symbols_Microcontroller_Philips-NXP_RevA_06Oct2013
LIBS:SymbolsSimilarEN60617+oldDIN617-RevE8
LIBS:Symbols_Socket-DIN41612_RevA
LIBS:Symbols_Transformer-Diskrete_RevA
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Bosch
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:ESP32
LIBS:ESP32-footprints-Shem-Lib
LIBS:espressif-xess
LIBS:adafruit
LIBS:GeekAmmo
LIBS:LilyPad-Wearables
LIBS:SparkFun-Aesthetics
LIBS:SparkFun-AnalogIC
LIBS:SparkFun-Boards
LIBS:SparkFun-Capacitors
LIBS:SparkFun-Connectors
LIBS:SparkFun-DiscreteSemi
LIBS:SparkFun-Displays
LIBS:SparkFun-Electromechanical
LIBS:SparkFun-FreqCtrl
LIBS:SparkFun-LED
LIBS:SparkFun-Passives
LIBS:SparkFun-PowerIC
LIBS:SparkFun-Resistors
LIBS:SparkFun-Retired
LIBS:SparkFun-RF
LIBS:SparkFun-Sensors
LIBS:Teensy_3_and_LC_Series_Boards_v1.1
LIBS:User-Submitted
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP73831T-2ACI/OT U8
U 1 1 5A712305
P 4950 4150
F 0 "U8" H 4950 4650 50  0000 C CNN
F 1 "MCP73831T-2ACI/OT" H 4950 4550 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5_HandSoldering" H 4950 3750 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001984g.pdf" H 4950 3650 50  0001 C CNN
F 4 "MCP73831T-2ACI/OT" H 4950 3450 50  0001 C CNN "MPN"
F 5 "Microchip" H 4950 3350 50  0001 C CNN "Manuf"
F 6 "Microchip MCP73831T-2ACI/OT" H 4950 3550 50  0001 C CNN "BOM"
	1    4950 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4350 4300 4350
Wire Wire Line
	4300 4350 4300 4800
Wire Wire Line
	5450 3950 6400 3950
Wire Wire Line
	5550 3950 5550 3650
$Comp
L +BATT #PWR091
U 1 1 5A712359
P 5550 3650
F 0 "#PWR091" H 5550 3500 50  0001 C CNN
F 1 "+BATT" H 5550 3790 50  0000 C CNN
F 2 "" H 5550 3650 50  0001 C CNN
F 3 "" H 5550 3650 50  0001 C CNN
	1    5550 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 4350 5550 4350
Wire Wire Line
	5550 4350 5550 4450
$Comp
L R_Small R4
U 1 1 5A712377
P 5550 4550
F 0 "R4" H 5580 4570 50  0000 L CNN
F 1 "R_Small" H 5580 4510 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5550 4550 50  0001 C CNN
F 3 "" H 5550 4550 50  0001 C CNN
	1    5550 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4650 5550 4800
$Comp
L GND #PWR092
U 1 1 5A7123D8
P 5550 4800
F 0 "#PWR092" H 5550 4550 50  0001 C CNN
F 1 "GND" H 5550 4650 50  0000 C CNN
F 2 "" H 5550 4800 50  0001 C CNN
F 3 "" H 5550 4800 50  0001 C CNN
	1    5550 4800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR093
U 1 1 5A7123F0
P 4300 4800
F 0 "#PWR093" H 4300 4550 50  0001 C CNN
F 1 "GND" H 4300 4650 50  0000 C CNN
F 2 "" H 4300 4800 50  0001 C CNN
F 3 "" H 4300 4800 50  0001 C CNN
	1    4300 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3950 4300 3950
Text HLabel 4300 3950 0    60   Input ~ 0
VIN
Wire Wire Line
	6050 3950 6050 4100
Connection ~ 5550 3950
$Comp
L C_Small C38
U 1 1 5A71244A
P 6050 4200
F 0 "C38" H 6060 4270 50  0000 L CNN
F 1 "C_Small" H 6060 4120 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6050 4200 50  0001 C CNN
F 3 "" H 6050 4200 50  0001 C CNN
	1    6050 4200
	1    0    0    -1  
$EndComp
$Comp
L CP_Small C39
U 1 1 5A71246B
P 6400 4200
F 0 "C39" H 6410 4270 50  0000 L CNN
F 1 "CP_Small" H 6410 4120 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6400 4200 50  0001 C CNN
F 3 "" H 6400 4200 50  0001 C CNN
	1    6400 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3950 6400 4100
Connection ~ 6050 3950
Wire Wire Line
	6050 4300 6050 4450
Wire Wire Line
	6050 4450 6400 4450
Wire Wire Line
	6400 4450 6400 4300
Wire Wire Line
	6250 4450 6250 4800
Connection ~ 6250 4450
$Comp
L GND #PWR094
U 1 1 5A7124D8
P 6250 4800
F 0 "#PWR094" H 6250 4550 50  0001 C CNN
F 1 "GND" H 6250 4650 50  0000 C CNN
F 2 "" H 6250 4800 50  0001 C CNN
F 3 "" H 6250 4800 50  0001 C CNN
	1    6250 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4150 4300 4150
Text HLabel 4300 4150 0    60   Input ~ 0
STAT
$Comp
L +BATT #PWR095
U 1 1 5A7127AD
P 7350 3600
F 0 "#PWR095" H 7350 3450 50  0001 C CNN
F 1 "+BATT" H 7350 3740 50  0000 C CNN
F 2 "" H 7350 3600 50  0001 C CNN
F 3 "" H 7350 3600 50  0001 C CNN
	1    7350 3600
	1    0    0    -1  
$EndComp
$Comp
L Battery BT1
U 1 1 5A7127C2
P 7350 3900
F 0 "BT1" H 7450 4000 50  0000 L CNN
F 1 "Battery" H 7450 3900 50  0000 L CNN
F 2 "Connectors_JST:JST_PH_S2B-PH-SM4-TB_02x2.00mm_Angled" V 7350 3960 50  0001 C CNN
F 3 "" V 7350 3960 50  0001 C CNN
	1    7350 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 3600 7350 3700
Wire Wire Line
	7350 4100 7350 4200
$Comp
L GND #PWR096
U 1 1 5A7128A0
P 7350 4200
F 0 "#PWR096" H 7350 3950 50  0001 C CNN
F 1 "GND" H 7350 4050 50  0000 C CNN
F 2 "" H 7350 4200 50  0001 C CNN
F 3 "" H 7350 4200 50  0001 C CNN
	1    7350 4200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
