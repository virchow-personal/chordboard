EESchema Schematic File Version 2
LIBS:chordboard-components
LIBS:chordboard-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:standard
LIBS:teensy
LIBS:switches
LIBS:Symbols_DCDC-ACDC-Converter_RevC_20Jul2012
LIBS:Symbols_EN60617_13Mar2013
LIBS:Symbols_EN60617-10_HF-Radio_DRAFT_12Sep2013
LIBS:Symbols_ICs-Diskrete_RevD10
LIBS:Symbols_ICs-Opto_RevB_16Sep2013
LIBS:Symbols_Microcontroller_Philips-NXP_RevA_06Oct2013
LIBS:SymbolsSimilarEN60617+oldDIN617-RevE8
LIBS:Symbols_Socket-DIN41612_RevA
LIBS:Symbols_Transformer-Diskrete_RevA
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Bosch
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:ESP32
LIBS:ESP32-footprints-Shem-Lib
LIBS:espressif-xess
LIBS:adafruit
LIBS:GeekAmmo
LIBS:LilyPad-Wearables
LIBS:SparkFun-Aesthetics
LIBS:SparkFun-AnalogIC
LIBS:SparkFun-Boards
LIBS:SparkFun-Capacitors
LIBS:SparkFun-Connectors
LIBS:SparkFun-DiscreteSemi
LIBS:SparkFun-Displays
LIBS:SparkFun-Electromechanical
LIBS:SparkFun-FreqCtrl
LIBS:SparkFun-LED
LIBS:SparkFun-Passives
LIBS:SparkFun-PowerIC
LIBS:SparkFun-Resistors
LIBS:SparkFun-Retired
LIBS:SparkFun-RF
LIBS:SparkFun-Sensors
LIBS:Teensy_3_and_LC_Series_Boards_v1.1
LIBS:User-Submitted
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L +3.3V #PWR020
U 1 1 5A6A2586
P 3750 1700
F 0 "#PWR020" H 3750 1550 50  0001 C CNN
F 1 "+3.3V" H 3750 1840 50  0000 C CNN
F 2 "" H 3750 1700 50  0000 C CNN
F 3 "" H 3750 1700 50  0000 C CNN
	1    3750 1700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 5A7074E6
P 4700 2250
F 0 "#PWR021" H 4700 2000 50  0001 C CNN
F 1 "GND" H 4700 2100 50  0000 C CNN
F 2 "" H 4700 2250 50  0001 C CNN
F 3 "" H 4700 2250 50  0001 C CNN
	1    4700 2250
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 5A6CC380
P 3750 5100
F 0 "#PWR022" H 3750 4850 50  0001 C CNN
F 1 "GND" H 3750 4950 50  0000 C CNN
F 2 "" H 3750 5100 50  0001 C CNN
F 3 "" H 3750 5100 50  0001 C CNN
	1    3750 5100
	-1   0    0    -1  
$EndComp
Text HLabel 4450 3700 2    60   Input ~ 0
SCL
Text HLabel 4450 3800 2    60   Input ~ 0
SDA
Text HLabel 4450 3000 2    60   Input ~ 0
INTA
Text HLabel 4450 3100 2    60   Input ~ 0
INTB
Wire Wire Line
	4250 3800 4450 3800
Wire Wire Line
	4250 3700 4450 3700
Wire Wire Line
	3750 4400 3750 5100
Wire Wire Line
	4350 1800 4350 2600
Wire Wire Line
	4350 2600 4250 2600
Wire Wire Line
	3750 1700 3750 2400
Connection ~ 2850 4000
Connection ~ 2850 3900
Connection ~ 2850 3800
Connection ~ 2850 3700
Connection ~ 2850 3600
Connection ~ 2850 3500
Connection ~ 2850 3300
Connection ~ 2850 3200
Connection ~ 2850 3100
Connection ~ 2850 3000
Connection ~ 2850 2900
Connection ~ 2850 2800
Connection ~ 2850 2700
Wire Wire Line
	2850 2600 2850 4450
Connection ~ 3750 1800
Wire Wire Line
	2850 4450 3750 4450
Connection ~ 3750 4450
Wire Wire Line
	4250 3100 4450 3100
Wire Wire Line
	4250 3000 4450 3000
Wire Wire Line
	4700 1800 4700 1950
Wire Wire Line
	4700 2150 4700 2250
Wire Wire Line
	3250 2600 3200 2600
Wire Wire Line
	2900 2600 2850 2600
Wire Wire Line
	3250 2700 3200 2700
Wire Wire Line
	2900 2700 2850 2700
Wire Wire Line
	3250 2800 3200 2800
Wire Wire Line
	3250 2900 3200 2900
Wire Wire Line
	2900 2800 2850 2800
Wire Wire Line
	2900 2900 2850 2900
Wire Wire Line
	3250 3000 3200 3000
Wire Wire Line
	2900 3000 2850 3000
Wire Wire Line
	3250 3100 3200 3100
Wire Wire Line
	2900 3100 2850 3100
Wire Wire Line
	3250 3200 3200 3200
Wire Wire Line
	3250 3300 3200 3300
Wire Wire Line
	2900 3200 2850 3200
Wire Wire Line
	2900 3300 2850 3300
Wire Wire Line
	3250 3500 3200 3500
Wire Wire Line
	2900 3500 2850 3500
Wire Wire Line
	3250 3600 3200 3600
Wire Wire Line
	2900 3600 2850 3600
Wire Wire Line
	3250 3700 3200 3700
Wire Wire Line
	3250 3800 3200 3800
Wire Wire Line
	2900 3700 2850 3700
Wire Wire Line
	2900 3800 2850 3800
Wire Wire Line
	3250 3900 3200 3900
Wire Wire Line
	2900 3900 2850 3900
Wire Wire Line
	3250 4000 3200 4000
Wire Wire Line
	2900 4000 2850 4000
Wire Wire Line
	3250 4100 3200 4100
Wire Wire Line
	3250 4200 3200 4200
Connection ~ 4350 1800
$Comp
L MCP23017 U4
U 1 1 5A6DF4F4
P 3750 3400
F 0 "U4" H 3650 4425 50  0000 R CNN
F 1 "MCP23017" H 3650 4350 50  0000 R CNN
F 2 "Housings_SSOP:SSOP-28_5.3x10.2mm_Pitch0.65mm" H 3800 2450 50  0001 L CNN
F 3 "" H 4000 4400 50  0001 C CNN
	1    3750 3400
	1    0    0    -1  
$EndComp
$Comp
L KSW K17
U 1 1 5A6DFF15
P 3050 2600
F 0 "K17" H 2950 2650 60  0000 C CNN
F 1 "KSW" H 3200 2650 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 3050 2600 60  0001 C CNN
F 3 "" H 3050 2600 60  0001 C CNN
	1    3050 2600
	-1   0    0    -1  
$EndComp
$Comp
L KSW K18
U 1 1 5A6E0070
P 3050 2700
F 0 "K18" H 2950 2750 60  0000 C CNN
F 1 "KSW" H 3200 2750 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 2750 60  0001 C CNN
F 3 "" H 3050 2700 60  0001 C CNN
	1    3050 2700
	-1   0    0    -1  
$EndComp
$Comp
L KSW K19
U 1 1 5A703CBB
P 3050 2800
F 0 "K19" H 2950 2850 60  0000 C CNN
F 1 "KSW" H 3200 2850 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 2850 60  0001 C CNN
F 3 "" H 3050 2800 60  0001 C CNN
	1    3050 2800
	-1   0    0    -1  
$EndComp
$Comp
L KSW K20
U 1 1 5A6E01AD
P 3050 2900
F 0 "K20" H 2950 2950 60  0000 C CNN
F 1 "KSW" H 3200 2950 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 2950 60  0001 C CNN
F 3 "" H 3050 2900 60  0001 C CNN
	1    3050 2900
	-1   0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5A7074E2
P 4700 2050
F 0 "C2" H 4710 2120 50  0000 L CNN
F 1 "0.1uF" H 4710 1970 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4700 2050 50  0001 C CNN
F 3 "" H 4700 2050 50  0001 C CNN
	1    4700 2050
	-1   0    0    -1  
$EndComp
$Comp
L KSW K21
U 1 1 5A703CC5
P 3050 3000
F 0 "K21" H 2950 3050 60  0000 C CNN
F 1 "KSW" H 3200 3050 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3050 60  0001 C CNN
F 3 "" H 3050 3000 60  0001 C CNN
	1    3050 3000
	-1   0    0    -1  
$EndComp
$Comp
L KSW K22
U 1 1 5A703CC6
P 3050 3100
F 0 "K22" H 2950 3150 60  0000 C CNN
F 1 "KSW" H 3200 3150 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3150 60  0001 C CNN
F 3 "" H 3050 3100 60  0001 C CNN
	1    3050 3100
	-1   0    0    -1  
$EndComp
$Comp
L KSW K23
U 1 1 5A703CC7
P 3050 3200
F 0 "K23" H 2950 3250 60  0000 C CNN
F 1 "KSW" H 3200 3250 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3250 60  0001 C CNN
F 3 "" H 3050 3200 60  0001 C CNN
	1    3050 3200
	-1   0    0    -1  
$EndComp
$Comp
L KSW K25
U 1 1 5A703CC9
P 3050 3500
F 0 "K25" H 2950 3550 60  0000 C CNN
F 1 "KSW" H 3200 3550 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3550 60  0001 C CNN
F 3 "" H 3050 3500 60  0001 C CNN
	1    3050 3500
	-1   0    0    -1  
$EndComp
$Comp
L KSW K26
U 1 1 5A703CCA
P 3050 3600
F 0 "K26" H 2950 3650 60  0000 C CNN
F 1 "KSW" H 3200 3650 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3650 60  0001 C CNN
F 3 "" H 3050 3600 60  0001 C CNN
	1    3050 3600
	-1   0    0    -1  
$EndComp
$Comp
L KSW K27
U 1 1 5A7074E3
P 3050 3700
F 0 "K27" H 2950 3750 60  0000 C CNN
F 1 "KSW" H 3200 3750 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3750 60  0001 C CNN
F 3 "" H 3050 3700 60  0001 C CNN
	1    3050 3700
	-1   0    0    -1  
$EndComp
$Comp
L KSW K28
U 1 1 5A7074E4
P 3050 3800
F 0 "K28" H 2950 3850 60  0000 C CNN
F 1 "KSW" H 3200 3850 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3850 60  0001 C CNN
F 3 "" H 3050 3800 60  0001 C CNN
	1    3050 3800
	-1   0    0    -1  
$EndComp
$Comp
L KSW K29
U 1 1 5A6E0C01
P 3050 3900
F 0 "K29" H 2950 3950 60  0000 C CNN
F 1 "KSW" H 3200 3950 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3950 60  0001 C CNN
F 3 "" H 3050 3900 60  0001 C CNN
	1    3050 3900
	-1   0    0    -1  
$EndComp
$Comp
L KSW K30
U 1 1 5A7074EE
P 3050 4000
F 0 "K30" H 2950 4050 60  0000 C CNN
F 1 "KSW" H 3200 4050 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 4050 60  0001 C CNN
F 3 "" H 3050 4000 60  0001 C CNN
	1    3050 4000
	-1   0    0    -1  
$EndComp
Connection ~ 3750 4800
$Comp
L KSW K24
U 1 1 5A703CC8
P 3050 3300
F 0 "K24" H 2950 3350 60  0000 C CNN
F 1 "KSW" H 3200 3350 60  0000 C CNN
F 2 "chordboard-footprints:CHERRY_PCB_100H" H 4000 3350 60  0001 C CNN
F 3 "" H 3050 3300 60  0001 C CNN
	1    3050 3300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4250 4000 4450 4000
Wire Wire Line
	4250 4100 4450 4100
Wire Wire Line
	4250 4200 4450 4200
Text HLabel 4450 4000 2    60   Input ~ 0
A0
Text HLabel 4450 4100 2    60   Input ~ 0
A1
Text HLabel 4450 4200 2    60   Input ~ 0
A2
Wire Wire Line
	3750 1800 4700 1800
$EndSCHEMATC
