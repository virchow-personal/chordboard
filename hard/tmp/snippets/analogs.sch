EESchema Schematic File Version 2
LIBS:chordboard-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:teensy
LIBS:switches
LIBS:Symbols_DCDC-ACDC-Converter_RevC_20Jul2012
LIBS:Symbols_EN60617_13Mar2013
LIBS:Symbols_EN60617-10_HF-Radio_DRAFT_12Sep2013
LIBS:Symbols_ICs-Diskrete_RevD10
LIBS:Symbols_ICs-Opto_RevB_16Sep2013
LIBS:Symbols_Microcontroller_Philips-NXP_RevA_06Oct2013
LIBS:SymbolsSimilarEN60617+oldDIN617-RevE8
LIBS:Symbols_Socket-DIN41612_RevA
LIBS:Symbols_Transformer-Diskrete_RevA
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Bosch
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:mcp73831t-2aci-ot
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:standard
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:Wetmelon
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L +3.3V #PWR?
U 1 1 5A710F0F
P 3550 1600
F 0 "#PWR?" H 3550 1450 50  0001 C CNN
F 1 "+3.3V" H 3550 1740 50  0000 C CNN
F 2 "" H 3550 1600 50  0001 C CNN
F 3 "" H 3550 1600 50  0001 C CNN
	1    3550 1600
	-1   0    0    -1  
$EndComp
Text Label 4150 2650 3    60   ~ 0
AP_3
Text Label 3800 2650 3    60   ~ 0
AP_2
Text Label 3450 2650 3    60   ~ 0
AP_1
$Comp
L POT RV?
U 1 1 5A710F18
P 3450 2500
F 0 "RV?" H 3400 2600 50  0000 R CNN
F 1 "POT_TEMPO" H 3400 2400 50  0000 R CNN
F 2 "Potentiometers:Potentiometer_Bourns_PTV09A-1_Horizontal" H 3450 2500 50  0001 C CNN
F 3 "" H 3450 2500 50  0001 C CNN
	1    3450 2500
	0    -1   1    0   
$EndComp
$Comp
L POT RV?
U 1 1 5A710F1F
P 3800 2500
F 0 "RV?" H 3750 2600 50  0000 R CNN
F 1 "POT_VELOCITY" H 3550 2400 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Bourns_PTV09A-1_Horizontal" H 3800 2500 50  0001 C CNN
F 3 "" H 3800 2500 50  0001 C CNN
	1    3800 2500
	0    -1   1    0   
$EndComp
$Comp
L POT RV?
U 1 1 5A710F26
P 4150 2500
F 0 "RV?" H 4100 2600 50  0000 R CNN
F 1 "POT_VOLUME" H 4100 2400 50  0000 R CNN
F 2 "Potentiometers:Potentiometer_Bourns_PTV09A-1_Horizontal" H 4150 2500 50  0001 C CNN
F 3 "" H 4150 2500 50  0001 C CNN
	1    4150 2500
	0    -1   1    0   
$EndComp
$Comp
L JOYSTICK_BUTTON JS?
U 1 1 5A710F2D
P 6050 2600
F 0 "JS?" H 5650 3150 60  0000 C CNN
F 1 "JOYSTICK" H 6050 2150 60  0000 C CNN
F 2 "SparkFun-Electromechanical:JOYSTICK" H 6050 2600 60  0001 C CNN
F 3 "" H 6050 2600 60  0000 C CNN
	1    6050 2600
	1    0    0    -1  
$EndComp
Text Label 5400 2300 2    60   ~ 0
AJ_V
Text Label 5400 2650 2    60   ~ 0
AJ_H
Text Label 5400 2900 2    60   ~ 0
JSW1
$Comp
L AGND #PWR?
U 1 1 5A710F37
P 3600 3650
F 0 "#PWR?" H 3600 3400 50  0001 C CNN
F 1 "AGND" H 3600 3450 50  0000 C CNN
F 2 "" H 3600 3650 50  0000 C CNN
F 3 "" H 3600 3650 50  0000 C CNN
	1    3600 3650
	-1   0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5A710F3D
P 3800 2850
F 0 "C?" H 3825 2950 50  0000 L CNN
F 1 "0.1uF" V 3900 2600 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 3838 2700 50  0001 C CNN
F 3 "" H 3800 2850 50  0001 C CNN
	1    3800 2850
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5A710F44
P 4150 2850
F 0 "C?" H 4175 2950 50  0000 L CNN
F 1 "0.1uF" V 4250 2600 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 4188 2700 50  0001 C CNN
F 3 "" H 4150 2850 50  0001 C CNN
	1    4150 2850
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5A710F4B
P 3450 2850
F 0 "C?" H 3475 2950 50  0000 L CNN
F 1 "0.1uF" V 3550 2600 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 3488 2700 50  0001 C CNN
F 3 "" H 3450 2850 50  0001 C CNN
	1    3450 2850
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5A710F52
P 5000 2300
F 0 "C?" H 5025 2400 50  0000 L CNN
F 1 "0.1uF" V 5100 2050 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5038 2150 50  0001 C CNN
F 3 "" H 5000 2300 50  0001 C CNN
	1    5000 2300
	0    1    1    0   
$EndComp
$Comp
L C C?
U 1 1 5A710F59
P 5000 2650
F 0 "C?" H 5025 2750 50  0000 L CNN
F 1 "0.1uF" V 5100 2400 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5038 2500 50  0001 C CNN
F 3 "" H 5000 2650 50  0001 C CNN
	1    5000 2650
	0    1    1    0   
$EndComp
Connection ~ 4650 2650
Wire Wire Line
	4650 2650 4850 2650
Wire Wire Line
	5150 2650 5400 2650
Wire Wire Line
	5150 2300 5400 2300
Connection ~ 4650 2300
Wire Wire Line
	4650 2300 4850 2300
Wire Wire Line
	4150 2650 4150 2700
Wire Wire Line
	3800 2650 3800 2700
Wire Wire Line
	3450 2650 3450 2700
Connection ~ 4650 2750
Connection ~ 4650 2400
Wire Wire Line
	4650 3500 4650 2300
Connection ~ 4550 2550
Connection ~ 4550 2200
Wire Wire Line
	4550 1750 4550 2550
Wire Wire Line
	3600 2500 3600 3650
Wire Wire Line
	3950 2500 3950 3500
Wire Wire Line
	4300 3500 4300 2500
Wire Wire Line
	4000 1750 4000 2500
Wire Wire Line
	3650 1750 3650 2500
Wire Wire Line
	3300 2500 3300 1750
Wire Wire Line
	3600 3500 4650 3500
Connection ~ 3600 3500
Wire Wire Line
	3300 1750 4550 1750
Wire Wire Line
	3550 1600 3550 1750
Connection ~ 3550 1750
Wire Wire Line
	5400 2200 4550 2200
Wire Wire Line
	4550 2550 5400 2550
Wire Wire Line
	4650 2750 5400 2750
Wire Wire Line
	5400 2400 4650 2400
Connection ~ 3650 1750
Connection ~ 4000 1750
Wire Wire Line
	4150 3000 4150 3100
Wire Wire Line
	4150 3100 4300 3100
Wire Wire Line
	3800 3000 3800 3100
Wire Wire Line
	3800 3100 3950 3100
Wire Wire Line
	3450 3000 3450 3100
Wire Wire Line
	3450 3100 3600 3100
Connection ~ 3600 3100
Connection ~ 3950 3500
Connection ~ 3950 3100
Connection ~ 4300 3500
Connection ~ 4300 3100
Wire Wire Line
	5400 3000 4800 3000
Wire Wire Line
	4800 3000 4800 3650
$Comp
L GND #PWR?
U 1 1 5A710F8D
P 4800 3650
F 0 "#PWR?" H 4800 3400 50  0001 C CNN
F 1 "GND" H 4800 3500 50  0000 C CNN
F 2 "" H 4800 3650 50  0000 C CNN
F 3 "" H 4800 3650 50  0000 C CNN
	1    4800 3650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
