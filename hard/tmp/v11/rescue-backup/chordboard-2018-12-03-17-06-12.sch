EESchema Schematic File Version 2
LIBS:chordboard-rescue
LIBS:ac-dc
LIBS:adc-dac
LIBS:Altera
LIBS:analog_devices
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:Battery_Management
LIBS:bbd
LIBS:Bosch
LIBS:brooktre
LIBS:Connector
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:Decawave
LIBS:device
LIBS:digital-audio
LIBS:Diode
LIBS:Display
LIBS:driver_gate
LIBS:dsp
LIBS:DSP_Microchip_DSPIC33
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:Espressif
LIBS:FPGA_Actel
LIBS:ftdi
LIBS:gennum
LIBS:Graphic
LIBS:hc11
LIBS:infineon
LIBS:intel
LIBS:interface
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:LED
LIBS:LEM
LIBS:linear
LIBS:Logic_74xgxx
LIBS:Logic_74xx
LIBS:Logic_CMOS_4000
LIBS:Logic_CMOS_IEEE
LIBS:logic_programmable
LIBS:Logic_TTL_IEEE
LIBS:maxim
LIBS:MCU_Microchip_PIC10
LIBS:MCU_Microchip_PIC12
LIBS:MCU_Microchip_PIC16
LIBS:MCU_Microchip_PIC18
LIBS:MCU_Microchip_PIC24
LIBS:MCU_Microchip_PIC32
LIBS:MCU_NXP_Kinetis
LIBS:MCU_NXP_LPC
LIBS:MCU_NXP_S08
LIBS:MCU_Parallax
LIBS:MCU_ST_STM8
LIBS:MCU_ST_STM32
LIBS:MCU_Texas_MSP430
LIBS:Mechanical
LIBS:memory
LIBS:microchip
LIBS:microcontrollers
LIBS:modules
LIBS:Motor
LIBS:motor_drivers
LIBS:motorola
LIBS:nordicsemi
LIBS:nxp
LIBS:onsemi
LIBS:opto
LIBS:Oscillators
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:Relay
LIBS:RF_Bluetooth
LIBS:rfcom
LIBS:RFSolutions
LIBS:Sensor_Current
LIBS:Sensor_Humidity
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:supertex
LIBS:Switch
LIBS:texas
LIBS:Transformer
LIBS:Transistor
LIBS:triac_thyristor
LIBS:Valve
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xilinx-artix7
LIBS:xilinx-kintex7
LIBS:xilinx-spartan6
LIBS:xilinx-virtex5
LIBS:xilinx-virtex6
LIBS:xilinx-virtex7
LIBS:zetex
LIBS:Zilog
LIBS:teensy
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:display
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:mcp73831t-2aci-ot
LIBS:mechanical
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:standard
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:switches
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:Wetmelon
LIBS:chordboard-components
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title "Chordboard"
Date "2018-01-19"
Rev "v4"
Comp "aafshar@gmail.com"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP23017-RESCUE-chordboard U5
U 1 1 59FC3D10
P 15500 2900
F 0 "U5" H 15800 3400 50  0000 R CNN
F 1 "MCP23017" H 15800 3500 50  0000 R CNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 15550 1950 50  0001 L CNN
F 3 "" H 15750 3900 50  0001 C CNN
	1    15500 2900
	-1   0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 59FC6FE8
P 5250 4750
F 0 "R1" V 5150 4700 50  0000 C CNN
F 1 "4.7K" V 5250 4750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5180 4750 50  0001 C CNN
F 3 "" H 5250 4750 50  0000 C CNN
	1    5250 4750
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 59FC7069
P 5350 4750
F 0 "R2" V 5450 4700 50  0000 C CNN
F 1 "4.7K" V 5350 4750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5280 4750 50  0001 C CNN
F 3 "" H 5350 4750 50  0000 C CNN
	1    5350 4750
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR01
U 1 1 59FC7113
P 5300 3600
F 0 "#PWR01" H 5300 3450 50  0001 C CNN
F 1 "+3.3V" H 5300 3740 50  0000 C CNN
F 2 "" H 5300 3600 50  0000 C CNN
F 3 "" H 5300 3600 50  0000 C CNN
	1    5300 3600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 59FE1F2D
P 2150 900
F 0 "#PWR02" H 2150 650 50  0001 C CNN
F 1 "GND" H 2150 750 50  0000 C CNN
F 2 "" H 2150 900 50  0000 C CNN
F 3 "" H 2150 900 50  0000 C CNN
	1    2150 900 
	-1   0    0    1   
$EndComp
$Comp
L Teensy3.6 U1
U 1 1 59FC6CA8
P 3150 3400
F 0 "U1" H 3150 5700 60  0000 C CNN
F 1 "Teensy3.6" H 3150 1100 60  0000 C CNN
F 2 "chordboard-footprints:Teensy35_36_external_pins" H 3150 3400 60  0001 C CNN
F 3 "" H 3150 3400 60  0000 C CNN
	1    3150 3400
	1    0    0    -1  
$EndComp
Text Label 5550 5300 0    60   ~ 0
SCL
Text Label 5550 5400 0    60   ~ 0
SDA
$Comp
L MCP23017-RESCUE-chordboard U4
U 1 1 5A142090
P 13900 2900
F 0 "U4" H 13900 3400 50  0000 R CNN
F 1 "MCP23017" H 14200 3500 50  0000 R CNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 13950 1950 50  0001 L CNN
F 3 "" H 14150 3900 50  0001 C CNN
	1    13900 2900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5A142096
P 14700 4300
F 0 "#PWR03" H 14700 4050 50  0001 C CNN
F 1 "GND" H 14700 4150 50  0000 C CNN
F 2 "" H 14700 4300 50  0000 C CNN
F 3 "" H 14700 4300 50  0000 C CNN
	1    14700 4300
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR04
U 1 1 5A14209C
P 14700 1050
F 0 "#PWR04" H 14700 900 50  0001 C CNN
F 1 "+3.3V" H 14700 1190 50  0000 C CNN
F 2 "" H 14700 1050 50  0000 C CNN
F 3 "" H 14700 1050 50  0000 C CNN
	1    14700 1050
	-1   0    0    -1  
$EndComp
Text Label 14650 3300 2    60   ~ 0
SDA
Text Label 14650 3200 2    60   ~ 0
SCL
$Comp
L C-RESCUE-chordboard C1
U 1 1 5A15E7CA
P 13700 1550
F 0 "C1" H 13725 1650 50  0000 L CNN
F 1 "0.1uF" V 13800 1300 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 13738 1400 50  0001 C CNN
F 3 "" H 13700 1550 50  0001 C CNN
	1    13700 1550
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-chordboard C2
U 1 1 5A15E8E7
P 15650 1550
F 0 "C2" H 15675 1650 50  0000 L CNN
F 1 "0.1uF" V 15600 1300 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 15688 1400 50  0001 C CNN
F 3 "" H 15650 1550 50  0001 C CNN
	1    15650 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5A15F4B6
P 15650 1750
F 0 "#PWR05" H 15650 1500 50  0001 C CNN
F 1 "GND" H 15650 1600 50  0000 C CNN
F 2 "" H 15650 1750 50  0001 C CNN
F 3 "" H 15650 1750 50  0001 C CNN
	1    15650 1750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5A15FA29
P 13700 1750
F 0 "#PWR06" H 13700 1500 50  0001 C CNN
F 1 "GND" H 13700 1600 50  0000 C CNN
F 2 "" H 13700 1750 50  0001 C CNN
F 3 "" H 13700 1750 50  0001 C CNN
	1    13700 1750
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR07
U 1 1 5A170360
P 14900 3500
F 0 "#PWR07" H 14900 3350 50  0001 C CNN
F 1 "+3.3V" H 14900 3640 50  0000 C CNN
F 2 "" H 14900 3500 50  0001 C CNN
F 3 "" H 14900 3500 50  0001 C CNN
	1    14900 3500
	0    -1   -1   0   
$EndComp
Text Label 14400 2500 0    60   ~ 0
INT1B
Text Label 14400 2600 0    60   ~ 0
INT1A
Text Label 15000 2500 2    60   ~ 0
INT2B
Text Label 15000 2600 2    60   ~ 0
INT2A
Text Label 2150 1500 2    60   ~ 0
UART_TX
Text Label 2150 1400 2    60   ~ 0
UART_RX
NoConn ~ 2150 3700
NoConn ~ 4150 4800
NoConn ~ 4150 4500
NoConn ~ 4150 4400
NoConn ~ 4150 3600
NoConn ~ 4150 3500
NoConn ~ 4150 3400
NoConn ~ 4150 3300
NoConn ~ 4150 3200
NoConn ~ 4150 3100
NoConn ~ 4150 3000
NoConn ~ 4150 2900
NoConn ~ 4150 2800
NoConn ~ 4150 2700
NoConn ~ 4150 2600
NoConn ~ 4150 2500
NoConn ~ 4150 2400
NoConn ~ 4150 2300
NoConn ~ 4150 2200
NoConn ~ 4150 1900
NoConn ~ 4150 1800
NoConn ~ 4150 1700
NoConn ~ 4150 1600
NoConn ~ 4150 1500
NoConn ~ 4150 1400
NoConn ~ 4150 1300
Text Label 2150 2500 2    60   ~ 0
AU_MCLK
Text Label 2150 2400 2    60   ~ 0
AUSD_CS
Text Label 2150 2300 2    60   ~ 0
AU_BCLK
$Comp
L Mounting_Hole MK3
U 1 1 5A1F0B53
P 20000 12750
F 0 "MK3" H 20000 12950 50  0000 C CNN
F 1 "Mounting_Hole" H 20000 12875 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_Pad_Via" H 20000 12750 60  0001 C CNN
F 3 "" H 20000 12750 60  0001 C CNN
	1    20000 12750
	1    0    0    -1  
$EndComp
$Comp
L Mounting_Hole MK5
U 1 1 5A1F0B59
P 20000 12950
F 0 "MK5" H 20000 13150 50  0000 C CNN
F 1 "Mounting_Hole" H 20000 13075 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_Pad_Via" H 20000 12950 60  0001 C CNN
F 3 "" H 20000 12950 60  0001 C CNN
	1    20000 12950
	1    0    0    -1  
$EndComp
$Comp
L Mounting_Hole MK1
U 1 1 5A1F0B65
P 20000 12550
F 0 "MK1" H 20000 12750 50  0000 C CNN
F 1 "Mounting_Hole" H 20000 12675 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_Pad_Via" H 20000 12550 60  0001 C CNN
F 3 "" H 20000 12550 60  0001 C CNN
	1    20000 12550
	1    0    0    -1  
$EndComp
$Comp
L SW_SPST CORE_SW1
U 1 1 5A4897A5
P 1450 4000
F 0 "CORE_SW1" H 1300 4050 50  0000 C CNN
F 1 "SW_PGM" H 1650 4050 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 1450 4000 50  0001 C CNN
F 3 "" H 1450 4000 50  0001 C CNN
	1    1450 4000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5A4BC558
P 950 5300
F 0 "#PWR08" H 950 5050 50  0001 C CNN
F 1 "GND" H 950 5150 50  0000 C CNN
F 2 "" H 950 5300 50  0000 C CNN
F 3 "" H 950 5300 50  0000 C CNN
	1    950  5300
	1    0    0    -1  
$EndComp
$Comp
L USB_A_afshar J2
U 1 1 5A4E4CD3
P 4850 3900
F 0 "J2" H 4650 4350 50  0000 L CNN
F 1 "USB_A_afshar" H 4650 4250 50  0000 L CNN
F 2 "Connectors:USB_A" H 5000 3850 50  0001 C CNN
F 3 "" H 5000 3850 50  0001 C CNN
	1    4850 3900
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR09
U 1 1 5A5558C8
P 950 2700
F 0 "#PWR09" H 950 2550 50  0001 C CNN
F 1 "+3.3V" H 950 2840 50  0000 C CNN
F 2 "" H 950 2700 50  0001 C CNN
F 3 "" H 950 2700 50  0001 C CNN
	1    950  2700
	1    0    0    -1  
$EndComp
NoConn ~ 2150 3800
$Comp
L Mounting_Hole MK7
U 1 1 5A570F6D
P 20000 13150
F 0 "MK7" H 20000 13350 50  0000 C CNN
F 1 "Mounting_Hole" H 20000 13275 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_Pad_Via" H 20000 13150 60  0001 C CNN
F 3 "" H 20000 13150 60  0001 C CNN
	1    20000 13150
	1    0    0    -1  
$EndComp
$Comp
L Mounting_Hole MK2
U 1 1 5A57108E
P 20200 12550
F 0 "MK2" H 20200 12750 50  0000 C CNN
F 1 "Mounting_Hole" H 20200 12675 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_Pad_Via" H 20200 12550 60  0001 C CNN
F 3 "" H 20200 12550 60  0001 C CNN
	1    20200 12550
	1    0    0    -1  
$EndComp
$Comp
L Mounting_Hole MK4
U 1 1 5A5711AC
P 20200 12750
F 0 "MK4" H 20200 12950 50  0000 C CNN
F 1 "Mounting_Hole" H 20200 12875 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_Pad_Via" H 20200 12750 60  0001 C CNN
F 3 "" H 20200 12750 60  0001 C CNN
	1    20200 12750
	1    0    0    -1  
$EndComp
$Comp
L Mounting_Hole MK6
U 1 1 5A5712D5
P 20200 12950
F 0 "MK6" H 20200 13150 50  0000 C CNN
F 1 "Mounting_Hole" H 20200 13075 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_Pad_Via" H 20200 12950 60  0001 C CNN
F 3 "" H 20200 12950 60  0001 C CNN
	1    20200 12950
	1    0    0    -1  
$EndComp
$Comp
L Mounting_Hole MK8
U 1 1 5A5713F3
P 20200 13150
F 0 "MK8" H 20200 13350 50  0000 C CNN
F 1 "Mounting_Hole" H 20200 13275 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_2.7mm_Pad_Via" H 20200 13150 60  0001 C CNN
F 3 "" H 20200 13150 60  0001 C CNN
	1    20200 13150
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR010
U 1 1 5A5B87D7
P 4150 4700
F 0 "#PWR010" H 4150 4450 50  0001 C CNN
F 1 "AGND" H 4150 4500 50  0000 C CNN
F 2 "" H 4150 4700 50  0000 C CNN
F 3 "" H 4150 4700 50  0000 C CNN
	1    4150 4700
	0    -1   -1   0   
$EndComp
Text Label 2150 5200 2    60   ~ 0
AU_RX
Text Label 4150 4900 0    60   ~ 0
AU_LRCLK
Text Label 4150 5000 0    60   ~ 0
AU_TX
$Comp
L CONN_01X04 J1
U 1 1 5A5DEE38
P 800 1550
F 0 "J1" H 800 1800 50  0000 C CNN
F 1 "CONN_01X04" V 900 1550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 800 1550 50  0001 C CNN
F 3 "" H 800 1550 50  0001 C CNN
	1    800  1550
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5A5DFABA
P 1250 1900
F 0 "#PWR011" H 1250 1650 50  0001 C CNN
F 1 "GND" H 1250 1750 50  0000 C CNN
F 2 "" H 1250 1900 50  0000 C CNN
F 3 "" H 1250 1900 50  0000 C CNN
	1    1250 1900
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR012
U 1 1 5A5DFC24
P 1250 1200
F 0 "#PWR012" H 1250 1050 50  0001 C CNN
F 1 "+3.3V" H 1250 1340 50  0000 C CNN
F 2 "" H 1250 1200 50  0001 C CNN
F 3 "" H 1250 1200 50  0001 C CNN
	1    1250 1200
	1    0    0    -1  
$EndComp
Text Label 2150 2600 2    60   ~ 0
AUSD_MISO
$Comp
L Teensy_Audio_Board-RESCUE-chordboard U2
U 1 1 5A687A22
P 8800 3100
F 0 "U2" H 8600 4250 60  0000 C CNN
F 1 "Teensy_Audio_Board" H 8800 2550 60  0000 C CNN
F 2 "afshar-kicad-lib:Teensy_Audio_Board" H 8600 2550 60  0001 C CNN
F 3 "" H 8600 2550 60  0000 C CNN
	1    8800 3100
	1    0    0    -1  
$EndComp
Text Label 7550 3350 2    60   ~ 0
AU_MCLK
Text Label 7550 3150 2    60   ~ 0
AU_BCLK
Text Label 10100 3450 0    60   ~ 0
AU_RX
Text Label 10100 2850 0    60   ~ 0
SCL
Text Label 10100 2950 0    60   ~ 0
SDA
$Comp
L GND #PWR013
U 1 1 5A68C9D4
P 7200 2300
F 0 "#PWR013" H 7200 2050 50  0001 C CNN
F 1 "GND" H 7200 2150 50  0000 C CNN
F 2 "" H 7200 2300 50  0000 C CNN
F 3 "" H 7200 2300 50  0000 C CNN
	1    7200 2300
	1    0    0    -1  
$EndComp
Text Label 10100 2550 0    60   ~ 0
AU_TX
Text Label 10100 2450 0    60   ~ 0
AU_LRCLK
$Comp
L +3.3V #PWR014
U 1 1 5A68DAE5
P 10700 1800
F 0 "#PWR014" H 10700 1650 50  0001 C CNN
F 1 "+3.3V" H 10700 1940 50  0000 C CNN
F 2 "" H 10700 1800 50  0001 C CNN
F 3 "" H 10700 1800 50  0001 C CNN
	1    10700 1800
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR015
U 1 1 5A68DC9D
P 10100 2250
F 0 "#PWR015" H 10100 2000 50  0001 C CNN
F 1 "AGND" H 10100 2050 50  0000 C CNN
F 2 "" H 10100 2250 50  0000 C CNN
F 3 "" H 10100 2250 50  0000 C CNN
	1    10100 2250
	0    -1   -1   0   
$EndComp
Text Label 7550 3450 2    60   ~ 0
AU_SDMISO
Text Label 2150 5300 2    60   ~ 0
AUSD_SCLK
Text Label 10100 3350 0    60   ~ 0
AUSD_SCLK
Text Label 7550 3250 2    60   ~ 0
AUSD_CS
Text Label 2150 2100 2    60   ~ 0
AUSD_MOSI
Text Label 7550 2950 2    60   ~ 0
AUSD_MOSI
Text Label 2150 2000 2    60   ~ 0
AUMEM_CS
Text Label 7550 2850 2    60   ~ 0
AUMEM_CS
Text Label 5850 7300 2    60   ~ 0
PIXEL1_IN
$Comp
L +5V #PWR016
U 1 1 5A6DC2C7
P 5800 8050
F 0 "#PWR016" H 5800 7900 50  0001 C CNN
F 1 "+5V" H 5800 8190 50  0000 C CNN
F 2 "" H 5800 8050 50  0001 C CNN
F 3 "" H 5800 8050 50  0001 C CNN
	1    5800 8050
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-chordboard C5
U 1 1 5A6DC2CD
P 5700 8250
F 0 "C5" H 5725 8350 50  0000 L CNN
F 1 "0.1uF" V 5800 8000 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 5738 8100 50  0001 C CNN
F 3 "" H 5700 8250 50  0001 C CNN
	1    5700 8250
	1    0    0    -1  
$EndComp
$Comp
L 74LS245_aa U3
U 1 1 5A6DC2D8
P 6650 7800
F 0 "U3" H 6750 8375 50  0000 L BNN
F 1 "74LS245_aa" H 6700 7225 50  0000 L TNN
F 2 "Housings_DIP:DIP-20_W7.62mm" H 6650 7800 50  0001 C CNN
F 3 "" H 6650 7800 50  0001 C CNN
	1    6650 7800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR017
U 1 1 5A6DC2DF
P 6650 6900
F 0 "#PWR017" H 6650 6750 50  0001 C CNN
F 1 "+5V" H 6650 7040 50  0000 C CNN
F 2 "" H 6650 6900 50  0001 C CNN
F 3 "" H 6650 6900 50  0001 C CNN
	1    6650 6900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR018
U 1 1 5A6DC2E5
P 6650 8750
F 0 "#PWR018" H 6650 8500 50  0001 C CNN
F 1 "GND" H 6650 8600 50  0000 C CNN
F 2 "" H 6650 8750 50  0000 C CNN
F 3 "" H 6650 8750 50  0000 C CNN
	1    6650 8750
	1    0    0    -1  
$EndComp
$Comp
L MCP23017-RESCUE-chordboard U7
U 1 1 5A6E0484
P 19600 2900
F 0 "U7" H 19900 3400 50  0000 R CNN
F 1 "MCP23017" H 19900 3500 50  0000 R CNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 19650 1950 50  0001 L CNN
F 3 "" H 19850 3900 50  0001 C CNN
	1    19600 2900
	-1   0    0    -1  
$EndComp
$Comp
L C-RESCUE-chordboard C4
U 1 1 5A6E04EA
P 19750 1550
F 0 "C4" H 19775 1650 50  0000 L CNN
F 1 "0.1uF" V 19700 1300 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 19788 1400 50  0001 C CNN
F 3 "" H 19750 1550 50  0001 C CNN
	1    19750 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 5A6E04F0
P 19750 1750
F 0 "#PWR019" H 19750 1500 50  0001 C CNN
F 1 "GND" H 19750 1600 50  0000 C CNN
F 2 "" H 19750 1750 50  0001 C CNN
F 3 "" H 19750 1750 50  0001 C CNN
	1    19750 1750
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR020
U 1 1 5A6E04F6
P 19050 3600
F 0 "#PWR020" H 19050 3450 50  0001 C CNN
F 1 "+3.3V" H 19050 3740 50  0000 C CNN
F 2 "" H 19050 3600 50  0001 C CNN
F 3 "" H 19050 3600 50  0001 C CNN
	1    19050 3600
	0    -1   -1   0   
$EndComp
Text Label 19100 2500 2    60   ~ 0
INT4B
Text Label 19100 2600 2    60   ~ 0
INT4A
Text Label 18950 3200 2    60   ~ 0
SCL
Text Label 18950 3300 2    60   ~ 0
SDA
Text Label 5850 7400 2    60   ~ 0
PIXEL2_IN
NoConn ~ 4150 2000
NoConn ~ 4150 2100
NoConn ~ 4150 4200
NoConn ~ 4150 4300
$Comp
L +5V #PWR021
U 1 1 5A7333C1
P 4400 4450
F 0 "#PWR021" H 4400 4300 50  0001 C CNN
F 1 "+5V" H 4400 4590 50  0000 C CNN
F 2 "" H 4400 4450 50  0001 C CNN
F 3 "" H 4400 4450 50  0001 C CNN
	1    4400 4450
	1    0    0    -1  
$EndComp
$Comp
L OSHW-SCHEM-PCB6mm SYM1
U 1 1 5A738751
P 20650 12500
F 0 "SYM1" H 20800 12500 50  0000 L CNN
F 1 "OSHW-SCHEM-PCB6mm" H 20650 12450 50  0001 C CNN
F 2 "Symbols:OSHW-Logo_5.7x6mm_SilkScreen" H 20700 12400 50  0001 C CNN
F 3 "" H 20650 12500 50  0000 C CNN
	1    20650 12500
	1    0    0    -1  
$EndComp
$Comp
L OSHW-SCHEM-PCB6mm SYM2
U 1 1 5A73FD3C
P 20650 12750
F 0 "SYM2" H 20800 12750 50  0000 L CNN
F 1 "OSHW-SCHEM-PCB6mm" H 20650 12700 50  0001 C CNN
F 2 "Symbols:KiCad-Logo_6mm_SilkScreen" H 20700 12650 50  0001 C CNN
F 3 "" H 20650 12750 50  0000 C CNN
	1    20650 12750
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x08 J4
U 1 1 5B489D7C
P 13200 2400
F 0 "J4" H 13200 2800 50  0000 C CNN
F 1 "Conn_01x08" H 13200 1900 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 13200 2400 50  0001 C CNN
F 3 "" H 13200 2400 50  0001 C CNN
	1    13200 2400
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x08 J8
U 1 1 5B48A43D
P 13200 3300
F 0 "J8" H 13200 3700 50  0000 C CNN
F 1 "Conn_01x08" H 13200 2800 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 13200 3300 50  0001 C CNN
F 3 "" H 13200 3300 50  0001 C CNN
	1    13200 3300
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x08 J5
U 1 1 5B48A58A
P 16200 2400
F 0 "J5" H 16200 2800 50  0000 C CNN
F 1 "Conn_01x08" H 16200 1900 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 16200 2400 50  0001 C CNN
F 3 "" H 16200 2400 50  0001 C CNN
	1    16200 2400
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x08 J9
U 1 1 5B48A961
P 16200 3300
F 0 "J9" H 16200 3700 50  0000 C CNN
F 1 "Conn_01x08" H 16200 2800 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 16200 3300 50  0001 C CNN
F 3 "" H 16200 3300 50  0001 C CNN
	1    16200 3300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x08 J11
U 1 1 5B48AAC0
P 20300 3300
F 0 "J11" H 20300 3700 50  0000 C CNN
F 1 "Conn_01x08" H 20300 2800 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 20300 3300 50  0001 C CNN
F 3 "" H 20300 3300 50  0001 C CNN
	1    20300 3300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x08 J7
U 1 1 5B48AC4C
P 20300 2400
F 0 "J7" H 20300 2800 50  0000 C CNN
F 1 "Conn_01x08" H 20300 1900 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 20300 2400 50  0001 C CNN
F 3 "" H 20300 2400 50  0001 C CNN
	1    20300 2400
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x08 J3
U 1 1 5B48D822
P 7550 7600
F 0 "J3" H 7550 8000 50  0000 C CNN
F 1 "Conn_01x08" H 7550 7100 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 7550 7600 50  0001 C CNN
F 3 "" H 7550 7600 50  0001 C CNN
	1    7550 7600
	1    0    0    -1  
$EndComp
$Comp
L MCP23017-RESCUE-chordboard U6
U 1 1 5B490FE5
P 17600 2900
F 0 "U6" H 17600 3400 50  0000 R CNN
F 1 "MCP23017" H 17900 3500 50  0000 R CNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 17650 1950 50  0001 L CNN
F 3 "" H 17850 3900 50  0001 C CNN
	1    17600 2900
	1    0    0    -1  
$EndComp
Text Label 18350 3300 2    60   ~ 0
SDA
Text Label 18350 3200 2    60   ~ 0
SCL
$Comp
L C-RESCUE-chordboard C3
U 1 1 5B490FED
P 17400 1550
F 0 "C3" H 17425 1650 50  0000 L CNN
F 1 "0.1uF" V 17500 1300 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 17438 1400 50  0001 C CNN
F 3 "" H 17400 1550 50  0001 C CNN
	1    17400 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 5B490FF3
P 17400 1750
F 0 "#PWR022" H 17400 1500 50  0001 C CNN
F 1 "GND" H 17400 1600 50  0000 C CNN
F 2 "" H 17400 1750 50  0001 C CNN
F 3 "" H 17400 1750 50  0001 C CNN
	1    17400 1750
	1    0    0    -1  
$EndComp
Text Label 18100 2500 0    60   ~ 0
INT3B
Text Label 18100 2600 0    60   ~ 0
INT3A
$Comp
L Conn_01x08 J6
U 1 1 5B491005
P 16900 2400
F 0 "J6" H 16900 2800 50  0000 C CNN
F 1 "Conn_01x08" H 16900 1900 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 16900 2400 50  0001 C CNN
F 3 "" H 16900 2400 50  0001 C CNN
	1    16900 2400
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x08 J10
U 1 1 5B49100B
P 16900 3300
F 0 "J10" H 16900 3700 50  0000 C CNN
F 1 "Conn_01x08" H 16900 2800 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 16900 3300 50  0001 C CNN
F 3 "" H 16900 3300 50  0001 C CNN
	1    16900 3300
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR023
U 1 1 5B49158C
P 18200 3700
F 0 "#PWR023" H 18200 3550 50  0001 C CNN
F 1 "+3.3V" H 18200 3840 50  0000 C CNN
F 2 "" H 18200 3700 50  0001 C CNN
F 3 "" H 18200 3700 50  0001 C CNN
	1    18200 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 1700 1250 1900
Wire Wire Line
	1000 1700 1250 1700
Wire Wire Line
	1250 1600 1250 1200
Wire Wire Line
	1000 1600 1250 1600
Wire Wire Line
	2150 1500 1000 1500
Wire Wire Line
	2150 1400 1000 1400
Wire Wire Line
	4150 4100 4550 4100
Wire Wire Line
	4150 4000 4550 4000
Wire Wire Line
	4150 3900 4550 3900
Wire Wire Line
	4550 3800 4150 3800
Wire Wire Line
	4150 3700 4550 3700
Connection ~ 950  4000
Wire Wire Line
	950  3900 2150 3900
Connection ~ 950  4100
Wire Wire Line
	950  3900 950  5300
Wire Wire Line
	950  4100 1250 4100
Wire Wire Line
	950  4000 1250 4000
Wire Wire Line
	1650 4100 2150 4100
Wire Wire Line
	1650 4000 2150 4000
Wire Wire Line
	950  2700 2150 2700
Wire Wire Line
	13700 1300 19750 1300
Connection ~ 15500 1300
Connection ~ 14700 1300
Wire Wire Line
	14700 1300 14700 1050
Wire Wire Line
	13900 3950 19600 3950
Wire Wire Line
	14900 3500 15000 3500
Wire Wire Line
	13700 1700 13700 1750
Wire Wire Line
	15650 1700 15650 1750
Wire Wire Line
	15650 1300 15650 1400
Connection ~ 13900 1300
Wire Wire Line
	13700 1300 13700 1400
Wire Wire Line
	2150 1300 2150 900 
Wire Wire Line
	15000 3300 14400 3300
Wire Wire Line
	15000 3200 14400 3200
Connection ~ 13900 3950
Wire Wire Line
	13900 3900 13900 3950
Connection ~ 14700 3950
Wire Wire Line
	14500 1300 14500 2100
Wire Wire Line
	14500 2100 14400 2100
Connection ~ 14500 1300
Wire Wire Line
	13900 1300 13900 1900
Wire Wire Line
	14700 3500 14400 3500
Connection ~ 15500 3950
Wire Wire Line
	15500 3950 15500 3900
Wire Wire Line
	14700 3500 14700 4300
Connection ~ 14900 1300
Wire Wire Line
	14900 2100 14900 1300
Wire Wire Line
	15000 2100 14900 2100
Wire Wire Line
	15500 1300 15500 1900
Wire Wire Line
	4150 5400 5550 5400
Wire Wire Line
	4150 5300 5550 5300
Connection ~ 14700 3600
Wire Wire Line
	14400 3600 15000 3600
Connection ~ 14700 3700
Wire Wire Line
	14400 3700 15000 3700
Connection ~ 5300 4600
Wire Wire Line
	5300 4600 5300 3600
Wire Wire Line
	5250 4600 5350 4600
Connection ~ 5350 5300
Wire Wire Line
	5350 5300 5350 4900
Connection ~ 5250 5400
Wire Wire Line
	5250 5400 5250 4900
Wire Wire Line
	2150 5100 950  5100
Connection ~ 950  5100
Wire Wire Line
	7200 2150 7550 2150
Wire Wire Line
	7200 2150 7200 2300
Wire Wire Line
	10100 2350 10700 2350
Wire Wire Line
	10700 2350 10700 1800
Wire Wire Line
	5850 7300 5950 7300
Wire Wire Line
	5800 8200 5950 8200
Wire Wire Line
	5800 8050 5800 8200
Wire Wire Line
	5950 8300 5800 8300
Wire Wire Line
	5800 8300 5800 8700
Wire Wire Line
	5800 8100 5700 8100
Connection ~ 5800 8100
Wire Wire Line
	5800 8400 5700 8400
Connection ~ 5800 8400
Wire Wire Line
	6650 6950 6650 6900
Wire Wire Line
	6650 8650 6650 8750
Wire Wire Line
	5800 8700 6650 8700
Connection ~ 6650 8700
Connection ~ 19600 1300
Wire Wire Line
	19050 3600 19100 3600
Wire Wire Line
	19750 1700 19750 1750
Wire Wire Line
	19750 1300 19750 1400
Wire Wire Line
	18100 3300 19100 3300
Wire Wire Line
	18100 3200 19100 3200
Wire Wire Line
	19600 3950 19600 3900
Connection ~ 19000 1300
Wire Wire Line
	19000 2100 19000 1300
Wire Wire Line
	19100 2100 19000 2100
Wire Wire Line
	19600 1300 19600 1900
Wire Wire Line
	18750 3700 19100 3700
Wire Wire Line
	19100 3500 18750 3500
Wire Wire Line
	18750 3500 18750 3950
Connection ~ 18750 3950
Connection ~ 18750 3700
Connection ~ 16400 3950
Connection ~ 15650 1300
Wire Wire Line
	5850 7400 5950 7400
Connection ~ 19600 3950
Connection ~ 4400 4100
Wire Wire Line
	17400 1700 17400 1750
Wire Wire Line
	17400 1300 17400 1400
Wire Wire Line
	18200 1300 18200 2100
Wire Wire Line
	18200 2100 18100 2100
Wire Wire Line
	17600 1300 17600 1900
Wire Wire Line
	18500 3500 18100 3500
Wire Wire Line
	18100 3600 18500 3600
Wire Wire Line
	18100 3700 18200 3700
Connection ~ 17400 1300
Connection ~ 17600 1300
Connection ~ 18200 1300
Wire Wire Line
	18500 3500 18500 3950
Connection ~ 18500 3950
Connection ~ 18500 3600
Wire Wire Line
	17600 3900 17600 3950
Connection ~ 17600 3950
Text Label 2150 2200 2    60   ~ 0
PIXEL1_IN
Text Label 2150 3600 2    60   ~ 0
PIXEL2_IN
Text Label 10100 3250 0    60   ~ 0
A1
Text Label 2150 5400 2    60   ~ 0
A1
Wire Wire Line
	4150 4600 4400 4600
Wire Wire Line
	4400 4600 4400 4450
Text Label 2150 2900 2    60   ~ 0
INT1B
Text Label 2150 2800 2    60   ~ 0
INT1A
$Comp
L Conn_01x08 J12
U 1 1 5B4A4678
P 1950 4500
F 0 "J12" H 1950 4900 50  0000 C CNN
F 1 "Conn_01x08" H 1950 4000 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 1950 4500 50  0001 C CNN
F 3 "" H 1950 4500 50  0001 C CNN
	1    1950 4500
	-1   0    0    -1  
$EndComp
$Comp
L ESP-01_aa U9
U 1 1 5B4A4F03
P -2200 4200
F 0 "U9" H -2000 4450 60  0000 L CNN
F 1 "ESP-01_aa" H -2000 4350 60  0000 L CNN
F 2 "ESP8266:ESP-01" H -2200 4200 60  0001 C CNN
F 3 "" H -2200 4200 60  0001 C CNN
	1    -2200 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	-2200 4200 -2250 4200
Wire Wire Line
	-2250 4200 -2250 3900
$Comp
L R R3
U 1 1 5B4A57A2
P -2250 3750
F 0 "R3" V -2350 3700 50  0000 C CNN
F 1 "10K" V -2250 3750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V -2320 3750 50  0001 C CNN
F 3 "" H -2250 3750 50  0000 C CNN
	1    -2250 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	-1000 4200 -850 4200
Wire Wire Line
	-850 4200 -850 3900
$Comp
L R R4
U 1 1 5B4A5C15
P -850 3750
F 0 "R4" V -950 3700 50  0000 C CNN
F 1 "10K" V -850 3750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V -920 3750 50  0001 C CNN
F 3 "" H -850 3750 50  0000 C CNN
	1    -850 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	-1000 4500 -550 4500
Wire Wire Line
	-550 4500 -550 3900
$Comp
L R R5
U 1 1 5B4A5D7C
P -550 3750
F 0 "R5" V -650 3700 50  0000 C CNN
F 1 "10K" V -550 3750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V -620 3750 50  0001 C CNN
F 3 "" H -550 3750 50  0000 C CNN
	1    -550 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	-550 3600 -550 3550
Wire Wire Line
	-850 3600 -850 3550
Wire Wire Line
	-2250 3600 -2250 3550
Wire Wire Line
	-1000 4400 2150 4400
Wire Wire Line
	-1000 4300 2150 4300
Wire Wire Line
	-1600 3900 -1600 3600
Wire Wire Line
	-1600 4800 -1600 5000
$Comp
L GND #PWR024
U 1 1 5B4A6C33
P -1600 5000
F 0 "#PWR024" H -1600 4750 50  0001 C CNN
F 1 "GND" H -1600 4850 50  0000 C CNN
F 2 "" H -1600 5000 50  0000 C CNN
F 3 "" H -1600 5000 50  0000 C CNN
	1    -1600 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	-3000 3100 -2850 3100
Wire Wire Line
	-2850 2800 -2850 3350
Wire Wire Line
	-3600 3100 -3800 3100
Wire Wire Line
	-3800 2800 -3800 3300
$Comp
L GND #PWR025
U 1 1 5B4A784D
P -3300 3950
F 0 "#PWR025" H -3300 3700 50  0001 C CNN
F 1 "GND" H -3300 3800 50  0000 C CNN
F 2 "" H -3300 3950 50  0000 C CNN
F 3 "" H -3300 3950 50  0000 C CNN
	1    -3300 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	-3300 3950 -3300 3400
Connection ~ -2850 3100
Connection ~ -3800 3100
$Comp
L +3.3VA #PWR026
U 1 1 5B4A7BDF
P -550 3550
F 0 "#PWR026" H -550 3400 50  0001 C CNN
F 1 "+3.3VA" H -550 3690 50  0000 C CNN
F 2 "" H -550 3550 50  0001 C CNN
F 3 "" H -550 3550 50  0001 C CNN
	1    -550 3550
	1    0    0    -1  
$EndComp
$Comp
L +3.3VA #PWR027
U 1 1 5B4A7C6F
P -850 3550
F 0 "#PWR027" H -850 3400 50  0001 C CNN
F 1 "+3.3VA" H -850 3690 50  0000 C CNN
F 2 "" H -850 3550 50  0001 C CNN
F 3 "" H -850 3550 50  0001 C CNN
	1    -850 3550
	1    0    0    -1  
$EndComp
$Comp
L +3.3VA #PWR028
U 1 1 5B4A7CF8
P -1600 3600
F 0 "#PWR028" H -1600 3450 50  0001 C CNN
F 1 "+3.3VA" H -1600 3740 50  0000 C CNN
F 2 "" H -1600 3600 50  0001 C CNN
F 3 "" H -1600 3600 50  0001 C CNN
	1    -1600 3600
	1    0    0    -1  
$EndComp
$Comp
L +3.3VA #PWR029
U 1 1 5B4A7D81
P -2250 3550
F 0 "#PWR029" H -2250 3400 50  0001 C CNN
F 1 "+3.3VA" H -2250 3690 50  0000 C CNN
F 2 "" H -2250 3550 50  0001 C CNN
F 3 "" H -2250 3550 50  0001 C CNN
	1    -2250 3550
	1    0    0    -1  
$EndComp
$Comp
L +3.3VA #PWR030
U 1 1 5B4A7E48
P -2850 2800
F 0 "#PWR030" H -2850 2650 50  0001 C CNN
F 1 "+3.3VA" H -2850 2940 50  0000 C CNN
F 2 "" H -2850 2800 50  0001 C CNN
F 3 "" H -2850 2800 50  0001 C CNN
	1    -2850 2800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR031
U 1 1 5B4A811D
P -3800 2800
F 0 "#PWR031" H -3800 2650 50  0001 C CNN
F 1 "+5V" H -3800 2940 50  0000 C CNN
F 2 "" H -3800 2800 50  0001 C CNN
F 3 "" H -3800 2800 50  0001 C CNN
	1    -3800 2800
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-chordboard C6
U 1 1 5B4A8470
P -3800 3450
F 0 "C6" H -3775 3550 50  0000 L CNN
F 1 "0.1uF" V -3700 3200 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H -3762 3300 50  0001 C CNN
F 3 "" H -3800 3450 50  0001 C CNN
	1    -3800 3450
	1    0    0    -1  
$EndComp
$Comp
L CP C7
U 1 1 5B4A86D0
P -2850 3500
F 0 "C7" H -2825 3600 50  0000 L CNN
F 1 "CP" H -2825 3400 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.00mm" H -2812 3350 50  0001 C CNN
F 3 "" H -2850 3500 50  0001 C CNN
	1    -2850 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	-2850 3800 -2850 3650
Wire Wire Line
	-3800 3800 -2850 3800
Connection ~ -3300 3800
Wire Wire Line
	-3800 3800 -3800 3600
$Comp
L LM1117-3.3 U8
U 1 1 5B4A8D88
P -3300 3100
F 0 "U8" H -3450 3225 50  0000 C CNN
F 1 "LM1117-3.3" H -3300 3225 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Horizontal" H -3300 3100 50  0001 C CNN
F 3 "" H -3300 3100 50  0001 C CNN
	1    -3300 3100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
