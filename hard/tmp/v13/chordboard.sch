EESchema Schematic File Version 4
LIBS:chordboard-rescue
LIBS:teensy
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Bosch
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:display
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:Lattice
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:mcp73831t-2aci-ot
LIBS:mechanical
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:onsemi
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:power
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:silabs
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:standard
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:supertex
LIBS:switches
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:Wetmelon
LIBS:chordboard-components
LIBS:chordboard-cache
EELAYER 26 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title "Chordboard"
Date "2018-01-19"
Rev "v4"
Comp "aafshar@gmail.com"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R2
U 1 1 59FC7069
P 5350 4750
F 0 "R2" V 5450 4700 50  0000 C CNN
F 1 "4.7K" V 5350 4750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5280 4750 50  0001 C CNN
F 3 "" H 5350 4750 50  0000 C CNN
	1    5350 4750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR09
U 1 1 59FC7113
P 5300 3600
F 0 "#PWR09" H 5300 3450 50  0001 C CNN
F 1 "+3.3V" H 5300 3740 50  0000 C CNN
F 2 "" H 5300 3600 50  0000 C CNN
F 3 "" H 5300 3600 50  0000 C CNN
	1    5300 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 59FE1F2D
P 2150 900
F 0 "#PWR01" H 2150 650 50  0001 C CNN
F 1 "GND" H 2150 750 50  0000 C CNN
F 2 "" H 2150 900 50  0000 C CNN
F 3 "" H 2150 900 50  0000 C CNN
	1    2150 900 
	-1   0    0    1   
$EndComp
Text Label 5550 5300 0    60   ~ 0
SCL
Text Label 5550 5400 0    60   ~ 0
SDA
Text Label 9900 8650 0    60   ~ 0
SCL
$Comp
L chordboard-components:KSW K17
U 1 1 5A1420CD
P 8350 8550
F 0 "K17" H 8500 8600 50  0000 C CNN
F 1 "SW_SPST" H 8200 8600 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 8550 50  0001 C CNN
F 3 "" H 8350 8550 50  0000 C CNN
	1    8350 8550
	-1   0    0    -1  
$EndComp
Text Label 2150 1500 2    60   ~ 0
UART_TX
Text Label 2150 1400 2    60   ~ 0
UART_RX
NoConn ~ 2150 3700
NoConn ~ 4150 4800
NoConn ~ 4150 4500
NoConn ~ 4150 4400
NoConn ~ 4150 3600
NoConn ~ 4150 3500
NoConn ~ 4150 3400
NoConn ~ 4150 3300
NoConn ~ 4150 3200
NoConn ~ 4150 3100
NoConn ~ 4150 3000
NoConn ~ 4150 2900
NoConn ~ 4150 2800
NoConn ~ 4150 2700
NoConn ~ 4150 2600
NoConn ~ 4150 2500
NoConn ~ 4150 2400
NoConn ~ 4150 2300
NoConn ~ 4150 2200
NoConn ~ 4150 1900
NoConn ~ 4150 1800
NoConn ~ 4150 1700
NoConn ~ 4150 1600
NoConn ~ 4150 1500
NoConn ~ 4150 1400
NoConn ~ 4150 1300
Text Label 2150 2500 2    60   ~ 0
AU_MCLK
Text Label 2150 2400 2    60   ~ 0
AUSD_CS
Text Label 2150 2300 2    60   ~ 0
AU_BCLK
$Comp
L Switch:SW_SPST CORE_SW2
U 1 1 5A489629
P 1450 4100
F 0 "CORE_SW2" H 1300 4150 50  0000 C CNN
F 1 "SW_RST" H 1650 4150 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_CuK_OS102011MA1QN1_SPDT_Angled" H 1450 4100 50  0001 C CNN
F 3 "" H 1450 4100 50  0001 C CNN
	1    1450 4100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST CORE_SW1
U 1 1 5A4897A5
P 1450 4000
F 0 "CORE_SW1" H 1300 4050 50  0000 C CNN
F 1 "SW_PGM" H 1650 4050 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 1450 4000 50  0001 C CNN
F 3 "" H 1450 4000 50  0001 C CNN
	1    1450 4000
	1    0    0    -1  
$EndComp
Text Label 2150 4900 2    60   ~ 0
AP_1
Text Label 2150 5000 2    60   ~ 0
AP_2
$Comp
L power:GND #PWR013
U 1 1 5A4BC558
P 950 5300
F 0 "#PWR013" H 950 5050 50  0001 C CNN
F 1 "GND" H 950 5150 50  0000 C CNN
F 2 "" H 950 5300 50  0000 C CNN
F 3 "" H 950 5300 50  0000 C CNN
	1    950  5300
	1    0    0    -1  
$EndComp
$Comp
L chordboard-components:USB_A J2
U 1 1 5A4E4CD3
P 4850 3900
F 0 "J2" H 4650 4350 50  0000 L CNN
F 1 "USB_A_afshar" H 4650 4250 50  0000 L CNN
F 2 "Connectors:USB_A" H 5000 3850 50  0001 C CNN
F 3 "" H 5000 3850 50  0001 C CNN
	1    4850 3900
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-rescue:+3.3V- #PWR05
U 1 1 5A5558C8
P 950 2700
F 0 "#PWR05" H 950 2550 50  0001 C CNN
F 1 "+3.3V" H 950 2840 50  0000 C CNN
F 2 "" H 950 2700 50  0001 C CNN
F 3 "" H 950 2700 50  0001 C CNN
	1    950  2700
	1    0    0    -1  
$EndComp
NoConn ~ 2150 3800
$Comp
L power:GNDA #PWR010
U 1 1 5A5B87D7
P 4150 4700
F 0 "#PWR010" H 4150 4450 50  0001 C CNN
F 1 "GNDA" H 4150 4500 50  0000 C CNN
F 2 "" H 4150 4700 50  0000 C CNN
F 3 "" H 4150 4700 50  0000 C CNN
	1    4150 4700
	0    -1   -1   0   
$EndComp
Text Label 2150 5200 2    60   ~ 0
AU_RX
Text Label 4150 4900 0    60   ~ 0
AU_LRCLK
Text Label 4150 5000 0    60   ~ 0
AU_TX
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5A5DEE38
P 800 1500
F 0 "J1" H 800 1750 50  0000 C CNN
F 1 "CONN_01X04" V 900 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 800 1500 50  0001 C CNN
F 3 "" H 800 1500 50  0001 C CNN
	1    800  1500
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5A5DFABA
P 1250 1900
F 0 "#PWR03" H 1250 1650 50  0001 C CNN
F 1 "GND" H 1250 1750 50  0000 C CNN
F 2 "" H 1250 1900 50  0000 C CNN
F 3 "" H 1250 1900 50  0000 C CNN
	1    1250 1900
	1    0    0    -1  
$EndComp
$Comp
L chordboard-rescue:+3.3V- #PWR02
U 1 1 5A5DFC24
P 1250 1200
F 0 "#PWR02" H 1250 1050 50  0001 C CNN
F 1 "+3.3V" H 1250 1340 50  0000 C CNN
F 2 "" H 1250 1200 50  0001 C CNN
F 3 "" H 1250 1200 50  0001 C CNN
	1    1250 1200
	1    0    0    -1  
$EndComp
Text Label 2150 2600 2    60   ~ 0
AUSD_MISO
$Comp
L chordboard-components:Teensy_Audio_Board U2
U 1 1 5A687A22
P 9900 3600
F 0 "U2" H 9700 4750 60  0000 C CNN
F 1 "Teensy_Audio_Board" H 9900 3050 60  0000 C CNN
F 2 "chordboard-footprints:Teensy_Audio_Board" H 9700 3050 60  0001 C CNN
F 3 "" H 9700 3050 60  0000 C CNN
	1    9900 3600
	1    0    0    -1  
$EndComp
Text Label 9150 3900 2    60   ~ 0
AU_MCLK
Text Label 9150 3700 2    60   ~ 0
AU_BCLK
Text Label 10600 4000 0    60   ~ 0
AU_RX
Text Label 10600 3400 0    60   ~ 0
SCL
Text Label 10600 3500 0    60   ~ 0
SDA
$Comp
L power:GND #PWR07
U 1 1 5A68C9D4
P 8800 2850
F 0 "#PWR07" H 8800 2600 50  0001 C CNN
F 1 "GND" H 8800 2700 50  0000 C CNN
F 2 "" H 8800 2850 50  0000 C CNN
F 3 "" H 8800 2850 50  0000 C CNN
	1    8800 2850
	1    0    0    -1  
$EndComp
Text Label 10600 3100 0    60   ~ 0
AU_TX
Text Label 10600 3000 0    60   ~ 0
AU_LRCLK
$Comp
L chordboard-rescue:+3.3V- #PWR04
U 1 1 5A68DAE5
P 11200 2350
F 0 "#PWR04" H 11200 2200 50  0001 C CNN
F 1 "+3.3V" H 11200 2490 50  0000 C CNN
F 2 "" H 11200 2350 50  0001 C CNN
F 3 "" H 11200 2350 50  0001 C CNN
	1    11200 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR06
U 1 1 5A68DC9D
P 10600 2800
F 0 "#PWR06" H 10600 2550 50  0001 C CNN
F 1 "GNDA" H 10600 2600 50  0000 C CNN
F 2 "" H 10600 2800 50  0000 C CNN
F 3 "" H 10600 2800 50  0000 C CNN
	1    10600 2800
	0    -1   -1   0   
$EndComp
Text Label 9150 4000 2    60   ~ 0
AU_SDMISO
Text Label 2150 5300 2    60   ~ 0
AUSD_SCLK
Text Label 10600 3900 0    60   ~ 0
AUSD_SCLK
Text Label 9150 3800 2    60   ~ 0
AUSD_CS
Text Label 2150 2100 2    60   ~ 0
AUSD_MOSI
Text Label 9150 3500 2    60   ~ 0
AUSD_MOSI
Text Label 2150 2000 2    60   ~ 0
AUMEM_CS
Text Label 9150 3400 2    60   ~ 0
AUMEM_CS
Text Label 2300 11650 2    60   ~ 0
PIXEL1_IN
Wire Wire Line
	1250 1700 1250 1900
Wire Wire Line
	1000 1700 1250 1700
Wire Wire Line
	1250 1600 1250 1200
Wire Wire Line
	1000 1600 1250 1600
Wire Wire Line
	2150 1500 1000 1500
Wire Wire Line
	2150 1400 1000 1400
Wire Wire Line
	4150 4100 4400 4100
Wire Wire Line
	4150 4000 4550 4000
Wire Wire Line
	4150 3900 4550 3900
Wire Wire Line
	4550 3800 4150 3800
Wire Wire Line
	4150 3700 4550 3700
Connection ~ 950  4000
Wire Wire Line
	950  3900 2150 3900
Connection ~ 950  4100
Wire Wire Line
	950  3900 950  4000
Wire Wire Line
	950  4100 1250 4100
Wire Wire Line
	950  4000 1250 4000
Wire Wire Line
	1650 4100 2150 4100
Wire Wire Line
	1650 4000 2150 4000
Wire Wire Line
	950  2700 2150 2700
Wire Wire Line
	2150 1300 2150 900 
Wire Wire Line
	4150 5400 5250 5400
Wire Wire Line
	4150 5300 5350 5300
Connection ~ 5300 4600
Wire Wire Line
	5300 4600 5300 3600
Wire Wire Line
	5250 4600 5300 4600
Connection ~ 5350 5300
Wire Wire Line
	5350 5300 5350 4900
Connection ~ 5250 5400
Wire Wire Line
	5250 5400 5250 4900
Wire Wire Line
	2150 5100 950  5100
Connection ~ 950  5100
Wire Wire Line
	8800 2700 9150 2700
Wire Wire Line
	8800 2700 8800 2850
Wire Wire Line
	10600 2900 11200 2900
Wire Wire Line
	11200 2900 11200 2350
Text Label 4150 5500 0    60   ~ 0
AP_3
Text Label -550 2350 2    60   ~ 0
RE1_A
Text Label -550 2450 2    60   ~ 0
RE1_B
Text Label -550 2550 2    60   ~ 0
RE2_A
Text Label -550 2650 2    60   ~ 0
RE2_B
Text Label -600 2800 2    60   ~ 0
RE3_A
Text Label -600 2900 2    60   ~ 0
RE3_B
Text Label -600 3000 2    60   ~ 0
RE4_A
Text Label -600 3100 2    60   ~ 0
RE4_B
Text Label -600 3200 2    60   ~ 0
RE5_A
Text Label -600 3300 2    60   ~ 0
RE5_B
Text Label -600 3400 2    60   ~ 0
RE6_A
Text Label -600 3500 2    60   ~ 0
RE6_B
Text Label 3850 9450 2    60   ~ 0
RE1_SW
Text Label 3850 9550 2    60   ~ 0
RE2_SW
Text Label 3850 9650 2    60   ~ 0
RE3_SW
Text Label 3850 9750 2    60   ~ 0
RE4_SW
Text Label 3850 9850 2    60   ~ 0
RE5_SW
Text Label 3850 9950 2    60   ~ 0
RE6_SW
Text Label 3850 10050 2    60   ~ 0
RE7_SW
Text Label 2150 2200 2    60   ~ 0
PIXEL1_IN
Text Label 1700 5850 2    60   ~ 0
DAC0
Text Label 1700 5950 2    60   ~ 0
DAC1
NoConn ~ 4150 2000
NoConn ~ 4150 2100
NoConn ~ 4150 4200
NoConn ~ 4150 4300
Wire Wire Line
	4400 4100 4400 3350
Connection ~ 4400 4100
$Comp
L power:+5V #PWR08
U 1 1 5A7333C1
P 4400 3350
F 0 "#PWR08" H 4400 3200 50  0001 C CNN
F 1 "+5V" H 4400 3490 50  0000 C CNN
F 2 "" H 4400 3350 50  0001 C CNN
F 3 "" H 4400 3350 50  0001 C CNN
	1    4400 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  4000 950  4100
Wire Wire Line
	950  4100 950  5100
Wire Wire Line
	5300 4600 5350 4600
Wire Wire Line
	5350 5300 5550 5300
Wire Wire Line
	5250 5400 5550 5400
Wire Wire Line
	950  5100 950  5300
Wire Wire Line
	4400 4100 4550 4100
Text Label -550 3800 2    60   ~ 0
RE7_B
Text Label -550 3700 2    60   ~ 0
RE7_A
Text Label -550 4000 2    60   ~ 0
RE8_B
Text Label -550 3900 2    60   ~ 0
RE8_A
$Comp
L Device:Rotary_Encoder_Switch ENC3
U 1 1 5BDC5A37
P 26250 5350
F 0 "ENC3" H 26200 5750 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 26300 5000 60  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 26250 5650 60  0000 C CNN
F 3 "" H 26250 5350 60  0000 C CNN
	1    26250 5350
	-1   0    0    -1  
$EndComp
Text Label 26550 5250 0    60   ~ 0
RE3_A
Text Label 26550 5450 0    60   ~ 0
RE3_B
Text Label 25950 5250 2    60   ~ 0
RE3_SW
$Comp
L Device:C C5
U 1 1 5BDC5A43
P 26700 5800
F 0 "C5" V 26650 5850 50  0000 L CNN
F 1 "0.1uF" V 26650 5550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 26738 5650 50  0001 C CNN
F 3 "" H 26700 5800 50  0001 C CNN
	1    26700 5800
	-1   0    0    1   
$EndComp
$Comp
L Device:C C6
U 1 1 5BDC5A49
P 27000 5800
F 0 "C6" V 26950 5850 50  0000 L CNN
F 1 "0.1uF" V 26950 5550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 27038 5650 50  0001 C CNN
F 3 "" H 27000 5800 50  0001 C CNN
	1    27000 5800
	-1   0    0    1   
$EndComp
$Comp
L chordboard-components:Teensy3.6 U1
U 1 1 5BDF6B49
P 3150 3400
F 0 "U1" H 3150 5787 60  0000 C CNN
F 1 "Teensy3.6" H 3150 5681 60  0000 C CNN
F 2 "chordboard-footprints:Teensy35_36_external_pins" H 3150 3400 60  0001 C CNN
F 3 "" H 3150 3400 60  0000 C CNN
	1    3150 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 59FC6FE8
P 5250 4750
F 0 "R1" V 5150 4700 50  0000 C CNN
F 1 "4.7K" V 5250 4750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5180 4750 50  0001 C CNN
F 3 "" H 5250 4750 50  0000 C CNN
	1    5250 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	26550 5450 26700 5450
Wire Wire Line
	26700 5450 26700 5650
Wire Wire Line
	26550 5250 27000 5250
Wire Wire Line
	27000 5250 27000 5650
Wire Wire Line
	26700 5950 26700 6050
Wire Wire Line
	26700 6050 26850 6050
Wire Wire Line
	27000 6050 27000 5950
Wire Wire Line
	25950 5450 25900 5450
Wire Wire Line
	25900 5450 25900 6050
Wire Wire Line
	25900 6050 26700 6050
Connection ~ 26700 6050
Wire Wire Line
	26700 6050 26700 6250
$Comp
L power:GND #PWR015
U 1 1 5BEF0B62
P 26700 6250
F 0 "#PWR015" H 26700 6000 50  0001 C CNN
F 1 "GND" H 26705 6077 50  0000 C CNN
F 2 "" H 26700 6250 50  0001 C CNN
F 3 "" H 26700 6250 50  0001 C CNN
	1    26700 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	26550 5350 26850 5350
Wire Wire Line
	26850 5350 26850 6050
Connection ~ 26850 6050
Wire Wire Line
	26850 6050 27000 6050
$Comp
L Device:Rotary_Encoder_Switch ENC1
U 1 1 5C18DCA2
P 26250 3850
F 0 "ENC1" H 26200 4250 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 26300 3500 60  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 26250 4150 60  0000 C CNN
F 3 "" H 26250 3850 60  0000 C CNN
	1    26250 3850
	-1   0    0    -1  
$EndComp
Text Label 26550 3750 0    60   ~ 0
RE1_A
Text Label 26550 3950 0    60   ~ 0
RE1_B
Text Label 25950 3750 2    60   ~ 0
RE1_SW
$Comp
L Device:C C1
U 1 1 5C18DCAC
P 26700 4300
F 0 "C1" V 26650 4350 50  0000 L CNN
F 1 "0.1uF" V 26650 4050 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 26738 4150 50  0001 C CNN
F 3 "" H 26700 4300 50  0001 C CNN
	1    26700 4300
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 5C18DCB3
P 27000 4300
F 0 "C2" V 26950 4350 50  0000 L CNN
F 1 "0.1uF" V 26950 4050 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 27038 4150 50  0001 C CNN
F 3 "" H 27000 4300 50  0001 C CNN
	1    27000 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	26550 3950 26700 3950
Wire Wire Line
	26700 3950 26700 4150
Wire Wire Line
	26550 3750 27000 3750
Wire Wire Line
	27000 3750 27000 4150
Wire Wire Line
	26700 4450 26700 4550
Wire Wire Line
	26700 4550 26850 4550
Wire Wire Line
	27000 4550 27000 4450
Wire Wire Line
	25950 3950 25900 3950
Wire Wire Line
	25900 3950 25900 4550
Wire Wire Line
	25900 4550 26700 4550
Connection ~ 26700 4550
Wire Wire Line
	26700 4550 26700 4750
$Comp
L power:GND #PWR011
U 1 1 5C18DCC6
P 26700 4750
F 0 "#PWR011" H 26700 4500 50  0001 C CNN
F 1 "GND" H 26705 4577 50  0000 C CNN
F 2 "" H 26700 4750 50  0001 C CNN
F 3 "" H 26700 4750 50  0001 C CNN
	1    26700 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	26550 3850 26850 3850
Wire Wire Line
	26850 3850 26850 4550
Connection ~ 26850 4550
Wire Wire Line
	26850 4550 27000 4550
$Comp
L Device:Rotary_Encoder_Switch ENC7
U 1 1 5C1F8BAA
P 26250 8350
F 0 "ENC7" H 26200 8750 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 26300 8000 60  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 26250 8650 60  0000 C CNN
F 3 "" H 26250 8350 60  0000 C CNN
	1    26250 8350
	-1   0    0    -1  
$EndComp
Text Label 26550 8250 0    60   ~ 0
RE7_A
Text Label 26550 8450 0    60   ~ 0
RE7_B
Text Label 25950 8250 2    60   ~ 0
RE7_SW
$Comp
L Device:C C13
U 1 1 5C1F8BB4
P 26700 8800
F 0 "C13" V 26650 8850 50  0000 L CNN
F 1 "0.1uF" V 26650 8550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 26738 8650 50  0001 C CNN
F 3 "" H 26700 8800 50  0001 C CNN
	1    26700 8800
	-1   0    0    1   
$EndComp
$Comp
L Device:C C14
U 1 1 5C1F8BBB
P 27000 8800
F 0 "C14" V 26950 8850 50  0000 L CNN
F 1 "0.1uF" V 26950 8550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 27038 8650 50  0001 C CNN
F 3 "" H 27000 8800 50  0001 C CNN
	1    27000 8800
	-1   0    0    1   
$EndComp
Wire Wire Line
	26550 8450 26700 8450
Wire Wire Line
	26700 8450 26700 8650
Wire Wire Line
	26550 8250 27000 8250
Wire Wire Line
	27000 8250 27000 8650
Wire Wire Line
	26700 8950 26700 9050
Wire Wire Line
	26700 9050 26850 9050
Wire Wire Line
	27000 9050 27000 8950
Wire Wire Line
	25950 8450 25900 8450
Wire Wire Line
	25900 8450 25900 9050
Wire Wire Line
	25900 9050 26700 9050
Connection ~ 26700 9050
Wire Wire Line
	26700 9050 26700 9250
$Comp
L power:GND #PWR025
U 1 1 5C1F8BCE
P 26700 9250
F 0 "#PWR025" H 26700 9000 50  0001 C CNN
F 1 "GND" H 26705 9077 50  0000 C CNN
F 2 "" H 26700 9250 50  0001 C CNN
F 3 "" H 26700 9250 50  0001 C CNN
	1    26700 9250
	1    0    0    -1  
$EndComp
Wire Wire Line
	26550 8350 26850 8350
Wire Wire Line
	26850 8350 26850 9050
Connection ~ 26850 9050
Wire Wire Line
	26850 9050 27000 9050
$Comp
L Device:Rotary_Encoder_Switch ENC5
U 1 1 5C1F8BD8
P 26250 6850
F 0 "ENC5" H 26200 7250 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 26300 6500 60  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 26250 7150 60  0000 C CNN
F 3 "" H 26250 6850 60  0000 C CNN
	1    26250 6850
	-1   0    0    -1  
$EndComp
Text Label 26550 6750 0    60   ~ 0
RE5_A
Text Label 26550 6950 0    60   ~ 0
RE5_B
Text Label 25950 6750 2    60   ~ 0
RE5_SW
$Comp
L Device:C C9
U 1 1 5C1F8BE2
P 26700 7300
F 0 "C9" V 26650 7350 50  0000 L CNN
F 1 "0.1uF" V 26650 7050 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 26738 7150 50  0001 C CNN
F 3 "" H 26700 7300 50  0001 C CNN
	1    26700 7300
	-1   0    0    1   
$EndComp
$Comp
L Device:C C10
U 1 1 5C1F8BE9
P 27000 7300
F 0 "C10" V 26950 7350 50  0000 L CNN
F 1 "0.1uF" V 26950 7050 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 27038 7150 50  0001 C CNN
F 3 "" H 27000 7300 50  0001 C CNN
	1    27000 7300
	-1   0    0    1   
$EndComp
Wire Wire Line
	26550 6950 26700 6950
Wire Wire Line
	26700 6950 26700 7150
Wire Wire Line
	26550 6750 27000 6750
Wire Wire Line
	27000 6750 27000 7150
Wire Wire Line
	26700 7450 26700 7550
Wire Wire Line
	26700 7550 26850 7550
Wire Wire Line
	27000 7550 27000 7450
Wire Wire Line
	25950 6950 25900 6950
Wire Wire Line
	25900 6950 25900 7550
Wire Wire Line
	25900 7550 26700 7550
Connection ~ 26700 7550
Wire Wire Line
	26700 7550 26700 7750
$Comp
L power:GND #PWR021
U 1 1 5C1F8BFC
P 26700 7750
F 0 "#PWR021" H 26700 7500 50  0001 C CNN
F 1 "GND" H 26705 7577 50  0000 C CNN
F 2 "" H 26700 7750 50  0001 C CNN
F 3 "" H 26700 7750 50  0001 C CNN
	1    26700 7750
	1    0    0    -1  
$EndComp
Wire Wire Line
	26550 6850 26850 6850
Wire Wire Line
	26850 6850 26850 7550
Connection ~ 26850 7550
Wire Wire Line
	26850 7550 27000 7550
$Comp
L Device:Rotary_Encoder_Switch ENC4
U 1 1 5C26BB9A
P 29450 5350
F 0 "ENC4" H 29400 5750 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 29500 5000 60  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 29450 5650 60  0000 C CNN
F 3 "" H 29450 5350 60  0000 C CNN
	1    29450 5350
	-1   0    0    -1  
$EndComp
Text Label 29750 5250 0    60   ~ 0
RE4_A
Text Label 29750 5450 0    60   ~ 0
RE4_B
Text Label 29150 5250 2    60   ~ 0
RE4_SW
$Comp
L Device:C C7
U 1 1 5C26BBA3
P 29900 5800
F 0 "C7" V 29850 5850 50  0000 L CNN
F 1 "0.1uF" V 29850 5550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 29938 5650 50  0001 C CNN
F 3 "" H 29900 5800 50  0001 C CNN
	1    29900 5800
	-1   0    0    1   
$EndComp
$Comp
L Device:C C8
U 1 1 5C26BBA9
P 30200 5800
F 0 "C8" V 30150 5850 50  0000 L CNN
F 1 "0.1uF" V 30150 5550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 30238 5650 50  0001 C CNN
F 3 "" H 30200 5800 50  0001 C CNN
	1    30200 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	29750 5450 29900 5450
Wire Wire Line
	29900 5450 29900 5650
Wire Wire Line
	29750 5250 30200 5250
Wire Wire Line
	30200 5250 30200 5650
Wire Wire Line
	29900 5950 29900 6050
Wire Wire Line
	29900 6050 30050 6050
Wire Wire Line
	30200 6050 30200 5950
Wire Wire Line
	29150 5450 29100 5450
Wire Wire Line
	29100 5450 29100 6050
Wire Wire Line
	29100 6050 29900 6050
Connection ~ 29900 6050
Wire Wire Line
	29900 6050 29900 6250
$Comp
L power:GND #PWR016
U 1 1 5C26BBBB
P 29900 6250
F 0 "#PWR016" H 29900 6000 50  0001 C CNN
F 1 "GND" H 29905 6077 50  0000 C CNN
F 2 "" H 29900 6250 50  0001 C CNN
F 3 "" H 29900 6250 50  0001 C CNN
	1    29900 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	29750 5350 30050 5350
Wire Wire Line
	30050 5350 30050 6050
Connection ~ 30050 6050
Wire Wire Line
	30050 6050 30200 6050
$Comp
L Device:Rotary_Encoder_Switch ENC2
U 1 1 5C26BBC5
P 29450 3850
F 0 "ENC2" H 29400 4250 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 29500 3500 60  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 29450 4150 60  0000 C CNN
F 3 "" H 29450 3850 60  0000 C CNN
	1    29450 3850
	-1   0    0    -1  
$EndComp
Text Label 29750 3750 0    60   ~ 0
RE2_A
Text Label 29750 3950 0    60   ~ 0
RE2_B
Text Label 29150 3750 2    60   ~ 0
RE2_SW
$Comp
L Device:C C3
U 1 1 5C26BBCE
P 29900 4300
F 0 "C3" V 29850 4350 50  0000 L CNN
F 1 "0.1uF" V 29850 4050 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 29938 4150 50  0001 C CNN
F 3 "" H 29900 4300 50  0001 C CNN
	1    29900 4300
	-1   0    0    1   
$EndComp
$Comp
L Device:C C4
U 1 1 5C26BBD4
P 30200 4300
F 0 "C4" V 30150 4350 50  0000 L CNN
F 1 "0.1uF" V 30150 4050 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 30238 4150 50  0001 C CNN
F 3 "" H 30200 4300 50  0001 C CNN
	1    30200 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	29750 3950 29900 3950
Wire Wire Line
	29900 3950 29900 4150
Wire Wire Line
	29750 3750 30200 3750
Wire Wire Line
	30200 3750 30200 4150
Wire Wire Line
	29900 4450 29900 4550
Wire Wire Line
	29900 4550 30050 4550
Wire Wire Line
	30200 4550 30200 4450
Wire Wire Line
	29150 3950 29100 3950
Wire Wire Line
	29100 3950 29100 4550
Wire Wire Line
	29100 4550 29900 4550
Connection ~ 29900 4550
Wire Wire Line
	29900 4550 29900 4750
$Comp
L power:GND #PWR012
U 1 1 5C26BBE6
P 29900 4750
F 0 "#PWR012" H 29900 4500 50  0001 C CNN
F 1 "GND" H 29905 4577 50  0000 C CNN
F 2 "" H 29900 4750 50  0001 C CNN
F 3 "" H 29900 4750 50  0001 C CNN
	1    29900 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	29750 3850 30050 3850
Wire Wire Line
	30050 3850 30050 4550
Connection ~ 30050 4550
Wire Wire Line
	30050 4550 30200 4550
$Comp
L Device:Rotary_Encoder_Switch ENC8
U 1 1 5C26BBF0
P 29450 8350
F 0 "ENC8" H 29400 8750 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 29500 8000 60  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 29450 8650 60  0000 C CNN
F 3 "" H 29450 8350 60  0000 C CNN
	1    29450 8350
	-1   0    0    -1  
$EndComp
Text Label 29750 8250 0    60   ~ 0
RE8_A
Text Label 29750 8450 0    60   ~ 0
RE8_B
Text Label 29150 8250 2    60   ~ 0
RE8_SW
$Comp
L Device:C C15
U 1 1 5C26BBF9
P 29900 8800
F 0 "C15" V 29850 8850 50  0000 L CNN
F 1 "0.1uF" V 29850 8550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 29938 8650 50  0001 C CNN
F 3 "" H 29900 8800 50  0001 C CNN
	1    29900 8800
	-1   0    0    1   
$EndComp
$Comp
L Device:C C16
U 1 1 5C26BBFF
P 30200 8800
F 0 "C16" V 30150 8850 50  0000 L CNN
F 1 "0.1uF" V 30150 8550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 30238 8650 50  0001 C CNN
F 3 "" H 30200 8800 50  0001 C CNN
	1    30200 8800
	-1   0    0    1   
$EndComp
Wire Wire Line
	29750 8450 29900 8450
Wire Wire Line
	29900 8450 29900 8650
Wire Wire Line
	29750 8250 30200 8250
Wire Wire Line
	30200 8250 30200 8650
Wire Wire Line
	29900 8950 29900 9050
Wire Wire Line
	29900 9050 30050 9050
Wire Wire Line
	30200 9050 30200 8950
Wire Wire Line
	29150 8450 29100 8450
Wire Wire Line
	29100 8450 29100 9050
Wire Wire Line
	29100 9050 29900 9050
Connection ~ 29900 9050
Wire Wire Line
	29900 9050 29900 9250
$Comp
L power:GND #PWR026
U 1 1 5C26BC11
P 29900 9250
F 0 "#PWR026" H 29900 9000 50  0001 C CNN
F 1 "GND" H 29905 9077 50  0000 C CNN
F 2 "" H 29900 9250 50  0001 C CNN
F 3 "" H 29900 9250 50  0001 C CNN
	1    29900 9250
	1    0    0    -1  
$EndComp
Wire Wire Line
	29750 8350 30050 8350
Wire Wire Line
	30050 8350 30050 9050
Connection ~ 30050 9050
Wire Wire Line
	30050 9050 30200 9050
$Comp
L Device:Rotary_Encoder_Switch ENC6
U 1 1 5C26BC1B
P 29450 6850
F 0 "ENC6" H 29400 7250 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 29500 6500 60  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 29450 7150 60  0000 C CNN
F 3 "" H 29450 6850 60  0000 C CNN
	1    29450 6850
	-1   0    0    -1  
$EndComp
Text Label 29750 6750 0    60   ~ 0
RE6_A
Text Label 29750 6950 0    60   ~ 0
RE6_B
Text Label 29150 6750 2    60   ~ 0
RE6_SW
$Comp
L Device:C C11
U 1 1 5C26BC24
P 29900 7300
F 0 "C11" V 29850 7350 50  0000 L CNN
F 1 "0.1uF" V 29850 7050 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 29938 7150 50  0001 C CNN
F 3 "" H 29900 7300 50  0001 C CNN
	1    29900 7300
	-1   0    0    1   
$EndComp
$Comp
L Device:C C12
U 1 1 5C26BC2A
P 30200 7300
F 0 "C12" V 30150 7350 50  0000 L CNN
F 1 "0.1uF" V 30150 7050 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 30238 7150 50  0001 C CNN
F 3 "" H 30200 7300 50  0001 C CNN
	1    30200 7300
	-1   0    0    1   
$EndComp
Wire Wire Line
	29750 6950 29900 6950
Wire Wire Line
	29900 6950 29900 7150
Wire Wire Line
	29750 6750 30200 6750
Wire Wire Line
	30200 6750 30200 7150
Wire Wire Line
	29900 7450 29900 7550
Wire Wire Line
	29900 7550 30050 7550
Wire Wire Line
	30200 7550 30200 7450
Wire Wire Line
	29150 6950 29100 6950
Wire Wire Line
	29100 6950 29100 7550
Wire Wire Line
	29100 7550 29900 7550
Connection ~ 29900 7550
Wire Wire Line
	29900 7550 29900 7750
$Comp
L power:GND #PWR022
U 1 1 5C26BC3C
P 29900 7750
F 0 "#PWR022" H 29900 7500 50  0001 C CNN
F 1 "GND" H 29905 7577 50  0000 C CNN
F 2 "" H 29900 7750 50  0001 C CNN
F 3 "" H 29900 7750 50  0001 C CNN
	1    29900 7750
	1    0    0    -1  
$EndComp
Wire Wire Line
	29750 6850 30050 6850
Wire Wire Line
	30050 6850 30050 7550
Connection ~ 30050 7550
Wire Wire Line
	30050 7550 30200 7550
$Comp
L chordboard-components:KSW K18
U 1 1 5C3861B4
P 8350 8650
F 0 "K18" H 8500 8700 50  0000 C CNN
F 1 "SW_SPST" H 8200 8700 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 8650 50  0001 C CNN
F 3 "" H 8350 8650 50  0000 C CNN
	1    8350 8650
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K19
U 1 1 5C3862D2
P 8350 8750
F 0 "K19" H 8500 8800 50  0000 C CNN
F 1 "SW_SPST" H 8200 8800 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 8750 50  0001 C CNN
F 3 "" H 8350 8750 50  0000 C CNN
	1    8350 8750
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K20
U 1 1 5C3863F2
P 8350 8850
F 0 "K20" H 8500 8900 50  0000 C CNN
F 1 "SW_SPST" H 8200 8900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 8850 50  0001 C CNN
F 3 "" H 8350 8850 50  0000 C CNN
	1    8350 8850
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K21
U 1 1 5C386514
P 8350 8950
F 0 "K21" H 8500 9000 50  0000 C CNN
F 1 "SW_SPST" H 8200 9000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 8950 50  0001 C CNN
F 3 "" H 8350 8950 50  0000 C CNN
	1    8350 8950
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K22
U 1 1 5C386638
P 8350 9050
F 0 "K22" H 8500 9100 50  0000 C CNN
F 1 "SW_SPST" H 8200 9100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 9050 50  0001 C CNN
F 3 "" H 8350 9050 50  0000 C CNN
	1    8350 9050
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K23
U 1 1 5C38675E
P 8350 9150
F 0 "K23" H 8500 9200 50  0000 C CNN
F 1 "SW_SPST" H 8200 9200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 9150 50  0001 C CNN
F 3 "" H 8350 9150 50  0000 C CNN
	1    8350 9150
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K24
U 1 1 5C386886
P 8350 9250
F 0 "K24" H 8500 9300 50  0000 C CNN
F 1 "SW_SPST" H 8200 9300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 9250 50  0001 C CNN
F 3 "" H 8350 9250 50  0000 C CNN
	1    8350 9250
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K25
U 1 1 5C386CB2
P 8350 9450
F 0 "K25" H 8500 9500 50  0000 C CNN
F 1 "SW_SPST" H 8200 9500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 9450 50  0001 C CNN
F 3 "" H 8350 9450 50  0000 C CNN
	1    8350 9450
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K26
U 1 1 5C386CB8
P 8350 9550
F 0 "K26" H 8500 9600 50  0000 C CNN
F 1 "SW_SPST" H 8200 9600 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 9550 50  0001 C CNN
F 3 "" H 8350 9550 50  0000 C CNN
	1    8350 9550
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K27
U 1 1 5C386CBE
P 8350 9650
F 0 "K27" H 8500 9700 50  0000 C CNN
F 1 "SW_SPST" H 8200 9700 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 9650 50  0001 C CNN
F 3 "" H 8350 9650 50  0000 C CNN
	1    8350 9650
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K28
U 1 1 5C386CC4
P 8350 9750
F 0 "K28" H 8500 9800 50  0000 C CNN
F 1 "SW_SPST" H 8200 9800 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 9750 50  0001 C CNN
F 3 "" H 8350 9750 50  0000 C CNN
	1    8350 9750
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K29
U 1 1 5C386CCA
P 8350 9850
F 0 "K29" H 8500 9900 50  0000 C CNN
F 1 "SW_SPST" H 8200 9900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 9850 50  0001 C CNN
F 3 "" H 8350 9850 50  0000 C CNN
	1    8350 9850
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K30
U 1 1 5C386CD0
P 8350 9950
F 0 "K30" H 8500 10000 50  0000 C CNN
F 1 "SW_SPST" H 8200 10000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 9950 50  0001 C CNN
F 3 "" H 8350 9950 50  0000 C CNN
	1    8350 9950
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K31
U 1 1 5C386CD6
P 8350 10050
F 0 "K31" H 8500 10100 50  0000 C CNN
F 1 "SW_SPST" H 8200 10100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 10050 50  0001 C CNN
F 3 "" H 8350 10050 50  0000 C CNN
	1    8350 10050
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K32
U 1 1 5C386CDC
P 8350 10150
F 0 "K32" H 8500 10200 50  0000 C CNN
F 1 "SW_SPST" H 8200 10200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 10150 50  0001 C CNN
F 3 "" H 8350 10150 50  0000 C CNN
	1    8350 10150
	-1   0    0    -1  
$EndComp
$Comp
L Interface_Expansion:MCP23017_SP U3
U 1 1 5C3B4BB0
P 9200 6650
F 0 "U3" H 9200 7150 50  0000 R CNN
F 1 "MCP23017" H 9500 7250 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm_LongPads" H 9250 5700 50  0001 L CNN
F 3 "" H 9450 7650 50  0001 C CNN
	1    9200 6650
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K1
U 1 1 5C3B4BB6
P 8350 5850
F 0 "K1" H 8500 5900 50  0000 C CNN
F 1 "SW_SPST" H 8200 5900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 5850 50  0001 C CNN
F 3 "" H 8350 5850 50  0000 C CNN
	1    8350 5850
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K2
U 1 1 5C3B4BBC
P 8350 5950
F 0 "K2" H 8500 6000 50  0000 C CNN
F 1 "SW_SPST" H 8200 6000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 5950 50  0001 C CNN
F 3 "" H 8350 5950 50  0000 C CNN
	1    8350 5950
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K3
U 1 1 5C3B4BC2
P 8350 6050
F 0 "K3" H 8500 6100 50  0000 C CNN
F 1 "SW_SPST" H 8200 6100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 6050 50  0001 C CNN
F 3 "" H 8350 6050 50  0000 C CNN
	1    8350 6050
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K4
U 1 1 5C3B4BC8
P 8350 6150
F 0 "K4" H 8500 6200 50  0000 C CNN
F 1 "SW_SPST" H 8200 6200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 6150 50  0001 C CNN
F 3 "" H 8350 6150 50  0000 C CNN
	1    8350 6150
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K5
U 1 1 5C3B4BCE
P 8350 6250
F 0 "K5" H 8500 6300 50  0000 C CNN
F 1 "SW_SPST" H 8200 6300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 6250 50  0001 C CNN
F 3 "" H 8350 6250 50  0000 C CNN
	1    8350 6250
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K6
U 1 1 5C3B4BD4
P 8350 6350
F 0 "K6" H 8500 6400 50  0000 C CNN
F 1 "SW_SPST" H 8200 6400 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 6350 50  0001 C CNN
F 3 "" H 8350 6350 50  0000 C CNN
	1    8350 6350
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K7
U 1 1 5C3B4BDA
P 8350 6450
F 0 "K7" H 8500 6500 50  0000 C CNN
F 1 "SW_SPST" H 8200 6500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 6450 50  0001 C CNN
F 3 "" H 8350 6450 50  0000 C CNN
	1    8350 6450
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K8
U 1 1 5C3B4BE0
P 8350 6550
F 0 "K8" H 8500 6600 50  0000 C CNN
F 1 "SW_SPST" H 8200 6600 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 6550 50  0001 C CNN
F 3 "" H 8350 6550 50  0000 C CNN
	1    8350 6550
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K9
U 1 1 5C3B4BE6
P 8350 6750
F 0 "K9" H 8500 6800 50  0000 C CNN
F 1 "SW_SPST" H 8200 6800 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 6750 50  0001 C CNN
F 3 "" H 8350 6750 50  0000 C CNN
	1    8350 6750
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K10
U 1 1 5C3B4BEC
P 8350 6850
F 0 "K10" H 8500 6900 50  0000 C CNN
F 1 "SW_SPST" H 8200 6900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 6850 50  0001 C CNN
F 3 "" H 8350 6850 50  0000 C CNN
	1    8350 6850
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K11
U 1 1 5C3B4BF2
P 8350 6950
F 0 "K11" H 8500 7000 50  0000 C CNN
F 1 "SW_SPST" H 8200 7000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 6950 50  0001 C CNN
F 3 "" H 8350 6950 50  0000 C CNN
	1    8350 6950
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K12
U 1 1 5C3B4BF8
P 8350 7050
F 0 "K12" H 8500 7100 50  0000 C CNN
F 1 "SW_SPST" H 8200 7100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 7050 50  0001 C CNN
F 3 "" H 8350 7050 50  0000 C CNN
	1    8350 7050
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K13
U 1 1 5C3B4BFE
P 8350 7150
F 0 "K13" H 8500 7200 50  0000 C CNN
F 1 "SW_SPST" H 8200 7200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 7150 50  0001 C CNN
F 3 "" H 8350 7150 50  0000 C CNN
	1    8350 7150
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K14
U 1 1 5C3B4C04
P 8350 7250
F 0 "K14" H 8500 7300 50  0000 C CNN
F 1 "SW_SPST" H 8200 7300 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 7250 50  0001 C CNN
F 3 "" H 8350 7250 50  0000 C CNN
	1    8350 7250
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K15
U 1 1 5C3B4C0A
P 8350 7350
F 0 "K15" H 8500 7400 50  0000 C CNN
F 1 "SW_SPST" H 8200 7400 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 7350 50  0001 C CNN
F 3 "" H 8350 7350 50  0000 C CNN
	1    8350 7350
	-1   0    0    -1  
$EndComp
$Comp
L chordboard-components:KSW K16
U 1 1 5C3B4C10
P 8350 7450
F 0 "K16" H 8500 7500 50  0000 C CNN
F 1 "SW_SPST" H 8200 7500 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 8350 7450 50  0001 C CNN
F 3 "" H 8350 7450 50  0000 C CNN
	1    8350 7450
	-1   0    0    -1  
$EndComp
$Comp
L Interface_Expansion:MCP23017_SP U4
U 1 1 5C46AAAD
P 6250 7550
F 0 "U4" H 6250 8050 50  0000 R CNN
F 1 "MCP23017" H 6550 8150 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm_LongPads" H 6300 6600 50  0001 L CNN
F 3 "" H 6500 8550 50  0001 C CNN
	1    6250 7550
	-1   0    0    -1  
$EndComp
Text Label 3850 10150 2    60   ~ 0
RE8_SW
Text Label 9900 8550 0    60   ~ 0
SDA
Text Label 9900 5950 0    60   ~ 0
SCL
Text Label 9900 5850 0    60   ~ 0
SDA
Text Label 5250 8650 0    60   ~ 0
SCL
Text Label 5250 8550 0    60   ~ 0
SDA
$Comp
L power:+3.3V #PWR023
U 1 1 5C58DA58
P 4550 8250
F 0 "#PWR023" H 4550 8100 50  0001 C CNN
F 1 "+3.3V" H 4565 8423 50  0000 C CNN
F 2 "" H 4550 8250 50  0001 C CNN
F 3 "" H 4550 8250 50  0001 C CNN
	1    4550 8250
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR014
U 1 1 5C58DC30
P 9200 5550
F 0 "#PWR014" H 9200 5400 50  0001 C CNN
F 1 "+3.3V" H 9215 5723 50  0000 C CNN
F 2 "" H 9200 5550 50  0001 C CNN
F 3 "" H 9200 5550 50  0001 C CNN
	1    9200 5550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR024
U 1 1 5C58DE08
P 9200 8250
F 0 "#PWR024" H 9200 8100 50  0001 C CNN
F 1 "+3.3V" H 9215 8423 50  0000 C CNN
F 2 "" H 9200 8250 50  0001 C CNN
F 3 "" H 9200 8250 50  0001 C CNN
	1    9200 8250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR033
U 1 1 5C58DFA8
P 4550 10450
F 0 "#PWR033" H 4550 10200 50  0001 C CNN
F 1 "GND" H 4555 10277 50  0000 C CNN
F 2 "" H 4550 10450 50  0001 C CNN
F 3 "" H 4550 10450 50  0001 C CNN
	1    4550 10450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5C58E3A2
P 9200 7750
F 0 "#PWR019" H 9200 7500 50  0001 C CNN
F 1 "GND" H 9205 7577 50  0000 C CNN
F 2 "" H 9200 7750 50  0001 C CNN
F 3 "" H 9200 7750 50  0001 C CNN
	1    9200 7750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR035
U 1 1 5C58E7A5
P 9200 10450
F 0 "#PWR035" H 9200 10200 50  0001 C CNN
F 1 "GND" H 9205 10277 50  0000 C CNN
F 2 "" H 9200 10450 50  0001 C CNN
F 3 "" H 9200 10450 50  0001 C CNN
	1    9200 10450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 5850 8200 5950
Wire Wire Line
	8200 5950 8200 6050
Connection ~ 8200 5950
Wire Wire Line
	8200 6050 8200 6150
Connection ~ 8200 6050
Wire Wire Line
	8200 6150 8200 6250
Connection ~ 8200 6150
Wire Wire Line
	8200 6250 8200 6350
Connection ~ 8200 6250
Wire Wire Line
	8200 6350 8200 6450
Connection ~ 8200 6350
Wire Wire Line
	8200 6450 8200 6550
Connection ~ 8200 6450
Wire Wire Line
	8200 6550 8200 6750
Connection ~ 8200 6550
Wire Wire Line
	8200 6750 8200 6850
Connection ~ 8200 6750
Wire Wire Line
	8200 6850 8200 6950
Connection ~ 8200 6850
Wire Wire Line
	8200 6950 8200 7050
Connection ~ 8200 6950
Wire Wire Line
	8200 7050 8200 7150
Connection ~ 8200 7050
Wire Wire Line
	8200 7150 8200 7250
Connection ~ 8200 7150
Wire Wire Line
	8200 7250 8200 7350
Connection ~ 8200 7250
Wire Wire Line
	8200 7350 8200 7450
Connection ~ 8200 7350
Wire Wire Line
	8200 7450 8200 7750
Connection ~ 8200 7450
$Comp
L power:GND #PWR018
U 1 1 5C7522BA
P 8200 7750
F 0 "#PWR018" H 8200 7500 50  0001 C CNN
F 1 "GND" H 8205 7577 50  0000 C CNN
F 2 "" H 8200 7750 50  0001 C CNN
F 3 "" H 8200 7750 50  0001 C CNN
	1    8200 7750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 8550 8200 8650
Wire Wire Line
	8200 8650 8200 8750
Connection ~ 8200 8650
Wire Wire Line
	8200 8750 8200 8850
Connection ~ 8200 8750
Wire Wire Line
	8200 8850 8200 8950
Connection ~ 8200 8850
Wire Wire Line
	8200 9050 8200 8950
Connection ~ 8200 8950
Wire Wire Line
	8200 9050 8200 9150
Connection ~ 8200 9050
Wire Wire Line
	8200 9150 8200 9250
Connection ~ 8200 9150
Wire Wire Line
	8200 9250 8200 9450
Connection ~ 8200 9250
Wire Wire Line
	8200 9450 8200 9550
Connection ~ 8200 9450
Wire Wire Line
	8200 9550 8200 9650
Connection ~ 8200 9550
Wire Wire Line
	8200 9650 8200 9750
Connection ~ 8200 9650
Wire Wire Line
	8200 9750 8200 9850
Connection ~ 8200 9750
Wire Wire Line
	8200 9950 8200 9850
Connection ~ 8200 9850
Wire Wire Line
	8200 9950 8200 10050
Connection ~ 8200 9950
Wire Wire Line
	8200 10050 8200 10150
Connection ~ 8200 10050
Wire Wire Line
	8200 10150 8200 10450
$Comp
L power:GND #PWR034
U 1 1 5C94CE9A
P 8200 10450
F 0 "#PWR034" H 8200 10200 50  0001 C CNN
F 1 "GND" H 8205 10277 50  0000 C CNN
F 2 "" H 8200 10450 50  0001 C CNN
F 3 "" H 8200 10450 50  0001 C CNN
	1    8200 10450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 7250 9900 7350
Wire Wire Line
	9900 7350 9900 7450
Connection ~ 9900 7350
Wire Wire Line
	9900 7450 9900 7750
Connection ~ 9900 7450
$Comp
L power:GND #PWR020
U 1 1 5C9D5530
P 9900 7750
F 0 "#PWR020" H 9900 7500 50  0001 C CNN
F 1 "GND" H 9905 7577 50  0000 C CNN
F 2 "" H 9900 7750 50  0001 C CNN
F 3 "" H 9900 7750 50  0001 C CNN
	1    9900 7750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR036
U 1 1 5CA1ADDB
P 9900 10450
F 0 "#PWR036" H 9900 10200 50  0001 C CNN
F 1 "GND" H 9905 10277 50  0000 C CNN
F 2 "" H 9900 10450 50  0001 C CNN
F 3 "" H 9900 10450 50  0001 C CNN
	1    9900 10450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR028
U 1 1 5CA3E452
P 9900 9450
F 0 "#PWR028" H 9900 9300 50  0001 C CNN
F 1 "+3.3V" V 9915 9578 50  0000 L CNN
F 2 "" H 9900 9450 50  0001 C CNN
F 3 "" H 9900 9450 50  0001 C CNN
	1    9900 9450
	0    1    1    0   
$EndComp
Wire Wire Line
	9900 10150 9900 10450
Wire Wire Line
	9900 10050 9900 10150
Connection ~ 9900 10150
$Comp
L Interface_Expansion:MCP23017_SP U5
U 1 1 5A142090
P 9200 9350
F 0 "U5" H 9200 9850 50  0000 R CNN
F 1 "MCP23017" H 9500 9950 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm_LongPads" H 9250 8400 50  0001 L CNN
F 3 "" H 9450 10350 50  0001 C CNN
	1    9200 9350
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR030
U 1 1 5CA3E9C1
P 9900 9950
F 0 "#PWR030" H 9900 9800 50  0001 C CNN
F 1 "+3.3V" V 9915 10078 50  0000 L CNN
F 2 "" H 9900 9950 50  0001 C CNN
F 3 "" H 9900 9950 50  0001 C CNN
	1    9900 9950
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR031
U 1 1 5CA3EDB4
P 5250 10050
F 0 "#PWR031" H 5250 9900 50  0001 C CNN
F 1 "+3.3V" V 5265 10178 50  0000 L CNN
F 2 "" H 5250 10050 50  0001 C CNN
F 3 "" H 5250 10050 50  0001 C CNN
	1    5250 10050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR029
U 1 1 5CA3EEB9
P 5250 9950
F 0 "#PWR029" H 5250 9700 50  0001 C CNN
F 1 "GND" V 5255 9822 50  0000 R CNN
F 2 "" H 5250 9950 50  0001 C CNN
F 3 "" H 5250 9950 50  0001 C CNN
	1    5250 9950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5CA3F1B2
P 5250 10150
F 0 "#PWR032" H 5250 9900 50  0001 C CNN
F 1 "GND" V 5255 10022 50  0000 R CNN
F 2 "" H 5250 10150 50  0001 C CNN
F 3 "" H 5250 10150 50  0001 C CNN
	1    5250 10150
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR017
U 1 1 5CA4015D
P 9900 6750
F 0 "#PWR017" H 9900 6600 50  0001 C CNN
F 1 "+3.3V" V 9915 6878 50  0000 L CNN
F 2 "" H 9900 6750 50  0001 C CNN
F 3 "" H 9900 6750 50  0001 C CNN
	1    9900 6750
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR027
U 1 1 5CA40262
P 5250 9450
F 0 "#PWR027" H 5250 9300 50  0001 C CNN
F 1 "+3.3V" V 5265 9578 50  0000 L CNN
F 2 "" H 5250 9450 50  0001 C CNN
F 3 "" H 5250 9450 50  0001 C CNN
	1    5250 9450
	0    1    1    0   
$EndComp
$Comp
L LED:WS2812B D2
U 1 1 5CA72DA6
P 3250 11650
F 0 "D2" H 3591 11696 50  0000 L CNN
F 1 "WS2812B" H 3591 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 3300 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 3350 11275 50  0001 L TNN
	1    3250 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D1
U 1 1 5CA810F4
P 2600 11650
F 0 "D1" H 2941 11696 50  0000 L CNN
F 1 "WS2812B" H 2941 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 2650 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 2700 11275 50  0001 L TNN
	1    2600 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 11650 2950 11650
Wire Wire Line
	3550 11650 3600 11650
Wire Wire Line
	2600 11950 3250 11950
$Comp
L power:+5V #PWR037
U 1 1 5CAC8FAE
P 1600 11350
F 0 "#PWR037" H 1600 11200 50  0001 C CNN
F 1 "+5V" H 1615 11523 50  0000 C CNN
F 2 "" H 1600 11350 50  0001 C CNN
F 3 "" H 1600 11350 50  0001 C CNN
	1    1600 11350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 11350 3250 11350
$Comp
L LED:WS2812B D4
U 1 1 5CAD7ED2
P 4550 11650
F 0 "D4" H 4891 11696 50  0000 L CNN
F 1 "WS2812B" H 4891 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 4600 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 4650 11275 50  0001 L TNN
	1    4550 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D3
U 1 1 5CAD7ED8
P 3900 11650
F 0 "D3" H 4241 11696 50  0000 L CNN
F 1 "WS2812B" H 4241 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 3950 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 4000 11275 50  0001 L TNN
	1    3900 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 11650 4250 11650
Wire Wire Line
	4850 11650 4900 11650
Wire Wire Line
	3900 11950 4550 11950
Wire Wire Line
	3900 11350 4550 11350
Wire Wire Line
	3250 11350 3900 11350
Connection ~ 3250 11350
Connection ~ 3900 11350
Wire Wire Line
	3250 11950 3900 11950
Connection ~ 3250 11950
Connection ~ 3900 11950
$Comp
L LED:WS2812B D6
U 1 1 5CB066B6
P 5850 11650
F 0 "D6" H 6191 11696 50  0000 L CNN
F 1 "WS2812B" H 6191 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5900 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5950 11275 50  0001 L TNN
	1    5850 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D5
U 1 1 5CB066BD
P 5200 11650
F 0 "D5" H 5541 11696 50  0000 L CNN
F 1 "WS2812B" H 5541 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5250 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5300 11275 50  0001 L TNN
	1    5200 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 11650 5550 11650
Wire Wire Line
	6150 11650 6200 11650
Wire Wire Line
	5200 11950 5850 11950
Wire Wire Line
	5200 11350 5850 11350
$Comp
L LED:WS2812B D8
U 1 1 5CB066C8
P 7150 11650
F 0 "D8" H 7491 11696 50  0000 L CNN
F 1 "WS2812B" H 7491 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 7200 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 7250 11275 50  0001 L TNN
	1    7150 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D7
U 1 1 5CB066CF
P 6500 11650
F 0 "D7" H 6841 11696 50  0000 L CNN
F 1 "WS2812B" H 6841 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 6550 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 6600 11275 50  0001 L TNN
	1    6500 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 11650 6850 11650
Wire Wire Line
	7450 11650 7500 11650
Wire Wire Line
	6500 11950 7150 11950
Wire Wire Line
	6500 11350 7150 11350
Wire Wire Line
	5850 11350 6500 11350
Connection ~ 5850 11350
Connection ~ 6500 11350
Wire Wire Line
	5850 11950 6500 11950
Connection ~ 5850 11950
Connection ~ 6500 11950
Wire Wire Line
	4550 11350 5200 11350
Connection ~ 4550 11350
Connection ~ 5200 11350
Wire Wire Line
	5200 11950 4550 11950
Connection ~ 5200 11950
Connection ~ 4550 11950
$Comp
L LED:WS2812B D10
U 1 1 5CB3A808
P 8450 11650
F 0 "D10" H 8791 11696 50  0000 L CNN
F 1 "WS2812B" H 8791 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 8500 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 8550 11275 50  0001 L TNN
	1    8450 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D9
U 1 1 5CB3A80E
P 7800 11650
F 0 "D9" H 8141 11696 50  0000 L CNN
F 1 "WS2812B" H 8141 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 7850 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 7900 11275 50  0001 L TNN
	1    7800 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 11650 8150 11650
Wire Wire Line
	8750 11650 8800 11650
Wire Wire Line
	7800 11950 8450 11950
Wire Wire Line
	7800 11350 8450 11350
$Comp
L LED:WS2812B D12
U 1 1 5CB3A818
P 9750 11650
F 0 "D12" H 10091 11696 50  0000 L CNN
F 1 "WS2812B" H 10091 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 9800 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 9850 11275 50  0001 L TNN
	1    9750 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D11
U 1 1 5CB3A81E
P 9100 11650
F 0 "D11" H 9441 11696 50  0000 L CNN
F 1 "WS2812B" H 9441 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 9150 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 9200 11275 50  0001 L TNN
	1    9100 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 11650 9450 11650
Wire Wire Line
	10050 11650 10100 11650
Wire Wire Line
	9100 11950 9750 11950
Wire Wire Line
	9100 11350 9750 11350
Wire Wire Line
	8450 11350 9100 11350
Connection ~ 8450 11350
Connection ~ 9100 11350
Wire Wire Line
	8450 11950 9100 11950
Connection ~ 8450 11950
Connection ~ 9100 11950
$Comp
L LED:WS2812B D14
U 1 1 5CB3A82E
P 11050 11650
F 0 "D14" H 11391 11696 50  0000 L CNN
F 1 "WS2812B" H 11391 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 11100 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 11150 11275 50  0001 L TNN
	1    11050 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D13
U 1 1 5CB3A834
P 10400 11650
F 0 "D13" H 10741 11696 50  0000 L CNN
F 1 "WS2812B" H 10741 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 10450 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 10500 11275 50  0001 L TNN
	1    10400 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10700 11650 10750 11650
Wire Wire Line
	11350 11650 11400 11650
Wire Wire Line
	10400 11950 11050 11950
Wire Wire Line
	10400 11350 11050 11350
$Comp
L LED:WS2812B D16
U 1 1 5CB3A83E
P 12350 11650
F 0 "D16" H 12691 11696 50  0000 L CNN
F 1 "WS2812B" H 12691 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 12400 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 12450 11275 50  0001 L TNN
	1    12350 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D15
U 1 1 5CB3A844
P 11700 11650
F 0 "D15" H 12041 11696 50  0000 L CNN
F 1 "WS2812B" H 12041 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 11750 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 11800 11275 50  0001 L TNN
	1    11700 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	12000 11650 12050 11650
Wire Wire Line
	12650 11650 12700 11650
Wire Wire Line
	11700 11950 12350 11950
Wire Wire Line
	11700 11350 12350 11350
Wire Wire Line
	11050 11350 11700 11350
Connection ~ 11050 11350
Connection ~ 11700 11350
Wire Wire Line
	11050 11950 11700 11950
Connection ~ 11050 11950
Connection ~ 11700 11950
Wire Wire Line
	9750 11350 10400 11350
Connection ~ 9750 11350
Connection ~ 10400 11350
Wire Wire Line
	10400 11950 9750 11950
Connection ~ 10400 11950
Connection ~ 9750 11950
Wire Wire Line
	7150 11350 7800 11350
Connection ~ 7150 11350
Connection ~ 7800 11350
Wire Wire Line
	7150 11950 7800 11950
Connection ~ 7150 11950
Connection ~ 7800 11950
$Comp
L LED:WS2812B D18
U 1 1 5CB7C5A9
P 13650 11650
F 0 "D18" H 13991 11696 50  0000 L CNN
F 1 "WS2812B" H 13991 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 13700 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 13750 11275 50  0001 L TNN
	1    13650 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D17
U 1 1 5CB7C5B0
P 13000 11650
F 0 "D17" H 13341 11696 50  0000 L CNN
F 1 "WS2812B" H 13341 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 13050 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 13100 11275 50  0001 L TNN
	1    13000 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	13300 11650 13350 11650
Wire Wire Line
	13950 11650 14000 11650
Wire Wire Line
	13000 11950 13650 11950
Wire Wire Line
	13000 11350 13650 11350
$Comp
L LED:WS2812B D20
U 1 1 5CB7C5BB
P 14950 11650
F 0 "D20" H 15291 11696 50  0000 L CNN
F 1 "WS2812B" H 15291 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 15000 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 15050 11275 50  0001 L TNN
	1    14950 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D19
U 1 1 5CB7C5C2
P 14300 11650
F 0 "D19" H 14641 11696 50  0000 L CNN
F 1 "WS2812B" H 14641 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 14350 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 14400 11275 50  0001 L TNN
	1    14300 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	14600 11650 14650 11650
Wire Wire Line
	15250 11650 15300 11650
Wire Wire Line
	14300 11950 14950 11950
Wire Wire Line
	14300 11350 14950 11350
Wire Wire Line
	13650 11350 14300 11350
Connection ~ 13650 11350
Connection ~ 14300 11350
Wire Wire Line
	13650 11950 14300 11950
Connection ~ 13650 11950
Connection ~ 14300 11950
$Comp
L LED:WS2812B D22
U 1 1 5CB7C5D3
P 16250 11650
F 0 "D22" H 16591 11696 50  0000 L CNN
F 1 "WS2812B" H 16591 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 16300 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 16350 11275 50  0001 L TNN
	1    16250 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D21
U 1 1 5CB7C5DA
P 15600 11650
F 0 "D21" H 15941 11696 50  0000 L CNN
F 1 "WS2812B" H 15941 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 15650 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 15700 11275 50  0001 L TNN
	1    15600 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	15900 11650 15950 11650
Wire Wire Line
	16550 11650 16600 11650
Wire Wire Line
	15600 11950 16250 11950
Wire Wire Line
	15600 11350 16250 11350
$Comp
L LED:WS2812B D24
U 1 1 5CB7C5E5
P 17550 11650
F 0 "D24" H 17891 11696 50  0000 L CNN
F 1 "WS2812B" H 17891 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 17600 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 17650 11275 50  0001 L TNN
	1    17550 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D23
U 1 1 5CB7C5EC
P 16900 11650
F 0 "D23" H 17241 11696 50  0000 L CNN
F 1 "WS2812B" H 17241 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 16950 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 17000 11275 50  0001 L TNN
	1    16900 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	17200 11650 17250 11650
Wire Wire Line
	17850 11650 17900 11650
Wire Wire Line
	16900 11950 17550 11950
Wire Wire Line
	16900 11350 17550 11350
Wire Wire Line
	16250 11350 16900 11350
Connection ~ 16250 11350
Connection ~ 16900 11350
Wire Wire Line
	16250 11950 16900 11950
Connection ~ 16250 11950
Connection ~ 16900 11950
Wire Wire Line
	14950 11350 15600 11350
Connection ~ 14950 11350
Connection ~ 15600 11350
Wire Wire Line
	15600 11950 14950 11950
Connection ~ 15600 11950
Connection ~ 14950 11950
$Comp
L LED:WS2812B D26
U 1 1 5CB7C603
P 18850 11650
F 0 "D26" H 19191 11696 50  0000 L CNN
F 1 "WS2812B" H 19191 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 18900 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 18950 11275 50  0001 L TNN
	1    18850 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D25
U 1 1 5CB7C60A
P 18200 11650
F 0 "D25" H 18541 11696 50  0000 L CNN
F 1 "WS2812B" H 18541 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 18250 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 18300 11275 50  0001 L TNN
	1    18200 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	18500 11650 18550 11650
Wire Wire Line
	19150 11650 19200 11650
Wire Wire Line
	18200 11950 18850 11950
Wire Wire Line
	18200 11350 18850 11350
$Comp
L LED:WS2812B D28
U 1 1 5CB7C615
P 20150 11650
F 0 "D28" H 20491 11696 50  0000 L CNN
F 1 "WS2812B" H 20491 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 20200 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 20250 11275 50  0001 L TNN
	1    20150 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D27
U 1 1 5CB7C61C
P 19500 11650
F 0 "D27" H 19841 11696 50  0000 L CNN
F 1 "WS2812B" H 19841 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 19550 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 19600 11275 50  0001 L TNN
	1    19500 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	19800 11650 19850 11650
Wire Wire Line
	20450 11650 20500 11650
Wire Wire Line
	19500 11950 20150 11950
Wire Wire Line
	19500 11350 20150 11350
Wire Wire Line
	18850 11350 19500 11350
Connection ~ 18850 11350
Connection ~ 19500 11350
Wire Wire Line
	18850 11950 19500 11950
Connection ~ 18850 11950
Connection ~ 19500 11950
$Comp
L LED:WS2812B D30
U 1 1 5CB7C62D
P 21450 11650
F 0 "D30" H 21791 11696 50  0000 L CNN
F 1 "WS2812B" H 21791 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 21500 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 21550 11275 50  0001 L TNN
	1    21450 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D29
U 1 1 5CB7C634
P 20800 11650
F 0 "D29" H 21141 11696 50  0000 L CNN
F 1 "WS2812B" H 21141 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 20850 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 20900 11275 50  0001 L TNN
	1    20800 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	21100 11650 21150 11650
Wire Wire Line
	21750 11650 21800 11650
Wire Wire Line
	20800 11950 21450 11950
Wire Wire Line
	20800 11350 21450 11350
$Comp
L LED:WS2812B D32
U 1 1 5CB7C63F
P 22750 11650
F 0 "D32" H 23091 11696 50  0000 L CNN
F 1 "WS2812B" H 23091 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 22800 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 22850 11275 50  0001 L TNN
	1    22750 11650
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D31
U 1 1 5CB7C646
P 22100 11650
F 0 "D31" H 22441 11696 50  0000 L CNN
F 1 "WS2812B" H 22441 11605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 22150 11350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 22200 11275 50  0001 L TNN
	1    22100 11650
	1    0    0    -1  
$EndComp
Wire Wire Line
	22400 11650 22450 11650
Wire Wire Line
	23050 11650 23100 11650
Wire Wire Line
	22100 11950 22750 11950
Wire Wire Line
	22100 11350 22750 11350
Wire Wire Line
	21450 11350 22100 11350
Connection ~ 21450 11350
Connection ~ 22100 11350
Wire Wire Line
	21450 11950 22100 11950
Connection ~ 21450 11950
Connection ~ 22100 11950
Wire Wire Line
	20150 11350 20800 11350
Connection ~ 20150 11350
Connection ~ 20800 11350
Wire Wire Line
	20800 11950 20150 11950
Connection ~ 20800 11950
Connection ~ 20150 11950
Wire Wire Line
	17550 11350 18200 11350
Connection ~ 17550 11350
Connection ~ 18200 11350
Wire Wire Line
	17550 11950 18200 11950
Connection ~ 17550 11950
Connection ~ 18200 11950
Wire Wire Line
	12350 11350 13000 11350
Connection ~ 12350 11350
Connection ~ 13000 11350
Wire Wire Line
	12350 11950 13000 11950
Connection ~ 12350 11950
Connection ~ 13000 11950
$Comp
L Device:CP C17
U 1 1 5CC11332
P 1600 11650
F 0 "C17" H 1718 11696 50  0000 L CNN
F 1 "CP" H 1718 11605 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 1638 11500 50  0001 C CNN
F 3 "~" H 1600 11650 50  0001 C CNN
	1    1600 11650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR038
U 1 1 5CC11510
P 1600 11950
F 0 "#PWR038" H 1600 11700 50  0001 C CNN
F 1 "GND" H 1605 11777 50  0000 C CNN
F 2 "" H 1600 11950 50  0001 C CNN
F 3 "" H 1600 11950 50  0001 C CNN
	1    1600 11950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 11950 2600 11950
Connection ~ 2600 11950
Wire Wire Line
	1600 11950 1600 11800
Connection ~ 1600 11950
Wire Wire Line
	1600 11500 1600 11350
Wire Wire Line
	1600 11350 2600 11350
Connection ~ 1600 11350
Connection ~ 2600 11350
$Comp
L Graphic:Logo_Open_Hardware_Small #LOGO1
U 1 1 5CCED375
P 21100 12600
F 0 "#LOGO1" H 21100 12875 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 21100 12375 50  0001 C CNN
F 2 "" H 21100 12600 50  0001 C CNN
F 3 "~" H 21100 12600 50  0001 C CNN
	1    21100 12600
	1    0    0    -1  
$EndComp
Text Label -550 4450 2    60   ~ 0
RE1_SW
Text Label -550 4100 2    60   ~ 0
RE2_SW
Text Label -550 4200 2    60   ~ 0
RE3_SW
Text Label -550 4300 2    60   ~ 0
RE4_SW
Text Label 2150 5400 2    60   ~ 0
RE5_SW
Text Label 2150 5500 2    60   ~ 0
RE6_SW
Text Label 4150 5200 0    60   ~ 0
RE7_SW
Text Label 4150 5100 0    60   ~ 0
RE8_SW
$Comp
L Graphic:Logo_Open_Hardware_Small #LOGO2
U 1 1 5CD98474
P 21700 12600
F 0 "#LOGO2" H 21700 12875 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 21700 12375 50  0001 C CNN
F 2 "" H 21700 12600 50  0001 C CNN
F 3 "~" H 21700 12600 50  0001 C CNN
	1    21700 12600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5BD4A650
P 15650 13250
F 0 "H4" H 15750 13296 50  0000 L CNN
F 1 "MountingHole" H 15750 13205 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad" H 15650 13250 50  0001 C CNN
F 3 "~" H 15650 13250 50  0001 C CNN
	1    15650 13250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5BD4A89B
P 15650 13000
F 0 "H3" H 15750 13046 50  0000 L CNN
F 1 "MountingHole" H 15750 12955 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad" H 15650 13000 50  0001 C CNN
F 3 "~" H 15650 13000 50  0001 C CNN
	1    15650 13000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5BD4A99F
P 15650 12750
F 0 "H2" H 15750 12796 50  0000 L CNN
F 1 "MountingHole" H 15750 12705 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad" H 15650 12750 50  0001 C CNN
F 3 "~" H 15650 12750 50  0001 C CNN
	1    15650 12750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5BD4AA83
P 15650 12500
F 0 "H1" H 15750 12546 50  0000 L CNN
F 1 "MountingHole" H 15750 12455 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad" H 15650 12500 50  0001 C CNN
F 3 "~" H 15650 12500 50  0001 C CNN
	1    15650 12500
	1    0    0    -1  
$EndComp
$Comp
L MCU_Module:Maple_Mini A?
U 1 1 5BE217AC
P 7400 2000
F 0 "A?" H 7400 814 50  0000 C CNN
F 1 "Maple_Mini" H 7400 723 50  0000 C CNN
F 2 "Module:Maple_Mini" H 7450 950 50  0001 L CNN
F 3 "http://docs.leaflabs.com/static.leaflabs.com/pub/leaflabs/maple-docs/0.0.12/hardware/maple-mini.html" H 7450 0   50  0001 L CNN
	1    7400 2000
	1    0    0    -1  
$EndComp
Text Label 8200 1100 0    60   ~ 0
RE1_A
Text Label 8200 1200 0    60   ~ 0
RE1_B
Text Label 8200 1300 0    60   ~ 0
RE2_A
Text Label 8200 1400 0    60   ~ 0
RE2_B
Text Label 8200 1500 0    60   ~ 0
RE3_A
Text Label 8200 1600 0    60   ~ 0
RE3_B
Text Label 8200 1700 0    60   ~ 0
RE4_A
Text Label 8200 1800 0    60   ~ 0
RE4_B
Text Label 8200 1900 0    60   ~ 0
RE5_A
Text Label 8200 2000 0    60   ~ 0
RE5_B
Text Label 8200 2100 0    60   ~ 0
RE6_A
Text Label 8200 2200 0    60   ~ 0
RE6_B
Text Label 8200 2400 0    60   ~ 0
RE7_B
Text Label 8200 2300 0    60   ~ 0
RE7_A
Text Label 8200 2600 0    60   ~ 0
RE8_B
Text Label 8200 2500 0    60   ~ 0
RE8_A
Text Label 6600 1200 2    60   ~ 0
RE2_SW
Text Label 6600 1300 2    60   ~ 0
RE3_SW
Text Label 6600 1400 2    60   ~ 0
RE4_SW
Text Label 6600 2100 2    60   ~ 0
RE5_SW
Text Label 6600 2200 2    60   ~ 0
RE6_SW
Text Label 6600 1100 2    60   ~ 0
RE1_SW
Text Label 6600 2300 2    60   ~ 0
RE7_SW
Text Label 6600 2400 2    60   ~ 0
RE8_SW
Text Label 6600 2000 2    60   ~ 0
RE_TX
Text Label 6600 1900 2    60   ~ 0
RE_RX
Text Label 6600 1500 2    60   ~ 0
RE_LED
Text Label 6600 1600 2    60   ~ 0
RE_SW1
Text Label 6600 1700 2    60   ~ 0
RE_SW2
$EndSCHEMATC
