EESchema Schematic File Version 4
LIBS:encoderpad-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Rotary_Encoder_Switch SW1
U 1 1 5BE2AC31
P 2950 2150
F 0 "SW1" H 2950 2517 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 2950 2426 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm_CircularMountingHoles" H 2800 2310 50  0001 C CNN
F 3 "~" H 2950 2410 50  0001 C CNN
	1    2950 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW2
U 1 1 5BE2AC98
P 2950 2700
F 0 "SW2" H 2950 3067 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 2950 2976 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm_CircularMountingHoles" H 2800 2860 50  0001 C CNN
F 3 "~" H 2950 2960 50  0001 C CNN
	1    2950 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW3
U 1 1 5BE2ACC4
P 2950 3250
F 0 "SW3" H 2950 3617 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 2950 3526 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm_CircularMountingHoles" H 2800 3410 50  0001 C CNN
F 3 "~" H 2950 3510 50  0001 C CNN
	1    2950 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW7
U 1 1 5BE2AD4B
P 2950 3800
F 0 "SW7" H 2950 4167 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 2950 4076 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm_CircularMountingHoles" H 2800 3960 50  0001 C CNN
F 3 "~" H 2950 4060 50  0001 C CNN
	1    2950 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW4
U 1 1 5BE2AE75
P 4200 2150
F 0 "SW4" H 4200 2517 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 4200 2426 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm_CircularMountingHoles" H 4050 2310 50  0001 C CNN
F 3 "~" H 4200 2410 50  0001 C CNN
	1    4200 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW5
U 1 1 5BE2AE7B
P 4200 2700
F 0 "SW5" H 4200 3067 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 4200 2976 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm_CircularMountingHoles" H 4050 2860 50  0001 C CNN
F 3 "~" H 4200 2960 50  0001 C CNN
	1    4200 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW6
U 1 1 5BE2AE81
P 4200 3250
F 0 "SW6" H 4200 3617 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 4200 3526 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm_CircularMountingHoles" H 4050 3410 50  0001 C CNN
F 3 "~" H 4200 3510 50  0001 C CNN
	1    4200 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW8
U 1 1 5BE2AE87
P 4200 3800
F 0 "SW8" H 4200 4167 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 4200 4076 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm_CircularMountingHoles" H 4050 3960 50  0001 C CNN
F 3 "~" H 4200 4060 50  0001 C CNN
	1    4200 3800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5BE2B1CF
P 1950 2100
F 0 "H1" H 2050 2146 50  0000 L CNN
F 1 "MountingHole" H 2050 2055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1950 2100 50  0001 C CNN
F 3 "~" H 1950 2100 50  0001 C CNN
	1    1950 2100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5BE2B247
P 1950 2350
F 0 "H2" H 2050 2396 50  0000 L CNN
F 1 "MountingHole" H 2050 2305 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1950 2350 50  0001 C CNN
F 3 "~" H 1950 2350 50  0001 C CNN
	1    1950 2350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5BE2B293
P 1950 2600
F 0 "H3" H 2050 2646 50  0000 L CNN
F 1 "MountingHole" H 2050 2555 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1950 2600 50  0001 C CNN
F 3 "~" H 1950 2600 50  0001 C CNN
	1    1950 2600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5BE2B2D7
P 1950 2900
F 0 "H4" H 2050 2946 50  0000 L CNN
F 1 "MountingHole" H 2050 2855 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1950 2900 50  0001 C CNN
F 3 "~" H 1950 2900 50  0001 C CNN
	1    1950 2900
	1    0    0    -1  
$EndComp
$Comp
L Interface_Expansion:MCP23017_SP U1
U 1 1 5BE2B521
P 6900 1850
F 0 "U1" H 6900 3128 50  0000 C CNN
F 1 "MCP23017_SP" H 6900 3037 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 7100 850 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001952C.pdf" H 7100 750 50  0001 L CNN
	1    6900 1850
	1    0    0    -1  
$EndComp
$Comp
L Interface_Expansion:MCP23017_SP U2
U 1 1 5BE2B601
P 8950 1850
F 0 "U2" H 8950 3128 50  0000 C CNN
F 1 "MCP23017_SP" H 8950 3037 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 9150 850 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001952C.pdf" H 9150 750 50  0001 L CNN
	1    8950 1850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DIP_x03 SW9
U 1 1 5BE2BB2A
P 6550 3700
F 0 "SW9" H 6550 4167 50  0000 C CNN
F 1 "SW_DIP_x03" H 6550 4076 50  0000 C CNN
F 2 "Button_Switch_THT:SW_DIP_SPSTx03_Slide_9.78x9.8mm_W7.62mm_P2.54mm" H 6550 3700 50  0001 C CNN
F 3 "" H 6550 3700 50  0001 C CNN
	1    6550 3700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DIP_x03 SW10
U 1 1 5BE2BD4D
P 8850 3750
F 0 "SW10" H 8850 4217 50  0000 C CNN
F 1 "SW_DIP_x03" H 8850 4126 50  0000 C CNN
F 2 "Button_Switch_THT:SW_DIP_SPSTx03_Slide_9.78x9.8mm_W7.62mm_P2.54mm" H 8850 3750 50  0001 C CNN
F 3 "" H 8850 3750 50  0001 C CNN
	1    8850 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5BE2C034
P 6600 4450
F 0 "J1" H 6680 4442 50  0000 L CNN
F 1 "Conn_01x04" H 6680 4351 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B04B-EH-A_1x04_P2.50mm_Vertical" H 6600 4450 50  0001 C CNN
F 3 "~" H 6600 4450 50  0001 C CNN
	1    6600 4450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
