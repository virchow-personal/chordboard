EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H1
U 1 1 5BE2B1CF
P 1950 2100
F 0 "H1" H 2050 2146 50  0000 L CNN
F 1 "MountingHole" H 2050 2055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1950 2100 50  0001 C CNN
F 3 "~" H 1950 2100 50  0001 C CNN
	1    1950 2100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5BE2B247
P 1950 2350
F 0 "H2" H 2050 2396 50  0000 L CNN
F 1 "MountingHole" H 2050 2305 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1950 2350 50  0001 C CNN
F 3 "~" H 1950 2350 50  0001 C CNN
	1    1950 2350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5BE2B293
P 1950 2600
F 0 "H3" H 2050 2646 50  0000 L CNN
F 1 "MountingHole" H 2050 2555 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1950 2600 50  0001 C CNN
F 3 "~" H 1950 2600 50  0001 C CNN
	1    1950 2600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5BE2B2D7
P 1950 2900
F 0 "H4" H 2050 2946 50  0000 L CNN
F 1 "MountingHole" H 2050 2855 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1950 2900 50  0001 C CNN
F 3 "~" H 1950 2900 50  0001 C CNN
	1    1950 2900
	1    0    0    -1  
$EndComp
$Comp
L Interface_Expansion:MCP23017_SP U1
U 1 1 5BE2B521
P 6900 1850
F 0 "U1" H 6900 3128 50  0000 C CNN
F 1 "MCP23017_SP" H 6900 3037 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 7100 850 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001952C.pdf" H 7100 750 50  0001 L CNN
	1    6900 1850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DIP_x03 SW9
U 1 1 5BE2BB2A
P 6550 3700
F 0 "SW9" H 6550 4167 50  0000 C CNN
F 1 "SW_DIP_x03" H 6550 4076 50  0000 C CNN
F 2 "Button_Switch_THT:SW_DIP_SPSTx03_Slide_9.78x9.8mm_W7.62mm_P2.54mm" H 6550 3700 50  0001 C CNN
F 3 "" H 6550 3700 50  0001 C CNN
	1    6550 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5BE2C034
P 6600 4450
F 0 "J1" H 6680 4442 50  0000 L CNN
F 1 "Conn_01x04" H 6680 4351 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B04B-EH-A_1x04_P2.50mm_Vertical" H 6600 4450 50  0001 C CNN
F 3 "~" H 6600 4450 50  0001 C CNN
	1    6600 4450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5BE2CE94
P 3300 2100
F 0 "SW1" H 3300 2385 50  0000 C CNN
F 1 "SW_Push" H 3300 2294 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 3300 2300 50  0001 C CNN
F 3 "" H 3300 2300 50  0001 C CNN
	1    3300 2100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5BE2CF0C
P 3300 2500
F 0 "SW2" H 3300 2785 50  0000 C CNN
F 1 "SW_Push" H 3300 2694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 3300 2700 50  0001 C CNN
F 3 "" H 3300 2700 50  0001 C CNN
	1    3300 2500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5BE2CF40
P 3300 2800
F 0 "SW3" H 3300 3085 50  0000 C CNN
F 1 "SW_Push" H 3300 2994 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 3300 3000 50  0001 C CNN
F 3 "" H 3300 3000 50  0001 C CNN
	1    3300 2800
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5BE2CF70
P 3300 3100
F 0 "SW4" H 3300 3385 50  0000 C CNN
F 1 "SW_Push" H 3300 3294 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 3300 3300 50  0001 C CNN
F 3 "" H 3300 3300 50  0001 C CNN
	1    3300 3100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5BE2CFAE
P 3300 3400
F 0 "SW5" H 3300 3685 50  0000 C CNN
F 1 "SW_Push" H 3300 3594 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 3300 3600 50  0001 C CNN
F 3 "" H 3300 3600 50  0001 C CNN
	1    3300 3400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5BE2CFE2
P 3300 3650
F 0 "SW6" H 3300 3935 50  0000 C CNN
F 1 "SW_Push" H 3300 3844 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 3300 3850 50  0001 C CNN
F 3 "" H 3300 3850 50  0001 C CNN
	1    3300 3650
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 5BE2D061
P 3300 3900
F 0 "SW7" H 3300 4185 50  0000 C CNN
F 1 "SW_Push" H 3300 4094 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 3300 4100 50  0001 C CNN
F 3 "" H 3300 4100 50  0001 C CNN
	1    3300 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 5BE2D0D1
P 3300 4150
F 0 "SW8" H 3300 4435 50  0000 C CNN
F 1 "SW_Push" H 3300 4344 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX1A_1.00u_PCB" H 3300 4350 50  0001 C CNN
F 3 "" H 3300 4350 50  0001 C CNN
	1    3300 4150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
