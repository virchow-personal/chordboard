EESchema Schematic File Version 4
LIBS:chordboard-rescue
LIBS:teensy
LIBS:Rotary-Encoders
LIBS:eurocad
LIBS:afshar
LIBS:74hct125d
LIBS:74hct245
LIBS:4000-ic
LIBS:7400-ic
LIBS:75176
LIBS:Abracon
LIBS:ABS07-32.768KHZ-T
LIBS:acorn_electron_expansion_connector
LIBS:ActiveSemi
LIBS:akn_holtek
LIBS:akn_maxim
LIBS:akn_misc
LIBS:akn_transformers
LIBS:altera
LIBS:Altera
LIBS:Amplifiers
LIBS:AMS
LIBS:analog-devices
LIBS:AnalogDevices
LIBS:analog-ic
LIBS:AOS
LIBS:arm-swd-header
LIBS:Atmel
LIBS:avr-mcu
LIBS:bluegiga
LIBS:Bosch
LIBS:Chipkit_Shield_Max32-cache
LIBS:Chipkit_Shield_uC-cache
LIBS:conn-2mm
LIBS:conn-100mil
LIBS:conn-amphenol
LIBS:conn-assmann
LIBS:conn-cui
LIBS:connector
LIBS:Connectors
LIBS:conn-fci
LIBS:conn-jae
LIBS:conn-linx
LIBS:conn_mics
LIBS:conn-molex
LIBS:conn-special-headers
LIBS:conn-tagconnect
LIBS:conn-te
LIBS:conn-test
LIBS:CubeSatKit_StackBoard-cache
LIBS:DataStorage
LIBS:diode-inc-ic
LIBS:Diodes
LIBS:DiodesInc
LIBS:display
LIBS:EKB
LIBS:electomech-misc
LIBS:_electromech
LIBS:esp8266-esp-01
LIBS:esp8266-esp-03
LIBS:esp8266-esp-12e
LIBS:ESP8266
LIBS:Fairchild
LIBS:freescale-ic
LIBS:FTDI
LIBS:ftdi-ic
LIBS:hm-11
LIBS:Infineon
LIBS:Intersil
LIBS:iso15
LIBS:kbox
LIBS:kbox-cache
LIBS:KCDA02-123
LIBS:Lattice
LIBS:led
LIBS:_linear
LIBS:LinearTech
LIBS:Littelfuse
LIBS:_logic
LIBS:logic-4000
LIBS:logic-7400
LIBS:logic-7400-new
LIBS:LogicDevices
LIBS:lpc11u14fbd48
LIBS:lt
LIBS:MACOM
LIBS:Macrofab
LIBS:max
LIBS:maxim
LIBS:maxim-ic
LIBS:mcp1700t-3302e-tt
LIBS:mcp73831t-2aci-ot
LIBS:mechanical
LIBS:micrel-ic
LIBS:Microchip
LIBS:microchip-ic
LIBS:Micron
LIBS:Microprocessors
LIBS:micro_usb_socket
LIBS:MiscellaneousDevices
LIBS:mke02z64vld2
LIBS:mke04z8vtg4
LIBS:Murata
LIBS:nrf24l01p_smd
LIBS:nrf51822-04
LIBS:nRF24L01+
LIBS:NXP
LIBS:nxp-ic
LIBS:OceanOptics
LIBS:onsemi
LIBS:on-semi-ic
LIBS:opamps
LIBS:_passive
LIBS:Passives
LIBS:pasv-BelFuse
LIBS:pasv-BiTech
LIBS:pasv-Bourns
LIBS:pasv-cap
LIBS:pasv-ind
LIBS:pasv-Murata
LIBS:pasv-res
LIBS:pasv-TDK
LIBS:pasv-xtal
LIBS:pcb
LIBS:PMOD
LIBS:power
LIBS:PowerComponents
LIBS:pp_ws2812b
LIBS:random-mics
LIBS:Recom
LIBS:recom-r1se
LIBS:regulator
LIBS:relays-a
LIBS:RepeaterParts
LIBS:RF_OEM_Parts
LIBS:Richtek
LIBS:rohm
LIBS:_semi
LIBS:semi-diode-DiodesInc
LIBS:semi-diode-generic
LIBS:semi-diode-MCC
LIBS:semi-diode-NXP
LIBS:semi-diode-OnSemi
LIBS:semi-diode-Semtech
LIBS:semi-diode-ST
LIBS:semi-diode-Toshiba
LIBS:semi-opto-generic
LIBS:semi-opto-liteon
LIBS:semi-thyristor-generic
LIBS:semi-trans-AOS
LIBS:semi-trans-DiodesInc
LIBS:semi-trans-EPC
LIBS:semi-trans-Fairchild
LIBS:semi-trans-generic
LIBS:semi-trans-Infineon
LIBS:semi-trans-IRF
LIBS:semi-trans-IXYS
LIBS:semi-trans-NXP
LIBS:semi-trans-OnSemi
LIBS:semi-trans-Panasonic
LIBS:semi-trans-ST
LIBS:semi-trans-TI
LIBS:semi-trans-Toshiba
LIBS:semi-trans-Vishay
LIBS:Sensors
LIBS:sharp-relay
LIBS:silabs
LIBS:skyworks
LIBS:Snickerdoodle
LIBS:sparkfun
LIBS:ST
LIBS:standard
LIBS:st_ic
LIBS:stm32f37xxx_48pin
LIBS:stm32f102xx_48pin
LIBS:stm32f103c8t6-module-china
LIBS:stm32f302xx_48pin
LIBS:stmicro-mcu
LIBS:supertex
LIBS:switches
LIBS:symbol
LIBS:TexasInstruments
LIBS:ti
LIBS:ti-ic
LIBS:tinkerforge
LIBS:transistor
LIBS:TransistorParts
LIBS:transistors-mics
LIBS:txs2sa-relay
LIBS:uart_pp
LIBS:u-blox
LIBS:uln-ic
LIBS:usb_plug
LIBS:Vishay
LIBS:Winbond
LIBS:ws2812b
LIBS:Xilinx
LIBS:SparkFun-DigitalIC
LIBS:k66lqfp
LIBS:Wetmelon
LIBS:chordboard-components
LIBS:chordboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Rotary_Encoder_Switch ENC?
U 1 1 5BFBB782
P 3000 2100
AR Path="/5BFBB782" Ref="ENC?"  Part="1" 
AR Path="/5BFBB447/5BFBB782" Ref="ENC?"  Part="1" 
AR Path="/5C021D7B/5BFBB782" Ref="ENC?"  Part="1" 
AR Path="/5C0BD716/5BFBB782" Ref="ENC1"  Part="1" 
AR Path="/5C0BD711/5BFBB782" Ref="ENC2"  Part="1" 
AR Path="/5C0BD70C/5BFBB782" Ref="ENC3"  Part="1" 
AR Path="/5C0BD707/5BFBB782" Ref="ENC?"  Part="1" 
AR Path="/5C0572F4/5BFBB782" Ref="ENC?"  Part="1" 
AR Path="/5C0572F9/5BFBB782" Ref="ENC?"  Part="1" 
F 0 "ENC?" H 2950 2500 60  0000 C CNN
F 1 "ROTARY_ENCODER" H 3050 1750 60  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 3000 2400 60  0000 C CNN
F 3 "" H 3000 2100 60  0000 C CNN
	1    3000 2100
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5BFBB78C
P 3450 2550
AR Path="/5BFBB78C" Ref="C?"  Part="1" 
AR Path="/5BFBB447/5BFBB78C" Ref="C1"  Part="1" 
AR Path="/5C021D7B/5BFBB78C" Ref="C3"  Part="1" 
AR Path="/5C0BD716/5BFBB78C" Ref="C3"  Part="1" 
AR Path="/5C0BD711/5BFBB78C" Ref="C3"  Part="1" 
AR Path="/5C0BD70C/5BFBB78C" Ref="C3"  Part="1" 
AR Path="/5C0BD707/5BFBB78C" Ref="C3"  Part="1" 
AR Path="/5C0572F4/5BFBB78C" Ref="C3"  Part="1" 
AR Path="/5C0572F9/5BFBB78C" Ref="C3"  Part="1" 
F 0 "C1" V 3400 2600 50  0000 L CNN
F 1 "0.1uF" V 3400 2300 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 3488 2400 50  0001 C CNN
F 3 "" H 3450 2550 50  0001 C CNN
	1    3450 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5BFBB793
P 3750 2550
AR Path="/5BFBB793" Ref="C?"  Part="1" 
AR Path="/5BFBB447/5BFBB793" Ref="C2"  Part="1" 
AR Path="/5C021D7B/5BFBB793" Ref="C4"  Part="1" 
AR Path="/5C0BD716/5BFBB793" Ref="C4"  Part="1" 
AR Path="/5C0BD711/5BFBB793" Ref="C4"  Part="1" 
AR Path="/5C0BD70C/5BFBB793" Ref="C4"  Part="1" 
AR Path="/5C0BD707/5BFBB793" Ref="C4"  Part="1" 
AR Path="/5C0572F4/5BFBB793" Ref="C4"  Part="1" 
AR Path="/5C0572F9/5BFBB793" Ref="C4"  Part="1" 
F 0 "C2" V 3700 2600 50  0000 L CNN
F 1 "0.1uF" V 3700 2300 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 3788 2400 50  0001 C CNN
F 3 "" H 3750 2550 50  0001 C CNN
	1    3750 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	3300 2200 3450 2200
Wire Wire Line
	3450 2200 3450 2400
Wire Wire Line
	3300 2000 3750 2000
Wire Wire Line
	3750 2000 3750 2400
Wire Wire Line
	3450 2700 3450 2800
Wire Wire Line
	3450 2800 3600 2800
Wire Wire Line
	3750 2800 3750 2700
Wire Wire Line
	2700 2200 2650 2200
Wire Wire Line
	2650 2200 2650 2800
Wire Wire Line
	2650 2800 3450 2800
Connection ~ 3450 2800
Wire Wire Line
	3450 2800 3450 3000
$Comp
L power:GND #PWR?
U 1 1 5BFBB7A6
P 3450 3000
AR Path="/5BFBB7A6" Ref="#PWR?"  Part="1" 
AR Path="/5BFBB447/5BFBB7A6" Ref="#PWR0102"  Part="1" 
AR Path="/5C021D7B/5BFBB7A6" Ref="#PWR0103"  Part="1" 
AR Path="/5C0BD716/5BFBB7A6" Ref="#PWR0109"  Part="1" 
AR Path="/5C0BD711/5BFBB7A6" Ref="#PWR0108"  Part="1" 
AR Path="/5C0BD70C/5BFBB7A6" Ref="#PWR0107"  Part="1" 
AR Path="/5C0BD707/5BFBB7A6" Ref="#PWR0106"  Part="1" 
AR Path="/5C0572F4/5BFBB7A6" Ref="#PWR0104"  Part="1" 
AR Path="/5C0572F9/5BFBB7A6" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0102" H 3450 2750 50  0001 C CNN
F 1 "GND" H 3455 2827 50  0000 C CNN
F 2 "" H 3450 3000 50  0001 C CNN
F 3 "" H 3450 3000 50  0001 C CNN
	1    3450 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2100 3600 2100
Wire Wire Line
	3600 2100 3600 2800
Connection ~ 3600 2800
Wire Wire Line
	3600 2800 3750 2800
Text HLabel 3300 2000 2    60   Input ~ 0
RE_A
Text HLabel 3300 2200 2    60   Input ~ 0
RE_B
Text HLabel 2700 2000 0    60   Input ~ 0
RE_SW
$EndSCHEMATC
