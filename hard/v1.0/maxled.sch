EESchema Schematic File Version 2
LIBS:chordboard-rescue
LIBS:maxim
LIBS:chordboard-components
LIBS:afshar
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:switches
LIBS:standard
LIBS:mechanical
LIBS:led
LIBS:akn_maxim
LIBS:SparkFun-Electromechanical
LIBS:chordboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MAX7219 U?
U 1 1 5A9B5713
P 3800 4650
F 0 "U?" H 3900 6000 60  0000 C CNN
F 1 "MAX7219" H 4350 5350 60  0000 C CNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 3800 4450 40  0001 L CIN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX7219-MAX7221.pdf" H 3800 4350 40  0001 L CIN
	1    3800 4650
	1    0    0    -1  
$EndComp
Text Label 5000 4250 0    60   ~ 0
X0
Text Label 3700 3550 2    60   ~ 0
Y0
$Comp
L +5V #PWR?
U 1 1 5A9B571C
P 4500 2650
F 0 "#PWR?" H 4500 2500 50  0001 C CNN
F 1 "+5V" H 4500 2790 50  0000 C CNN
F 2 "" H 4500 2650 50  0001 C CNN
F 3 "" H 4500 2650 50  0001 C CNN
	1    4500 2650
	1    0    0    -1  
$EndComp
Text Label 5000 3550 0    60   ~ 0
X7
Text Label 5000 3650 0    60   ~ 0
X6
Text Label 5000 3750 0    60   ~ 0
X5
Text Label 5000 3850 0    60   ~ 0
X4
Text Label 5000 3950 0    60   ~ 0
X3
Text Label 5000 4050 0    60   ~ 0
X2
Text Label 5000 4150 0    60   ~ 0
X1
Text Label 3700 3650 2    60   ~ 0
Y1
Text Label 3700 3750 2    60   ~ 0
Y2
Text Label 3700 3850 2    60   ~ 0
Y3
Text Label 3700 3950 2    60   ~ 0
Y4
Text Label 3700 4050 2    60   ~ 0
Y5
Text Label 3700 4150 2    60   ~ 0
Y6
Text Label 3700 4250 2    60   ~ 0
Y7
$Comp
L GND #PWR?
U 1 1 5A9B5730
P 4550 5250
F 0 "#PWR?" H 4550 5000 50  0001 C CNN
F 1 "GND" H 4550 5100 50  0000 C CNN
F 2 "" H 4550 5250 50  0000 C CNN
F 3 "" H 4550 5250 50  0000 C CNN
	1    4550 5250
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5A9B5736
P 5550 3350
F 0 "C?" H 5560 3420 50  0000 L CNN
F 1 "0.1uF" H 5560 3270 50  0000 L CNN
F 2 "chordboard-footprints:C_0805_HandSoldering" H 5550 3350 50  0001 C CNN
F 3 "" H 5550 3350 50  0001 C CNN
	1    5550 3350
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A9B573D
P 5550 3800
F 0 "#PWR?" H 5550 3550 50  0001 C CNN
F 1 "GND" H 5550 3650 50  0000 C CNN
F 2 "" H 5550 3800 50  0000 C CNN
F 3 "" H 5550 3800 50  0000 C CNN
	1    5550 3800
	1    0    0    -1  
$EndComp
$Comp
L CP C?
U 1 1 5A9B5743
P 5800 3350
F 0 "C?" H 5810 3420 50  0000 L CNN
F 1 "10uF" H 5810 3270 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D4.0mm_P2.00mm" H 5800 3350 50  0001 C CNN
F 3 "" H 5800 3350 50  0001 C CNN
	1    5800 3350
	1    0    0    -1  
$EndComp
Text Label 3050 4050 2    60   ~ 0
LED_DIN
$Comp
L Jumper JP?
U 1 1 5A9B574B
P 3150 4050
F 0 "JP?" H 3100 4100 50  0000 R CNN
F 1 "JWTX" H 3200 4100 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3150 4050 50  0001 C CNN
F 3 "" H 3150 4050 50  0001 C CNN
	1    3150 4050
	1    0    0    -1  
$EndComp
$Comp
L Jumper JP?
U 1 1 5A9B5752
P 3050 4150
F 0 "JP?" H 3000 4200 50  0000 R CNN
F 1 "JWTX" H 3100 4200 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3050 4150 50  0001 C CNN
F 3 "" H 3050 4150 50  0001 C CNN
	1    3050 4150
	1    0    0    -1  
$EndComp
$Comp
L Jumper JP?
U 1 1 5A9B5759
P 2950 4250
F 0 "JP?" H 2900 4300 50  0000 R CNN
F 1 "JWTX" H 3000 4300 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 2950 4250 50  0001 C CNN
F 3 "" H 2950 4250 50  0001 C CNN
	1    2950 4250
	1    0    0    -1  
$EndComp
Connection ~ 5550 3600
Wire Wire Line
	5800 3600 5550 3600
Wire Wire Line
	5800 3450 5800 3600
Connection ~ 5550 3050
Wire Wire Line
	5800 3050 5800 3250
Wire Wire Line
	5550 3450 5550 3800
Wire Wire Line
	5550 3050 5550 3250
Connection ~ 4500 3050
Wire Wire Line
	4100 3050 4200 3050
Wire Wire Line
	4100 2950 4200 2950
Wire Wire Line
	4100 2950 4100 3250
Connection ~ 4550 4900
Wire Wire Line
	4550 4900 4550 5250
Wire Wire Line
	4600 4900 4600 4750
Wire Wire Line
	4500 4900 4600 4900
Wire Wire Line
	4500 4750 4500 4900
Wire Wire Line
	4500 2650 4500 3250
Wire Wire Line
	2400 4400 3700 4400
Wire Wire Line
	2400 4500 3700 4500
Wire Wire Line
	2400 4600 3700 4600
Wire Wire Line
	3250 4050 3250 4400
Connection ~ 3250 4400
Wire Wire Line
	3150 4150 3150 4500
Connection ~ 3150 4500
Wire Wire Line
	3050 4250 3050 4600
Connection ~ 3050 4600
Wire Wire Line
	4400 2950 4500 2950
Connection ~ 4500 2950
Connection ~ 4100 3050
$Comp
L R RS?
U 1 1 5A9B577D
P 4300 2950
F 0 "RS?" V 4200 2900 50  0000 C CNN
F 1 "10K" V 4300 2950 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4230 2950 50  0001 C CNN
F 3 "" H 4300 2950 50  0000 C CNN
	1    4300 2950
	0    1    1    0   
$EndComp
Text Notes 3300 2750 0    60   ~ 0
Use only 1 RSET resistor.\nMultiple slots for testing.
Wire Wire Line
	4400 3050 5800 3050
$Comp
L R RS?
U 1 1 5A9B5786
P 4300 3050
F 0 "RS?" V 4200 3000 50  0000 C CNN
F 1 "10K" V 4300 3050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4230 3050 50  0001 C CNN
F 3 "" H 4300 3050 50  0000 C CNN
	1    4300 3050
	0    1    1    0   
$EndComp
$EndSCHEMATC
